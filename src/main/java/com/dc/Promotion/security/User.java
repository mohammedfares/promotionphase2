package com.dc.Promotion.security;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MerchantUser;

import java.io.Serializable;

public class User implements Serializable {

    private Long id;
    private String username;
    private String password;
    private String mobile;
    private String email;
    private String role;

    public User() {
    }

    public User(Long id, String username, String password, String mobile, String email, String role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.mobile = mobile;
        this.email = email;
        this.role = role;
    }

    public User(CustomerAccount customerAccount) {
        this.id = customerAccount.getId();
        this.username = "CUS_" + customerAccount.getEmail();
        this.password = customerAccount.getPassword();
        this.mobile = customerAccount.getMobile();
        this.email = customerAccount.getEmail();
        this.role = "";
    }

    public User(MediaAccount mediaAccount) {
        this.id = mediaAccount.getId();
        this.username = mediaAccount.getMediaType().getCode() + "_" + mediaAccount.getUserName();
        this.password = mediaAccount.getPassword();
        this.mobile = "";
        this.email = "";
        this.role = "";
    }

    public User(MerchantUser merchantUser) {
        this.id = merchantUser.getId();
        this.username = "MER_" + merchantUser.getUserName();
        this.password = merchantUser.getPassword();
        this.mobile = merchantUser.getMobile();
        this.email = merchantUser.getEmail();
        this.role = merchantUser.getMerchantUserType().getCode();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
