package com.dc.Promotion.security;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.CustomerAccountService;
import com.dc.Promotion.service.MediaAccountService;
import com.dc.Promotion.service.MediaTypeService;
import com.dc.Promotion.service.MerchantUserService;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.session.ExpiringSession;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

//import javax.rmi.CORBA.Util;
import java.time.LocalDateTime;
import java.util.Collection;

@Component
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private MerchantUserService merchantUserService;

    @Autowired
    private CustomerAccountService customerAccountService;

    @Autowired
    private MediaAccountService mediaAccountService;

    @Autowired
    private MediaTypeService mediaTypeService;

    @Autowired
    private FindByIndexNameSessionRepository<? extends ExpiringSession> sessions;

    @Autowired
    private Jedis jedis;

    @Override
    public UserDetails loadUserByUsername(String name) {
        String userNameArray[] = name.split(",");
        String username = userNameArray[0];
        String userType = userNameArray[1];
        String countryCode = null;
        try {
            countryCode = userNameArray[2];
        } catch (Exception ex) {
            countryCode = null;
        }

        this.invalidatePrevousSession(userType, username);

        User redisUser = null;
        switch (userType) {
            case "MER":
                MerchantUser merchantUser = this.findMerchantUser(username);
                redisUser = new User(merchantUser);
                return new AccountUserDetails(redisUser);
            case "CUS":
                CustomerAccount customerAccount = this.findCustomerAccount(username, countryCode);
                redisUser = new User(customerAccount);
                return new AccountUserDetails(redisUser);
            case "FCB":
                MediaAccount faceBookMediaAccount = this.findFacebookUser(username);
                redisUser = new User(faceBookMediaAccount);
                return new AccountUserDetails(redisUser);
            case "GML":
                MediaAccount googleMediaAccount = this.findGoogleUser(username);
                redisUser = new User(googleMediaAccount);
                return new AccountUserDetails(redisUser);
            default:
                throw new RuntimeException();
        }
    }


    private MerchantUser findMerchantUser(String username) {

        MerchantUser merchantUser = merchantUserService.findMerchantByUsernameOrEmail(username);

        if (merchantUser == null) {
            throw new RuntimeException();
        }

        if (merchantUser.getStatus().getCode().equals("MUS_INACTIVE")) {
            throw new RuntimeException();
        }

        return merchantUser;
    }

    private CustomerAccount findCustomerAccount(String username, String countryCode) {

        if (countryCode == null || countryCode.isEmpty()) {
            //username is email address

            if (!Utils.isEmailCorrect(username)) {
                throw new RuntimeException();
            }

            CustomerAccount customerAccount = customerAccountService.findByEmail(username);

            if (customerAccount.getStatus().getCode().equals("CAC_INACTIVE")) {
                throw new RuntimeException();
            }

            return customerAccount;

        } else {
            //username is mobile address
            String validMobileNumber = Utils.StandardPhoneFormat(countryCode, username);

            if (validMobileNumber == null) {
                throw new RuntimeException();
            }

            CustomerAccount customerAccount = customerAccountService.findByMobile(validMobileNumber);

            if (customerAccount.getStatus().getCode().equals("CAC_INACTIVE")) {
                throw new RuntimeException();
            }

            return customerAccount;
        }
    }


    private MediaAccount findFacebookUser(String username) {

        MediaType mediaType = mediaTypeService.findByCode("FCB");

        MediaAccount facebookMediaAccount = mediaAccountService.findByUsernameAndType(username, mediaType);

        if (facebookMediaAccount == null) {

            MediaAccount newMediaAccount = new MediaAccount();
            newMediaAccount.setUserName(username);
            newMediaAccount.setMediaType(mediaType);

            facebookMediaAccount = mediaAccountService.createFaceBookAccount(newMediaAccount);
        }

        if (facebookMediaAccount.getStatus().getCode().equals("MAC_INACTIVE")) {
            throw new RuntimeException();
        }

        return facebookMediaAccount;
    }

    private MediaAccount findGoogleUser(String username) {

        MediaType mediaType = mediaTypeService.findByCode("GML");

        MediaAccount googleMediaAccount = mediaAccountService.findByUsernameAndType(username, mediaType);

        if (googleMediaAccount == null) {

            MediaAccount newMediaAccount = new MediaAccount();
            newMediaAccount.setUserName(username);
            newMediaAccount.setMediaType(mediaType);

            googleMediaAccount = mediaAccountService.createFaceBookAccount(newMediaAccount);
        }

        if (googleMediaAccount.getStatus().getCode().equals("MAC_INACTIVE")) {
            throw new RuntimeException();
        }

        return googleMediaAccount;
    }


    private void invalidatePrevousSession(String userType, String username) {
        Collection<? extends ExpiringSession> usersSessions = sessions
                .findByIndexNameAndIndexValue(FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME, userType + "_" + username)
                .values();

        usersSessions.forEach((session) -> {
            String xAuthToken = session.getId();
            System.out.println("xAuthToken: " + xAuthToken);
            String[] keys = {"spring:session:sessions:" + xAuthToken, "spring:session:sessions:expires:" + xAuthToken};
            jedis.del(keys);
        });
    }
}

