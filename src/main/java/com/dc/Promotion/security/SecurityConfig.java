package com.dc.Promotion.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import org.springframework.security.web.savedrequest.NullRequestCache;
import org.springframework.session.web.http.HeaderHttpSessionStrategy;
import org.springframework.session.web.http.HttpSessionStrategy;
import redis.clients.jedis.Jedis;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Autowired
    private EntryPointUnauthorizedHandler entryPointUnauthorizedHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.addFilterBefore(new SimpleCORSFilter(), ChannelProcessingFilter.class);

        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/customer/**").authenticated()
                .antMatchers("/merchant/**").authenticated()
                .antMatchers("/promo/**").authenticated()
                .antMatchers("/dashboard/**").authenticated()
                .antMatchers("/file/**").authenticated()
                .anyRequest().permitAll()
                .and()
                .requestCache()
                .requestCache(new NullRequestCache())
                .and()
                .httpBasic()
                .authenticationEntryPoint(entryPointUnauthorizedHandler);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailService).passwordEncoder(secCriptoRsa());
    }


    @Bean
    public HttpSessionStrategy httpSessionStrategy() {
        return new HeaderHttpSessionStrategy();
    }

    @Bean
    public SECCriptoRsa secCriptoRsa() {
        return new SECCriptoRsa();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        //for customer
        web.ignoring().antMatchers("/customer/sendOtp");
        web.ignoring().antMatchers("/customer/register");
        web.ignoring().antMatchers("/customer/forgetPassword/sendOtp");
        web.ignoring().antMatchers("/customer/forgetPassword/verifyOtp");
        web.ignoring().antMatchers("/customer/forgetPassword/change");
        //for merchant
        web.ignoring().antMatchers("/merchant/register");
        web.ignoring().antMatchers("/merchant/approve");
        web.ignoring().antMatchers("/merchant/deleteReq");
        web.ignoring().antMatchers("/merchant/status");
        web.ignoring().antMatchers("/merchant/allReq");
        web.ignoring().antMatchers("/merchant/reject");
//        web.ignoring().antMatchers("/merchant/allBranch");
////        web.ignoring().antMatchers("/merchant/addBranch");
        web.ignoring().antMatchers("/merchant/findAll");
        web.ignoring().antMatchers("/promo/allCategory");
        web.ignoring().antMatchers("/promo/promos");
        web.ignoring().antMatchers("/promo/activePromo");
        web.ignoring().antMatchers("/promo/deletPromo");
        web.ignoring().antMatchers("/promo/deleteOurPromo");
        web.ignoring().antMatchers("/promo/rejectPromo");
        web.ignoring().antMatchers("/promo/tempById");
//        web.ignoring().antMatchers("/customer/compare");
//        web.ignoring().antMatchers("/merchant/findAll");
    }


    @Bean
    public Jedis jedis() {
        Jedis jedis = new Jedis("localhost", 6379);
        return jedis;
    }
}