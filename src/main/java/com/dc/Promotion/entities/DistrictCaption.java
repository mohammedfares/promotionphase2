package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "DISTRICT_CAPTION")
public class DistrictCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "DistrictCaptionSeq", sequenceName = "DISTRICT_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "DistrictCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "DCA_ID")
    private Long id;
    @Column(name = "DCA_NAME")
    private String name;
    @Column(name = "DCA_DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "DCA_DISTRICT_ID", referencedColumnName = "DIS_ID")
    private District district;

    @ManyToOne
    @JoinColumn(name = "DCA_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage language;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public OperationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(OperationLanguage language) {
        this.language = language;
    }
}
