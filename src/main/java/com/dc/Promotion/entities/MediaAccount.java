package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "MEDIA_ACCOUNT")
public class MediaAccount implements Serializable {

    @Id
    @SequenceGenerator(name = "MediaAccountSeq", sequenceName = "MEDIA_ACCOUNT_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MediaAccountSeq", strategy = GenerationType.AUTO)
    @Column(name = "MAC_ID")
    private Long id;

    @Column(name = "MAC_USERNAME")
    private String userName;

    @Column(name = "MAC_PASSWORD")
    private String password;

    @Column(name = "MAC_GENDER")
    private String gender;

    @Column(name = "MAC_BIRTHDATE")
    private String birthdate;

    @Column(name = "MAC_FIRSTNAME")
    private String firstName;

    @Column(name = "MAC_LASTNAME")
    private String lastName;

    @Column(name = "MAC_AGE")
    private Integer age;

    @Column(name = "MAC_BARCODE")
    private Long barCode;

    @ManyToOne
    @JoinColumn(name = "MAC_TYPE_ID", referencedColumnName = "MTY_ID")
    private MediaType mediaType;

    @ManyToOne
    @JoinColumn(name = "MAC_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "MAC_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage operationLanguage;

    @Column(name = "MAC_DEVICE_TOKEN")
    private String deviceToken;

    @Column(name = "MAC_DEVICE_PLATFORM")
    private String devicePlatform;

    @Column(name = "MAC_LONGITUDE")
    private Double longitude;

    @Column(name = "MAC_LATITUDE")
    private Double latitude;


    public Long getBarCode() {
        return barCode;
    }

    public void setBarCode(Long barCode) {
        this.barCode = barCode;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDevicePlatform() {
        return devicePlatform;
    }

    public void setDevicePlatform(String devicePlatform) {
        this.devicePlatform = devicePlatform;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
