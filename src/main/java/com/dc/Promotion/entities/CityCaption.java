package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CITY_CAPTION")
public class CityCaption implements Serializable {

    @Id
    @SequenceGenerator(name = "CITY_CAPTIONSeq", sequenceName = "CITY_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CITY_CAPTIONSeq", strategy = GenerationType.AUTO)
    @Column(name = "CCA_ID")
    private Long id;
    @Column(name = "CCA_DESCRIPTION")
    private String description;
    @Column(name = "CCA_NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "CCA_CITY_ID", referencedColumnName = "CIT_ID")
    private City city;

    @ManyToOne
    @JoinColumn(name = "CCA_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage language;

    public CityCaption() {
    }

    public CityCaption(String description, String name, City city, OperationLanguage language) {
        this.description = description;
        this.name = name;
        this.city = city;
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public OperationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(OperationLanguage language) {
        this.language = language;
    }
}
