package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "PROMO_ITEM")
public class PromoItem implements Serializable {
    @Id
    @SequenceGenerator(name = "PromoItemSeq", sequenceName = "PROMO_ITEM_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PromoItemSeq", strategy = GenerationType.AUTO)
    @Column(name = "PIT_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "PIT_CREATION_DATE")
    private LocalDateTime createDate;
    @Column(name = "PIT_EXPIRATION_DATE")
    private LocalDateTime expiryDate;
    @Column(name = "PIT_COVER_PHOTO")
    private String coverPhoto;
    @Column(name = "PIT_OLD_PRICE")
    private String oldPrice;
    @Column(name = "PIT_NEW_PRICE")
    private String newPrice;
    @Column(name = "PIT_VIEW_NO")
    private Long viewNo;

    @ManyToOne
    @JoinColumn(name = "PIT_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "PIT_RROMO_ID", referencedColumnName = "PRO_ID")
    private Promo promo;


    @ManyToOne
    @JoinColumn(name = "PIT_CATEGORY_ID", referencedColumnName = "ICA_ID")
    private ItemCategory category;

    @OneToMany(mappedBy = "promoItemId")
    @OrderBy(value = "PII_ID")
    private List<PromoItemImage> imageList;

    @OneToMany(mappedBy = "promoItem")
    private List<ItemSpecification> itemSpecificationList;

    @OneToMany(mappedBy = "promoItem")
    private List<PromoItemCaption> promoItemCaptionList;

    @OneToMany(mappedBy = "promoItem")
    private List<CustomerWatchList> customerWatchList;


    @OneToMany(mappedBy = "promoItem")
    private List<MediaWatchList> mediaWatchLists;

    @OneToMany(mappedBy = "promoItem")
    private List<CustomerWishListView> customerWishListViews;

    @OneToMany(mappedBy = "promoItem")
    private List<MediaWishListView> mediaWishListViews;

    @Transient
    private OperationLanguage operationLanguage;

    @Transient
    private Double distance;

    public PromoItem() {
    }

    public PromoItem(PromoItem promoItem) {
        this.id = promoItem.getId();
        this.createDate = promoItem.getCreateDate();
        this.expiryDate = promoItem.getExpiryDate();
        this.coverPhoto = promoItem.getCoverPhoto();
        this.oldPrice = promoItem.getOldPrice();
        this.newPrice = promoItem.getNewPrice();
        this.viewNo = promoItem.getViewNo();
        this.status = promoItem.getStatus();
        this.promo = promoItem.getPromo();
        this.category = promoItem.getCategory();
        this.imageList = promoItem.getImageList();
        this.itemSpecificationList = promoItem.getItemSpecificationList();
        this.promoItemCaptionList = promoItem.getPromoItemCaptionList();
        this.customerWatchList = promoItem.getCustomerWatchList();
        this.mediaWatchLists = promoItem.getMediaWatchLists();
        this.customerWishListViews = promoItem.getCustomerWishListViews();
        this.mediaWishListViews = promoItem.getMediaWishListViews();
    }

    public PromoItem(PromoItem promoItem, Double distance) {
        this.id = promoItem.getId();
        this.createDate = promoItem.getCreateDate();
        this.expiryDate = promoItem.getExpiryDate();
        this.coverPhoto = promoItem.getCoverPhoto();
        this.oldPrice = promoItem.getOldPrice();
        this.newPrice = promoItem.getNewPrice();
        this.viewNo = promoItem.getViewNo();
        this.status = promoItem.getStatus();
        this.promo = promoItem.getPromo();
        this.category = promoItem.getCategory();
        this.imageList = promoItem.getImageList();
        this.itemSpecificationList = promoItem.getItemSpecificationList();
        this.promoItemCaptionList = promoItem.getPromoItemCaptionList();
        this.customerWatchList = promoItem.getCustomerWatchList();
        this.mediaWatchLists = promoItem.getMediaWatchLists();
        this.customerWishListViews = promoItem.getCustomerWishListViews();
        this.mediaWishListViews = promoItem.getMediaWishListViews();
        this.distance = distance;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public ItemCategory getCategory() {
        return category;
    }

    public void setCategory(ItemCategory category) {
        this.category = category;
    }

    public List<MediaWatchList> getMediaWatchLists() {
        return mediaWatchLists;
    }

    public void setMediaWatchLists(List<MediaWatchList> mediaWatchLists) {
        this.mediaWatchLists = mediaWatchLists;
    }

    public PromoItem(LocalDateTime createDate, LocalDateTime expiryDate, String coverPhoto, String oldPrice, String newPrice, Long viewNo, Status status, Promo promo) {
        this.createDate = createDate;
        this.expiryDate = expiryDate;
        this.coverPhoto = coverPhoto;
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
        this.viewNo = viewNo;
        this.status = status;
        this.promo = promo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public Long getViewNo() {
        return viewNo;
    }

    public void setViewNo(Long viewNo) {
        this.viewNo = viewNo;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public List<PromoItemImage> getImageList() {
        return imageList;
    }

    public void setImageList(List<PromoItemImage> imageList) {
        this.imageList = imageList;
    }

    public List<ItemSpecification> getItemSpecificationList() {
        return itemSpecificationList;
    }

    public void setItemSpecificationList(List<ItemSpecification> itemSpecificationList) {
        this.itemSpecificationList = itemSpecificationList;
    }

    public List<PromoItemCaption> getPromoItemCaptionList() {
        return promoItemCaptionList;
    }

    public void setPromoItemCaptionList(List<PromoItemCaption> promoItemCaptionList) {
        this.promoItemCaptionList = promoItemCaptionList;
    }

    public List<CustomerWatchList> getCustomerWatchList() {
        return customerWatchList;
    }

    public void setCustomerWatchList(List<CustomerWatchList> customerWatchList) {
        this.customerWatchList = customerWatchList;
    }

    public List<CustomerWishListView> getCustomerWishListViews() {
        return customerWishListViews;
    }

    public void setCustomerWishListViews(List<CustomerWishListView> customerWishListViews) {
        this.customerWishListViews = customerWishListViews;
    }

    public List<MediaWishListView> getMediaWishListViews() {
        return mediaWishListViews;
    }

    public void setMediaWishListViews(List<MediaWishListView> mediaWishListViews) {
        this.mediaWishListViews = mediaWishListViews;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
