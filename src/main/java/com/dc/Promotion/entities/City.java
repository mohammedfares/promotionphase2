package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CITY")
public class City implements Serializable {
    @Id
    @SequenceGenerator(name = "CitySeq", sequenceName = "CITY_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CitySeq", strategy = GenerationType.AUTO)
    @Column(name = "CIT_ID")
    private Long id;
    @Column(name = "CIT_CODE")
    private String code;

    @ManyToOne
    @JoinColumn(name = "CIT_COUNTRY_ID", referencedColumnName = "OCO_ID")
    private OperationCountry country;

    @ManyToOne
    @JoinColumn(name = "CIT_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public OperationCountry getCountry() {
        return country;
    }

    public void setCountry(OperationCountry country) {
        this.country = country;
    }
}
