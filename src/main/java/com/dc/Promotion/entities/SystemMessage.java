package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SYSTEM_MESSAGE")
public class SystemMessage implements Serializable {
    @Id
    @SequenceGenerator(name = "SystemMessageSeq", sequenceName = "SYSTEM_MESSAGE_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "SystemMessageSeq", strategy = GenerationType.AUTO)
    @Column(name = "SME_ID")
    private Long id;
    @Column(name = "SME_CODE")
    private String code;

    @ManyToOne
    @JoinColumn(name = "SME_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
