package com.dc.Promotion.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "CUSTOMER_NOTIFICATION")
@Entity
public class CustomerNotification {
    @Id
    @SequenceGenerator(name = "CustomerNotificationSeq", sequenceName = "CustomerNotification_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CustomerNotificationSeq", strategy = GenerationType.AUTO)
    @Column(name = "CN_ID", nullable = false, unique = true)
    private Long id;

    @Column(name = "CN_TITLE")
    private String title;

    @Column(name = "CN_BODY")
    private String body;

    @Column(name = "CN_DATA")
    private String data;

    @Column(name = "CN_SEND_DATE")
    private LocalDateTime sendDate;

    @Column(name = "CN_RESPONSE_CODE")
    private String responseCode;

    @ManyToOne
    @JoinColumn(name = "CN_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "CN_SNR_ID", referencedColumnName = "SNR_ID")
    private SegmentNotificationRequest segmentNotificationRequest;


    @ManyToOne
    @JoinColumn(name = "MN_CAC_ID", referencedColumnName = "CAC_ID")
    private CustomerAccount customerAccount;


    public CustomerNotification(String title, String body, String data,
                                LocalDateTime sendDate, String responseCode,
                                Status status, SegmentNotificationRequest
                                        segmentNotificationRequest,
                                CustomerAccount customerAccount) {
        this.title = title;
        this.body = body;
        this.data = data;
        this.sendDate = sendDate;
        this.responseCode = responseCode;
        this.status = status;
        this.segmentNotificationRequest = segmentNotificationRequest;
        this.customerAccount = customerAccount;
    }


    public CustomerNotification() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public LocalDateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(LocalDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public SegmentNotificationRequest getSegmentNotificationRequest() {
        return segmentNotificationRequest;
    }

    public void setSegmentNotificationRequest(SegmentNotificationRequest segmentNotificationRequest) {
        this.segmentNotificationRequest = segmentNotificationRequest;
    }

    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(CustomerAccount customerAccount) {
        this.customerAccount = customerAccount;
    }
}
