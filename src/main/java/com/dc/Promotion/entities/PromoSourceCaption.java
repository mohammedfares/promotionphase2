package com.dc.Promotion.entities;

import javax.persistence.*;

@Entity
@Table(name = "PROMO_SOURCE_CAPTION")
public class PromoSourceCaption {
    @Id
    @SequenceGenerator(name = "PromoSourceCaptionSeq", sequenceName = "PROMO_SOURCE_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PromoSourceCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "PSC_ID", updatable = false, nullable = false, unique = true,insertable = false)
    private Long id;
    @Column(name = "PSC_NAME")
    private String name;

    @ManyToOne
    @JoinColumn(name = "PSC_PROMO_SOURCE",referencedColumnName = "PRS_ID")
    private PromoSource promoSource;
    @ManyToOne
    @JoinColumn(name = "PSC_LANG_ID",referencedColumnName = "OLA_ID")
    private OperationLanguage language;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PromoSource getPromoSource() {
        return promoSource;
    }

    public void setPromoSource(PromoSource promoSource) {
        this.promoSource = promoSource;
    }

    public OperationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(OperationLanguage language) {
        this.language = language;
    }
}
