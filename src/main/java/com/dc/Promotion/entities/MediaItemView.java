package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "MEDIA_ITEM_VIEW")
public class MediaItemView implements Serializable {
    @Id
    @SequenceGenerator(name = "MediaItemViewSeq", sequenceName = "MEDIA_ITEM_VIEW_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MediaItemViewSeq", strategy = GenerationType.AUTO)
    @Column(name = "MIV_ID")
    private Long id;
    @Column(name = "MIV_CREATE_DATE")
    private LocalDateTime createDate;

    @ManyToOne
    @JoinColumn(name = "MIV_MEDIA_ID", referencedColumnName = "MAC_ID")
    private MediaAccount mediaAccount;

    @ManyToOne
    @JoinColumn(name = "MIV_ITEM_ID", referencedColumnName = "PIT_ID")
    private PromoItem promoItem;

    @ManyToOne
    @JoinColumn(name = "MIV_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @Transient
    OperationLanguage operationLanguage;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public MediaAccount getMediaAccount() {
        return mediaAccount;
    }

    public void setMediaAccount(MediaAccount mediaAccount) {
        this.mediaAccount = mediaAccount;
    }

    public PromoItem getPromoItem() {
        return promoItem;
    }

    public void setPromoItem(PromoItem promoItem) {
        this.promoItem = promoItem;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }
}
