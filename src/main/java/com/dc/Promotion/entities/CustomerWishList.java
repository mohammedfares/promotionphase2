package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "CUSTOMER_WISH_LIST")
public class CustomerWishList implements Serializable {
    @Id
    @SequenceGenerator(name = "CustomerWishListSeq", sequenceName = "CUSTOMER_WISH_LIST_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CustomerWishListSeq", strategy = GenerationType.AUTO)
    @Column(name = "CWI_ID")
    private Long id;
    @Column(name = "CWI_ALIAS_NAME")
    private String aliasName;
    @Column(name = "CWI_TAGS")
    private String tags;
    @Column(name = "CWI_CREATE_DATE")
    private LocalDateTime createDate;

    @ManyToOne
    @JoinColumn(name = "CWI_MER_ID", referencedColumnName = "MER_ID")
    private Merchant merchant;
    @ManyToOne
    @JoinColumn(name = "CWI_CATEGORY_ID", referencedColumnName = "ICA_ID")
    private ItemCategory category;
    @ManyToOne
    @JoinColumn(name = "CWI_CUST_ID", referencedColumnName = "CAC_ID")
    private CustomerAccount customerAccount;

    @ManyToOne
    @JoinColumn(name = "CWI_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public ItemCategory getCategory() {
        return category;
    }

    public void setCategory(ItemCategory category) {
        this.category = category;
    }

    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(CustomerAccount customerAccount) {
        this.customerAccount = customerAccount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
