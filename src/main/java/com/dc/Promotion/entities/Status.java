package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "STATUS")
public class Status implements Serializable {
    @Id
    @SequenceGenerator(name = "StatusSeq", sequenceName = "STATUS_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "StatusSeq", strategy = GenerationType.AUTO)
    @Column(name = "STS_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "STS_CODE")
    private String code;

    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Status(String code) {
        this.code = code;
    }

    public Status() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
