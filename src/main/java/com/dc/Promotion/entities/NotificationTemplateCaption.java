package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Notification_Template_Caption")
public class NotificationTemplateCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "NotificationTemplateCaptionSeq", sequenceName = "Notification_Template_Caption_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "NotificationTemplateCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "NOT_CAP_ID", updatable = false, nullable = false, unique = true)
    private long id;

    @Column(name = "NOT_CAP_TITLE")
    private String captionTitle;

    @Column(name = "NOT_CAP_BODY")
    private String captionBody;

    @ManyToOne
    @JoinColumn(name = "NOT_CAP_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage operationLanguage;

    @ManyToOne
    @JoinColumn(name = "NOT_ID", referencedColumnName = "NTE_ID")
    private NotificationTemplate notificationTemplate;

    @Transient
    private String languageCode;

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCaptionTitle() {
        return captionTitle;
    }

    public void setCaptionTitle(String captionTitle) {
        this.captionTitle = captionTitle;
    }

    public String getCaptionBody() {
        return captionBody;
    }

    public void setCaptionBody(String captionBody) {
        this.captionBody = captionBody;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public NotificationTemplate getNotificationTemplate() {
        return notificationTemplate;
    }

    public void setNotificationTemplate(NotificationTemplate notificationTemplate) {
        this.notificationTemplate = notificationTemplate;
    }
}
