package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ITEM_CATEGORY_CAPTION")
public class ItemCategoryCaption implements Serializable {

    @Id
    @SequenceGenerator(name = "ItemCategoryCaptionSeq", sequenceName = "ITEM_CATEGORY_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "ItemCategoryCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "ICC_ID")
    private Long id;
    @Column(name = "ICC_NAME")
    private String name;
    @Column(name = "ICC_DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "ICC_CATEGORY_ID", referencedColumnName = "ICA_ID")
    private ItemCategory category;
    @ManyToOne
    @JoinColumn(name = "ICC_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage languageId;
    @ManyToOne
    @JoinColumn(name = "ICC_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    public OperationLanguage getLanguageId() {
        return languageId;
    }

    public void setLanguageId(OperationLanguage languageId) {
        this.languageId = languageId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemCategory getCategory() {
        return category;
    }

    public void setCategory(ItemCategory category) {
        this.category = category;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
