package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "CUSTOMER_PROMO_VIEW")
public class CustomerPromoView implements Serializable {

    @Id
    @SequenceGenerator(name = "CustomerPromoViewSeq", sequenceName = "CUSTOMER_PROMO_VIEW_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CustomerPromoViewSeq", strategy = GenerationType.AUTO)
    @Column(name = "CPV_ID")
    private Long id;
    @Column(name = "CPV_CREATE_DATE")
    private LocalDateTime createDate;

    @ManyToOne
    @JoinColumn(name = "CPV_CUST_ID", referencedColumnName = "CAC_ID")
    private CustomerAccount customerAccount;

    @ManyToOne
    @JoinColumn(name = "CPV_PROMO_ID", referencedColumnName = "PRO_ID")
    private Promo promo;

    @ManyToOne
    @JoinColumn(name = "CPV_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(CustomerAccount customerAccount) {
        this.customerAccount = customerAccount;
    }

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
