package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MERCHANT_INDUSTRY")
public class MerchantIndustry implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantIndustrySeq", sequenceName = "MERCHANT_INDUSTRY_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantIndustrySeq", strategy = GenerationType.AUTO)
    @Column(name = "MIN_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "MIN_MER_ID", referencedColumnName = "MER_ID")
    private Merchant merchant;
    @ManyToOne
    @JoinColumn(name = "MIN_INDUSTRY_ID", referencedColumnName = "IND_ID")
    private Industry industry;
    @ManyToOne
    @JoinColumn(name = "MER_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public MerchantIndustry() {
    }

    public MerchantIndustry(Merchant merchant, Industry industries, Status status) {
        this.merchant = merchant;
        this.industry = industries;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }


    public Industry getIndustry() {
        return industry;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }
}
