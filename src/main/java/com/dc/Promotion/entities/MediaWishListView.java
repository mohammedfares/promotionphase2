package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
@Table(name = "MEDIA_WISH_LIST_VIEW")
public class MediaWishListView implements Serializable {

    @Id
    @SequenceGenerator(name = "MediaWishListViewSeq", sequenceName = "MEDIA_WISH_LIST_VIEW_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MediaWishListViewSeq", strategy = GenerationType.AUTO)
    @Column(name = "MWL_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "MWL_MAC_ID", referencedColumnName = "MAC_ID")
    private MediaAccount mediaAccount;

    @ManyToOne
    @JoinColumn(name = "MWL_ITEM_ID", referencedColumnName = "PIT_ID")
    private PromoItem promoItem;

    @ManyToOne
    @JoinColumn(name = "MWL_MWI_ID", referencedColumnName = "MWI_ID")
    private MediaWishList mediaWishList;

    @Transient
    private OperationLanguage operationLanguage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MediaAccount getMediaAccount() {
        return mediaAccount;
    }

    public void setMediaAccount(MediaAccount mediaAccount) {
        this.mediaAccount = mediaAccount;
    }

    public PromoItem getPromoItem() {
        return promoItem;
    }

    public void setPromoItem(PromoItem promoItem) {
        this.promoItem = promoItem;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public MediaWishList getMediaWishList() {
        return mediaWishList;
    }

    public void setMediaWishList(MediaWishList mediaWishList) {
        this.mediaWishList = mediaWishList;
    }
}
