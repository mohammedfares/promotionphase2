package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;


@Entity
@Table(name = "MEDIA_WATCH_LIST")
public class MediaWatchList implements Serializable {
    @Id
    @SequenceGenerator(name = "MediaWatchListSeq", sequenceName = "MEDIA_WATCH_LIST_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MediaWatchListSeq", strategy = GenerationType.AUTO)
    @Column(name = "MWA_ID")
    private Long id;
    @Column(name = "MWA_CREATE_DATE")
    private LocalDateTime createDate;

    @ManyToOne
    @JoinColumn(name = "MWA_CUST_ID", referencedColumnName = "MAC_ID")
    private MediaAccount mediaAccount;

    @ManyToOne
    @JoinColumn(name = "MWA_ITEM_ID", referencedColumnName = "PIT_ID")
    private PromoItem promoItem;

    @ManyToOne
    @JoinColumn(name = "MWA_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    private OperationLanguage operationLanguage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public MediaAccount getMediaAccount() {
        return mediaAccount;
    }

    public void setMediaAccount(MediaAccount mediaAccount) {
        this.mediaAccount = mediaAccount;
    }

    public PromoItem getPromoItem() {
        return promoItem;
    }

    public void setPromoItem(PromoItem promoItem) {
        this.promoItem = promoItem;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }
}
