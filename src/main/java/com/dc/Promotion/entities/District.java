package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "DISTRICT")
public class District implements Serializable {
    @Id
    @SequenceGenerator(name = "DistrictSeq", sequenceName = "DISTRICT_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "DistrictSeq", strategy = GenerationType.AUTO)
    @Column(name = "DIS_ID",insertable = false,updatable = false)
    private Long id;
    @Column(name = "DIS_CODE")
    private String code;

    @ManyToOne
    @JoinColumn(name = "DIS_STS_ID",referencedColumnName = "STS_ID")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "DIS_CITY_ID",referencedColumnName = "CIT_ID")
    private City city;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
