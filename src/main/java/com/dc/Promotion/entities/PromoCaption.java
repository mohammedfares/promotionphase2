package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PROMO_CAPTION")
public class PromoCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "PromoCaptionSeq", sequenceName = "PROMO_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PromoCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "PCA_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "PCA_Name")
    private String name;
    @Column(name = "PCA_DESCRIPTION",length = 1000)
    private String description;

    @ManyToOne
    @JoinColumn(name = "PCA_PROMO_ID", referencedColumnName = "PRO_ID")
    private Promo promo;
    @ManyToOne
    @JoinColumn(name = "PCA_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage operationLanguage;

    public PromoCaption() {
    }

    public PromoCaption(String name, String description, Promo promo) {
        this.name = name;
        this.description = description;
        this.promo = promo;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }
}
