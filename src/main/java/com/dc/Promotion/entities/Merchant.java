package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "MERCHANT")
public class Merchant implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantSeq", sequenceName = "MERCHANT_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantSeq", strategy = GenerationType.AUTO)
    @Column(name = "MER_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "MER_IMAGE")
    private String image;
    @ManyToOne
    @JoinColumn(name = "MER_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @OneToMany(mappedBy = "merchant")
    private List<MerchantIndustry> merchantIndustryList;

    @Transient
    private OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Merchant() {
    }

    public Merchant(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<MerchantIndustry> getMerchantIndustryList() {
        return merchantIndustryList;
    }

    public void setMerchantIndustryList(List<MerchantIndustry> merchantIndustryList) {
        this.merchantIndustryList = merchantIndustryList;
    }
}
