package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "CUSTOMER_WATCH_LIST")
public class CustomerWatchList implements Serializable {

    @Id
    @SequenceGenerator(name = "CustomerWatchListSeq", sequenceName = "CUSTOMER_WATCH_LIST_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CustomerWatchListSeq", strategy = GenerationType.AUTO)
    @Column(name = "CWA_ID")
    private Long id;
    @Column(name = "CWA_CREATE_DATE")
    private LocalDateTime createDate;

    @ManyToOne
    @JoinColumn(name = "CWA_CUST_ID", referencedColumnName = "CAC_ID")
    private CustomerAccount customerAccount;

    @ManyToOne
    @JoinColumn(name = "CWA_ITEM_ID", referencedColumnName = "PIT_ID")
    private PromoItem promoItem;

    @ManyToOne
    @JoinColumn(name = "CWT_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(CustomerAccount customerAccount) {
        this.customerAccount = customerAccount;
    }

    public PromoItem getPromoItem() {
        return promoItem;
    }

    public void setPromoItem(PromoItem promoItem) {
        this.promoItem = promoItem;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
