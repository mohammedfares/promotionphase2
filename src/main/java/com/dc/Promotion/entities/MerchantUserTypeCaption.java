package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MERCHANT_USER_TYPE_CAPTION")
public class MerchantUserTypeCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantUserTypeCaptionSeq", sequenceName = "MERCHANT_USER_TYPE_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantUserTypeCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "MTC_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "MTC_NAME")
    private String name;
    @Column(name = "MTC_DESCRIPTION")
    private String description;
    @ManyToOne
    @JoinColumn(name = "MTC_TYPE_ID", referencedColumnName = "MTY_ID")
    private MerchantUserType merchantUserType;
    @ManyToOne
    @JoinColumn(name = "MTC_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage operationLanguage;

    public MerchantUserTypeCaption() {
    }

    public MerchantUserTypeCaption(String name, String description, MerchantUserType merchantUserType) {
        this.name = name;
        this.description = description;
        this.merchantUserType = merchantUserType;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MerchantUserType getMerchantUserType() {
        return merchantUserType;
    }

    public void setMerchantUserType(MerchantUserType merchantUserType) {
        this.merchantUserType = merchantUserType;
    }
}
