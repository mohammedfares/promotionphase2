package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "INDUSTRY")
public class Industry implements Serializable {
    @Id
    @SequenceGenerator(name = "IndustrySeq", sequenceName = "INDUSTRY_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "IndustrySeq", strategy = GenerationType.AUTO)
    @Column(name = "IND_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "IND_PHOTO")
    private String image;
    @ManyToOne
    @JoinColumn(name = "IND_STS_ID", referencedColumnName = "STS_ID")
    private Status statusId;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Status getStatusId() {
        return statusId;
    }

    public void setStatusId(Status statusId) {
        this.statusId = statusId;
    }

    public Industry() {
    }

    public Industry(String image) {
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
