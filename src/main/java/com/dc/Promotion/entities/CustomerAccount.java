package com.dc.Promotion.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "CUSTOMER_ACCOUNT")
public class CustomerAccount implements Serializable {

    @Id
    @SequenceGenerator(name = "CustomerAccountSeq", sequenceName = "CUSTOMER_ACCOUNT_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CustomerAccountSeq", strategy = GenerationType.AUTO)
    @Column(name = "CAC_ID")
    private Long id;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "CAC_CREATE_DATE")
    private LocalDateTime createDate;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "CAC_EXPIRE_DATE")
    private LocalDateTime expireDate;

    @Column(name = "CAC_EMAIL", nullable = false, unique = true)
    private String email;

    @Column(name = "CAC_MOBILE", length = 14, nullable = false, unique = true)
    private String mobile;

    @Column(name = "CAC_IMAGE")
    private String image;

    @Column(name = "CAC_GENDER")
    private String gender;

    @Column(name = "CAC_AGE")
    private Integer age;

    @Column(name = "CAC_PASSWORD", nullable = false)
    private String password;

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @Column(name = "CAC_BIRTHDATE")
    private LocalDateTime birthDate;

    @Column(name = "CAC_BUILDING_NUMBER")
    private Long buildingNumber;

    @Column(name = "CAC_APARTMENT_NUMBER")
    private Long apartmentNumber;

    @Column(name = "CAC_BARCODE")
    private Long barCode;

    @Column(name = "CAC_FIRST_NAME")
    private String firstName;

    @Column(name = "CAC_LAST_NAME")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "CAC_CNT_ID", referencedColumnName = "COU_ID")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "CAC_RANGE_ID", referencedColumnName = "ARA_ID")
    private AgeRange range;

    @ManyToOne
    @JoinColumn(name = "CAC_CITY_ID", referencedColumnName = "CIT_ID")
    private City city;

    @ManyToOne
    @JoinColumn(name = "CAC_DISTRICT_ID", referencedColumnName = "DIS_ID")
    private District district;

    @ManyToOne
    @JoinColumn(name = "CAC_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Column(name = "CAC_DEVICE_TOKEN")
    private String deviceToken;

    @Column(name = "CAC_DEVICE_PLATFORM")
    private String devicePlatform;

    @Column(name = "CAC_LONGITUDE")
    private Double longitude;

    @Column(name = "CAC_LATITUDE")
    private Double latitude;

    @ManyToOne
    @JoinColumn(name = "CAC_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage operationLanguage;

    public Long getBarCode() {
        return barCode;
    }

    public void setBarCode(Long barCode) {
        this.barCode = barCode;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public Long getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(Long buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Long getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(Long apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public AgeRange getRange() {
        return range;
    }

    public void setRange(AgeRange range) {
        this.range = range;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDevicePlatform() {
        return devicePlatform;
    }

    public void setDevicePlatform(String devicePlatform) {
        this.devicePlatform = devicePlatform;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }
}
