package com.dc.Promotion.entities;

import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;

@Entity
@Table(name = "COUNTRY_SEGMENT")
public class CountrySegment {
    @Id
    @SequenceGenerator(name = "CountrySegmentSeq", sequenceName = "COUNTRY_SEGMENT_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CountrySegmentSeq", strategy = GenerationType.AUTO)
    @Column(name = "CSE_ID", nullable = false, unique = true)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CSE_COUNTRY", referencedColumnName = "COU_ID")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "CSE_SEGMENT", referencedColumnName = "MCS_ID")
    private MerchantCustomerSegment merchantCustomerSegment;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public MerchantCustomerSegment getMerchantCustomerSegment() {
        return merchantCustomerSegment;
    }

    public void setMerchantCustomerSegment(MerchantCustomerSegment merchantCustomerSegment) {
        this.merchantCustomerSegment = merchantCustomerSegment;
    }
}
