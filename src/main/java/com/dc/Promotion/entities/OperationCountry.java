package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "OPERATION_COUNTRY")
public class OperationCountry implements Serializable {

    @Id
    @SequenceGenerator(name = "OperationCountrySeq", sequenceName = "OPERATION_COUNTRY_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "OperationCountrySeq", strategy = GenerationType.AUTO)
    @Column(name = "OCO_ID")
    private Long id;

    @OneToOne
    @JoinColumn(name = "OCO_COUNTRY_ID", referencedColumnName = "COU_ID")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "OCO_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    private String languageCode;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
}
