package com.dc.Promotion.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Operation_Language")
public class OperationLanguage implements Serializable {

    @Id
    @SequenceGenerator(name = "OperationLanguageSeq", sequenceName = "Operation_Language_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "OperationLanguageSeq", strategy = GenerationType.AUTO)
    @Column(name = "OLA_ID", updatable = false, nullable = false, unique = true)
    private Long id;

    @Column(name = "OLA_code", unique = true, nullable = false, length = 3)
    private String code;

    @Column(name = "OLA_name")
    private String name;

    @Column(name = "OLA_Dir")
    private String direction;

    @ManyToOne
    @JoinColumn(name = "OLA_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @OneToMany(mappedBy = "language", fetch = FetchType.LAZY)
    private List<SpecificationCaption> specificationList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public List<SpecificationCaption> getSpecificationList() {
        return specificationList;
    }

    public void setSpecificationList(List<SpecificationCaption> specificationList) {
        this.specificationList = specificationList;
    }
}
