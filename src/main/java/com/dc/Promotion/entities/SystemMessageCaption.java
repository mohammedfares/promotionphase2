package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SYSTEM_MESSAGE_CAPTION")
public class SystemMessageCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "SystemMessageCaptionSeq", sequenceName = "SYSTEM_MESSAGE_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "SystemMessageCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "SMC_ID")
    private Long id;
    @Column(name = "SMC_DESCRIPTION")
    private String description;
    @Column(name = "SMC_TEXT")
    private String text;

    @ManyToOne
    @JoinColumn(name = "SMC_MESSAGE_ID", referencedColumnName = "SME_ID")
    private SystemMessage message;

    @ManyToOne
    @JoinColumn(name = "SMC_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage language;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public SystemMessage getMessage() {
        return message;
    }

    public void setMessage(SystemMessage message) {
        this.message = message;
    }

    public OperationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(OperationLanguage language) {
        this.language = language;
    }
}
