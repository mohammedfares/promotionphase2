package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "COUNTRY")
public class Country implements Serializable {
    @Id
    @SequenceGenerator(name = "CountrySeq", sequenceName = "COUNTRY_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CountrySeq", strategy = GenerationType.AUTO)
    @Column(name = "COU_ID")
    private Long id;
    @Column(name = "COU_CODE")
    private String code;
    @Column(name = "COU_KEY")
    private String key;

    @Column(name = "COU_FLAG")
    private String flag;

    @ManyToOne
    @JoinColumn(name = "COU_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @Transient
    OperationLanguage operationLanguage;


    @OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
    List<CountryCaption> countryCaptionList;

    @OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
    List<OperationCountry> operationCountryList;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<CountryCaption> getCountryCaptionList() {
        return countryCaptionList;
    }

    public void setCountryCaptionList(List<CountryCaption> countryCaptionList) {
        this.countryCaptionList = countryCaptionList;
    }

    public List<OperationCountry> getOperationCountryList() {
        return operationCountryList;
    }

    public void setOperationCountryList(List<OperationCountry> operationCountryList) {
        this.operationCountryList = operationCountryList;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
