package com.dc.Promotion.entities;


import javax.persistence.*;

@Entity
@Table(name = "PROMO_BRANCH")
public class PromoBranch {
    @Id
    @SequenceGenerator(name = "PromoBranchSeq", sequenceName = "PROMO_BRANCH_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PromoBranchSeq", strategy = GenerationType.AUTO)
    @Column(name = "PBR_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "PBR_PROMO_ID", referencedColumnName = "PRO_ID")
    private Promo promo;
    @ManyToOne
    @JoinColumn(name = "PBR_BRANCH_ID", referencedColumnName = "BRA_ID")
    private MerchantBranch branch;

    @Transient
    private OperationLanguage operationLanguage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public MerchantBranch getBranch() {
        return branch;
    }

    public void setBranch(MerchantBranch branch) {
        this.branch = branch;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }
}
