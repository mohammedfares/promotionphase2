package com.dc.Promotion.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Notification_Template")
public class NotificationTemplate implements Serializable {

    @Id
    @SequenceGenerator(name = "NotificationTemplateSeq", sequenceName = "NOTIFICATION_TEMPLATE_SEQ",
            initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "NotificationTemplateSeq", strategy = GenerationType.AUTO)
    @Column(name = "NTE_ID")
    private Long id;

    @Column(name = "NTE_CODE", nullable = false)
    private String code;

    @Column(name = "NTE_DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "NTE_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    private String languageCode;

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
