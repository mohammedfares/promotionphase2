package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MEDIA_TYPE")
public class MediaType implements Serializable {

    @Id
    @SequenceGenerator(name = "MediaTypeSeq", sequenceName = "MEDIA_TYPE_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MediaTypeSeq", strategy = GenerationType.AUTO)
    @Column(name = "MTY_ID")
    private Long id;
    @Column(name = "MTY_CODE")
    private String code;

    @ManyToOne
    @JoinColumn(name = "MTY_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
