package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "MEDIA_WISH_LIST")
public class MediaWishList implements Serializable {
    @Id
    @SequenceGenerator(name = "MediaWishListSeq", sequenceName = "MEDIA_WISH_LIST_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MediaWishListSeq", strategy = GenerationType.AUTO)
    @Column(name = "MWI_ID")
    private Long id;
    @Column(name = "MWI_ALIAS_NAME")
    private String aliasName;
    @Column(name = "MWI_TAGS")
    private String tags;
    @Column(name = "MWI_CREATE_DATE")
    private LocalDateTime createDate;

    @ManyToOne
    @JoinColumn(name = "MWI_CATEGORY_ID", referencedColumnName = "ICA_ID")
    private ItemCategory category;

    @ManyToOne
    @JoinColumn(name = "MWI_Media_ID", referencedColumnName = "MAC_ID")
    private MediaAccount mediaAccount;
    @ManyToOne
    @JoinColumn(name = "MWI_MER_ID", referencedColumnName = "MER_ID")
    private Merchant merchant;
    @ManyToOne
    @JoinColumn(name = "MWI_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    
    @Transient
    OperationLanguage operationLanguage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public ItemCategory getCategory() {
        return category;
    }

    public void setCategory(ItemCategory category) {
        this.category = category;
    }

    public MediaAccount getMediaAccount() {
        return mediaAccount;
    }

    public void setMediaAccount(MediaAccount mediaAccount) {
        this.mediaAccount = mediaAccount;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

}
