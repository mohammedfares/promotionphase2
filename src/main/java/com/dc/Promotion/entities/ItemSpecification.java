package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ITEM_SPECIFICATION")
public class ItemSpecification implements Serializable {
    @Id
    @SequenceGenerator(name = "ItemSpecificationSeq", sequenceName = "ITEM_SPECIFICATION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "ItemSpecificationSeq", strategy = GenerationType.AUTO)
    @Column(name = "ISP_ID")
    private Long id;

    @Column(name = "ISP_KEY")
    private String key;

    @Column(name = "ISP_VALUE")
    private String value;

    @ManyToOne
    @JoinColumn(name = "ISP_PRE_SPECS", referencedColumnName = "SPE_ID")
    private Specification specification;

    @ManyToOne
    @JoinColumn(name = "ISP_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "ISP_ITEM_ID", referencedColumnName = "PIT_ID")
    private PromoItem promoItem;

    @ManyToOne
    @JoinColumn(name = "ISP_lANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage operationLanguage;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public PromoItem getPromoItem() {
        return promoItem;
    }

    public void setPromoItem(PromoItem promoItem) {
        this.promoItem = promoItem;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Specification getSpecification() {
        return specification;
    }

    public void setSpecification(Specification specification) {
        this.specification = specification;
    }
}
