package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "MERCHANT_BRANCH")
public class MerchantBranch implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantBranchSeq", sequenceName = "MERCHANT_BRANCH_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantBranchSeq", strategy = GenerationType.AUTO)
    @Column(name = "BRA_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "BRA_CONTACT_NUMBER")
    private String contactNumber;
    @Column(name = "BRA_CREATION_DATE")
    private LocalDateTime createDate;
    @Column(name = "BRA_EXPIRATION_DATE")
    private LocalDateTime expiryDate;
    @Column(name = "BRA_ADDRESS")
    private String address;
    @Column(name = "BRA_LONGITUDE")
    private Double longitude;
    @Column(name = "BRA_LATITUDE")
    private Double latitude;

    @ManyToOne
    @JoinColumn(name = "BRA_MER_ID", referencedColumnName = "MER_ID")
    private Merchant merchant;
    @ManyToOne
    @JoinColumn(name = "BRA_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @OneToMany(mappedBy = "merchantBranch")
    private List<MerchantBranchCaption> merchantBranchCaptionList;

    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public MerchantBranch() {
    }

    public MerchantBranch(String contactNumber, LocalDateTime createDate, LocalDateTime expiryDate, String address, Double longitude, Double latitude, Merchant merchant) {
        this.contactNumber = contactNumber;
        this.createDate = createDate;
        this.expiryDate = expiryDate;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.merchant = merchant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public List<MerchantBranchCaption> getMerchantBranchCaptionList() {
        return merchantBranchCaptionList;
    }

    public void setMerchantBranchCaptionList(List<MerchantBranchCaption> merchantBranchCaptionList) {
        this.merchantBranchCaptionList = merchantBranchCaptionList;
    }
}
