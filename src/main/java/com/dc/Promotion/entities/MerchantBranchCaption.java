package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MERCHANT_BRANCH_CAPTION")
public class MerchantBranchCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantBranchCaptionSeq", sequenceName = "MERCHANT_BRANCH_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantBranchCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "MBC_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "MBC_NAME")
    private String name;
    @Column(name = "MBC_DESCRIPTION")
    private String description;
    @Column(name = "MBC_ADDRESS")
    private String address;

    @ManyToOne
    @JoinColumn(name = "MBC_BRANCH_ID", referencedColumnName = "BRA_ID")
    private MerchantBranch merchantBranch;
    @ManyToOne
    @JoinColumn(name = "MCA_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage operationLanguage;

    public MerchantBranchCaption() {
    }

    public MerchantBranchCaption(String name, String description, MerchantBranch merchantBranch) {
        this.name = name;
        this.description = description;
        this.merchantBranch = merchantBranch;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MerchantBranch getMerchantBranch() {
        return merchantBranch;
    }

    public void setMerchantBranch(MerchantBranch merchantBranch) {
        this.merchantBranch = merchantBranch;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
