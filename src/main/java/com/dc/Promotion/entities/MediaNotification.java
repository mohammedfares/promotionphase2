package com.dc.Promotion.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "MEDIA_NOTIFICATION")
@Entity
public class MediaNotification {

    @Id
    @SequenceGenerator(name = "MediaNotificationSeq", sequenceName = "MediaNotification_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MediaNotificationSeq", strategy = GenerationType.AUTO)
    @Column(name = "MN_ID", nullable = false, unique = true)
    private Long id;

    @Column(name = "MN_TITLE")
    private String title;

    @Column(name = "MN_BODY")
    private String body;

    @Column(name = "MN_DATA")
    private String data;

    @Column(name = "MN_SEND_DATE")
    private LocalDateTime sendDate;

    @Column(name = "MN_RESPONSE_CODE")
    private String responseCode;

    @ManyToOne
    @JoinColumn(name = "MN_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "MN_SNR_ID", referencedColumnName = "SNR_ID")
    private SegmentNotificationRequest segmentNotificationRequest;

    @ManyToOne
    @JoinColumn(name = "MN_MAC_ID", referencedColumnName = "MAC_ID")
    private MediaAccount mediaAccount;


    public MediaNotification(String title,
                             String body
            , String data, LocalDateTime
                                     sendDate
            , String responseCode, Status status
            , SegmentNotificationRequest segmentNotificationRequest,
                             MediaAccount mediaAccount) {
        this.title = title;
        this.body = body;
        this.data = data;
        this.sendDate = sendDate;
        this.responseCode = responseCode;
        this.status = status;
        this.segmentNotificationRequest = segmentNotificationRequest;
        this.mediaAccount = mediaAccount;
    }

    public MediaNotification() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public LocalDateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(LocalDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public SegmentNotificationRequest getSegmentNotificationRequest() {
        return segmentNotificationRequest;
    }

    public void setSegmentNotificationRequest(SegmentNotificationRequest segmentNotificationRequest) {
        this.segmentNotificationRequest = segmentNotificationRequest;
    }

    public MediaAccount getMediaAccount() {
        return mediaAccount;
    }

    public void setMediaAccount(MediaAccount mediaAccount) {
        this.mediaAccount = mediaAccount;
    }

    @Override
    public String toString() {
        return "MediaNotification{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", data='" + data + '\'' +
                ", sendDate=" + sendDate +
                ", responseCode='" + responseCode + '\'' +
                ", status=" + status +
                ", segmentNotificationRequest=" + segmentNotificationRequest +
                ", mediaAccount=" + mediaAccount +
                '}';
    }
}
