package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MERCHANT_CAPTION")
public class MerchantCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantCaptionSeq", sequenceName = "MERCHANT_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "MCA_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "MCA_NAME")
    private String name;
    @Column(name = "MCA_DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "MCA_MER_ID", referencedColumnName = "MER_ID")
    private Merchant merchant;

    @ManyToOne
    @JoinColumn(name = "MCA_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage operationLanguage;

    public MerchantCaption() {
    }

    public MerchantCaption(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
