package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "AGE_RANGE")
public class AgeRange implements Serializable {
    @Id
    @SequenceGenerator(name = "AgeRangeSeq", sequenceName = "AGE_RANGE_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "AgeRangeSeq", strategy = GenerationType.AUTO)
    @Column(name = "ARA_ID")
    private Long id;
    @Column(name = "ARA_CODE")
    private String code;
    
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


}
