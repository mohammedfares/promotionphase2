//package com.dc.Promotion.entities;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "Nationality_Caption")
//public class NationalityCaption {
//    @Id
//    @SequenceGenerator(name = "Nationality_CaptionSeq", sequenceName = "Nationality_Caption_SEQ"
//            , initialValue = 1, allocationSize = 1)
//    @GeneratedValue(generator = "Nationality_CaptionSeq", strategy = GenerationType.AUTO)
//    @Column(name = "NAC_ID")
//    private Long id;
//    @Column(name = "NAC_NAME")
//    private String name;
//    @Column(name = "NAC_DESCRIPTION")
//    private String description;
//
//    @ManyToOne
//    @JoinColumn(name = "NAC_Nationality_ID",referencedColumnName = "NAT_ID")
//    private Nationality nationality;
//
//    @ManyToOne
//    @JoinColumn(name = "NAC_LANG_ID",referencedColumnName = "OLA_ID")
//    private OperationLanguage operationLanguage;
//
//    public OperationLanguage getOperationLanguage() {
//        return operationLanguage;
//    }
//
//    public void setOperationLanguage(OperationLanguage operationLanguage) {
//        this.operationLanguage = operationLanguage;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getCountryName() {
//        return name;
//    }
//
//    public void setCountryName(String name) {
//        this.name = name;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public Nationality getCountry() {
//        return nationality;
//    }
//
//    public void setCountry(Nationality nationality) {
//        this.nationality = nationality;
//    }
//}
//
//
//
