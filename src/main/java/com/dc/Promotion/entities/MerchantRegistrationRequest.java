package com.dc.Promotion.entities;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "MERCHANT_REGISTRATION_REQUEST")
public class MerchantRegistrationRequest implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantRegistrationRequestSeq", sequenceName = "MERCHANT_REGISTRATION_REQUEST_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantRegistrationRequestSeq", strategy = GenerationType.AUTO)
    @Column(name = "MRR_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "MRR_EMAIL", updatable = false, nullable = false, unique = true)
    private String email;
    @Column(name = "MRR_PASSWORD")
    private String password;
    @Column(name = "MRR_MOBILE")
    private String mobile;
    @Column(name = "MRR_CREATION_DATE")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createDate;
    @Column(name = "MRR_EXPIRATION_DATE")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime expiryDate;
    @Column(name = "MRR_CRN", unique = true, nullable = false)
    private String crn;
    @Column(name = "MRR_MERCHANT_NAME")
    private String merchantName;
    @Column(name = "MRR_USERNAME", updatable = false, nullable = false, unique = true)
    private String username;
    @ManyToOne
    @JoinColumn(name = "MRR_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @OneToMany(mappedBy = "merchantRegistrationRequest", fetch = FetchType.LAZY)
    private List<RegistrationRequestIndustry> requestIndustryList;

    @Transient
    private OperationLanguage operationLanguage;


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public MerchantRegistrationRequest() {
    }

    public MerchantRegistrationRequest(String email, String password, String mobile, LocalDateTime createDate, LocalDateTime expiryDate, String crn) {
        this.email = email;
        this.password = password;
        this.mobile = mobile;
        this.createDate = createDate;
        this.expiryDate = expiryDate;
        this.crn = crn;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    @JsonFormat(pattern = "yyyy-MM-dd")
    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCrn() {
        return crn;
    }

    public void setCrn(String crn) {
        this.crn = crn;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public List<RegistrationRequestIndustry> getRequestIndustryList() {
        return requestIndustryList;
    }

    public void setRequestIndustryList(List<RegistrationRequestIndustry> requestIndustryList) {
        this.requestIndustryList = requestIndustryList;
    }


}
