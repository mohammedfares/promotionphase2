package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "PROMO")
public class Promo implements Serializable {
    @Id
    @SequenceGenerator(name = "PromoSeq", sequenceName = "PROMO_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PromoSeq", strategy = GenerationType.AUTO)
    @Column(name = "PRO_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "PRO_COVER_PHOTO")
    private String coverPhoto;
    @Column(name = "PRO_EXPIRATION_DATE")
    private LocalDateTime expiryDate;
    @Column(name = "PRO_CREATION_DATE")
    private LocalDateTime createDate;
    @Column(name = "PRO_START_DATE")
    private LocalDateTime startDate;
    @Column(name = "PRO_CODE")
    private String code;
    @Column(name = "PRO_BARCODE")
    private String barcode;
    @Column(name = "PRO_VIEW_NO")
    private Long viewNo;
    @Column(name = "PRO_POINT")
    private Long point;

    @ManyToOne
    @JoinColumn(name = "PRO_MER_ID", referencedColumnName = "MER_ID")
    private Merchant merchant;

    @ManyToOne
    @JoinColumn(name = "PRO_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @OneToMany(mappedBy = "promo")
    private List<PromoCaption> promoCaptionList;

    @OneToMany(mappedBy = "promo")
    private List<PromoBranch> promoBranchList;

    @ManyToOne
    @JoinColumn(name = "PRO_PROMO_SOURCE",referencedColumnName = "PRS_ID")
    private PromoSource promoSource;

    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public List<PromoCaption> getPromoCaptionList() {
        return promoCaptionList;
    }

    public void setPromoCaptionList(List<PromoCaption> promoCaptionList) {
        this.promoCaptionList = promoCaptionList;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Promo() {
    }

    public Promo(String coverPhoto, LocalDateTime expiryDate, LocalDateTime createDate, LocalDateTime startDate, String code, String barcode, Long viewNo, Merchant merchant, Status status) {
        this.coverPhoto = coverPhoto;
        this.expiryDate = expiryDate;
        this.createDate = createDate;
        this.startDate = startDate;
        this.code = code;
        this.barcode = barcode;
        this.viewNo = viewNo;
        this.merchant = merchant;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Long getViewNo() {
        return viewNo;
    }

    public void setViewNo(Long viewNo) {
        this.viewNo = viewNo;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    public List<PromoBranch> getPromoBranchList() {
        return promoBranchList;
    }

    public void setPromoBranchList(List<PromoBranch> promoBranchList) {
        this.promoBranchList = promoBranchList;
    }

    public PromoSource getPromoSource() {
        return promoSource;
    }

    public void setPromoSource(PromoSource promoSource) {
        this.promoSource = promoSource;
    }
}
