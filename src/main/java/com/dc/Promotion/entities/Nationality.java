//package com.dc.Promotion.entities;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "Nationality")
//public class Nationality {
//    @Id
//    @SequenceGenerator(name = "NationalitySeq", sequenceName = "Nationality_SEQ"
//            , initialValue = 1, allocationSize = 1)
//    @GeneratedValue(generator = "NationalitySeq", strategy = GenerationType.AUTO)
//    @Column(name = "NAT_ID")
//    private Long id;
//    @Column(name = "NAT_CODE")
//    private String code;
//    @Column(name = "NAT_KEY")
//    private String key;
//
//    @Transient
//    OperationLanguage operationLanguage;
//
//    public OperationLanguage getOperationLanguage() {
//        return operationLanguage;
//    }
//
//    public void setOperationLanguage(OperationLanguage operationLanguage) {
//        this.operationLanguage = operationLanguage;
//    }
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getCode() {
//        return code;
//    }
//
//    public void setCode(String code) {
//        this.code = code;
//    }
//
//    public String getKey() {
//        return key;
//    }
//
//    public void setKey(String key) {
//        this.key = key;
//    }
//}
