package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SPECIFICATION_CAPTION")
public class SpecificationCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "SpecificationCaptionSeq", sequenceName = "SPECIFICATION_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "SpecificationCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "SEC_ID")
    private Long id;

    @Column(name = "SEC_KEY")
    private String key;

    @ManyToOne
    @JoinColumn(name = "SEC_SPEC_ID", referencedColumnName = "SPE_ID")
    private Specification specification;

    @ManyToOne
    @JoinColumn(name = "SEC_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage language;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Specification getSpecification() {
        return specification;
    }

    public void setSpecification(Specification specification) {
        this.specification = specification;
    }

    public OperationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(OperationLanguage language) {
        this.language = language;
    }
}
