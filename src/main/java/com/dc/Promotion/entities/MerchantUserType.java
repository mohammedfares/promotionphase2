package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MERCHANT_USER_TYPE")
public class MerchantUserType implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantUserTypeSeq", sequenceName = "MERCHANT_USER_TYPE_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantUserTypeSeq", strategy = GenerationType.AUTO)
    @Column(name = "MTY_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "MTY_CODE")
    private String code;
    @ManyToOne
    @JoinColumn(name = "MTY_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public MerchantUserType() {
    }

    public MerchantUserType(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
