package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "INDUSTRY_CAPTION")
public class IndustryCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "IndustryCaptionSeq", sequenceName = "INDUSTRY_CAPTION_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "IndustryCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "ICA_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "ICA_NAME")
    private String name;
    @ManyToOne
    @JoinColumn(name = "ICA_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage languageId;

    @ManyToOne
    @JoinColumn(name = "ICA_INDUSTRY_ID", referencedColumnName = "IND_ID")
    private Industry industry;

    public IndustryCaption() {
    }

    public OperationLanguage getLanguageId() {
        return languageId;
    }

    public void setLanguageId(OperationLanguage languageId) {
        this.languageId = languageId;
    }

    public IndustryCaption(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Industry getIndustry() {
        return industry;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }
}
