package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "STATUS_CAPTION")
public class StatusCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "StatusCaptionSeq", sequenceName = "STATUS_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "StatusCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "SCA_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "SCA_NAME")
    private String name;
    @Column(name = "SCA_DESCRIPTION")
    private String description;
    @ManyToOne
    @JoinColumn(name = "SCA_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @ManyToOne
    @JoinColumn(name = "SCA_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage language;

    public StatusCaption() {
    }

    public StatusCaption(String name, String description, Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    public OperationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(OperationLanguage language) {
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
