package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "ITEM_CATEGORY")
public class ItemCategory implements Serializable {
    @Id
    @SequenceGenerator(name = "ItemCategorySeq", sequenceName = "ITEM_CATEGORY_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "ItemCategorySeq", strategy = GenerationType.AUTO)
    @Column(name = "ICA_ID")
    private Long id;

    @Column(name = "ICA_CODE")
    private String code;

    @ManyToOne
    @JoinColumn(name = "ICA_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @OneToMany(mappedBy = "category")
    private List<PromoItem> promoItemList;

    @OneToMany(mappedBy = "category")
    private List<ItemCategoryCaption> itemCategoryCaptions;

    @Transient
    private OperationLanguage operationLanguage;

    @Transient
    private Long totalViews;

    public ItemCategory() {
    }

    public ItemCategory(Long id, Long totalViews) {
        this.id = id;
        this.totalViews = totalViews;
    }

    public List<ItemCategoryCaption> getItemCategoryCaptions() {
        return itemCategoryCaptions;
    }

    public void setItemCategoryCaptions(List<ItemCategoryCaption> itemCategoryCaptions) {
        this.itemCategoryCaptions = itemCategoryCaptions;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<PromoItem> getPromoItemList() {
        return promoItemList;
    }

    public void setPromoItemList(List<PromoItem> promoItemList) {
        this.promoItemList = promoItemList;
    }

    public Long getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(Long totalViews) {
        this.totalViews = totalViews;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
