package com.dc.Promotion.entities;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "MERCHANT_CUSTOMER_SEGMENT")
public class MerchantCustomerSegment {

    @Id
    @SequenceGenerator(name = " MerchantCustomerSegmentSeq", sequenceName = "MERCHANT_CUSTOMER_SEGMENT_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = " MerchantCustomerSegmentSeq", strategy = GenerationType.AUTO)
    @Column(name = "MCS_ID", nullable = false, unique = true)
    private Long id;
    @Column(name = "MCS_ALIAS")
    private String alias;
    @Column(name = "MCS_GENDER")
    private String gender;
    @Column(name = "MCS_LONGITUDE")
    private Double longitude;
    @Column(name = "MCS_LATITUDE")
    private Double latitude;
    @Column(name = "MCS_AGE_FROME")
    private Integer ageFrom;
    @Column(name = "MCS_AGE_TO")
    private Integer ageTo;
    @Column(name = "MCS_NOTIFIED_CUSTOMER")
    private Long notifiedCustomer;
    @Column(name = "MCS_RADIUS")
    private Double radius;
    @Column(name = "MCS_CREATE_DATE")
    private LocalDateTime createDate;

    @Transient
    private Long totalCustomer;

    @Transient
    private Long totalNotification;

    @Transient
    private Long totalViews;

    @ManyToOne
    @JoinColumn(name = "MCS_MERCHANT_ID", referencedColumnName = "MER_ID")
    private Merchant merchant;

    @ManyToOne
    @JoinColumn(name = "MCS_MERCHANT_USER_ID", referencedColumnName = "MUS_ID")
    private MerchantUser merchantUser;

    @ManyToOne
    @JoinColumn(name = "MCS_COUNTRY_ID", referencedColumnName = "COU_ID")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "MCS_CITY_ID", referencedColumnName = "CIT_ID")
    private City city;

    @ManyToOne
    @JoinColumn(name = "MCS_DISTRICT_ID", referencedColumnName = "DIS_ID")
    private District district;

    @ManyToOne
    @JoinColumn(name = "MCS_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    OperationLanguage operationLanguage;


    @OneToMany(mappedBy = "customerSegment")
    private List<SegmentNotificationRequest> segmentNotificationRequests;

    @OneToMany(mappedBy = "merchantCustomerSegment")
    private List<CountrySegment> countrySegmentList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getAgeFrom() {
        return ageFrom;
    }

    public void setAgeFrom(Integer ageFrom) {
        this.ageFrom = ageFrom;
    }

    public Integer getAgeTo() {
        return ageTo;
    }

    public void setAgeTo(Integer ageTo) {
        this.ageTo = ageTo;
    }

    public Long getNotifiedCustomer() {
        return notifiedCustomer;
    }

    public void setNotifiedCustomer(Long notifiedCustomer) {
        this.notifiedCustomer = notifiedCustomer;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public MerchantUser getMerchantUser() {
        return merchantUser;
    }

    public void setMerchantUser(MerchantUser merchantUser) {
        this.merchantUser = merchantUser;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public List<SegmentNotificationRequest> getSegmentNotificationRequests() {
        return segmentNotificationRequests;
    }

    public void setSegmentNotificationRequests(List<SegmentNotificationRequest> segmentNotificationRequests) {
        this.segmentNotificationRequests = segmentNotificationRequests;
    }

    public Long getTotalCustomer() {
        return totalCustomer;
    }

    public void setTotalCustomer(Long totalCustomer) {
        this.totalCustomer = totalCustomer;
    }

    public Long getTotalNotification() {
        return totalNotification;
    }

    public void setTotalNotification(Long totalNotification) {
        this.totalNotification = totalNotification;
    }

    public Long getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(Long totalViews) {
        this.totalViews = totalViews;
    }

    public List<CountrySegment> getCountrySegmentList() {
        return countrySegmentList;
    }

    public void setCountrySegmentList(List<CountrySegment> countrySegmentList) {
        this.countrySegmentList = countrySegmentList;
    }
}
