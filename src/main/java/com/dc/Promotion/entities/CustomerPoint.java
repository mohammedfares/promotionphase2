package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "CUSTOMER_POINT")
public class CustomerPoint implements Serializable {

    @Id
    @SequenceGenerator(name = "CustomerPointSeq", sequenceName = "CUSTOMER_POINT_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CustomerPointSeq", strategy = GenerationType.AUTO)
    @Column(name = "CPO_ID")
    private Long id;
    @Column(name = "CPO_CUST_ID", nullable = false, unique = true)
    private Long custId;
    @Column(name = "CPO_CUST_TYPE",nullable = false)
    private Long custType;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Long getCustType() {
        return custType;
    }

    public void setCustType(Long custType) {
        this.custType = custType;
    }
}
