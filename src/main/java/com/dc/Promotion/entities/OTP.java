package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "VALIDATION_OTP")
public class OTP implements Serializable {
    @Id
    @SequenceGenerator(name = "ValidationOTPSeq", sequenceName = "VALIDATION_OTP_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "ValidationOTPSeq", strategy = GenerationType.AUTO)
    @Column(name = "OTP_ID")
    private Long id;
    @Column(name = "OTP_MOBILE ")
    private String mobile;
    @Column(name = "OTP_EMAIL")
    private String email;
    @Column(name = "OTP_CODE")
    private String code;
    @Column(name = "OTP_CREATION_DATE")
    private LocalDateTime creationDate;
    @Column(name = "OTP_EXPIRY_DATE")
    private LocalDateTime expiryDate;
    @ManyToOne
    @JoinColumn(name = "OTP_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
