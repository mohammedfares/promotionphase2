package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "REGISTRATION_REQUEST_INDUSTRY")
public class RegistrationRequestIndustry implements Serializable {
    @Id
    @SequenceGenerator(name = "RegistrationRequestIndustrySeq", sequenceName = "REGISTRATION_REQUEST_INDUSTRY_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "RegistrationRequestIndustrySeq", strategy = GenerationType.AUTO)
    @Column(name = "RRI_ID", updatable = false, nullable = false, unique = true)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "RRI_MERCHANT_REGISTRATION_REQUEST_ID", referencedColumnName = "MRR_ID")
    private MerchantRegistrationRequest merchantRegistrationRequest;

    @ManyToOne
    @JoinColumn(name = "RRI_INDUSTRY_ID", referencedColumnName = "IND_ID")
    private Industry industry;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MerchantRegistrationRequest getMerchantRegistrationRequest() {
        return merchantRegistrationRequest;
    }

    public void setMerchantRegistrationRequest(MerchantRegistrationRequest merchantRegistrationRequest) {
        this.merchantRegistrationRequest = merchantRegistrationRequest;
    }

    public Industry getIndustry() {
        return industry;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }
}
