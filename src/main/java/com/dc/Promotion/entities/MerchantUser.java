package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "MERCHANT_USER")
public class MerchantUser implements Serializable {
    @Id
    @SequenceGenerator(name = "MerchantUserSeq", sequenceName = "MERCHANT_USER_SEQ", initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MerchantUserSeq", strategy = GenerationType.AUTO)
    @Column(name = "MUS_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "MUS_USERNAME", nullable = false, unique = true)
    private String userName;
    @Column(name = "MUS_EMAIL", nullable = false, unique = true)
    private String email;
    @Column(name = "MUS_PASSWORD")
    private String password;
    @Column(name = "MUS_MOBILE")
    private String mobile;
    @Column(name = "MUS_CREATION_DATE")
    private LocalDateTime createDate;
    @Column(name = "MUS_EXPIRATION_DATE")
    private LocalDateTime expireDate;
    @Column(name = "MUS_CRN")
    private String crn;
    @Column(name = "MUS_FIRST_NAME")
    private String firstName;
    @Column(name = "MUS_LAST_NAME")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "MUS_MER_ID", referencedColumnName = "MER_ID")
    private Merchant merchant;
    @ManyToOne
    @JoinColumn(name = "MUS_TYPE_ID", referencedColumnName = "MTY_ID")
    private MerchantUserType merchantUserType;
    @ManyToOne
    @JoinColumn(name = "MUS_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public MerchantUser() {
    }

    public MerchantUser(String userName, String email, String password, String mobile, LocalDateTime createDate, LocalDateTime expireDate, String crn) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.mobile = mobile;
        this.createDate = createDate;
        this.expireDate = expireDate;
        this.crn = crn;
    }

    public MerchantUserType getMerchantUserType() {
        return merchantUserType;
    }

    public void setMerchantUserType(MerchantUserType merchantUserType) {
        this.merchantUserType = merchantUserType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public String getCrn() {
        return crn;
    }

    public void setCrn(String crn) {
        this.crn = crn;
    }
}
