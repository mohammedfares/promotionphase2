package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PROMO_ITEM_CAPTION")
public class PromoItemCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "PromoItemCaptionSeq", sequenceName = "PROMO_ITEM_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PromoItemCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "PIC_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "PIC_NAME")
    private String name;
    @Column(name = "PIC_DESCRIPTION",length = 1000)
    private String description;
    @ManyToOne
    @JoinColumn(name = "PIC_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage language;

    @ManyToOne
    @JoinColumn(name = "PIC_ITEM_ID", referencedColumnName = "PIT_ID")
    private PromoItem promoItem;

    public PromoItemCaption() {
    }

    public PromoItemCaption(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public OperationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(OperationLanguage language) {
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PromoItem getPromoItem() {
        return promoItem;
    }

    public void setPromoItem(PromoItem promoItem) {
        this.promoItem = promoItem;
    }
}
