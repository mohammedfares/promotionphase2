package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "MEDIA_PROMO_VIEW")
public class MediaPromoView implements Serializable {
    @Id
    @SequenceGenerator(name = "MediaPromoViewSeq", sequenceName = "MEDIA_PROMO_VIEW_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "MediaPromoViewSeq", strategy = GenerationType.AUTO)
    @Column(name = "MPV_ID")
    private Long id;
    @Column(name = "MPV_CREATE_DATE")
    private LocalDateTime createDate;

    @ManyToOne
    @JoinColumn(name = "MPV_MEDIA_ID", referencedColumnName = "MAC_ID")
    private MediaAccount mediaAccount;

    @ManyToOne
    @JoinColumn(name = "MPV_PROMO_ID", referencedColumnName = "PRO_ID")
    private Promo promo;

    @ManyToOne
    @JoinColumn(name = "MPV_STS_ID", referencedColumnName = "STS_ID")
    private Status status;
    @Transient
    OperationLanguage operationLanguage;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public MediaAccount getMediaAccount() {
        return mediaAccount;
    }

    public void setMediaAccount(MediaAccount mediaAccount) {
        this.mediaAccount = mediaAccount;
    }

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }
}


