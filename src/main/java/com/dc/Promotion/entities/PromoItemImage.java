package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PROMO_ITEM_IMAGE")
public class PromoItemImage implements Serializable {
    @Id
    @SequenceGenerator(name = "PromoItemImageSeq", sequenceName = "PROMO_ITEM_IMAGE_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PromoItemImageSeq", strategy = GenerationType.AUTO)
    @Column(name = "PII_ID", updatable = false, nullable = false, unique = true)
    private Long id;
    @Column(name = "PII_URL")
    private String imageUrl;
    
    @ManyToOne
    @JoinColumn(name = "PII_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @ManyToOne
    @JoinColumn(name = "PII_ITEM_ID", referencedColumnName = "PIT_ID")
    private PromoItem promoItemId;
    @Transient
    OperationLanguage operationLanguage;

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public PromoItemImage() {
    }

    public PromoItemImage(String imageUrl, Status status, PromoItem promoItemId) {
        this.imageUrl = imageUrl;
        this.status = status;
        this.promoItemId = promoItemId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public PromoItem getPromoItemId() {
        return promoItemId;
    }

    public void setPromoItemId(PromoItem promoItemId) {
        this.promoItemId = promoItemId;
    }
}
