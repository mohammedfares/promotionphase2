package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "COUNTRY_CAPTION")
public class CountryCaption implements Serializable {
    @Id
    @SequenceGenerator(name = "CountryCaptionSeq", sequenceName = "COUNTRY_CAPTION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CountryCaptionSeq", strategy = GenerationType.AUTO)
    @Column(name = "COC_ID")
    private Long id;
    @Column(name = "COC_COUNTY_NAME")
    private String countryName;
    @Column(name = "COC_NATION_NAME")
    private String nationalityName;
    @Column(name = "COC_DESCRIPTION")
    private String description;

    @ManyToOne
    @JoinColumn(name = "COC_COUNTRY_ID", referencedColumnName = "COU_ID")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "COC_LANG_ID", referencedColumnName = "OLA_ID")
    private OperationLanguage language;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public OperationLanguage getLanguage() {
        return language;
    }

    public void setLanguage(OperationLanguage language) {
        this.language = language;
    }
}
