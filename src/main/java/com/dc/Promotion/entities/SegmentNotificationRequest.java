package com.dc.Promotion.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "SegmentNotificationRequest")
@Entity
public class SegmentNotificationRequest {
    @Id
    @SequenceGenerator(name = "SegmentNotificationRequestSeq", sequenceName = "SegmentNotificationRequest_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "SegmentNotificationRequestSeq", strategy = GenerationType.AUTO)
    @Column(name = "SNR_ID", nullable = false, unique = true)
    private Long id;

    @Column(name = "SNR_CREATION_DATE")
    private LocalDateTime creationTime;

    @Column(name = "SNR_TOTAL_Notification")
    private Long totalNotification;

    @Column(name = "SNR_TOTAL_View")
    private Long totalView;

    @Column(name = "SNR_TOTAL_CUSTOMER")
    private Long totalCustomer;


    @ManyToOne
    @JoinColumn(name = "SNR_MER_CUS_SEG_ID", referencedColumnName = "MCS_ID")
    private MerchantCustomerSegment customerSegment;

    @OneToOne
    @JoinColumn(name = "SNR_PROMO_ID", referencedColumnName = "PRO_ID")
    private Promo promo;

    public Promo getPromo() {
        return promo;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public SegmentNotificationRequest(LocalDateTime creationTime, Long totalNotification, Long totalCustomer, MerchantCustomerSegment customerSegment) {
        this.creationTime = creationTime;
        this.totalNotification = totalNotification;
        this.totalCustomer = totalCustomer;
        this.customerSegment = customerSegment;
    }

    public SegmentNotificationRequest() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public Long getTotalNotification() {
        return totalNotification;
    }

    public void setTotalNotification(Long totalNotification) {
        this.totalNotification = totalNotification;
    }

    public Long getTotalCustomer() {
        return totalCustomer;
    }

    public void setTotalCustomer(Long totalCustomer) {
        this.totalCustomer = totalCustomer;
    }

    public MerchantCustomerSegment getCustomerSegment() {
        return customerSegment;
    }

    public void setCustomerSegment(MerchantCustomerSegment customerSegment) {
        this.customerSegment = customerSegment;
    }

    public Long getTotalView() {
        return totalView;
    }

    public void setTotalView(Long totalView) {
        this.totalView = totalView;
    }

    @Override
    public String toString() {
        return "SegmentNotificationRequest{" +
                "id=" + id +
                ", creationTime=" + creationTime +
                ", totalNotification=" + totalNotification +
                ", totalCustomer=" + totalCustomer +
                ", customerSegment=" + customerSegment +
                '}';
    }
}
