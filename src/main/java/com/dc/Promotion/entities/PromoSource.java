package com.dc.Promotion.entities;

import javax.persistence.*;

@Entity
@Table(name = "PROMO_SOURCE")
public class PromoSource {

    @Id
    @SequenceGenerator(name = "PromoSourceSeq", sequenceName = "PROMO_SOURCE_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "PromoSourceSeq", strategy = GenerationType.AUTO)
    @Column(name = "PRS_ID",updatable = false, insertable = false)
    private Long id;
    @Column(name = "PRS_CODE")
    private String code;

    @ManyToOne
    @JoinColumn(name = "PRS_STS",referencedColumnName = "STS_ID")
    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
