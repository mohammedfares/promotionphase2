package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "SPECIFICATION")
public class Specification implements Serializable {
    @Id
    @SequenceGenerator(name = "SpecificationSeq", sequenceName = "SPECIFICATION_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "SpecificationSeq", strategy = GenerationType.AUTO)
    @Column(name = "SPE_ID")
    private Long id;

    @Column(name = "SPE_Key")
    private String key;

    @ManyToOne
    @JoinColumn(name = "SPE_STS_ID", referencedColumnName = "STS_ID")
    private Status status;

    @Transient
    OperationLanguage operationLanguage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }
}
