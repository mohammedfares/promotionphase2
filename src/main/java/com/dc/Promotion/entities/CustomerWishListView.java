package com.dc.Promotion.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "CUSTOMER_WISH_VIEW")
public class CustomerWishListView implements Serializable {

    @Id
    @SequenceGenerator(name = "CustomerWishListViewSeq", sequenceName = "CUSTOMER_WISH_VIEW_SEQ"
            , initialValue = 1, allocationSize = 1)
    @GeneratedValue(generator = "CustomerWishListViewSeq", strategy = GenerationType.AUTO)
    @Column(name = "CWL_ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CWL_CUST_ID", referencedColumnName = "CAC_ID")
    private CustomerAccount customerAccount;

    @ManyToOne
    @JoinColumn(name = "CWL_ITEM_ID", referencedColumnName = "PIT_ID", unique = true)
    private PromoItem promoItem;

    @ManyToOne
    @JoinColumn(name = "CWL_CWI_ID", referencedColumnName = "CWI_ID")
    private CustomerWishList customerWishList;

    @Transient
    private OperationLanguage operationLanguage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(CustomerAccount customerAccount) {
        this.customerAccount = customerAccount;
    }

    public PromoItem getPromoItem() {
        return promoItem;
    }

    public void setPromoItem(PromoItem promoItem) {
        this.promoItem = promoItem;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public CustomerWishList getCustomerWishList() {
        return customerWishList;
    }

    public void setCustomerWishList(CustomerWishList customerWishList) {
        this.customerWishList = customerWishList;
    }
}
