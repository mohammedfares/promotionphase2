package com.dc.Promotion.utils;


import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantBranch;
import com.dc.Promotion.service.MerchantBranchService;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
//import sun.misc.BASE64Decoder;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

public class Utils {
    private static ApplicationContext applicationContext;


    public static String StandardPhoneFormat(String countryCode, String number) {
        try {
            PhoneNumberUtil util = PhoneNumberUtil.getInstance();
            int parsedCountryCode = Integer.parseInt(countryCode);
            Phonenumber.PhoneNumber parsedNumber = util.parse(number,
                    util.getRegionCodeForCountryCode(parsedCountryCode));

            boolean validationFlag = PhoneNumberUtil.getInstance().isValidNumber(parsedNumber);

            if (!validationFlag) {
                return null;
            }
            return util.format(parsedNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static boolean isEmailCorrect(String email) {
        String emailRegex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"" +
                "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@" +
                "(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[" +
                "(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\\.){3}" +
                "(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:" +
                "(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

        if (Pattern.compile(emailRegex).matcher(email).matches())
            return true;
        else
            return false;
    }

    //     public static String encodePassword(String Password) {
//         BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//         String encode = passwordEncoder.encode(Password);
//         return encode;
//     }
    public static String randomNumber(int length) {

        char[] characterSet = "0123456789".toCharArray();

        Random random = new SecureRandom();

        char[] result = new char[length];
        for (int i = 0; i < result.length; i++) {
            int randomCharIndex = random.nextInt(characterSet.length);
            result[i] = characterSet[randomCharIndex];
        }
        return new String(result);
    }

    public static boolean hasText(String text) {
        boolean containsDigit = false;

        if (text != null && !text.isEmpty()) {
            for (char c : text.toCharArray()) {
                if (containsDigit = !Character.isDigit(c)) {
                    break;
                }
            }
        }

        return containsDigit;
    }

    public static BufferedImage decodeToImage(String imageString) {

        String[] imageData = imageString.split(",");

        BufferedImage image = null;
        byte[] imageByte;
        try {
            //BASE64Decoder decoder = new BASE64Decoder();
            imageByte = Base64.decodeBase64((imageData[1]));
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    public static String extractExtention(String encoded) {

        try {
            int extentionStartIndex = encoded.indexOf('/');
            int extensionEndIndex = encoded.indexOf(';');
            int filetypeStartIndex = encoded.indexOf(':');

            String fileType = encoded.substring(filetypeStartIndex + 1, extentionStartIndex);
            String fileExtension = encoded.substring(extentionStartIndex + 1, extensionEndIndex);

            System.out.println("fileType : " + fileType);
            System.out.println("file Extension :" + fileExtension);

            return fileExtension;

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

    }

    public static String ConvertImage(String img) {

        try {

            try {
                URL imageUrl = new URL(img);
                return img;
            } catch (Exception ex) {
                System.out.println("ex: " + ex);
            }

            BufferedImage bufferedImageCover = Utils.decodeToImage(img);

            long currentTimeStampCover = System.currentTimeMillis();
            if (bufferedImageCover == null) {
                // throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400" null);
            }
            String coverExtention = Utils.extractExtention(img);

            File outputfile = new File("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\attachment\\" + currentTimeStampCover + "." + coverExtention);
            ImageIO.write(bufferedImageCover, coverExtention, outputfile);

            String coverPhotoUrl = "http://91.106.99.2:8080/attachment/" + currentTimeStampCover + "." + coverExtention;//amazonS3ClientService.getFileUrl(currentTimeStampCover + "." + coverExtention);

            return coverPhotoUrl;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void setApplicationContext(ApplicationContext applicationContext) {
        Utils.applicationContext = applicationContext;
    }

    public static Object handleEmptyOrNullObject(Object object) {
        if (object == null) {
            return null;
        }

        if (object instanceof String) {
            if (((String) object).isEmpty()) {
                return null;
            } else {
                return object;
            }
        }

        return null;
    }

    public static LocalDateTime parseLocalDateTime(String date, String pattern) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            LocalDateTime dateTime = LocalDateTime.parse(date, formatter);
            return dateTime;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static LocalDate parseLocalDate(String date, String pattern) {
        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);

            LocalDate localDate = LocalDate.parse(date, formatter);
            return localDate;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static LocalDateTime startOfDay(LocalDate localDate) {

        if (localDate == null) {
            return null;
        }

        LocalDateTime localDateTime = localDate.atStartOfDay();

        return localDateTime;
    }

    public static LocalDateTime endOfDay(LocalDate localDate) {

        if (localDate == null) {
            return null;
        }

        LocalDateTime localDateTime = localDate.atStartOfDay().plusDays(1);

        return localDateTime;
    }

    public static boolean isInsideCircle(String fromPointX, String fromPointY, String toPointX, String toPointY, Integer radius) {
        return true;
    }

    public static Double parseDouble(String doubleStr) {

        if (doubleStr == null || doubleStr.isEmpty()) {
            return null;
        }

        try {
            return Double.parseDouble(doubleStr);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String appendLikeOperator(String value) {
        if (value == null || value.isEmpty()) {
            return null;
        }
        return ("%" + value + "%").toLowerCase();
    }

    public static String fetchUserType(String username) {

        try {


            if (username == null || username.isEmpty()) {
                return null;
            }
            return username.split("_")[0];
        } catch (Exception ex) {
            return null;
        }
    }

    public static Long findTotalCategoryViews(List<ItemCategory> itemCategoryList) {

        Long sum = 0L;
        for (ItemCategory itemCategory : itemCategoryList) {
            sum += itemCategory.getTotalViews();
        }
        return sum;
    }

    public static String initPromoNotificationData(Long promoId, Long notificationId) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("promoId", promoId);
        jsonObject.put("notificationId", notificationId);

        return jsonObject.toString();
    }

    public static Double[] initPriceIntervalValue(Double priceFrom, Double priceTo) {

        Double[] priceInterval = new Double[2];

        if (priceFrom == null) {
            priceFrom = 0.0;
        }

        if (priceTo == null) {
            priceTo = 0.0;
        }

        if (priceFrom > priceTo) {
            priceInterval[0] = priceTo;
        } else {
            priceInterval[0] = priceFrom;
        }

        if (priceTo < priceFrom) {
            priceInterval[1] = priceFrom;
        } else {
            priceInterval[1] = priceTo;
        }

        return priceInterval;
    }

    public static String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] bytes = loadFile(file);
        byte[] encoded = org.apache.commons.codec.binary.Base64.encodeBase64(bytes);
        String encodedString = new String(encoded);

        return encodedString;
    }

    public static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int) length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;
    }

    public static int calculateAge(LocalDate birthDate) {

        LocalDate currentDate = LocalDate.now();

        birthDate = parseLocalDate(birthDate.toString(), "yyyy-MM-dd");
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }


    public static String ConvertImageExcel(Sheet sheet, int rowNumber) {

        try {
            long currentTimeStampCover = System.currentTimeMillis();

            XSSFDrawing drawing = (XSSFDrawing) sheet.getDrawingPatriarch();

            for (XSSFShape shape : drawing.getShapes()) {
                System.out.println("Whole group is anchored upper left:");
                int groupRow = ((XSSFClientAnchor) shape.getAnchor()).getRow1();

                System.out.print("Row: " + groupRow);

                if (groupRow == rowNumber) {
                    if (shape instanceof XSSFPicture) {

                        XSSFPicture pic = (XSSFPicture) shape;

                        byte[] data = pic.getPictureData().getData();

                        String coverExtention = pic.getPictureData().suggestFileExtension();

                        FileOutputStream out = new FileOutputStream("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\attachment\\" + currentTimeStampCover + "." + coverExtention);

                        out.write(data);

                        out.close();

                        String coverPhotoUrl = "http://91.106.99.2:8080/attachment/" + currentTimeStampCover + "." + coverExtention;

                        return coverPhotoUrl;
                    }
                }
            }
            return null;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    public static List<String> ConvertImageItemExcel(Sheet sheet, int rowNumber) {

        try {
            System.out.println(">>>>>>> start item >>>>>>>>>>>>>>:");
            System.out.println("rowNumber: "+rowNumber);
            List<String> imageList = new ArrayList<>();

            XSSFDrawing drawing = (XSSFDrawing) sheet.getDrawingPatriarch();

            for (XSSFShape shape : drawing.getShapes()) {
//                System.out.println("Whole group is anchored upper left:");
                int groupRow = ((XSSFClientAnchor) shape.getAnchor()).getRow1();

//                System.out.print("Row: " + groupRow);

                if (groupRow == rowNumber) {
                    if (shape instanceof XSSFPicture) {

                        XSSFPicture pic = (XSSFPicture) shape;

                        byte[] data = pic.getPictureData().getData();

                        String coverExtention = pic.getPictureData().suggestFileExtension();

                        String randomImageName = randomNumber(10);

                        FileOutputStream out = new FileOutputStream("C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\attachment\\" + randomImageName + "." + coverExtention);

                        out.write(data);

                        out.close();

                        String coverPhotoUrl = "http://91.106.99.2:8080/attachment/" + randomImageName + "." + coverExtention;

                        imageList.add(coverPhotoUrl);
                    }
                }
            }
            System.out.println("\n>>>>>>> end item >>>>>>>>>>>>>>:");

            return imageList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }


    public static List<MerchantBranch> parseMerchantBranchExcel(String merchantBranchCellValue, ApplicationContext applicationContext) {

        try {
            List<MerchantBranch> merchantBranchList = new ArrayList<>();

            String[] merchantBranchArray = merchantBranchCellValue.split(",");

            for (String branchStr : merchantBranchArray) {

                String[] merchantBranchStrValue = branchStr.split("-");

                MerchantBranch merchantBranch = applicationContext.getBean(MerchantBranchService.class).findById(Long.parseLong(merchantBranchStrValue[0]));

                if (merchantBranch != null) {
                    merchantBranchList.add(merchantBranch);
                }
            }
            return merchantBranchList;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }




    public static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        } else {
            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1))*
            Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist* 60 *1.1515;
            if (unit == "K") {
                dist = dist * 1.609344;
            } else if (unit == "N") {
                dist = dist * 0.8684;
            }
            return (dist);
        }
    }

 

    public static Double roundDouble(Double value) {
        if (value == null)
            return null;
        DecimalFormat newFormat = new DecimalFormat("#.##");
        double twoDecimal = Double.valueOf(newFormat.format(value));
        return twoDecimal;
    }



}
