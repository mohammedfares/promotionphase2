package com.dc.Promotion.utils;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.MerchantBranchCaption;
import com.dc.Promotion.entities.Specification;
import com.dc.Promotion.entities.SpecificationCaption;
import com.dc.Promotion.resources.ItemCategoryResources;
import com.dc.Promotion.resources.ItemSpecificationCaptionResources;
import com.dc.Promotion.resources.MerchantBranchResources;
import com.dc.Promotion.resources.SpecificationResources;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

public class PromoExcelFile {

    private String name;

    private String type;

    private String base64;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBase64() {
        return base64;
    }

    public void setBase64(String base64) {
        this.base64 = base64;
    }

    public static File createExcelFile(ApplicationContext applicationContext, String merchantName, List<MerchantBranchResources> merchantBranchResources, List<ItemCategoryResources> itemCategoryResources, List<ItemSpecificationCaptionResources> itemSpecificationCaptionResources) throws IOException, InvalidFormatException {

        File file = new File(applicationContext.getEnvironment().getProperty("excel_path") + "promo_template.xlsm");
//        File file = new File("C:\\Users\\abdal\\OneDrive\\Documents\\" + "promo_template.xlsm");

        LocalDateTime currentDateTime = LocalDateTime.now();

        Workbook workbook = WorkbookFactory.create(file);

        Sheet sheet = workbook.getSheetAt(0);

        DataFormat fmt = workbook.createDataFormat();
        CellStyle textStyle = workbook.createCellStyle();
        textStyle.setDataFormat(fmt.getFormat("@"));

        sheet.setDefaultColumnStyle(0, textStyle);
        sheet.setDefaultColumnStyle(1, textStyle);
        sheet.setDefaultColumnStyle(2, textStyle);
        sheet.setDefaultColumnStyle(3, textStyle);
        sheet.setDefaultColumnStyle(4, textStyle);
        sheet.setDefaultColumnStyle(5, textStyle);
        sheet.setDefaultColumnStyle(6, textStyle);
        sheet.setDefaultColumnStyle(7, textStyle);
        sheet.setDefaultColumnStyle(8, textStyle);
        sheet.setDefaultColumnStyle(9, textStyle);
        sheet.setDefaultColumnStyle(10, textStyle);
        sheet.setDefaultColumnStyle(11, textStyle);

        Row rowMerchantName = sheet.getRow(0);

        Cell cellMerchantName = rowMerchantName.getCell(1);
        cellMerchantName.setCellValue(merchantName);


        for (int rowIndex = 0; rowIndex < merchantBranchResources.size(); rowIndex++) {
            Row branchRow = sheet.getRow(rowIndex + 1);
            Cell cellBranchListName = branchRow.getCell(49);
            if (cellBranchListName != null) {
                cellBranchListName.setCellValue(merchantBranchResources.get(rowIndex).getMerchantBranch() + "-" + merchantBranchResources.get(rowIndex).getName());
            }
        }

        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(new File(applicationContext.getEnvironment().getProperty("excel_path") + currentDateTime.getYear() + "_" + currentDateTime.getMonthValue() + "_" + currentDateTime.getDayOfMonth() + "_" + currentDateTime.getHour() + "_" + currentDateTime.getMinute() + "_" + file.getName()));

        workbook.write(fileOut);
        fileOut.close();
        // Closing the workbook
        workbook.close();

        File generatedFile = new File(applicationContext.getEnvironment().getProperty("excel_path") + currentDateTime.getYear() + "_" + currentDateTime.getMonthValue() + "_" + currentDateTime.getDayOfMonth() + "_" + currentDateTime.getHour() + "_" + currentDateTime.getMinute() + "_" + file.getName());

        return generatedFile;
    }
}
