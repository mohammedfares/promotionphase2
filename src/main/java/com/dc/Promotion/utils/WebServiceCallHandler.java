package com.dc.Promotion.utils;

import com.dc.Promotion.entities.CustomerAccount;
import com.squareup.okhttp.*;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Base64;

public class WebServiceCallHandler {

    public static String generateTokenForCustomer(String username, String password) {
        OkHttpClient client = new OkHttpClient();

        JSONObject loginRequest = new JSONObject();

        loginRequest.put("username", username);

        loginRequest.put("password", password);

        MediaType mediaType = MediaType.parse("application/json;charset=utf-8");

        RequestBody body = RequestBody.create(mediaType, (loginRequest.toString()));

        String auth = username + ",CUS,:" + password;

        System.out.println("auth: "+auth);

        auth = "Basic " + Base64.getEncoder().encodeToString(((username + ",CUS,:" + password).getBytes()));

        System.out.println("auth: "+auth);

        Request request = new Request.Builder()
                .url("http://localhost:8083/customer/login")
                .post(body)
                .addHeader("Content-Type", "application/json;charset=utf-8")
                .addHeader("Authorization", auth)
                .addHeader("cache-control", "no-cache")
                .build();

        String responseCode = null;
        String token = null;
        Response response = null;
        try {
            response = client.newCall(request).execute();
            responseCode = String.valueOf(response.code());
            String responseBody = response.body().string();
            System.out.println("responseBody: "+responseBody);
            JSONObject jsonObject = new JSONObject(responseBody);
            token = jsonObject.getJSONObject("body").getString("token");
        } catch (IOException e) {
            e.printStackTrace();
            if (response != null)
                responseCode = String.valueOf(response.code());
            else
                responseCode = "500";
        }
       return token;
    }
}
