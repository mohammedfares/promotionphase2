package com.dc.Promotion.utils;

import com.squareup.okhttp.*;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;

import java.io.IOException;

public class PushNotificationHandler {

    public static void sendNotification(String textTitle , String textBody , String data , String token , ApplicationContext applicationContext) {

        OkHttpClient client = new OkHttpClient();

        JSONObject notificationJson = new JSONObject();

        notificationJson.put("title",textTitle);

        notificationJson.put("text",textBody);

        notificationJson.put("sound","default");

        JSONObject notificationData = new JSONObject(data);

        JSONObject fcmJsonRequest = new JSONObject();

        fcmJsonRequest.put("to", token);

        fcmJsonRequest.put("notification" , notificationJson);

        fcmJsonRequest.put("data", notificationData);

        System.out.println("json req: " + fcmJsonRequest.toString());

        String serverKey = applicationContext.getEnvironment().getProperty("fcm_key");

        System.out.println("serverKey: "+serverKey);

        MediaType mediaType = MediaType.parse("application/json;charset=utf-8");

        RequestBody body = RequestBody.create(mediaType, (fcmJsonRequest.toString()));

        Request request = new Request.Builder()
                .url(applicationContext.getEnvironment().getProperty("fcm_url"))
                .post(body)
                .addHeader("Content-Type", "application/json;charset=utf-8")
                .addHeader("Authorization", "key=" + serverKey)
                .addHeader("cache-control", "no-cache")
                .build();

        String responseCode = null;
        Response response = null;
        try {
            response = client.newCall(request).execute();
            responseCode = String.valueOf(response.code());
            System.out.println("body:::>>>"+response.body().toString());
        } catch (IOException e) {
            e.printStackTrace();
            if (response != null)
                responseCode = String.valueOf(response.code());
            else
                responseCode = "500";
        }
        System.out.println("responseCode:::>>>" + responseCode);
    }


}
