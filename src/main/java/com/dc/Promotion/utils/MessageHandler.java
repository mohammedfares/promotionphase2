package com.dc.Promotion.utils;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.SystemMessageCaption;

public class MessageHandler {
    public static MessageBody setMessageBody(SystemMessageCaption systemMessageCaption, Object obj)
    {
        MessageBody messageBody = MessageBody.getInstance();

        messageBody.setStatus(systemMessageCaption.getMessage().getCode());
        messageBody.setMsgWithLanguage(systemMessageCaption.getText());
//         messageBody.setText(systemMessageCaption.getCaption());
        messageBody.setBody(obj);
        return messageBody;
    }
}
