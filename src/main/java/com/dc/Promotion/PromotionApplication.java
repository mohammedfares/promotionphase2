package com.dc.Promotion;

import com.dc.Promotion.config.DatabaseSeeder;
import com.dc.Promotion.utils.Utils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.io.File;
import java.io.IOException;
import java.util.List;

@SpringBootApplication
public class PromotionApplication {

    public static void main(String[] args) throws IOException, InvalidFormatException {
        //2019_11_13_9_38_promo_template
//        File file = new File("C:\\Users\\abdal\\OneDrive\\Documents\\2019_11_13_9_38_promo_template.xlsm");
//
//        Workbook workbook = WorkbookFactory.create(file);
//
//        for (int itemRowIndex = 20; itemRowIndex <= workbook.getSheetAt(0).getLastRowNum(); itemRowIndex = itemRowIndex + 18) {
//            List<String> imageStringList = Utils.ConvertImageItemExcel(workbook.getSheetAt(0),itemRowIndex + 7);
//            System.out.println("imageStringList: "+imageStringList);
//        }

        ApplicationContext context = SpringApplication.run(PromotionApplication.class, args);

        Environment env = context.getBean(Environment.class);
        if (env.getProperty("seed_database").trim().equals("true")) {
            DatabaseSeeder databaseSeeder = context.getBean(DatabaseSeeder.class);
            databaseSeeder.seed();
        }

        System.out.println(Math.ceil(6.0/10.0));

    }

}
