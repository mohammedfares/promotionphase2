package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MediaNotification;
import org.springframework.data.repository.CrudRepository;

public interface MediaNotificationRepo extends CrudRepository<MediaNotification,Long> {

    public  MediaNotification findById(Long id);

    public  MediaNotification save(MediaNotification notification);





}
