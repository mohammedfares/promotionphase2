package com.dc.Promotion.repository;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.entities.StatusCaption;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StatusCaptionRepo extends CrudRepository<StatusCaption,Long> {
    public  StatusCaption findById(long id);


    @Query("select sc from StatusCaption sc where sc.language=:language and sc.status  in (select sts from Status sts where  sts.code like CONCAT (:code,'%'))")
    public List<StatusCaption> findByQuery (@Param("language") OperationLanguage language,@Param("code") String code);

    @Query("select sc from StatusCaption sc where sc.language=:language and sc.status  in (select sts from Status sts where sts.code like CONCAT (:code,'%') and sts.code not like CONCAT ('%','DELETE'))")
    public List<StatusCaption> findWithoutDeleted(@Param("language") OperationLanguage language,@Param("code") String code);

    public StatusCaption findByStatusAndLanguage(Status status,OperationLanguage language);

}
