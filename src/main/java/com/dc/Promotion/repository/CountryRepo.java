package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Country;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CountryRepo extends CrudRepository<Country,Long> {
    public Country findById(long id);
    public List<Country> findAll();
    public Country findByCode(String code);
}
