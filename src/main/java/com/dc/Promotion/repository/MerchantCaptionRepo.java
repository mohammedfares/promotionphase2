package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantCaption;
import com.dc.Promotion.entities.OperationLanguage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface MerchantCaptionRepo extends CrudRepository<MerchantCaption,Long> {
    public MerchantCaption findById(long id);
    public MerchantCaption save(MerchantCaption merchantCaption);

    @Query("select mca from MerchantCaption mca where mca.merchant in (select mer from Merchant mer" +
            " where mer in (select merchant from MerchantIndustry where industry=:industry ) )")
    public List<MerchantCaption> findByIndustry(@Param("industry") Industry industry);

    public List<MerchantCaption> findAll();

    public List<MerchantCaption> findMerchantCaptionByMerchant_Id(Long id);

    public MerchantCaption findByIdAndOperationLanguage(Long id, OperationLanguage language);

    public MerchantCaption findByMerchantAndOperationLanguage(Merchant merchant,OperationLanguage language);
}
