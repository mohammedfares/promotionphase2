package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Specification;
import org.springframework.data.repository.CrudRepository;

public interface SpecificationRepo extends CrudRepository <Specification,Long> {

    public Specification findById(long id);

    public Specification findByKey(String key);
}
