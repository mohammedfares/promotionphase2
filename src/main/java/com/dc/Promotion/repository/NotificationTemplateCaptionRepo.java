package com.dc.Promotion.repository;

import com.dc.Promotion.entities.NotificationTemplate;
import com.dc.Promotion.entities.NotificationTemplateCaption;
import com.dc.Promotion.entities.OperationLanguage;
import org.springframework.data.repository.CrudRepository;

public interface NotificationTemplateCaptionRepo extends CrudRepository<NotificationTemplateCaption,Long> {

    public  NotificationTemplateCaption findByNotificationTemplateAndOperationLanguage(NotificationTemplate template, OperationLanguage operationLanguage);

}
