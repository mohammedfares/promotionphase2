package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MerchantUserType;
import org.springframework.data.repository.CrudRepository;

public interface MerchantUserTypeRepo extends CrudRepository<MerchantUserType,Long> {
    public  MerchantUserType findById(Long id);
    public MerchantUserType findByCode(String code);
}
