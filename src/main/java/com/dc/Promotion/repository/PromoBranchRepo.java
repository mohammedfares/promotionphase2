package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoBranch;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PromoBranchRepo extends CrudRepository<PromoBranch,Long> {

    public PromoBranch findById(long id);

    public PromoBranch save(PromoBranch promoBranch);

    public List<PromoBranch> findAllByPromo(Promo promo);

    public void deleteAllByPromo(Promo promo);
}
