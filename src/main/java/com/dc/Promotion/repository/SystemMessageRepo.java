package com.dc.Promotion.repository;

import com.dc.Promotion.entities.SystemMessage;
import org.springframework.data.repository.CrudRepository;

public interface SystemMessageRepo extends CrudRepository<SystemMessage,Long> {
    public  SystemMessage findById(long id);
    public  SystemMessage findByCode(String code);
}
