package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CityCaptionRepo extends CrudRepository <CityCaption,Long>{

    public CityCaption findById(long id);

    public CityCaption findByCityAndLanguage(City city, OperationLanguage language);

    public List<CityCaption> findByLanguageAndCity_Country(OperationLanguage language, OperationCountry country);
}
