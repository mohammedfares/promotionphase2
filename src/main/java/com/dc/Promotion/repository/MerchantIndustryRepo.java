package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MerchantIndustry;
import org.springframework.data.repository.CrudRepository;

public interface MerchantIndustryRepo extends CrudRepository<MerchantIndustry,Long > {
    public  MerchantIndustry findById(long id);

    public MerchantIndustry save(MerchantIndustry merchantIndustry);
}
