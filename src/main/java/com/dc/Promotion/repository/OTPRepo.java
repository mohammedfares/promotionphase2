package com.dc.Promotion.repository;

import com.dc.Promotion.entities.OTP;
import com.dc.Promotion.entities.Status;
import org.springframework.data.repository.CrudRepository;

public interface OTPRepo extends CrudRepository<OTP, Long> {
    public OTP findById(long id);

    public OTP findByMobileAndCode(String mobile, String code);

    public OTP save(OTP otp);

    public OTP findByMobileAndStatus_Code(String mobile, String status);

    public OTP findByCode(String code);
}
