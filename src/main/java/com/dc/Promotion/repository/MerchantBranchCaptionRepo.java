package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MerchantBranchCaptionRepo extends CrudRepository<MerchantBranchCaption,Long > , JpaSpecificationExecutor {
    public MerchantBranchCaption findById(long id);
    public MerchantBranchCaption findByMerchantBranch(MerchantBranch branch);
    public List<MerchantBranchCaption> findAllByMerchantBranch_MerchantAndOperationLanguageAndMerchantBranch_Status(Merchant merchant, OperationLanguage language, Status status);

    public MerchantBranchCaption save(MerchantBranchCaption merchantBranchCaption);

    public MerchantBranchCaption findMerchantBranchCaptionByMerchantBranchAndOperationLanguage(MerchantBranch branch , OperationLanguage language);

    public List<MerchantBranchCaption> findAllByMerchantBranch(MerchantBranch merchantBranch);
}
