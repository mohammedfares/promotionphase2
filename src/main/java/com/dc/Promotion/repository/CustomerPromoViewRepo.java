package com.dc.Promotion.repository;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerItemView;
import com.dc.Promotion.entities.CustomerPromoView;
import com.dc.Promotion.entities.Promo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerPromoViewRepo extends CrudRepository<CustomerPromoView,Long> {

    public  CustomerPromoView findById(long id);

    public CustomerPromoView save(CustomerPromoView customerPromoView);

    public CustomerPromoView findByPromoAndCustomerAccount(Promo promo, CustomerAccount account);

    public List<CustomerPromoView> findAllByPromo(Promo promo);

    public void deleteAllByPromo(Promo promo);
}
