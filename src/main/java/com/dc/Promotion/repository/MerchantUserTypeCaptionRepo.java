package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MerchantUserType;
import com.dc.Promotion.entities.MerchantUserTypeCaption;
import com.dc.Promotion.entities.OperationLanguage;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MerchantUserTypeCaptionRepo extends CrudRepository<MerchantUserTypeCaption,Long> {
    public MerchantUserTypeCaption findById(long id);

    public List<MerchantUserTypeCaption> findByOperationLanguage( OperationLanguage language);

    public MerchantUserTypeCaption findByMerchantUserTypeAndOperationLanguage(MerchantUserType userType,OperationLanguage language);
}
