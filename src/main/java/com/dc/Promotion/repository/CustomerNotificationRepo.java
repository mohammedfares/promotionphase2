package com.dc.Promotion.repository;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerNotification;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerNotificationRepo extends CrudRepository<CustomerNotification, Long> {
    public CustomerNotification findById(Long id);

    public CustomerNotification save(CustomerNotification customerNotification);
}
