package com.dc.Promotion.repository;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.Country;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CityRepo extends CrudRepository<City,Long> {

    public City findById(long id);

    public List<City> findAllByCountry(Country country);
}
