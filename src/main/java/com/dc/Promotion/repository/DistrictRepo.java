package com.dc.Promotion.repository;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.District;
import com.dc.Promotion.entities.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DistrictRepo extends CrudRepository<District ,Long> {
    public  District findById(long id);
    public List<District> findAllByCityAndStatus(City city, Status status);
}
