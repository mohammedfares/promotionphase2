package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantCustomerSegment;
import com.dc.Promotion.entities.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MerchantCustomerSegmentRepo extends CrudRepository<MerchantCustomerSegment,Long> {

    public MerchantCustomerSegment findById(Long id);

    public MerchantCustomerSegment save(MerchantCustomerSegment merchantCustomerSegment);

    @Query("select mcs from MerchantCustomerSegment mcs where mcs.merchant=:merchant and mcs.status.code <> 'MCS_DELETE'")
    public List<MerchantCustomerSegment> findByQuery(@Param("merchant") Merchant merchant);

    @Query("select distinct mcs from MerchantCustomerSegment mcs join mcs.segmentNotificationRequests snr " +
            "where mcs.status.code = 'MCS_ACTIVE' " +
            "and mcs.merchant=:merchant ")
    public List<MerchantCustomerSegment> findTopSegment(@Param("merchant")Merchant merchant);
}
