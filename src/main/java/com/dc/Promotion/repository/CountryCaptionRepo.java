package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.CountryCaption;
import com.dc.Promotion.entities.OperationCountry;
import com.dc.Promotion.entities.OperationLanguage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountryCaptionRepo extends CrudRepository<CountryCaption,Long> {

    public CountryCaption findById(long id);
    public CountryCaption findByCountryAndLanguage(Country country, OperationLanguage language);

    @Query("select cc from CountryCaption cc where cc.language=:language and cc.country in (select opc.country from OperationCountry opc)")
    public List<CountryCaption> findByQuery(@Param("language") OperationLanguage language);

    @Query("select cc from CountryCaption  cc where cc.language=:language and cc.country in (select cou from Country cou)")
    public List<CountryCaption> findByQueryCountry(@Param("language") OperationLanguage language);



}
