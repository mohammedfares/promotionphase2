package com.dc.Promotion.repository;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.PromoItemCaption;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PromoItemCaptionRepo extends CrudRepository<PromoItemCaption,Long> {
    public  PromoItemCaption findById(Long id);

    public PromoItemCaption save(PromoItemCaption promoItemCaption);


    public List<PromoItemCaption> findAllByPromoItem_Promo(Promo promo);
    public PromoItemCaption findByPromoItemAndLanguage(PromoItem promoItem ,OperationLanguage operationLanguage);
    public List<PromoItemCaption> findPromoItemCaptionByPromoItem(PromoItem promoItem);

    public void deleteAllByPromoItem(PromoItem promoItem);
}
