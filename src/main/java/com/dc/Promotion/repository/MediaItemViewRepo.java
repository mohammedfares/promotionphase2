package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaItemView;
import com.dc.Promotion.entities.PromoItem;
import org.springframework.data.repository.CrudRepository;

public interface MediaItemViewRepo extends CrudRepository<MediaItemView,Long> {

    public MediaItemView findById(Long id);

    public MediaItemView save(MediaItemView itemView);

    public  MediaItemView findByPromoItemAndMediaAccount(PromoItem promoItem, MediaAccount mediaAccount);

    public void deleteAllByPromoItem(PromoItem promoItem);
}
