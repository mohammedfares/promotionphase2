package com.dc.Promotion.repository;

import com.dc.Promotion.entities.ItemSpecification;
import com.dc.Promotion.entities.PromoItem;
import org.springframework.data.repository.CrudRepository;

public interface ItemSpecificationRepo extends CrudRepository <ItemSpecification,Long> {

    public ItemSpecification findById(long id);

    public ItemSpecification save(ItemSpecification itemSpecification);

    public void deleteAllByPromoItem(PromoItem promoItem);
}
