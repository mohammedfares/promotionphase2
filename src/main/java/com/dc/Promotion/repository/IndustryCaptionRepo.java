package com.dc.Promotion.repository;

import com.dc.Promotion.entities.IndustryCaption;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantRegistrationRequest;
import com.dc.Promotion.entities.OperationLanguage;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface
IndustryCaptionRepo extends CrudRepository<IndustryCaption, Long> {

    public IndustryCaption findById(long id);

    public List<IndustryCaption> findAllByLanguageId(OperationLanguage language);

    @Query("select ica from IndustryCaption ica where ica.languageId=:language and ica.industry in (select merin.industry from MerchantIndustry merin where merin.merchant=:merchant)")
    public List<IndustryCaption> findByQuery(@Param("language") OperationLanguage language, @Param("merchant") Merchant merchant);

    @Query("select ica from IndustryCaption ica where ica.languageId=:language and ica.industry in (select rri.industry from RegistrationRequestIndustry rri where rri.merchantRegistrationRequest=:registrationRequest)")
    public List<IndustryCaption>findByMRR(@Param("language") OperationLanguage language, @Param("registrationRequest")MerchantRegistrationRequest registrationRequest);


}
