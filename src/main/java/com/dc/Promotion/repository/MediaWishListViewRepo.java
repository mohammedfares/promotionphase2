package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;
import org.springframework.data.repository.CrudRepository;

public interface MediaWishListViewRepo extends CrudRepository<MediaWishListView, Long> {

    public MediaWishListView findById(Long id);

    public MediaWishListView save(MediaWishListView mediaWishListView);

    public void deleteAllByMediaAccount(MediaAccount mediaAccount);

    public PromoItem findByPromoItem(PromoItem promoItem);
}
