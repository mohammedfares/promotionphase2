package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.CountrySegment;
import com.dc.Promotion.entities.MerchantCustomerSegment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CountrySegmentRepo  extends CrudRepository<CountrySegment,Long> {

    public CountrySegment findById(Long id);

    public CountrySegment save(CountrySegment countrySegment);

    @Query("select cse.country from CountrySegment  cse where cse.merchantCustomerSegment=:segment")
    public List<Country> findByQuery(@Param("segment") MerchantCustomerSegment segment);
}
