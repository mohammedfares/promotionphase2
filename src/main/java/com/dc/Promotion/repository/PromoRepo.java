package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;

import java.util.List;


public interface PromoRepo extends CrudRepository<Promo, Long>, JpaSpecificationExecutor {
    public Promo findById(Long id);

    public Promo save(Promo promo);

    public List<Promo> findAllByMerchantAndStatus(Merchant merchant, Status status);

    @Query("select distinct pro from Promo  pro join pro.promoCaptionList capList " +
            "where (:name is null  OR capList.name like :name ) " +
            "and (:description is null OR capList.description like :description) " +
            "and (CAST(:createDateStart AS timestamp) is null OR pro.createDate between :createDateStart and :createDateEnd) " +
            "and (CAST(:startDateStart AS timestamp) is null OR pro.startDate between :startDateStart and :startDateEnd) " +
            "and (CAST(:expiryDateStart AS timestamp) is null OR pro.expiryDate between :expiryDateStart and :expiryDateEnd) " +
            "and (:merchant is null OR pro.merchant =:merchant) " +
            "and (:status is null OR pro.status =:status) " +
            "and pro.status.code <> 'PRO_DELETE'")
    public Page<Promo> findByQuery(@Param("name") String name,
                                   @Param("description") String description,
                                   @Param("createDateStart") LocalDateTime createDateStart,
                                   @Param("createDateEnd") LocalDateTime createDateEnd,
                                   @Param("startDateStart") LocalDateTime startDateStart,
                                   @Param("startDateEnd") LocalDateTime startDateEnd,
                                   @Param("expiryDateStart") LocalDateTime expiryDateStart,
                                   @Param("expiryDateEnd") LocalDateTime expiryDateEnd,
                                   @Param("merchant") Merchant merchant,
                                   @Param("status") Status status,
                                   Pageable pageable);


    @Query("select distinct pro from Promo pro join pro.promoCaptionList capList join pro.promoBranchList proBrn " +
            "where (:name is null OR capList.name like :name) " +
            "and (sqrt( abs( ((proBrn.branch.longitude - :longitude) * (proBrn.branch.longitude - :longitude)) - ((proBrn.branch.latitude - :latitude) * (proBrn.branch.latitude - :latitude))))) < :radius")
    public Page<Promo> findNearMe(@Param("name") String name,
                                  @Param("longitude") Double longitude,
                                  @Param("latitude") Double latitude,
                                  @Param("radius") Double radius,
                                  Pageable pageable);


    @Query("select distinct pro from Promo pro " +
            "join pro.promoCaptionList capList " +
            "join pro.promoBranchList proBrn " +
            "join pro.merchant mer " +
            "join mer.merchantIndustryList merInd " +
            "where ((:name is null) OR ( lower(capList.name) like :name )) " +
            "and (:industry is null OR merInd.industry =:industry) " +
            "and (:merchant is null OR pro.merchant =:merchant) " +
            "and (:status is null OR pro.status =:status) " +
            "and ((:trendFlag is null or :trendFlag = 0) OR pro.viewNo > '0') " +
            "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate)" +
            "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) " +
            "and ((:longitude is null or :longitude = 0.0 or :latitude is null or :latitude = 0.0 ) OR ( (6371 * acos( cos( radians(proBrn.branch.latitude) ) * cos( radians(:latitude) ) * cos( radians(proBrn.branch.longitude) - radians(:longitude) ) + sin( radians(proBrn.branch.latitude) ) * sin( radians(:latitude) ) )) <= :radius ))")
    public Page<Promo> filterCustomerPromoAll(
            @Param("name") String name,
            @Param("industry") Industry industry,
            @Param("merchant") Merchant merchant,
            @Param("status") Status status,
            @Param("trendFlag") Integer trendFlag,
            @Param("startDate") LocalDateTime startDate,
            @Param("expiryDate") LocalDateTime expiryDate,
            @Param("longitude") Double longitude,
            @Param("latitude") Double latitude,
            @Param("radius") Double radius,
            Pageable pageable);

    @Query("select count (pro) from Promo pro where pro.merchant=:merchant and pro.status.code = 'PRO_ACTIVE'")
    public Long totalPromo(@Param("merchant") Merchant merchant);

    @Query("select  pro from Promo pro " +
            "where pro.status.code <> 'PRO_DELETE' " +
            "and  pro.status.code <> 'PRO_TEMP' " +
            "and pro.merchant=:merchant " +
            "and (CAST(:currenttime AS timestamp) is null OR  pro.expiryDate >= :currenttime) order by pro.viewNo desc ")
    public List<Promo> findTop(Pageable pageable, @Param("merchant") Merchant merchant, @Param("currenttime") LocalDateTime dateTime);

    @Query("select sum(CAST(pro.viewNo As integer)) from Promo pro where pro.merchant=:merchant and " +
            " pro.status.code <> 'PRO_DELETE' and  pro.status.code <> 'PRO_TEMP' " +
            "and (CAST(:currenttime AS timestamp) is null OR  pro.expiryDate >= :currenttime)")
    public Long sumOfView(@Param("merchant") Merchant merchant, @Param("currenttime") LocalDateTime dateTime);


    @Query("select pro from Promo pro " +
            "join pro.promoBranchList proBrn " +
            "where pro.status.code<>'PRO_DELETE' " +
            "and pro.status.code <> 'PRO_TEMP'" +
            "and (CAST(:startDate AS timestamp) is null OR pro.startDate >= :startDate)" +
            "and (CAST(:endDate AS timestamp) is null OR  pro.startDate <= :endDate) " +
            "and (:branchId is null OR proBrn.branch.id=:branchId )" +
            "and pro.merchant=:merchant"
             )
    public Page<Promo> promoReport( @Param("startDate") LocalDateTime startDate,
                                    @Param("endDate") LocalDateTime endDat,
                                    @Param("branchId") Long branchId,
                                    @Param("merchant") Merchant merchant,
                                    Pageable pageable);


    public void deleteById(Long promoId);

    public List<Promo> findAllById(Long promoId);

}
