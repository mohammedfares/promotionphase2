package com.dc.Promotion.repository;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.PromoSource;
import com.dc.Promotion.entities.PromoSourceCaption;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PromoSourceCaptionRepo extends CrudRepository<PromoSourceCaption,Long> {

    public PromoSourceCaption findByPromoSourceAndLanguage(PromoSource promoSource, OperationLanguage language);
    public List<PromoSourceCaption> findAllByPromoSource(PromoSource promoSource);
}
