package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.MerchantRegistrationRequest;
import com.dc.Promotion.entities.RegistrationRequestIndustry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RegistrationRequestIndustryRepo extends CrudRepository<RegistrationRequestIndustry,Long> {

    public RegistrationRequestIndustry findById(long id);

    public RegistrationRequestIndustry save(RegistrationRequestIndustry registrationRequestIndustry);

    public void deleteAllByMerchantRegistrationRequest_Id(long id);

    @Query("select rri.industry from RegistrationRequestIndustry rri where rri.merchantRegistrationRequest=:regRequest")
    public List<Industry> findByQuery(@Param("regRequest")MerchantRegistrationRequest regRequest);
}
