package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MerchantRegistrationRequest;
import com.dc.Promotion.entities.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MerchantRegistrationRequestRepo extends CrudRepository<MerchantRegistrationRequest, Long> {
    public MerchantRegistrationRequest findById(long id);

    public MerchantRegistrationRequest save(MerchantRegistrationRequest registrationRequest);

    public List<MerchantRegistrationRequest> findAllByStatus(Status status);

    public void deleteById(long id);

    public MerchantRegistrationRequest findByEmail(String email);

    public MerchantRegistrationRequest findByUsername(String username);

    public List<MerchantRegistrationRequest> findAll();

    public MerchantRegistrationRequest findByCrn(String crn);
}
