package com.dc.Promotion.repository;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OperationLanguageRepo extends CrudRepository<OperationLanguage,Long> {
    public OperationLanguage findById(Long id);
    public OperationLanguage findByCode(String code);

    public List<OperationLanguage> findAllByStatus(Status status);
}
