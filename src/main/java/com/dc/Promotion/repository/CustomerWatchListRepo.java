package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerWatchListRepo extends CrudRepository<CustomerWatchList,Long> {

    public  CustomerWatchList findById(Long id);

    public CustomerWatchList save(CustomerWatchList customerWatchList);

    public CustomerWatchList findByPromoItemAndCustomerAccount(PromoItem promoItem, CustomerAccount account);

    public CustomerWatchList findByPromoItemAndCustomerAccountAndStatus(PromoItem promoItem, CustomerAccount account, Status status);

    @Query("select cusWish from CustomerWatchList cusWish where cusWish.promoItem.id=:itemId")
    List<CustomerWatchList> findCustomerWatchListByPromoItem(@Param("itemId") Long itemId);

    public void deleteCustomerWatchListByPromoItem(PromoItem promoItem);
}
