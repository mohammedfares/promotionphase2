package com.dc.Promotion.repository;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoCaption;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PromoCaptionRepo extends CrudRepository<PromoCaption,Long> {
    public  PromoCaption findById(long id);
    public PromoCaption save(PromoCaption promoCaption);
    public PromoCaption findPromoCaptionByPromoAndOperationLanguage(Promo promo , OperationLanguage operationLanguage);
    public List<PromoCaption> findPromoCaptionByPromo(Promo promo);

    public List<PromoCaption> findAllByPromo(Promo promo);

    public void deleteAllByPromo(Promo promo);
}
