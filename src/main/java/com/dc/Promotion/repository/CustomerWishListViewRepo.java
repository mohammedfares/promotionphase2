package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerWishListViewRepo extends CrudRepository<CustomerWishListView, Long> {

    public CustomerWishListView findById(Long id);

    public CustomerWishListView save(CustomerWishListView customerWishListView);

    public void deleteAllByCustomerAccount(CustomerAccount customerAccount);

    public PromoItem findByPromoItem(PromoItem promoItem);
}
