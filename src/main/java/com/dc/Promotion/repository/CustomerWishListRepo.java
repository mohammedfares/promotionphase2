package com.dc.Promotion.repository;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerWishList;
import com.dc.Promotion.entities.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CustomerWishListRepo extends CrudRepository <CustomerWishList ,Long>{

    public  CustomerWishList findById(long id);

    public CustomerWishList save(CustomerWishList wishList);

    public List<CustomerWishList> findAllByCustomerAccountAndStatus(CustomerAccount customerAccount, Status status);

    public CustomerWishList findByCustomerAccountAndIdAndStatus(CustomerAccount account,Long id,Status status);
}
