package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaType;
import com.dc.Promotion.entities.Status;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MediaAccountRepo extends CrudRepository<MediaAccount, Long> {

    public MediaAccount findById(Long id);

    public MediaAccount findByUserNameAndMediaType(String username, MediaType mediaType);

    @Query("select distinct mac from MediaAccount mac " +
            "where ((:gender is null or :gender='both') or (mac.gender =:gender)) " +
            "and ((:ageFrom is null and :ageTo is null) or (mac.age between :ageFrom and :ageTo))" +
            "and (mac.status =:status)" +
            "and ((:longitude is null or :longitude = 0.0 or :latitude is null or :latitude = 0.0 ) OR ( (6371 * acos( cos( radians(mac.latitude) ) * cos( radians(:latitude) ) * cos( radians(mac.longitude) - radians(:longitude) ) + sin( radians(mac.latitude) ) * sin( radians(:latitude) ) )) <= :radius ))")
    public List<MediaAccount> findByQuery(@Param("gender") String gender,
                                          @Param("ageFrom") Integer ageFrom,
                                          @Param("ageTo") Integer ageTo,
                                          @Param("status") Status status,
                                          @Param("longitude") Double longitude,
                                          @Param("latitude") Double latitude,
                                          @Param("radius") Double radius);
}
