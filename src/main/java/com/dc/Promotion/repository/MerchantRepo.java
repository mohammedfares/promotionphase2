package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantIndustry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MerchantRepo extends CrudRepository<Merchant,Long> {
    public Merchant findById(Long id);
    public Merchant save(Merchant merchant);
    public List<Merchant> findAll();
@Query("select mer from Merchant mer where mer in (select merchant from MerchantIndustry where industry=:industry )")
    public  List<Merchant> findByIndustry(@Param("industry") Industry industry);
}
