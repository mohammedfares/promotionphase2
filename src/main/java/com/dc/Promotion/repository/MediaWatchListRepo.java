package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaWatchList;
import com.dc.Promotion.entities.PromoItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MediaWatchListRepo extends CrudRepository<MediaWatchList,Long> {
    public MediaWatchList findById(long id);

    public  MediaWatchList findByPromoItemAndMediaAccount(PromoItem promoItem, MediaAccount mediaAccount);

    public void deleteMediaWatchListByPromoItem(PromoItem promoItem);

    @Query("select mediaWish from MediaWatchList mediaWish where mediaWish.promoItem.id=:itemId")
    List<MediaWatchList> findMediaWatchListByPromoItem(@Param("itemId") Long itemId);}
