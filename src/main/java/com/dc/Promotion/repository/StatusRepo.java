package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StatusRepo extends CrudRepository<Status, Long> {
    public Status findById(long id);

    public List<Status> findAllByCode(String code);

    public Status findByCode(String code);
}
