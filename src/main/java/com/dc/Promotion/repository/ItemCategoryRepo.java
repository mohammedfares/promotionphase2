package com.dc.Promotion.repository;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.Merchant;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface ItemCategoryRepo extends CrudRepository<ItemCategory, Long> {

    public ItemCategory findById(Long id);

    @Query("select new ItemCategory(cat.id, sum(proItem.viewNo)) from PromoItem proItem " +
            "join proItem.category cat " +
            "where (CAST(:currenttime AS timestamp) is null OR  proItem.expiryDate >= :currenttime)"+
            "and proItem.status.code <> 'PIT_DELETE'"+
            "and proItem.promo.merchant=:merchant " +
            "group by cat.id ")
    public List<ItemCategory> findTotalViewCategory(@Param("merchant") Merchant merchant, @Param("currenttime") LocalDateTime dateTime);


    public  List<ItemCategory> findAll();

}
