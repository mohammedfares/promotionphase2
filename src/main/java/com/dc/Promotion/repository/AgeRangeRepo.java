package com.dc.Promotion.repository;

import com.dc.Promotion.entities.AgeRange;
import org.springframework.data.repository.CrudRepository;

public interface AgeRangeRepo extends CrudRepository<AgeRange,Long> {

    public AgeRange findById(long id);
}
