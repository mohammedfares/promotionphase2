package com.dc.Promotion.repository;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerItemView;
import com.dc.Promotion.entities.PromoItem;
import org.springframework.data.repository.CrudRepository;

public interface CustomerItemViewRepo extends CrudRepository<CustomerItemView,Long> {
    public CustomerItemView findById(long id);

    public CustomerItemView save(CustomerItemView customerItemView);

    public CustomerItemView findByPromoItemAndCustomerAccount(PromoItem promoItem, CustomerAccount account);

    public void deleteAllByPromoItem(PromoItem promoItem);
}
