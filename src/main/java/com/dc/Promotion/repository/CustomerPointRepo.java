package com.dc.Promotion.repository;

import com.dc.Promotion.entities.CustomerPoint;
import org.springframework.data.repository.CrudRepository;

public interface CustomerPointRepo extends CrudRepository<CustomerPoint,Long> {

    public CustomerPoint findById(long id);
}
