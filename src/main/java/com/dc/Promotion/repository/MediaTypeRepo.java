package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MediaType;
import org.springframework.data.repository.CrudRepository;

public interface MediaTypeRepo extends CrudRepository<MediaType,Long> {
    public MediaType findById(Long id);
    public MediaType findMediaTypeByCode(String code);
}
