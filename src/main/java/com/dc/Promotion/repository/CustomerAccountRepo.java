package com.dc.Promotion.repository;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.Status;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerAccountRepo extends CrudRepository<CustomerAccount, Long> {
    public CustomerAccount findById(long id);

    public CustomerAccount findByMobile(String mobile);

    public CustomerAccount findByEmail(String email);

    public CustomerAccount save(CustomerAccount account);

    @Query("select distinct cac from CustomerAccount cac " +
            "where ((:gender is null or :gender='both') or (lower(cac.gender) = lower(:gender))) " +
            "and ((:ageFrom is null and :ageTo is null) or (cac.age between :ageFrom and :ageTo))" +
            "and ((:districtId is null) or (cac.district.id =:districtId))" +
            "and ((:cityId is null) or (cac.city.id  =:cityId))" +
            "and (cac.status =:status) " +
            "and ((:segmentId is null) or (cac.country in (select cs.country from CountrySegment cs where cs.merchantCustomerSegment.id=:segmentId)))" +
            "and ((:longitude is null or :longitude = 46.751517 or :latitude is null or :latitude = 24.707539) OR ( (6371 * acos( cos( radians(cac.latitude) ) * cos( radians(:latitude) ) * cos( radians(cac.longitude) - radians(:longitude) ) + sin( radians(cac.latitude) ) * sin( radians(:latitude) ) )) <= :radius ))")
    public List<CustomerAccount> findByQuery(@Param("gender") String gender,
                                             @Param("ageFrom") Integer ageFrom,
                                             @Param("ageTo") Integer ageTo,
                                             @Param("districtId") Long districtId,
                                             @Param("cityId") Long cityId,
                                             @Param("status") Status status,
                                             @Param("segmentId") Long segmentId,
                                             @Param("longitude") Double longitude,
                                             @Param("latitude") Double latitude,
                                             @Param("radius") Double radius);
}
