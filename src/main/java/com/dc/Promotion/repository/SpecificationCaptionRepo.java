package com.dc.Promotion.repository;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.SpecificationCaption;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SpecificationCaptionRepo extends CrudRepository<SpecificationCaption, Long> {

    public SpecificationCaption findById(long id);

    public List<SpecificationCaption> findAllByOrderBySpecificationId();

    public List<SpecificationCaption> findSpecificationCaptionBySpecification_Id(Long id);

    public SpecificationCaption findSpecificationCaptionBySpecification_IdAndLanguage(Long id , OperationLanguage operationLanguage);
}
