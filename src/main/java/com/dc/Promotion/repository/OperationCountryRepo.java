package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.OperationCountry;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OperationCountryRepo extends CrudRepository<OperationCountry, Long> {
    public OperationCountry findById(long id);

    public List<OperationCountry> findAll();

    public OperationCountry findByCountry_Code(String code);

    public OperationCountry findByCountry(Country country);

}
