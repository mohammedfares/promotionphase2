package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;;

import java.util.List;

public interface MerchantBranchRepo extends CrudRepository<MerchantBranch,Long > , JpaSpecificationExecutor {
    public MerchantBranch findById(Long id);

    public MerchantBranch save(MerchantBranch merchantBranch);

    @Query("select mbr from MerchantBranch mbr where mbr.id in (select branch from PromoBranch where promo=:promo )")
    public List<MerchantBranch> findByQuery(@Param("promo")Promo promo);

    @Query("select distinct merBrn FROM MerchantBranch merBrn JOIN merBrn.merchantBranchCaptionList capList " +
            "WHERE (:name is null OR lower(capList.name) like :name) " +
            "AND (:address is null OR lower(capList.address) like :address) " +
            "AND (:contactNumber is null OR merBrn.contactNumber like :contactNumber) " +
            "AND (:merchant is null OR merBrn.merchant =:merchant) " +
            "AND (:status is null OR merBrn.status =:status) " +
            "AND (merBrn.status.code <> 'BRA_DELETE')")
    public List<MerchantBranch> findByQuery(@Param("name") String name,
                                            @Param("address") String address,
                                            @Param("contactNumber") String contactNumber,
                                            @Param("merchant") Merchant merchant,
                                            @Param("status") Status status);
}
