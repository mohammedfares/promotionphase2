package com.dc.Promotion.repository;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.SystemMessage;
import com.dc.Promotion.entities.SystemMessageCaption;
import org.springframework.data.repository.CrudRepository;

public interface SystemMessageCaptionRepo extends CrudRepository<SystemMessageCaption,Long> {
    public SystemMessageCaption findById(long id);
    public SystemMessageCaption findByMessageAndLanguage(SystemMessage message, OperationLanguage language);
}
