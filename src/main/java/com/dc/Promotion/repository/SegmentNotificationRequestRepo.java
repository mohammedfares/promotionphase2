package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MerchantCustomerSegment;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.SegmentNotificationRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SegmentNotificationRequestRepo extends CrudRepository<SegmentNotificationRequest,Long> {

    public SegmentNotificationRequest findById(Long id);

    public SegmentNotificationRequest save(SegmentNotificationRequest request);

    @Query("select sum(snr.totalCustomer) from SegmentNotificationRequest snr " +
            "where snr.customerSegment=:customerSegment " +
            "group by snr.customerSegment")
    public Long findTotalCustomer(@Param("customerSegment") MerchantCustomerSegment customerSegment);

    @Query("select sum(snr.totalNotification) from SegmentNotificationRequest snr " +
            "where snr.customerSegment=:customerSegment " +
            "group by snr.customerSegment")
    public Long findTotalNotification(@Param("customerSegment") MerchantCustomerSegment customerSegment);

    @Query("select sum(snr.totalView) from SegmentNotificationRequest snr " +
            "where snr.customerSegment=:customerSegment " +
            "group by snr.customerSegment")
    public Long findTotalViews(@Param("customerSegment") MerchantCustomerSegment customerSegment);

    public List<SegmentNotificationRequest> findAllByPromo(Promo promo);

    public void deleteAllByPromo(Promo promo);
}
