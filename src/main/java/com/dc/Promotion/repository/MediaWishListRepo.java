package com.dc.Promotion.repository;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaWishList;
import com.dc.Promotion.entities.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MediaWishListRepo extends CrudRepository<MediaWishList,Long> {
    public MediaWishList findById(long id);

    public MediaWishList save(MediaWishList wishList);

    public List<MediaWishList> findAllByMediaAccountAndStatus(MediaAccount mediaAccount, Status status);

    public MediaWishList findByMediaAccountAndStatusAndId(MediaAccount account,Status status,Long id);
}
