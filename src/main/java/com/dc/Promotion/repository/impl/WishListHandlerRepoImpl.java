package com.dc.Promotion.repository.impl;


import com.dc.Promotion.entities.*;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.repository.WishListHandlerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class WishListHandlerRepoImpl implements WishListHandlerRepo {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<PromoItem> findPromoItemByCustomerWish(CustomerWishList customerWishList) {

        String hibernateQuery = "select distinct proItm from PromoItem proItm " +
                "join proItm.promo pro " +
                "join proItm.promoItemCaptionList capList " +
                "join pro.promoBranchList proBrn " +
                "join pro.merchant mer " +
                "join mer.merchantIndustryList merInd " +
                "join proItm.itemSpecificationList specs " +
                "where (:merchant is null OR pro.merchant =:merchant) " +
                "and (:itemCategory is null OR proItm.category =:itemCategory) " +
                "and (:status is null OR proItm.status.code =:status) " +
                "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate)" +
                "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) ";

        String wishListTags = customerWishList.getTags();

        if (wishListTags != null && !wishListTags.trim().isEmpty()) {

            List<String> arrayTagsSplitter = new ArrayList<>();

            String[] arrayTagsSplitterLine = wishListTags.split("\n");

            for (String line : arrayTagsSplitterLine) {

                String[] arrayTagsSplitterComma = line.split(",");

                for (String value : arrayTagsSplitterComma) {
                    arrayTagsSplitter.add(value);
                }
            }

            String specsQuery = "";

            for (int i = 0; i < arrayTagsSplitter.size(); i++) {

                String tag = arrayTagsSplitter.get(i);

                tag = "%" + tag + "%".toLowerCase();

                if (i == arrayTagsSplitter.size() - 1) {
                    String specsCondition = "(specs.value like '" + tag + "')";
                    specsQuery = specsQuery.concat(specsCondition);
                } else {
                    String specsCondition = "(specs.value like '" + tag + "') or ";
                    specsQuery = specsQuery.concat(specsCondition);
                }
            }
            hibernateQuery = hibernateQuery.concat("and (" + specsQuery + ")");
        }

        System.out.println("hibernateQuery: " + hibernateQuery);

        Query queryExcution = entityManager.createQuery(hibernateQuery);
        queryExcution.setParameter("merchant", customerWishList.getMerchant());
        queryExcution.setParameter("itemCategory", customerWishList.getCategory());
        queryExcution.setParameter("status", "PIT_ACTIVE");
        queryExcution.setParameter("startDate", LocalDateTime.now());
        queryExcution.setParameter("expiryDate", LocalDateTime.now());

        return queryExcution.getResultList();
    }

    @Override
    public List<PromoItem> findPromoItemByMediaWish(MediaWishList mediaWishList) {
        String hibernateQuery = "select distinct proItm from PromoItem proItm " +
                "join proItm.promo pro " +
                "join proItm.promoItemCaptionList capList " +
                "join pro.promoBranchList proBrn " +
                "join pro.merchant mer " +
                "join mer.merchantIndustryList merInd " +
                "join proItm.itemSpecificationList specs " +
                "where (:merchant is null OR pro.merchant =:merchant) " +
                "and (:itemCategory is null OR proItm.category =:itemCategory) " +
                "and (:status is null OR proItm.status.code =:status) " +
                "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate)" +
                "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) ";

        String wishListTags = mediaWishList.getTags();

        if (wishListTags != null && !wishListTags.trim().isEmpty()) {

            List<String> arrayTagsSplitter = new ArrayList<>();

            String[] arrayTagsSplitterLine = wishListTags.split("\n");

            for (String line : arrayTagsSplitterLine) {

                String[] arrayTagsSplitterComma = line.split(",");

                for (String value : arrayTagsSplitterComma) {
                    arrayTagsSplitter.add(value);
                }
            }

            String specsQuery = "";

            for (int i = 0; i < arrayTagsSplitter.size(); i++) {

                String tag = arrayTagsSplitter.get(i);

                tag = "%" + tag + "%".toLowerCase();

                if (i == arrayTagsSplitter.size() - 1) {
                    String specsCondition = "(specs.value like '" + tag + "')";
                    specsQuery = specsQuery.concat(specsCondition);
                } else {
                    String specsCondition = "(specs.value like '" + tag + "') or ";
                    specsQuery = specsQuery.concat(specsCondition);
                }
            }
            hibernateQuery = hibernateQuery.concat("and (" + specsQuery + ")");
        }

        System.out.println("hibernateQuery: " + hibernateQuery);

        Query queryExcution = entityManager.createQuery(hibernateQuery);
        queryExcution.setParameter("merchant", mediaWishList.getMerchant());
        queryExcution.setParameter("itemCategory", mediaWishList.getCategory());
        queryExcution.setParameter("status", "PIT_ACTIVE");
        queryExcution.setParameter("startDate", LocalDateTime.now());
        queryExcution.setParameter("expiryDate", LocalDateTime.now());

        return queryExcution.getResultList();
    }
}
