package com.dc.Promotion.repository.impl;

import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.repository.CompareHandlerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CompareHandlerRepoImpl implements CompareHandlerRepo {

    @Autowired
    private EntityManager entityManager;

    @Override
    public List<PromoItem> compare(PromoItem promoItem, String type, Double longitude, Double latitude) {

        String hibernateQuery = "select distinct proItm as item, CAST(proItm.newPrice as double) as price , (6371 * acos( cos( radians(branch.latitude) ) * cos( radians(:latitude) ) * cos( radians(branch.longitude) - radians(:longitude) ) + sin( radians(branch.latitude) ) * sin( radians(:latitude)))) as distance from PromoItem proItm " +
                "inner join proItm.promo pro " +
                "inner join proItm.itemSpecificationList specs " + 
                "inner join pro.promoBranchList proBrn " +
                "inner join proBrn.branch branch " +
                "where (:itemCategory is null OR proItm.category =:itemCategory) " +
                "and (:status is null OR proItm.status.code =:status) " +
                "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate) " +
                "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) " +
                "and (proItm.id <>:id)";

        if (promoItem.getItemSpecificationList() != null && !promoItem.getItemSpecificationList().isEmpty()) {

            String specsQuery = "";

            for (int i = 0; i < promoItem.getItemSpecificationList().size(); i++) {

                String tag = promoItem.getItemSpecificationList().get(i).getValue();

                tag = "%" + tag + "%".toLowerCase();

                if (i == promoItem.getItemSpecificationList().size() - 1) {
                    String specsCondition = "(lower(specs.value) like '" + tag + "')";
                    specsQuery = specsQuery.concat(specsCondition);
                } else {
                    String specsCondition = "(lower(specs.value) like '" + tag + "') or ";
                    specsQuery = specsQuery.concat(specsCondition);
                }
            }
            hibernateQuery = hibernateQuery.concat("and (" + specsQuery + ")");
        }

        switch (type) {
            case "0": {
                break;
            }
            case "1": {
                hibernateQuery = hibernateQuery.concat(" order By price ASC");
                break;
            }
            case "2": {
                hibernateQuery = hibernateQuery.concat(" order by distance ASC");
                break;
            }
        }

        Query queryExcution = entityManager.createQuery(hibernateQuery);

        queryExcution.setParameter("itemCategory", promoItem.getCategory());
        queryExcution.setParameter("status", "PIT_ACTIVE");
        queryExcution.setParameter("startDate", LocalDateTime.now());
        queryExcution.setParameter("expiryDate", LocalDateTime.now());
        queryExcution.setParameter("longitude", longitude);
        queryExcution.setParameter("latitude", latitude);
        queryExcution.setParameter("id", promoItem.getId());

        List<Object[]> objectList = queryExcution.getResultList();

        List<PromoItem> promoItemList = new ArrayList<>();

//        List<PromoItem> promoItemList = queryExcution.getResultList();

        for (Object[] obj : objectList) {
            Double distance =  (Double) obj[2];
            PromoItem item = new PromoItem(((PromoItem) obj[0]),distance);
            promoItemList.add(item);
        }

        return promoItemList;
    }
}
