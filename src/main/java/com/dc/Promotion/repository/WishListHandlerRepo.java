package com.dc.Promotion.repository;


import com.dc.Promotion.entities.*;

import java.util.List;

public interface WishListHandlerRepo {
    public List<PromoItem> findPromoItemByCustomerWish(CustomerWishList customerWishList);
    public List<PromoItem> findPromoItemByMediaWish(MediaWishList mediaWishList);

}
