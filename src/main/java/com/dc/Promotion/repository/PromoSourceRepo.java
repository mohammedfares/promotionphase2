package com.dc.Promotion.repository;

import com.dc.Promotion.entities.PromoSource;
import org.springframework.data.repository.CrudRepository;

public interface PromoSourceRepo extends CrudRepository<PromoSource,Long> {

    public PromoSource findById(Long id);
    public PromoSource findByCode(String code);
}
