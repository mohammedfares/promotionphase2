package com.dc.Promotion.repository;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantUser;
import com.dc.Promotion.entities.MerchantUserType;
import com.dc.Promotion.entities.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MerchantUserRepo extends CrudRepository<MerchantUser, Long>, JpaSpecificationExecutor {
    public MerchantUser findMerchantUserById(Long id);

    public MerchantUser save(MerchantUser merchantUser);

    public MerchantUser findByEmail(String email);

    public MerchantUser findByMobile(String mobile);

    public MerchantUser findByIdAndMerchantUserType_Code(long id, String userType);

    public List<MerchantUser> findAllByMerchant_IdAndStatus_Code(long id, String code);

    public MerchantUser findByUserName(String username);

////    @Query("")
//    public List<MerchantUser> findAll();

    @Query("select merUsr from MerchantUser merUsr where merUsr.userName=:usernameOrEmail or merUsr.email=:usernameOrEmail")
    public MerchantUser findMerchantByUsernameOrEmail(@Param("usernameOrEmail") String usernameOrEmail);


}
