package com.dc.Promotion.repository;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.ItemCategoryCaption;
import com.dc.Promotion.entities.OperationLanguage;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ItemCategoryCaptionRepo extends CrudRepository<ItemCategoryCaption,Long> {

    public ItemCategoryCaption findById(long id);

    public List<ItemCategoryCaption> findAll();

    public List<ItemCategoryCaption> findAllByLanguageId(OperationLanguage language);

    public ItemCategoryCaption findByCategory_IdAndLanguageId(Long id, OperationLanguage languageId);

    public ItemCategoryCaption findByCategoryAndLanguageId(ItemCategory category,OperationLanguage language);
}
