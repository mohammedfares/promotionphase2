package com.dc.Promotion.repository;

import com.dc.Promotion.entities.PromoItem;

import java.util.List;

public interface CompareHandlerRepo {
    public List<PromoItem> compare(PromoItem promoItem,String type,Double longitude,Double latitude);
}
