//package com.dc.Promotion.repository;
//
//import com.dc.Promotion.entities.NationalityCaption;
//import com.dc.Promotion.entities.OperationLanguage;
//import org.springframework.data.repository.CrudRepository;
//
//import java.util.List;
//
//public interface NationalityCaptionRepo extends CrudRepository<NationalityCaption,Long> {
//    public NationalityCaption findById(long id);
//    public List<NationalityCaption> findAllByOperationLanguage_Id(long id);
//}
