package com.dc.Promotion.repository;

import com.dc.Promotion.entities.NotificationTemplate;
import org.springframework.data.repository.CrudRepository;

public interface NotificationTemplateRepo extends CrudRepository<NotificationTemplate,Long> {

    public  NotificationTemplate findByCode(String code);
}
