package com.dc.Promotion.repository;


import com.dc.Promotion.entities.Industry;
import org.springframework.data.repository.CrudRepository;

public interface IndustryRepo extends CrudRepository<Industry,Long> {

    public  Industry findById(Long id);
}
