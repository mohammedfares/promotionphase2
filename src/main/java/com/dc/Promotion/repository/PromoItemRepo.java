package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface PromoItemRepo extends CrudRepository<PromoItem, Long>, JpaSpecificationExecutor {
    public PromoItem findById(long id);

    public PromoItem save(PromoItem promoItem);

//    @Query("select pit from PromoItem pit where pit.promo =: promo  and pit.id in () ")
    public List<PromoItem> findAllByPromo(Promo promo);

    @Query("select pit from PromoItem pit where pit in (select promoItem.id from CustomerWatchList where customerAccount=:account and status =:status)")
    public List<PromoItem> findByQuery(@Param("account") CustomerAccount account, @Param("status") Status status);


    @Query("select pit from PromoItem pit where pit in (select promoItem.id from MediaWatchList where mediaAccount=:account and status =:status)")
    public List<PromoItem> findByQueryMedia(@Param("account") MediaAccount account, @Param("status") Status status);

    @Query("select distinct proItm from PromoItem proItm " +
            "join proItm.customerWatchList watchList " +
            "join proItm.promo pro " +
            "join proItm.promoItemCaptionList capList " +
            "join pro.promoBranchList proBrn " +
            "join pro.merchant mer " +
            "join mer.merchantIndustryList merInd " +
            "where ((:name is null) OR ( lower(capList.name) like :name )) " +
            "and (:industry is null OR merInd.industry =:industry) " +
            "and (:merchant is null OR pro.merchant =:merchant) " +
            "and (:itemCategory is null OR proItm.category =:itemCategory) " +
            "and (:status is null OR proItm.status =:status) " +
            "and ( (:customerAccount is null) OR (watchList.customerAccount =:customerAccount and watchList.status.code='CWA_ACTIVE')) " +
            "and ((:newPriceFrom is null OR :newPriceFrom = 0.0) OR (CAST(proItm.newPrice as double) >= CAST(:newPriceFrom as double))) " +
            "and ((:newPriceTo is null OR :newPriceTo = 0.0) OR (CAST(proItm.newPrice as double) <= CAST(:newPriceTo as double))) " +
            "and ((:trendFlag is null or :trendFlag = 0) OR proItm.viewNo > '0') " +
            "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate)" +
            "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) " +
            "and ((:longitude is null or :longitude = 0.0 or :latitude is null or :latitude = 0.0 ) OR ( (6371 * acos( cos( radians(proBrn.branch.latitude) ) * cos( radians(:latitude) ) * cos( radians(proBrn.branch.longitude) - radians(:longitude) ) + sin( radians(proBrn.branch.latitude) ) * sin( radians(:latitude) ) )) <= :radius ))")
    public Page<PromoItem> filterCustomerPromoItemWatchList(
            @Param("name") String name,
            @Param("industry") Industry industry,
            @Param("merchant") Merchant merchant,
            @Param("itemCategory") ItemCategory itemCategory,
            @Param("status") Status status,
            @Param("customerAccount") CustomerAccount customerAccount,
            @Param("newPriceFrom") Double newPriceFrom,
            @Param("newPriceTo") Double newPriceTo,
            @Param("trendFlag") Integer trendFlag,
            @Param("startDate") LocalDateTime startDate,
            @Param("expiryDate") LocalDateTime expiryDate,
            @Param("longitude") Double longitude,
            @Param("latitude") Double latitude,
            @Param("radius") Double radius,
            Pageable pageable);

    @Query("select distinct proItm from PromoItem proItm " +
            "join proItm.mediaWatchLists watchList " +
            "join proItm.promo pro " +
            "join proItm.promoItemCaptionList capList " +
            "join pro.promoBranchList proBrn " +
            "join pro.merchant mer " +
            "join mer.merchantIndustryList merInd " +
            "where ((:name is null) OR ( lower(capList.name) like :name )) " +
            "and (:industry is null OR merInd.industry =:industry) " +
            "and (:merchant is null OR pro.merchant =:merchant) " +
            "and (:itemCategory is null OR proItm.category =:itemCategory) " +
            "and (:status is null OR proItm.status =:status) " +
            "and ( (:mediaAccount is null) OR (watchList.mediaAccount =:mediaAccount and watchList.status.code='MWA_ACTIVE')) " +
            "and ((:newPriceFrom is null OR :newPriceFrom = 0.0) OR (CAST(proItm.newPrice as double) >= CAST(:newPriceFrom as double))) " +
            "and ((:newPriceTo is null OR :newPriceTo = 0.0) OR (CAST(proItm.newPrice as double) <= CAST(:newPriceTo as double))) " +
            "and ((:trendFlag is null or :trendFlag = 0) OR proItm.viewNo > '0') " +
            "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate)" +
            "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) " +
            "and ((:longitude is null or :longitude = 0.0 or :latitude is null or :latitude = 0.0 ) OR ( (6371 * acos( cos( radians(proBrn.branch.latitude) ) * cos( radians(:latitude) ) * cos( radians(proBrn.branch.longitude) - radians(:longitude) ) + sin( radians(proBrn.branch.latitude) ) * sin( radians(:latitude) ) )) <= :radius ))")
    public Page<PromoItem> filterMediaPromoItemWatchList(
            @Param("name") String name,
            @Param("industry") Industry industry,
            @Param("merchant") Merchant merchant,
            @Param("itemCategory") ItemCategory itemCategory,
            @Param("status") Status status,
            @Param("mediaAccount") MediaAccount mediaAccount,
            @Param("newPriceFrom") Double newPriceFrom,
            @Param("newPriceTo") Double newPriceTo,
            @Param("trendFlag") Integer trendFlag,
            @Param("startDate") LocalDateTime startDate,
            @Param("expiryDate") LocalDateTime expiryDate,
            @Param("longitude") Double longitude,
            @Param("latitude") Double latitude,
            @Param("radius") Double radius,
            Pageable pageable);

    @Query("select distinct proItm from PromoItem proItm " +
            "join proItm.promo pro " +
            "join proItm.promoItemCaptionList capList " +
            "join pro.promoBranchList proBrn " +
            "join pro.merchant mer " +
            "join mer.merchantIndustryList merInd " +
            "where ((:name is null) OR ( lower(capList.name) like :name )) " +
            "and (:industry is null OR merInd.industry =:industry) " +
            "and (:merchant is null OR pro.merchant =:merchant) " +
            "and (:itemCategory is null OR proItm.category =:itemCategory) " +
            "and (:status is null OR proItm.status =:status) " +
            "and ((:newPriceFrom is null OR :newPriceFrom = 0.0) OR (CAST(proItm.newPrice as double) >= CAST(:newPriceFrom as double))) " +
            "and ((:newPriceTo is null OR :newPriceTo = 0.0) OR (CAST(proItm.newPrice as double) <= CAST(:newPriceTo as double))) " +
            "and ((:trendFlag is null or :trendFlag = 0) OR proItm.viewNo > '0') " +
            "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate)" +
            "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) " +
            "and ((:longitude is null or :longitude = 0.0 or :latitude is null or :latitude = 0.0 ) OR ( (6371 * acos( cos( radians(proBrn.branch.latitude) ) * cos( radians(:latitude) ) * cos( radians(proBrn.branch.longitude) - radians(:longitude) ) + sin( radians(proBrn.branch.latitude) ) * sin( radians(:latitude) ) )) <= :radius )) " +
            "order by proItm.viewNo DESC, proItm.id DESC")
    public Page<PromoItem> filterCustomerPromoItemAll(
            @Param("name") String name,
            @Param("industry") Industry industry,
            @Param("merchant") Merchant merchant,
            @Param("itemCategory") ItemCategory itemCategory,
            @Param("status") Status status,
            @Param("newPriceFrom") Double newPriceFrom,
            @Param("newPriceTo") Double newPriceTo,
            @Param("trendFlag") Integer trendFlag,
            @Param("startDate") LocalDateTime startDate,
            @Param("expiryDate") LocalDateTime expiryDate,
            @Param("longitude") Double longitude,
            @Param("latitude") Double latitude,
            @Param("radius") Double radius,
            Pageable pageable);

    @Query("select count(pit) from PromoItem  pit where pit.promo.merchant =:merchant and pit.status.code = 'PIT_ACTIVE'")
    public Long totalPromoItem(@Param("merchant") Merchant merchant);


    @Query("select  pit from PromoItem pit where pit.status.code <> 'PIT_DELETE' and pit.status.code <> 'PIT_TEMP' " +
            "and pit.promo.merchant=:merchant " +
            "and (CAST(:currenttime AS timestamp) is null OR  pit.expiryDate >= :currenttime) order by pit.viewNo desc ")
    public List<PromoItem> findTop(Pageable pageable, @Param("merchant") Merchant merchant, @Param("currenttime") LocalDateTime dateTime);

    public void deleteAllByPromo(Promo promo);

    @Query(" select distinct pit from PromoItem  pit " +
            " join pit.itemSpecificationList specs" +
            " join pit.promo pro  " +
            " join pro.promoBranchList proBrn" +
            " where ((pit.category=:category) and " +
            "(specs.value in (select itemSpecs.value from ItemSpecification itemSpecs where itemSpecs.promoItem=:proItem ))" +
            " )" +
            " and ((:newPriceFrom is null OR :newPriceFrom = 0.0) OR (CAST(pit.newPrice as double) >= CAST(:newPriceFrom as double)))" +
            " and ((:longitude is null or :longitude = 0.0 or :latitude is null or :latitude = 0.0 )" +
            " OR ( (6371 * acos( cos( radians(proBrn.branch.latitude) ) * cos( radians(:latitude) ) * cos( radians(proBrn.branch.longitude) - radians(:longitude) ) + sin( radians(proBrn.branch.latitude) ) * sin( radians(:latitude) ) )) <= :radius ))" +
            " and  pit.status=:status " +
            " and pit.expiryDate >= :currenttime " +
            " and pit.id <> :itemId")
    public Page<PromoItem> findByCompareItem(
                                             @Param("currenttime") LocalDateTime time,
                                             @Param("status") Status status,
                                             @Param("itemId") Long itemId,
                                             @Param("category") ItemCategory category,
                                             @Param("proItem") PromoItem proItem,
                                             @Param("newPriceFrom") Double newPriceFrom,
                                             @Param("longitude") Double longitude,
                                             @Param("latitude") Double latitude,
                                             @Param("radius") Double radius,
                                             Pageable pageable);


    //------------------------ wish list repo -------------------------------------------------
    @Query("select distinct proItm from PromoItem proItm " +
            "join proItm.customerWishListViews wishList " +
            "join proItm.promo pro " +
            "join proItm.promoItemCaptionList capList " +
            "join pro.promoBranchList proBrn " +
            "join pro.merchant mer " +
            "join mer.merchantIndustryList merInd " +
            "where ((:name is null) OR ( lower(capList.name) like :name )) " +
            "and (:industry is null OR merInd.industry =:industry) " +
            "and (:merchant is null OR pro.merchant =:merchant) " +
            "and (:itemCategory is null OR proItm.category =:itemCategory) " +
            "and (:status is null OR proItm.status =:status) " +
            "and ((:customerAccount is null) OR (wishList.customerAccount =:customerAccount)) " +
            "and ((:newPriceFrom is null OR :newPriceFrom = 0.0) OR (CAST(proItm.newPrice as double) >= CAST(:newPriceFrom as double))) " +
            "and ((:newPriceTo is null OR :newPriceTo = 0.0) OR (CAST(proItm.newPrice as double) <= CAST(:newPriceTo as double))) " +
            "and ((:trendFlag is null or :trendFlag = 0) OR proItm.viewNo > '0') " +
            "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate)" +
            "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) " +
            "and ((:longitude is null or :longitude = 0.0 or :latitude is null or :latitude = 0.0 ) OR ( (6371 * acos( cos( radians(proBrn.branch.latitude) ) * cos( radians(:latitude) ) * cos( radians(proBrn.branch.longitude) - radians(:longitude) ) + sin( radians(proBrn.branch.latitude) ) * sin( radians(:latitude) ) )) <= :radius ))")
    public Page<PromoItem> filterCustomerPromoItemWishList(
            @Param("name") String name,
            @Param("industry") Industry industry,
            @Param("merchant") Merchant merchant,
            @Param("itemCategory") ItemCategory itemCategory,
            @Param("status") Status status,
            @Param("customerAccount") CustomerAccount customerAccount,
            @Param("newPriceFrom") Double newPriceFrom,
            @Param("newPriceTo") Double newPriceTo,
            @Param("trendFlag") Integer trendFlag,
            @Param("startDate") LocalDateTime startDate,
            @Param("expiryDate") LocalDateTime expiryDate,
            @Param("longitude") Double longitude,
            @Param("latitude") Double latitude,
            @Param("radius") Double radius,
            Pageable pageable);

    @Query("select distinct proItm from PromoItem proItm " +
            "join proItm.mediaWishListViews wishList " +
            "join proItm.promo pro " +
            "join proItm.promoItemCaptionList capList " +
            "join pro.promoBranchList proBrn " +
            "join pro.merchant mer " +
            "join mer.merchantIndustryList merInd " +
            "where ((:name is null) OR ( lower(capList.name) like :name )) " +
            "and (:industry is null OR merInd.industry =:industry) " +
            "and (:merchant is null OR pro.merchant =:merchant) " +
            "and (:itemCategory is null OR proItm.category =:itemCategory) " +
            "and (:status is null OR proItm.status =:status) " +
            "and ( (:mediaAccount is null) OR (wishList.mediaAccount =:mediaAccount)) " +
            "and ((:newPriceFrom is null OR :newPriceFrom = 0.0) OR (CAST(proItm.newPrice as double) >= CAST(:newPriceFrom as double))) " +
            "and ((:newPriceTo is null OR :newPriceTo = 0.0) OR (CAST(proItm.newPrice as double) <= CAST(:newPriceTo as double))) " +
            "and ((:trendFlag is null or :trendFlag = 0) OR proItm.viewNo > '0') " +
            "and (CAST(:startDate AS timestamp) is null OR pro.startDate <= :startDate)" +
            "and (CAST(:expiryDate AS timestamp) is null OR pro.expiryDate >= :expiryDate) " +
            "and ((:longitude is null or :longitude = 0.0 or :latitude is null or :latitude = 0.0 ) OR ( (6371 * acos( cos( radians(proBrn.branch.latitude) ) * cos( radians(:latitude) ) * cos( radians(proBrn.branch.longitude) - radians(:longitude) ) + sin( radians(proBrn.branch.latitude) ) * sin( radians(:latitude) ) )) <= :radius ))")
    public Page<PromoItem> filterMediaPromoItemWishList(
            @Param("name") String name,
            @Param("industry") Industry industry,
            @Param("merchant") Merchant merchant,
            @Param("itemCategory") ItemCategory itemCategory,
            @Param("status") Status status,
            @Param("mediaAccount") MediaAccount mediaAccount,
            @Param("newPriceFrom") Double newPriceFrom,
            @Param("newPriceTo") Double newPriceTo,
            @Param("trendFlag") Integer trendFlag,
            @Param("startDate") LocalDateTime startDate,
            @Param("expiryDate") LocalDateTime expiryDate,
            @Param("longitude") Double longitude,
            @Param("latitude") Double latitude,
            @Param("radius") Double radius,
            Pageable pageable);
    // ------------------------ end wish list repo ---------------------------------------------

    @Query("select pit from PromoItem pit " +
            "join pit.promo pro " +
            "where " +
            "pit.status.code<>'PIT_DELETE' " +
            "and (CAST(:startDate AS timestamp) is null OR pro.startDate >= :startDate)" +
            "and (CAST(:endDate AS timestamp) is null OR  pro.startDate <= :endDate) " +
            "and (:promo is null OR  pit.promo=:promo)" +
            "and pro.merchant=:merchant")
    public Page<PromoItem> findItemReport(@Param("startDate") LocalDateTime startDate,
                                          @Param("endDate") LocalDateTime endDat,
                                          @Param("promo") Promo promo,
                                          @Param("merchant") Merchant merchant,
                                          Pageable pageable);
}

