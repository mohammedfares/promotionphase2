package com.dc.Promotion.repository;

import com.dc.Promotion.entities.*;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DistrictCaptionRepo extends CrudRepository<DistrictCaption,Long> {

    public  DistrictCaption findById(long id);

    public DistrictCaption findByDistrictAndLanguage(District district, OperationLanguage language);

    public List<DistrictCaption> findAllByLanguageAndDistrict_City(OperationLanguage language, City city);

}
