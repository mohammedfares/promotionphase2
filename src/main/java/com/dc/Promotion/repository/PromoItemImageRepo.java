package com.dc.Promotion.repository;

import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.PromoItemImage;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PromoItemImageRepo extends CrudRepository<PromoItemImage, Long> {
    public PromoItemImage findById(Long id);

    public PromoItemImage save(PromoItemImage promoItemImage);

    public List<PromoItemImage> findByPromoItemId(PromoItem promoItem);

    public void deleteAllByPromoItemId(PromoItem promoItem);
}
