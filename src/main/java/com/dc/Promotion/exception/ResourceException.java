/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dc.Promotion.exception;


import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.SystemMessage;
import com.dc.Promotion.entities.SystemMessageCaption;
import com.dc.Promotion.service.OperationLanguageService;
import com.dc.Promotion.service.SystemMessageCaptionService;
import com.dc.Promotion.service.SystemMessageService;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;


public class ResourceException extends RuntimeException {

    private ApplicationContext applicationContext;
    private HttpStatus httpStatus;
    private SystemMessageService messageService;
    private com.dc.Promotion.service.OperationLanguageService OperationLanguageService;
    private SystemMessageCaptionService systemMessageCaptionService;

    public static int getLineNumber() {
        return Thread.currentThread().getStackTrace()[2].getLineNumber();
    }

    public
    ResourceException(ApplicationContext applicationContext, HttpStatus httpStatus, String msgCode, String langCode, String text)
    {
        String caption = "No Caption";

        this.applicationContext = applicationContext;
        this.OperationLanguageService = this.getBean(OperationLanguageService.class);
        this.messageService = this.getBean(SystemMessageService.class);
        this.systemMessageCaptionService = this.getBean(SystemMessageCaptionService.class);

        if (msgCode == null)
            throw new ResourceException(HttpStatus.NOT_FOUND, "Message Code Not Found");
        if (langCode == null)
            langCode = "en";

        OperationLanguage operationLanguage = OperationLanguageService.findByCode(langCode);
        SystemMessage message = messageService.findByCode(msgCode);
        if (message == null)
            throw new ResourceException(HttpStatus.NOT_FOUND, "System Message Not Found for code: " + msgCode);

        SystemMessageCaption systemMessageCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);

        if(systemMessageCaption == null)
            caption = message.getCode();
        else
            caption = systemMessageCaption.getText();

        if (text != null)
            throw new ResourceException(httpStatus, caption + " " + text);
        else
            throw new ResourceException(httpStatus, caption);
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ResourceException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public <T> T getBean(Class<T> beanClass) {
        return this.applicationContext.getBean(beanClass);
    }
}
