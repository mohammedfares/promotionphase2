package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.*;
import com.dc.Promotion.security.AccountUserDetails;
import com.dc.Promotion.security.SECCriptoRsa;
//import com.dc.Promotion.security.SessionHelper;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/merchant")
public class MerchantController {

    @Autowired
    private OperationLanguageService operationLanguageService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MerchantUserService merchantUserService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private MerchantUserTypeService merchantUserTypeService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private MerchantUserTypeCaptionService merchantUserTypeCaptionService;

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private MerchantRegistrationRequestService registrationRequestService;

    @Autowired
    private StatusCaptionService statusCaptionService;

    @Autowired
    private MerchantBranchService merchantBranchService;

    @Autowired
    private MerchantBranchCaptionService merchantBranchCaptionService;

    @Autowired
    private MerchantCaptionService merchantCaptionService;

    @Autowired
    private PromoService promoService;

    @Autowired
    private SegmentNotificationRequestService segmentNotificationRequestService;

    @Autowired
    private MerchantCustomerSegmentService merchantCustomerSegmentService;

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> createMerchantRequest(@RequestBody MerchantRegistrationRequestResources requestResources, HttpServletRequest request) {

        // get Request Language
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        // validate Request Data
        if (requestResources.getCrn() == null
                || requestResources.getEmail() == null
                || requestResources.getMobile() == null
                || requestResources.getPassword() == null
                || requestResources.getUsername() == null
                || requestResources.getConfirmPassword() == null
                || requestResources.getMerchantName() == null
                || requestResources.getIndustryList() == null
                || requestResources.getIndustryList().isEmpty()
                || requestResources.getCountryCode() == null) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }

        //check if CRN exist
        if (registrationRequestService.findByCRN(requestResources.getCrn()) != null)
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "crn302", operationLanguage.getCode(), null);

        //check validate Email
        if (!Utils.isEmailCorrect(requestResources.getEmail()))
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email406", operationLanguage.getCode(), null);

        //check if email reguest exist
        if (registrationRequestService.findByEmail(requestResources.getEmail()) != null)
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "email302", operationLanguage.getCode(), null);
        requestResources.setEmail(requestResources.getEmail().toLowerCase());

        //check if username request exist
        if (registrationRequestService.findByUsername(requestResources.getUsername()) != null)
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "username302", operationLanguage.getCode(), null);
        requestResources.setUsername(requestResources.getUsername().toLowerCase());

        // mobile in stander format
        String mobile = Utils.StandardPhoneFormat(requestResources.getCountryCode(), requestResources.getMobile());
        if (mobile == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "mobile406", operationLanguage.getCode(), null);

        if (!requestResources.getPassword().equals(requestResources.getConfirmPassword()))
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);

        //mobile in stander format
        requestResources.setMobile(mobile);
        //encrypt password
        requestResources.setPassword(SECCriptoRsa.encrypt(requestResources.getPassword()));

        //get Entity from Resource
        MerchantRegistrationRequest registrationRequest = requestResources.toMerchantRegistrationRequest();

        registrationRequest.setOperationLanguage(operationLanguage);
        registrationRequest = registrationRequestService.create(registrationRequest, requestResources.getIndustryList());

        MerchantRegistrationRequestResources resources = MerchantRegistrationRequestResources.toResources(registrationRequest);
        SystemMessage message = systemMessageService.findByCode("201");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/approve", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> approveMerchantRequest(@RequestBody MerchantRegistrationRequestResources requestResources, HttpServletRequest request) {

        // get Request Language
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        // validate Request Data
        if (requestResources.getId() == null) {
            //error code for not found data
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }
        MerchantRegistrationRequest registrationRequest = registrationRequestService.approveRequest(requestResources.getId(), operationLanguage);
        MerchantRegistrationRequestResources resources = MerchantRegistrationRequestResources.toResources(registrationRequest);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/reject", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> rejectMerchantRequest(@RequestBody MerchantRegistrationRequestResources requestResources, HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        if (requestResources.getId() == null) {
            //error code for not found data
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }

        MerchantRegistrationRequest registrationRequest = registrationRequestService.rejectRequest(requestResources.getId(), operationLanguage);
        MerchantRegistrationRequestResources resources = MerchantRegistrationRequestResources.toResources(registrationRequest);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> merchantLogin(@RequestBody MerchantUserResources userResources, HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (userResources.getLoginUsername() == null
                || userResources.getPassword() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        MerchantUser merchantUser = merchantUserService.findMerchantByUsernameOrEmail(userResources.getLoginUsername().toLowerCase());
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        if (!SECCriptoRsa.encrypt(userResources.getPassword()).equals((merchantUser.getPassword())))
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);

        userResources.setToken(request.getSession().getId());
        MerchantUserResources resources = MerchantUserResources.toResources(merchantUser);
        resources.setToken(userResources.getToken());
        resources.setPassword(null);
        resources.initResoucreCaption(applicationContext, operationLanguage);
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/deleteReq", method = RequestMethod.DELETE)
    public ResponseEntity<MessageBody> getAllMerchantRegistrationRequest(@RequestBody MerchantRegistrationRequestResources requestResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        if (requestResources.getId() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        MerchantRegistrationRequest registrationRequest = registrationRequestService.findById(requestResources.getId());
        if (registrationRequest == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mrr404", operationLanguage.getCode(), null);
        String DeleteStatus = registrationRequestService.deleteRequest(registrationRequest.getId(), operationLanguage);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, DeleteStatus), HttpStatus.OK);

    }

    @RequestMapping(value = "/allReq", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> getAllMerchantRegistrationRequest(@RequestBody StatusResources status, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        Status sts = null;
        if (status.getCode() == null)
            if (status.getId() == null)
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        if (status.getCode() != null)
            sts = statusService.findByCode(status.getCode());

        if (status.getId() != null)
            sts = statusService.findById(status.getId());

        if (sts == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);


        List<MerchantRegistrationRequest> requestList = null;
        if (!sts.getCode().equals("MRR_ALL"))
            requestList = registrationRequestService.findAllByStatus(sts);
        else
            requestList = registrationRequestService.findAll();
        List<MerchantRegistrationRequestResources> resources = MerchantRegistrationRequestResources.toResources(requestList);
        OperationLanguage finalOperationLanguage = operationLanguage;
        resources.forEach(requestResources -> {
            requestResources.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> addMerchantUser(@RequestBody MerchantUserResources merchantUserResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);

        merchantUser.setOperationLanguage(operationLanguage);

        if (merchantUserResources.getEmail() == null ||
                merchantUserResources.getMobile() == null ||
                merchantUserResources.getMerchantUserType() == null ||
                merchantUserResources.getUserName() == null ||
                merchantUserResources.getCountryCode() == null ||
                merchantUserResources.getFirstName() == null ||
                merchantUserResources.getLastName() == null) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        }
        String mobile = Utils.StandardPhoneFormat(merchantUserResources.getCountryCode(), merchantUserResources.getMobile());
        if (mobile == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "mobile406", operationLanguage.getCode(), null);

        if (!Utils.isEmailCorrect(merchantUserResources.getEmail()))
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email406", operationLanguage.getCode(), null);
        merchantUserResources.setPassword(SECCriptoRsa.encrypt(Utils.randomNumber(6)));

        if (merchantUserService.findByMobile(mobile) != null)
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "mobile302", operationLanguage.getCode(), null);

        merchantUserResources.setMobile(mobile);


        MerchantUser currentMerchantEmail = merchantUserService.findByUsername(merchantUserResources.getUserName());
        if (currentMerchantEmail != null) {
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "username302", operationLanguage.getCode(), null);

        }

        if (merchantUserService.findByEmail(merchantUserResources.getEmail()) != null)
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "email302", operationLanguage.getCode(), null);

        MerchantUser currentMerchantMobile = merchantUserService.findByMobile(mobile);
        if (currentMerchantMobile != null) {
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "mobile302", operationLanguage.getCode(), null);
        }
        MerchantUser newMerchantUser = merchantUserResources.toMerchantUser();
        MerchantUserType merchantUserType = merchantUserTypeService.findById(merchantUserResources.getMerchantUserType());
        newMerchantUser.setMerchantUserType(merchantUserType);
        newMerchantUser.setOperationLanguage(operationLanguage);

        newMerchantUser.setMerchant(merchantUser.getMerchant());
        newMerchantUser.setCrn(merchantUser.getCrn());

        newMerchantUser = merchantUserService.create(newMerchantUser);

        MerchantUserResources resources = MerchantUserResources.toResources(newMerchantUser);
        SystemMessage message = systemMessageService.findByCode("201");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/inActiveUser", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> inActiveUser(@RequestBody MerchantUserResources merchantUserResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        merchantUser.setOperationLanguage(operationLanguage);

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);


        MerchantUser user = merchantUserService.findByIdAndMerchantUserType(merchantUser.getId(), "MER_ADMIN");

        if (user == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "401", operationLanguage.getCode(), null);
        }


        if (!user.getStatus().getCode().equals("MUS_ACTIVE")) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "406", operationLanguage.getCode(), null);
        }
        MerchantUser userdeleted = merchantUserService.findById(merchantUserResources.getId());
        merchantUser.setOperationLanguage(operationLanguage);
        if (userdeleted == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);
        }
        if (userId.equals(userdeleted.getId())) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "406", operationLanguage.getCode(), null);
        }

        Status status = userdeleted.getStatus();
        if (status.getCode().equals("MUS_ACTIVE")) {
            status = statusService.findByCode("MUS_INACTIVE");
            userdeleted.setStatus(status);
        } else if (status.getCode().equals("MUS_INACTIVE")) {
            status = statusService.findByCode("MUS_ACTIVE");
            userdeleted.setStatus(status);

        }

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.OK);

    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> deleteUser(@RequestBody MerchantUserResources merchantUserResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        merchantUser.setOperationLanguage(operationLanguage);


        MerchantUser user = merchantUserService.findByIdAndMerchantUserType(merchantUser.getId(), "MER_ADMIN");

        if (user == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "406", operationLanguage.getCode(), null);
        }

        if (!user.getStatus().getCode().equals("MUS_ACTIVE")) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "406", operationLanguage.getCode(), null);
        }
        MerchantUser userdeleted = merchantUserService.findById(merchantUserResources.getId());
        merchantUser.setOperationLanguage(operationLanguage);
        if (userdeleted == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);
        }

        if (userId.equals(userdeleted.getId())) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "406", operationLanguage.getCode(), null);
        }
        Status status = statusService.findByCode("MUS_DELETE");
        if (status == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        }


        userdeleted.setStatus(status);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.OK);

    }

    @RequestMapping(value = "/allActiveUser", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> allActiveUser(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);


        List<MerchantUser> merchantUserList = merchantUserService.findAllByMerchant_IdAndStatus_Code(merchantUser.getMerchant().getId(), "MUS_ACTIVE");
        if (merchantUserList == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);
        }
        List<MerchantUserResources> merchantUserResourcesList = MerchantUserResources.toResources(merchantUserList);
        for (MerchantUserResources merchantUserResources : merchantUserResourcesList) {
            merchantUserResources.setStatusCaption(statusCaptionService.findById(merchantUserResources.getStatusId()).getName());
        }
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, merchantUser), HttpStatus.OK);

    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.PUT)
    public ResponseEntity<MessageBody> updateUser(HttpServletRequest request,
                                                  @RequestBody MerchantUserResources merchantUserResources) {
        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);

        MerchantUserType userRole = merchantUserTypeService.findById(merchantUserResources.getMerchantUserType());

        if (userRole == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mut404", operationLanguage.getCode(), null);
        String mobile = Utils.StandardPhoneFormat(merchantUserResources.getCountryCode(), merchantUserResources.getMobile());
        if (mobile == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "mobile406", operationLanguage.getCode(), null);


        if (!Utils.isEmailCorrect(merchantUserResources.getEmail()))
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email406", operationLanguage.getCode(), null);

        MerchantUser merchantUpdated = merchantUserService.findById(merchantUserResources.getId());

        MerchantUser merchantEmail = merchantUserService.findByEmail(merchantUserResources.getEmail());
        if (merchantEmail != null &&!(merchantEmail.getId().equals(merchantUpdated.getId()))) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email302", operationLanguage.getCode(), null);

        } else {
            if (merchantUpdated.getEmail().equals(merchantUserResources.getEmail())) {
                merchantUserResources.setEmail(merchantUserResources.getEmail());
            }


        }

        merchantUserResources.setPassword(Utils.randomNumber(6));

        merchantUserResources.setMobile(mobile);
        MerchantUser updateUser = merchantUserResources.toMerchantUser();
        updateUser.setId(merchantUserResources.getId());
        updateUser.setMerchantUserType(userRole);
        updateUser.setOperationLanguage(operationLanguage);
        updateUser.setMobile(mobile);
        updateUser = merchantUserService.updateUser(updateUser);

        MerchantUserResources userResources = MerchantUserResources.toResources(updateUser);
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, userResources), HttpStatus.OK);
    }

    @RequestMapping(value = "/changePass", method = RequestMethod.PUT)
    public ResponseEntity<MessageBody> changePassMerchantUser(HttpServletRequest request, @RequestBody MerchantUserResources merchantUserResources) {

        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        merchantUser.setOperationLanguage(operationLanguage);

        if (!merchantUser.getStatus().getCode().equals("MUS_ACTIVE")) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", languageCode, null);
        }


        if (merchantUserResources.getNewPassword() == null
                || merchantUserResources.getConfirmPassword() == null
                || merchantUserResources.getPassword() == null) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }


        MerchantUser currentUser = merchantUserService.findById(merchantUserResources.getId());
        if (currentUser == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);
        }

        if (!merchantUserResources.getNewPassword().equalsIgnoreCase(merchantUserResources.getConfirmPassword())) {
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);
        }

        currentUser.setOperationLanguage(operationLanguage);
        merchantUserService.changePassword(merchantUser, merchantUserResources.getNewPassword());

        MerchantUserResources userResources = MerchantUserResources.toResources(currentUser);

        userResources.setPassword(currentUser.getPassword());

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageCaption, userResources), HttpStatus.OK);
    }


    @RequestMapping(value = "/resetPass", method = RequestMethod.PUT)
    public ResponseEntity<MessageBody> resetPassMerchantUser(HttpServletRequest request, @RequestBody MerchantUserResources merchantUserResources) {

        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }
        merchantUser.setOperationLanguage(operationLanguage);

        if (merchantUserResources.getId() == null) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }

        MerchantUser currentMerchantUser = merchantUserService.findById(merchantUserResources.getId());

        if (currentMerchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        if (currentMerchantUser.getId() == merchantUser.getId())
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "401", operationLanguage.getCode(), null);


        currentMerchantUser.setOperationLanguage(operationLanguage);
        currentMerchantUser = merchantUserService.resetMerchantUserPassword(currentMerchantUser);

        MerchantUserResources userResources = MerchantUserResources.toResources(currentMerchantUser);
//        userResources.setPassword(SECCriptoRsa.decrypt(userResources.getPassword()));
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption =
                systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(
                MessageHandler.setMessageBody(systemMessageCaption, userResources), HttpStatus.OK);
    }


    @RequestMapping(value = "/allType", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> allNationality(HttpServletRequest request) {


        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        merchantUser.setOperationLanguage(operationLanguage);

        List<MerchantUserTypeCaption> typeCaptionList = merchantUserTypeCaptionService.findByOperationLanguage(operationLanguage);

        List<MerchantUserTypeCaptionResources> resources = MerchantUserTypeCaptionResources.toResource(typeCaptionList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);


    }

    @RequestMapping(value = "/user/filter", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findUserFilter(@RequestParam(name = "mobile", required = false) String mobile,
                                                      @RequestParam(name = "email", required = false) String email,
                                                      @RequestParam(name = "lastName", required = false) String lastName,
                                                      @RequestParam(name = "userRole", required = false) Long userRoleId,
                                                      @RequestParam(name = "userName", required = false) String userName,
                                                      @RequestParam(name = "firstName", required = false) String firstName,
                                                      @RequestParam(name = "statusCode", required = false) String statusCode,
                                                      HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);


        MerchantUserType merchantUserType = merchantUserTypeService.findById(userRoleId);

        Status status = statusService.findByCode(statusCode);

        MerchantUser searchCriteriaUser = new MerchantUser();
        searchCriteriaUser.setFirstName(firstName);
        searchCriteriaUser.setEmail(email);
        searchCriteriaUser.setLastName(lastName);
        searchCriteriaUser.setMobile(mobile);
        searchCriteriaUser.setUserName(userName);
        searchCriteriaUser.setMerchantUserType(merchantUserType);
        searchCriteriaUser.setStatus(status);
        searchCriteriaUser.setMerchant(merchantUser.getMerchant());


        List<MerchantUser> merchantUserList = merchantUserService.findAll(searchCriteriaUser);


        List<MerchantUserResources> userResources = MerchantUserResources.toResources(merchantUserList);

        OperationLanguage finalOperationLanguage = operationLanguage;
        userResources.forEach(userResourceObject -> {
            userResourceObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption =
                systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(
                MessageHandler.setMessageBody(systemMessageCaption, userResources), HttpStatus.OK);
    }


    @RequestMapping(value = "/branch/filter", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findBranchFilter(@RequestParam(name = "mobile", required = false) String mobile,
                                                        @RequestParam(name = "name", required = false) String name,
                                                        @RequestParam(name = "address", required = false) String address,
                                                        @RequestParam(name = "status", required = false) String status,
                                                        HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }


        MerchantUserType merchantUserType = merchantUserTypeService.findByCode("MER_ADMIN");
        if (merchantUserType == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);

        Status statusServiceByCode = statusService.findByCode(status);

        List<MerchantBranch> merchantBranchList = merchantBranchService.findAll(
                (name), (address), (mobile), merchantUser.getMerchant(), statusServiceByCode);

        List<MerchantBranchResources> merchantBranchResources = MerchantBranchResources.toResource(merchantBranchList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption =
                systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageCaption, merchantBranchResources), HttpStatus.OK);
    }

    @RequestMapping(value = "/allBranch", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getBranch(HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);

        Merchant merchant = merchantService.findById(merchantUser.getMerchant().getId());
        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);


        merchant.setOperationLanguage(operationLanguage);
        Status status = statusService.findByCode("BRA_ACTIVE");

        List<MerchantBranch> merchantBranchList = merchantBranchService.findAll(null, null, null, merchant, status);
        List<MerchantBranchResources> merchantBranchResourcesList = MerchantBranchResources.toResource(merchantBranchList);

        for (MerchantBranchResources merchantBranchResources : merchantBranchResourcesList) {
            merchantBranchResources.initResoucreCaption(applicationContext, operationLanguage,null,null);
        }

//        List<MerchantBranchCaption> branchCaptionList = merchantBranchCaptionService.findByMerchant(merchant, operationLanguage, status);
//        List<MerchantBranchCaptionResources> resources = MerchantBranchCaptionResources.toResource(branchCaptionList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, merchantBranchResourcesList), HttpStatus.OK);
    }

    @RequestMapping(value = "/addBranch", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> addBranch(@RequestBody MerchantBranchResources branchResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        if (branchResources.getContactNumber() == null
//                || branchResources.getAddress() == null
                || branchResources.getLatitude() == null
                || branchResources.getLongitude() == null
                || branchResources.getCaptionList() == null
                || branchResources.getCaptionList().isEmpty())
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);

        Merchant merchant = merchantUser.getMerchant();
        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        branchResources.setMerchantId(merchant.getId());
        MerchantBranch merchantBranch = merchantBranchService.create(branchResources.toMerchantBranch());
        MerchantBranchResources resources = MerchantBranchResources.toResource(merchantBranch);

        for (MerchantBranchCaptionResources captionResources : branchResources.getCaptionList()) {
            OperationLanguage language = languageService.findById(captionResources.getLanguageId());
            MerchantBranchCaption branchCaption = new MerchantBranchCaption();
            branchCaption.setDescription(captionResources.getDescription());
            branchCaption.setName(captionResources.getName());
            branchCaption.setMerchantBranch(merchantBranch);
            branchCaption.setOperationLanguage(language);
            branchCaption.setAddress(captionResources.getAddress());
            branchCaption = merchantBranchCaptionService.create(branchCaption);
        }


        SystemMessage message = systemMessageService.findByCode("201");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.CREATED);

    }


    @RequestMapping(value = "/updateBranch", method = RequestMethod.PUT)
    public ResponseEntity<MessageBody> updateBranch(HttpServletRequest request,
                                                    @RequestBody MerchantBranchResources merchantBranchResources) {
        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);


        MerchantUserType userRole = merchantUserTypeService.findById(merchantUser.getMerchantUserType().getId());

        if (userRole == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mut404", operationLanguage.getCode(), null);
        }

        MerchantBranch updateBranch = merchantBranchResources.toMerchantBranch();
        updateBranch.setId(merchantBranchResources.getId());
        updateBranch.setMerchant(merchantUser.getMerchant());
        updateBranch.setOperationLanguage(operationLanguage);
        updateBranch = merchantBranchService.updateBranch(updateBranch);


        List<MerchantBranchCaption> merchantBranchCaptionList = new ArrayList<>();
        for (MerchantBranchCaptionResources captionResources : merchantBranchResources.getCaptionList()) {
            OperationLanguage language = languageService.findById(captionResources.getLanguageId());
            MerchantBranchCaption branchCaption = new MerchantBranchCaption();
            branchCaption.setDescription(captionResources.getDescription());
            branchCaption.setName(captionResources.getName());
            branchCaption.setMerchantBranch(updateBranch);
            branchCaption.setOperationLanguage(language);
            branchCaption.setAddress(captionResources.getAddress());
            merchantBranchCaptionList.add(branchCaption);
        }

        merchantBranchCaptionService.updateBranchCaption(merchantBranchCaptionList);


        MerchantBranchResources userResources = MerchantBranchResources.toResource(updateBranch);

        List<MerchantBranchCaptionResources> merchantBranchCaptionResources = MerchantBranchCaptionResources.toResource(updateBranch.getMerchantBranchCaptionList());

        merchantBranchResources.setCaptionList(merchantBranchCaptionResources);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, merchantBranchResources), HttpStatus.OK);
    }


    @RequestMapping(value = "/inActiveBranch", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> inActiveBranch(@RequestBody MerchantBranchResources merchantBranchResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchant = merchantUserService.findById((userId));
        merchant.setOperationLanguage(operationLanguage);

        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);


        MerchantUser user = merchantUserService.findByIdAndMerchantUserType(merchant.getId(), "MER_ADMIN");

        if (user == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "406", operationLanguage.getCode(), null);
        }

        if (!user.getStatus().getCode().equals("MUS_ACTIVE")) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "406", operationLanguage.getCode(), null);
        }

        MerchantBranch branchDeleted = merchantBranchService.findById(merchantBranchResources.getId());
        merchant.setOperationLanguage(operationLanguage);
        if (branchDeleted == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mbr404", operationLanguage.getCode(), null);
        }
        Status status = statusService.findByCode("BRA_DELETE");

        if (status == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        }
        branchDeleted.setStatus(status);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.OK);

    }


    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAllMerchant(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        List<Merchant> merchantList = merchantService.findAll();
        List<MerchantResources> resources = MerchantResources.toResource(merchantList);
//        List<MerchantCaption> merchantCaptionList = merchantCaptionService.findAll();
        // List<MerchantCaptionResources> resources = MerchantCaptionResources.toResources(merchantCaptionList);
        OperationLanguage finalOperationLanguage = operationLanguage;
        resources.forEach(merchantResources -> {
            merchantResources.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);


    }


    @RequestMapping(value = "/deletPromo", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> deletePromo(@RequestBody PromoResources promoResources, HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);


        if (promoResources.getId() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        promoResources.setOperationLanguage(operationLanguage);
        Status deletePromoSTS = statusService.findByCode("PRO_DELETE");
        if (deletePromoSTS == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);

        Status deletePromoItemSTS = statusService.findByCode("PIT_DELETE");
        if (deletePromoItemSTS == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        Promo promo = promoService.changePromoStatus(promoResources, deletePromoSTS, deletePromoItemSTS);
        PromoResources resources = PromoResources.toResources(promo);
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }


    @RequestMapping(value = "/createNotificationRequest", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> CreateNotificationRequest(@RequestBody SegmentNotificationRequestResource segmentNotificationRequest,
                                                                 HttpServletRequest request) {
        String langCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(langCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);


        if (segmentNotificationRequest.getPromoId() == null ||
                segmentNotificationRequest.getCustomerSegmentId() == null) {

            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        }


        SegmentNotificationRequest notificationRequest = segmentNotificationRequest.toSegmentNotificationRequest();

        SegmentNotificationRequest resources = segmentNotificationRequestService.createRequest(notificationRequest);

        //Thread for push customers
        new Thread() {
            public void run() {
                segmentNotificationRequestService.notifyCustomers(resources);
            }
        }.start();


        //Thread for push media
        new Thread() {
            public void run() {
                segmentNotificationRequestService.notifyMedia(resources);
            }
        }.start();

        SegmentNotificationRequestResource notificationReq = SegmentNotificationRequestResource.toResource(resources);

        SystemMessage message = systemMessageService.findByCode("201");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, notificationReq), HttpStatus.CREATED);
    }


    @RequestMapping(value = "/changePass", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> changeMerchantPassword(@RequestBody MerchantUserResources userResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (userResources.getPassword() == null ||
                userResources.getNewPassword() == null ||
                userResources.getConfirmPassword() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);

        if (!merchantUser.getPassword().equals(SECCriptoRsa.encrypt(userResources.getPassword())))
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);

        if (!userResources.getNewPassword().equals(userResources.getConfirmPassword()))
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);

        merchantUser.setPassword(SECCriptoRsa.encrypt(userResources.getNewPassword()));
        merchantUser = merchantUserService.changePassword(merchantUser);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption =
                systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageCaption, null), HttpStatus.OK);

    }


}
