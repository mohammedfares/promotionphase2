package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.CountryResources;
import com.dc.Promotion.resources.MerchantCustomerSegmentResources;
//import com.dc.Promotion.security.SessionHelper;
import com.dc.Promotion.security.AccountUserDetails;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/merchant")
public class SegmentController {


    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private MerchantUserService merchantUserService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantCustomerSegmentService merchantCustomerSegmentService;

    @Autowired
    private CountrySegmentService countrySegmentService;

    @Autowired
    private CountryService countryService;


    @RequestMapping(value = "/segment/create", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> createSegment(@RequestBody MerchantCustomerSegmentResources segmentResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        Merchant merchant = merchantService.findById(merchantUser.getMerchant().getId());
        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);
        MerchantCustomerSegment segment = segmentResources.toMerchantCustomerSegment();
        segment.setOperationLanguage(operationLanguage);
        segment.setMerchantUser(merchantUser);
        segment.setMerchant(merchant);
        segment = merchantCustomerSegmentService.create(segment);
        MerchantCustomerSegment finalSegment = segment;

        segmentResources.getCountryList().forEach(countryResources -> {
            CountrySegment countrySegment=new CountrySegment();
            countrySegment.setCountry(countryService.findById(countryResources.getId()));
            countrySegment.setMerchantCustomerSegment(finalSegment);
            countrySegmentService.create(countrySegment);
        });
        List<Country>  countryList=countrySegmentService.findAllCountry(segment);
        List<CountryResources> countryResourcesList=CountryResources.toResources(countryList);

        MerchantCustomerSegmentResources resources = MerchantCustomerSegmentResources.toResources(segment);
        resources.setCountryList(countryResourcesList);
        SystemMessage message = systemMessageService.findByCode("201");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/segment/delete", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> deleteSegment(@RequestBody MerchantCustomerSegmentResources segmentResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        if (segmentResources.getId() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);


        MerchantCustomerSegment segment = merchantCustomerSegmentService.findById(segmentResources.getId());
        if (segment == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mcs404", operationLanguage.getCode(), null);
        Status status = statusService.findByCode("MCS_DELETE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        segment.setStatus(status);
        MerchantCustomerSegmentResources resources = MerchantCustomerSegmentResources.toResources(segment);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/segment/inactive", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> inactiveSegment(@RequestBody MerchantCustomerSegmentResources segmentResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        if (segmentResources.getId() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        MerchantCustomerSegment segment = merchantCustomerSegmentService.findById(segmentResources.getId());
        if (segment == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mcs404", operationLanguage.getCode(), null);
        Status status = statusService.findByCode("MCS_INACTIVE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        segment.setStatus(status);
        MerchantCustomerSegmentResources resources = MerchantCustomerSegmentResources.toResources(segment);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/segment/getAll", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAllByMerchant(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);


        Merchant merchant=merchantService.findById(merchantUser.getMerchant().getId());
        if(merchant==null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);

        merchant.setOperationLanguage(operationLanguage);
        List<MerchantCustomerSegment> segmentList=merchantCustomerSegmentService.findAllByMerchant(merchant);

        List<MerchantCustomerSegmentResources> resources=MerchantCustomerSegmentResources.toResources(segmentList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/segment/active", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> activeSegment(@RequestBody MerchantCustomerSegmentResources segmentResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        if (segmentResources.getId() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        MerchantCustomerSegment segment = merchantCustomerSegmentService.findById(segmentResources.getId());
        if (segment == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mcs404", operationLanguage.getCode(), null);
        Status status = statusService.findByCode("MCS_ACTIVE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        segment.setStatus(status);
        MerchantCustomerSegmentResources resources = MerchantCustomerSegmentResources.toResources(segment);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

}
