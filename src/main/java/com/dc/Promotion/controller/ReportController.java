package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.PageResource;
import com.dc.Promotion.resources.PromoItemResources;
import com.dc.Promotion.resources.PromoResources;
import com.dc.Promotion.security.AccountUserDetails;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private OperationLanguageService operationLanguageService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private PromoService promoService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private MerchantUserService merchantUserService;

    @Autowired
    private PromoItemService promoItemService;

    @RequestMapping(value = "/search/promo", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> searchPromo(@RequestParam(value = "startDate", required = false) String startDate,
                                                   @RequestParam(value = "expiryDate", required = false) String expiryDate,
                                                   @RequestParam(value = "branchId", required = false) Long branchId,
                                                   @RequestParam(name = "page", required = false) Integer page,
                                                   @RequestParam(name = "size", required = false) Integer size,
                                                   HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        Merchant merchant = merchantUser.getMerchant();
        Page<Promo> promoList = promoService.promoReport(
               Utils.startOfDay( Utils.parseLocalDate(startDate, "yyyy-MM-dd")),
                Utils.startOfDay( Utils.parseLocalDate(expiryDate, "yyyy-MM-dd")),
                branchId,
                merchant,
                page,
                size);
        List<PromoResources> promoResourcesList = PromoResources.toResources(promoList.getContent());
        OperationLanguage finalOperationLanguage = operationLanguage;
        promoResourcesList.forEach(promoResources -> {
            promoResources.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoList.getTotalElements());
        pageResource.setTotalPages(promoList.getTotalPages());
        pageResource.setPageList(promoResourcesList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);


    }


    @RequestMapping(value = "/search/item", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> searchItem(@RequestParam(value = "startDate", required = false) String startDate,
                                                  @RequestParam(value = "expiryDate", required = false) String expiryDate,
                                                  @RequestParam(value = "promoId", required = false) Long promoId,
                                                  @RequestParam(name = "page", required = false) Integer page,
                                                  @RequestParam(name = "size", required = false) Integer size,
                                                  HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        Merchant merchant = merchantUser.getMerchant();

        Promo promo = null;
        if (promoId != null && promoId != 0)
            promo = promoService.findById(promoId);

        Page<PromoItem> promoItemList = promoItemService.itemReport(
                Utils.startOfDay( Utils.parseLocalDate(startDate, "yyyy-MM-dd")),
                Utils.startOfDay( Utils.parseLocalDate(expiryDate, "yyyy-MM-dd")),
                promo,
                merchant,
                page,
                size);
        List<PromoItemResources> promoItemResourcesList = PromoItemResources.toResources(promoItemList.getContent());
        OperationLanguage finalOperationLanguage = operationLanguage;
        promoItemResourcesList.forEach(promoItemResources -> {
            promoItemResources.initResourceCaption(applicationContext, finalOperationLanguage, null, null,null,null);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoItemList.getTotalElements());
        pageResource.setTotalPages(promoItemList.getTotalPages());
        pageResource.setPageList(promoItemResourcesList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);

    }


}
