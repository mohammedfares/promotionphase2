package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.CityCaptionResources;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/city")
public class CityController {

    @Autowired
    private CityService cityService;

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private CityCaptionService cityCaptionService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private OperationCountryService operationCountryService;


    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findAllCity(@RequestParam(name = "countryCode") String countryCode, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        //find country:
        Country country = countryService.findByCode(countryCode);
        OperationCountry operationCountry = operationCountryService.findByCountry(country); //find opc by country
        if (operationCountry == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "oco404", operationLanguage.getCode(), null);
        }

        List<CityCaption> cityCaptionList = cityCaptionService.findByLanguageAndCity_Country(operationLanguage, operationCountry);
        List<CityCaptionResources> resources = CityCaptionResources.toResources(cityCaptionList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

}
