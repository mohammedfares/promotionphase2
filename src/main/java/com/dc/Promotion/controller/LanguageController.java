package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.SystemMessage;
import com.dc.Promotion.entities.SystemMessageCaption;
import com.dc.Promotion.resources.OperationLanguageResources;
import com.dc.Promotion.service.OperationLanguageService;
import com.dc.Promotion.service.SystemMessageCaptionService;
import com.dc.Promotion.service.SystemMessageService;
import com.dc.Promotion.utils.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/lang")
public class LanguageController {

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private OperationLanguageService languageService;

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findAllLang(HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }
        List<OperationLanguage> languageList = languageService.findAll();

        List<OperationLanguageResources> resources = OperationLanguageResources.toResources(languageList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

}
