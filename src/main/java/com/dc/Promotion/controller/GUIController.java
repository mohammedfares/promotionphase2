package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.PromoItemResources;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/gui")
public class GUIController {

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private PromoService promoService;

    @Autowired
    private PromoCaptionService promoCaptionService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private PromoItemService promoItemService;


    @RequestMapping(value =  "/promo/item",method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getPromoItem(@RequestParam("promoId") Long id,
                                                    HttpServletRequest request)
    {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        Promo promo=promoService.findById(id);
        if(promo==null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", operationLanguage.getCode(), null);

        List<PromoItem> promoItemList=promoItemService.findAllByPromo(promo);
        List<PromoItemResources> resources= PromoItemResources.toResources(promoItemList);
        OperationLanguage finalOperationLanguage = operationLanguage;
        resources.forEach(promoItemResources -> {
            promoItemResources.initResourceCaption(applicationContext, finalOperationLanguage,null,null,null,null);
        });



        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

}
