package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.DistrictCaptionResources;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/district")
public class DistrictController {

    @Autowired
    private CityService cityService;

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private DistrictCaptionService districtCaptionService;


    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findAllDistrict(@RequestParam(name = "cityId") long id, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        City city = cityService.findById(id);
        if (city == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "city404", operationLanguage.getCode(), null);


        //find All District Caption by City and Language
        List<DistrictCaption> captionList = districtCaptionService.findAllByLanguageAndDistrict_City(operationLanguage, city);

        List<DistrictCaptionResources> resources = DistrictCaptionResources.toResources(captionList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }
}
