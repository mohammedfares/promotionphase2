package com.dc.Promotion.controller;


import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.MerchantBranchResources;
import com.dc.Promotion.security.AccountUserDetails;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import com.dc.Promotion.utils.PromoExcelFile;
import com.dc.Promotion.utils.Utils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Decoder;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/file")
public class FileController {

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private MerchantUserService merchantUserService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MerchantBranchService merchantBranchService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private MerchantCaptionService merchantCaptionService;

    @Autowired
    private PromoService promoService;

    @RequestMapping(value = "/template", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getExcelTemplate(HttpServletRequest request) throws IOException {

        String lang = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(lang);
        if (operationLanguage == null) {

            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        Merchant merchant = merchantService.findById(merchantUser.getMerchant().getId());
        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);


        merchant.setOperationLanguage(operationLanguage);
        Status status = statusService.findByCode("BRA_ACTIVE");

        List<MerchantBranch> merchantBranchList = merchantBranchService.findAll(null, null, null, merchant, status);
        List<MerchantBranchResources> merchantBranchResourcesList = MerchantBranchResources.toResource(merchantBranchList);

        for (MerchantBranchResources merchantBranchResources : merchantBranchResourcesList) {
            merchantBranchResources.initResoucreCaption(applicationContext, operationLanguage,null,null);
        }

        MerchantCaption merchantCaption = merchantCaptionService.findByIdAndLanguage(merchant.getId(), operationLanguage);

        if (merchantCaption == null) {
            merchantCaption = merchantCaptionService.findByMerchantId(merchant.getId()).get(0);
        }

        File templateExcelFile = null;
        try {
            templateExcelFile = PromoExcelFile.createExcelFile(applicationContext, merchantCaption.getName(), merchantBranchResourcesList, null, null);
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }

        String fileDate = Utils.encodeFileToBase64Binary(templateExcelFile.getPath());


        PromoExcelFile responseFile = new PromoExcelFile();
        responseFile.setName(templateExcelFile.getName());
        responseFile.setBase64(fileDate);

        templateExcelFile.delete();

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, responseFile), HttpStatus.OK);
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> uploadExcelStudent(HttpServletRequest request, @RequestBody PromoExcelFile promoExcelFile) {

        String lang = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(lang);
        if (operationLanguage == null) {

            operationLanguage = languageService.findByCode("en");
        }

        if (promoExcelFile.getBase64() == null
                || promoExcelFile.getName() == null
                || promoExcelFile.getType() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "MER404", operationLanguage.getCode(), null);

        Merchant merchant = merchantService.findById(merchantUser.getMerchant().getId());
        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", operationLanguage.getCode(), null);

        merchant.setOperationLanguage(operationLanguage);

        String tempFileName = System.currentTimeMillis() + "";

        String path = this.applicationContext.getEnvironment().getProperty("attachment_path");

        File tempFile = new File(path + tempFileName + ".xlsm");

        Workbook workbook = null;

        try {
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] decodedBytes = decoder.decodeBuffer(promoExcelFile.getBase64());

            FileOutputStream fop = new FileOutputStream(tempFile);

            fop.write(decodedBytes);
            fop.flush();
            fop.close();

            workbook = WorkbookFactory.create(tempFile);

            promoService.uploadPromosExcelFile(workbook,merchant);

            workbook.close();

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        } finally {
            if (workbook != null) {
                try {
                    workbook.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (tempFile != null) {
                tempFile.delete();
            }
        }

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.OK);
    }

}
