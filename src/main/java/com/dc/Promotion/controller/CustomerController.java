package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.*;
import com.dc.Promotion.security.AccountUserDetails;
import com.dc.Promotion.security.SECCriptoRsa;
//import com.dc.Promotion.security.SessionHelper;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import com.dc.Promotion.utils.Utils;
import com.dc.Promotion.utils.WebServiceCallHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private OperationLanguageService operationLanguageService;

    @Autowired
    private MediaWatchListService mediaWatchListService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private CityService cityService;

    @Autowired
    private CountryService countryService;

    @Autowired
    private DistrictService districtService;

    @Autowired
    private OTPService otpService;

    @Autowired
    private CustomerAccountService customerAccountService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private CountryCaptionService countryCaptionService;

    @Autowired
    private CustomerWatchListService customerWatchListService;

    @Autowired
    private PromoItemService promoItemService;

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private PromoService promoService;

    @Autowired
    private PromoCaptionService promoCaptionService;

    @Autowired
    private MediaAccountService mediaAccountService;

    @Autowired
    private IndustryService industryService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private ItemCategoryService itemCategoryService;

    @Autowired
    private MediaWishListService mediaWishListService;

    @Autowired
    private CustomerWishListService customerWishListService;

    @Autowired
    private PromoItemCaptionService promoItemCaptionService;

    @Autowired
    private MediaNotificationService mediaNotificationService;

    @Autowired
    private CustomerNotificationService customerNotificationService;

    @Autowired
    private WishListHandlerService wishListHandlerService;

    @Autowired
    private ItemCategoryCaptionService itemCategoryCaptionService;

    @Autowired
    private CompareHandlerService compareHandlerService;


    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> customerRegistration(@RequestBody CustomerAccountResources accountResources,
                                                            HttpServletRequest request, HttpSession session) {

        String language = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(language);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        if (accountResources.getEmail() == null ||
                accountResources.getGender() == null ||
                accountResources.getBirthDate() == null ||
                accountResources.getFirstName() == null ||
                accountResources.getPassword() == null ||
                accountResources.getConfirmPassword() == null ||
                accountResources.getMobile() == null ||
                accountResources.getCityId() == null ||
                accountResources.getDistrictId() == null ||
                accountResources.getOtp() == null ||
                accountResources.getCountryCode() == null ||
                accountResources.getCountryId() == null ||
                accountResources.getLastName() == null
                ) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }


        if (Utils.StandardPhoneFormat(accountResources.getCountryCode(), accountResources.getMobile()) == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "mobile406", operationLanguage.getCode(), null);
        }
        String mobile = Utils.StandardPhoneFormat(accountResources.getCountryCode(), accountResources.getMobile());
        if (!accountResources.getPassword().equalsIgnoreCase(accountResources.getConfirmPassword())) {
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);
        }
        String pass = (SECCriptoRsa.encrypt(accountResources.getPassword()));
        accountResources.setPassword(pass);
        if (!Utils.isEmailCorrect(accountResources.getEmail())) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email406", operationLanguage.getCode(), null);
        }


        City city = cityService.findById(accountResources.getCityId());
        if (city == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "city404", operationLanguage.getCode(), null);
        }
        District district = districtService.findById(accountResources.getDistrictId());
        if (district == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "district404", operationLanguage.getCode(), null);
        }

        Country country = countryService.findById(accountResources.getCountryId());
        if (country == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "country404", operationLanguage.getCode(), null);


        CustomerAccount currentAccountEmail = customerAccountService.findByEmail(accountResources.getEmail());
        if (currentAccountEmail != null) {
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "email302", operationLanguage.getCode(), null);
        }

        CustomerAccount currentAccountMobile = customerAccountService.findByMobile(mobile);
        if (currentAccountMobile != null) {
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "mobile302", operationLanguage.getCode(), null);
        }
        //check OTP valid or not:
        OTP verificationOTP = otpService.findByMobileAndCode(mobile, accountResources.getOtp());
        if (verificationOTP == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "otp404", operationLanguage.getCode(), null);
        }
        if (verificationOTP != null && verificationOTP.getStatus().getCode().equalsIgnoreCase("OTP_INACTIVE")) {
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "otp302", operationLanguage.getCode(), null);
        }
        if (!verificationOTP.getMobile().equalsIgnoreCase(mobile)) {
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "otp409", operationLanguage.getCode(), null);
        }
        if (verificationOTP != null && verificationOTP.getExpiryDate().isBefore(LocalDateTime.now())) {
            otpService.deActive(verificationOTP);
            throw new ResourceException(applicationContext, HttpStatus.REQUEST_TIMEOUT, "otp408", operationLanguage.getCode(), null);
        }

        CustomerAccount account = accountResources.toCustomerAccount();
        account.setEmail(accountResources.getEmail());
        account.setBirthDate(accountResources.getBirthDate());
        account.setCity(city);
        account.setDistrict(district);
        account.setMobile(mobile);
        account.setCountry(country);
        account.setOperationLanguage(operationLanguage);
        Status status = statusService.findByCode("CAC_ACTIVE");
        account.setStatus(status);
        account.setCreateDate(LocalDateTime.now());
        account.setExpireDate(LocalDateTime.now().plusYears(2));
        account.setBarCode(Long.valueOf(Utils.randomNumber(10)));
        account = customerAccountService.save(account);

        otpService.deActive(verificationOTP);
//        String token = session.getId();
//        SessionHelper.insertToken("CUS:" + account.getId(), token);
        CustomerAccountResources customerAccountResources = CustomerAccountResources.toResources(account);
        customerAccountResources.setToken(WebServiceCallHandler.generateTokenForCustomer(customerAccountResources.getEmail(), SECCriptoRsa.decrypt(customerAccountResources.getPassword())));
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageCaption, customerAccountResources), HttpStatus.OK);
    }

    @RequestMapping(value = "/registerFacebook", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> createFacebookAccount(HttpSession session, HttpServletRequest request, @RequestBody MediaAccountResources mediaAccountResources) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        MediaAccount mediaAccount = mediaAccountService.findById(userId);

        /*
         * Update media based on mediaAccountResources
         */
//        int age = Utils.calculateAge(mediaAccount.getBirthdate().toLocalDate());

        mediaAccount.setOperationLanguage(operationLanguage);
        mediaAccount.setDevicePlatform(mediaAccountResources.getDevicePlatform());
        mediaAccount.setDeviceToken(mediaAccountResources.getDeviceToken());
        mediaAccount.setAge(mediaAccountResources.getAge());
        mediaAccountResources.setId(mediaAccount.getId());

        mediaAccountService.updateAccountInfo(mediaAccountResources.toMediaAccount());

        String token = session.getId();

        MediaAccountResources resources = MediaAccountResources.toResources(mediaAccount);
        resources.setToken(token);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/registerGoogle", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> createGoogleAccount(HttpSession session, HttpServletRequest request, @RequestBody MediaAccountResources mediaAccountResources) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        MediaAccount mediaAccount = mediaAccountService.findById(userId);

        /*
         * Update media based on mediaAccountResources
         */
//        int age = Utils.calculateAge(mediaAccount.getBirthdate().toString());
        mediaAccount.setOperationLanguage(operationLanguage);
        mediaAccount.setDevicePlatform(mediaAccountResources.getDevicePlatform());
        mediaAccount.setDeviceToken(mediaAccountResources.getDeviceToken());
        mediaAccount.setAge(mediaAccountResources.getAge());
        mediaAccountResources.setId(mediaAccount.getId());

        mediaAccountService.updateAccountInfo(mediaAccountResources.toMediaAccount());

        String token = session.getId();

        MediaAccountResources resources = MediaAccountResources.toResources(mediaAccount);
        resources.setToken(token);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/sendOtp", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> customerOtp(@RequestBody CustomerAccountResources customerAccountResource,
                                                   HttpServletRequest request) {
        String language = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(language);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        if (customerAccountResource.getFirstName() == null ||
                customerAccountResource.getLastName() == null ||
                customerAccountResource.getEmail() == null ||
                customerAccountResource.getBirthDate() == null ||
                customerAccountResource.getGender() == null ||
                customerAccountResource.getMobile() == null ||
                customerAccountResource.getPassword() == null ||
                customerAccountResource.getConfirmPassword() == null ||
                customerAccountResource.getCountryCode() == null ||
                customerAccountResource.getCityId() == null ||
                customerAccountResource.getDistrictId() == null
                ) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        }
        String mobile = customerAccountResource.getMobile();

        if (Utils.StandardPhoneFormat(customerAccountResource.getCountryCode(), mobile) == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "mobile406", operationLanguage.getCode(), null);
        }
        if (!customerAccountResource.getPassword().equalsIgnoreCase(customerAccountResource.getConfirmPassword())) {
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);
        }

        if (!Utils.isEmailCorrect(customerAccountResource.getEmail())) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email406", operationLanguage.getCode(), null);
        }

        Country country = countryService.findById(customerAccountResource.getCountryId());
        if (country == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "country404", operationLanguage.getCode(), null);

        City city = cityService.findById(customerAccountResource.getCityId());
        if (city == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "city404", operationLanguage.getCode(), null);
        }
        District district = districtService.findById(customerAccountResource.getDistrictId());
        if (district == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "district404", operationLanguage.getCode(), null);
        }
        CustomerAccount currentAccountEmail = customerAccountService.findByEmail(customerAccountResource.getEmail());
        if (currentAccountEmail != null) {
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "email302", operationLanguage.getCode(), null);
        }

        CustomerAccount currentAccountMobile = customerAccountService.findByMobile(mobile);
        if (currentAccountMobile != null) {
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "mobile302", operationLanguage.getCode(), null);
        }
        customerAccountResource.setMobile(Utils.StandardPhoneFormat(customerAccountResource.getCountryCode(), mobile));
        Status statusActive = statusService.findByCode("OTP_ACTIVE");

        Status statusInactive = statusService.findByCode("OTP_INACTIVE");

        OTP otp = new OTP();

        otp = otpService.findActiveOtp(mobile, statusActive.getCode());
        if (otp != null) {
            otp.setStatus(statusInactive);
        }

        otp = new OTP();
        otp.setMobile(customerAccountResource.getMobile());
        otp.setEmail(customerAccountResource.getEmail());
        otp.setStatus(statusActive);

        otpService.createOTP(otp);
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, new OTPResources().toResources(otp)), HttpStatus.OK);
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> customerLogin(@RequestBody CustomerAccountResources customerAccountResource,
                                                     HttpServletRequest request, HttpSession session) {
        String language = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(language);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        if (customerAccountResource.getUsername() == null ||
                customerAccountResource.getPassword() == null) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }

        String countryCode = customerAccountResource.getCountryCode();

        CustomerAccount currentAccount = null;

        if (countryCode == null || countryCode.isEmpty()) {
            //username is email
            if (!Utils.isEmailCorrect(customerAccountResource.getUsername()))
                throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email302", operationLanguage.getCode(), null);

            currentAccount = customerAccountService.findByEmail(customerAccountResource.getUsername());
            if (currentAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "email404", operationLanguage.getCode(), null);
            }
            if (!currentAccount.getPassword().equals(SECCriptoRsa.encrypt(customerAccountResource.getPassword()))) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pass404", operationLanguage.getCode(), null);
            }

        } else {
            //username is mobile
            String mobile = Utils.StandardPhoneFormat(customerAccountResource.getCountryCode(), customerAccountResource.getUsername());
            customerAccountResource.setMobile(mobile);

            currentAccount = customerAccountService.findByMobile(mobile);
            if (currentAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mobile404", operationLanguage.getCode(), null);
            }
            if (!currentAccount.getPassword().equals(SECCriptoRsa.encrypt(customerAccountResource.getPassword()))) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pass404", operationLanguage.getCode(), null);
            }
        }
        int age = Utils.calculateAge(currentAccount.getBirthDate().toLocalDate());
        currentAccount.setOperationLanguage(operationLanguage);
        currentAccount.setDevicePlatform(customerAccountResource.getDevicePlatform());
        currentAccount.setAge(age);
        currentAccount.setDeviceToken(customerAccountResource.getDeviceToken());

        customerAccountResource.setId(currentAccount.getId());
        customerAccountService.updateAccountInfo(customerAccountResource.toCustomerAccount());

        String token = session.getId();
//        SessionHelper.insertToken("CUS:" + currentAccount.getId(), token);

        CustomerAccountResources customerAcc = CustomerAccountResources.toResources(currentAccount);
        customerAcc.setToken(token);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, customerAcc), HttpStatus.OK);
    }

    @RequestMapping(value = "/changePass", method = RequestMethod.PUT)
    public ResponseEntity<MessageBody> changePassUser(HttpServletRequest request, @RequestBody CustomerAccountResources accountResources) {

        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", languageCode, null);
        }

        if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", languageCode, null);
        }


        if (accountResources.getNewPassword() == null
                || accountResources.getConfirmPassword() == null
                || accountResources.getPassword() == null) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }


        Long currentUser = account.getId();
        if (currentUser == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "user404", operationLanguage.getCode(), null);
        }

        if (!accountResources.getNewPassword().equalsIgnoreCase(accountResources.getConfirmPassword())) {
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);
        }


        account.setOperationLanguage(operationLanguage);

        customerAccountService.changePassword(account, accountResources.getNewPassword());


        CustomerAccountResources userResources = CustomerAccountResources.toResources(account);

        userResources.setPassword(account.getPassword());

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageCaption, userResources), HttpStatus.OK);
    }


    @RequestMapping(value = "/toWatchList", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> addToWatchList(@RequestBody CustomerWatchListResources watchListResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }


        CustomerAccount account = null;
        MediaAccount mediaAccount = null;


        if (!isCustomerAccount) {

            mediaAccount = mediaAccountService.findById((userId));

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!mediaAccount.getStatus().getCode().equals("MAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (watchListResources.getPromoItemId() == null)
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", LangCode, null);


            MediaWatchListResources mediaWatchListResources = new MediaWatchListResources();
            mediaWatchListResources.setPromoItemId(watchListResources.getPromoItemId());
            mediaWatchListResources.setMediaAccountId(userId);
            watchListResources.setOperationLanguage(operationLanguage);
            MediaWatchList mediaWatchList = mediaWatchListService.create(mediaWatchListResources.toMediaWatchList());

            MediaWatchListResources listResources = MediaWatchListResources.toResources(mediaWatchList);


            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, listResources), HttpStatus.OK);
        } else {
            account = customerAccountService.findById((userId));

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (watchListResources.getPromoItemId() == null)
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", LangCode, null);

            watchListResources.setCustomerAccountId(account.getId());
            watchListResources.setOperationLanguage(operationLanguage);
            CustomerWatchList customerWatchList = customerWatchListService.create(watchListResources.toCustomerWatchList());
            CustomerWatchListResources resources = CustomerWatchListResources.toResources(customerWatchList);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/getWatchList", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getWatchList(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }


        CustomerAccount account = null;
        MediaAccount mediaAccount = null;


        if (!isCustomerAccount) {

            mediaAccount = mediaAccountService.findById((userId));

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }
            List<PromoItem> promoItemList = promoItemService.findWatchListMedia(mediaAccount);
            List<PromoItemResources> resources = PromoItemResources.toResources(promoItemList);
            OperationLanguage finalOperationLanguage = operationLanguage;
            MediaAccount finalMediaAccount1 = mediaAccount;
            resources.forEach(promoItemResources -> {
                promoItemResources.initResourceCaption(applicationContext, finalOperationLanguage, account, finalMediaAccount1, null, null);
            });

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
        } else {
            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            List<PromoItem> promoItemList = promoItemService.findWatchListCustomer(account);
            List<PromoItemResources> resources = PromoItemResources.toResources(promoItemList);
            OperationLanguage finalOperationLanguage = operationLanguage;
            CustomerAccount finalAccount = account;
            MediaAccount finalMediaAccount = mediaAccount;
            resources.forEach(promoItemResources -> {
                promoItemResources.initResourceCaption(applicationContext, finalOperationLanguage, finalAccount, finalMediaAccount, null, null);
            });

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/getPromo", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getPromoById(HttpServletRequest request,
                                                    @RequestParam(name = "promoId") Long promoId) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;
        if (!isCustomerAccount) {

            mediaAccount = mediaAccountService.findById((userId));

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }
        }

        Promo promo = promoService.findById(promoId);
        if (promo == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pro404", operationLanguage.getCode(), null);

        PromoResources resources = PromoResources.toResources(promo);
        List<PromoItem> promoItemList = promoItemService.findAllByPromo(promo);
        List<PromoItemResources> promoItemResources = PromoItemResources.toResources(promoItemList);
        for (PromoItemResources itemResources : promoItemResources)
            itemResources.initResourceCaption(applicationContext, operationLanguage, account, mediaAccount, null, null);
        List<PromoCaption> promoCaptionList = promoCaptionService.findAllByPromo(promo);
        resources.initResoucreCaption(applicationContext, operationLanguage);
        resources.setCaptionList(PromoCaptionResources.toResources(promoCaptionList));
        resources.setPromoItemList(promoItemResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

    }

    @RequestMapping(value = "/forgetPassword/verifyOtp", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> forgetPasswordCustomerVerifyOtp(HttpServletRequest request, @RequestBody CustomerAccountResources accountResource) {
        /*
         *Find operation language if null default english
         */
        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");

        if (accountResource.getUsername() == null
                || accountResource.getUsername().isEmpty()
                || accountResource.getOtp() == null
                || accountResource.getOtp().isEmpty()) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }
        String countryCode = accountResource.getCountryCode();

        CustomerAccount currentAccount = null;

        if (countryCode == null || countryCode.isEmpty()) {
            //username is email
            if (!Utils.isEmailCorrect(accountResource.getUsername()))
                throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email302", operationLanguage.getCode(), null);

            currentAccount = customerAccountService.findByEmail(accountResource.getUsername());
            if (currentAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "email404", operationLanguage.getCode(), null);
            }


        } else {
            //username is mobile
            String mobile = Utils.StandardPhoneFormat(accountResource.getCountryCode(), accountResource.getUsername());
            accountResource.setMobile(mobile);

            currentAccount = customerAccountService.findByMobile(mobile);
            if (currentAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mobile404", operationLanguage.getCode(), null);
            }
        }

        Status status = statusService.findByCode(currentAccount.getStatus().getCode());

        if (status.getCode() == "CUS_INACTIVE") {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }
        // Validate OTP

        OTP otp = otpService.findByMobileAndCode(currentAccount.getMobile(), accountResource.getOtp());
        if (otp == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "otp404", operationLanguage.getCode(), null);
        if (otp != null && otp.getStatus().getCode().equalsIgnoreCase("OTP_INACTIVE"))
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "otp302", operationLanguage.getCode(), null);
        if (!otp.getMobile().equalsIgnoreCase(currentAccount.getMobile()))
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "otp409", operationLanguage.getCode(), null);
        if (otp != null && otp.getExpiryDate().isBefore(LocalDateTime.now())) {
            otpService.deActive(otp);
            throw new ResourceException(applicationContext, HttpStatus.REQUEST_TIMEOUT, "otp408", operationLanguage.getCode(), null);
        }
        otp.setOperationLanguage(operationLanguage);

        otpService.deActive(otp);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption =
                systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);

        return new ResponseEntity<>(
                MessageHandler.setMessageBody(systemMessageCaption, new OTPResources().toResources(otp)), HttpStatus.OK);
    }

    @RequestMapping(value = "/forgetPassword/change", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> forgetPasswordCustomerChange(HttpServletRequest request, @RequestBody CustomerAccountResources accountResource) {

        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");

        if (accountResource.getUsername() == null
                || accountResource.getUsername().isEmpty()
                || accountResource.getOtp() == null
                || accountResource.getOtp().isEmpty()
                || accountResource.getNewPassword() == null
                || accountResource.getNewPassword().isEmpty()
                || accountResource.getConfirmPassword() == null
                || accountResource.getConfirmPassword().isEmpty()) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }
        CustomerAccount currentAccount = null;

        String countryCode = accountResource.getCountryCode();
        if (countryCode == null || countryCode.isEmpty()) {
            //username is email
            if (!Utils.isEmailCorrect(accountResource.getUsername())) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email302", operationLanguage.getCode(), null);
            }

            currentAccount = customerAccountService.findByEmail(accountResource.getUsername());
            if (currentAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "email404", operationLanguage.getCode(), null);
            }


        } else {
            //username is mobile
            String mobile = Utils.StandardPhoneFormat(accountResource.getCountryCode(), accountResource.getUsername());
            accountResource.setMobile(mobile);

            currentAccount = customerAccountService.findByMobile(mobile);
            if (currentAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mobile404", operationLanguage.getCode(), null);
            }

        }

        Status status = statusService.findByCode(currentAccount.getStatus().getCode());

        if (status.getCode() == "CUS_INACTIVE") {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }


        if (!accountResource.getNewPassword().equalsIgnoreCase(accountResource.getConfirmPassword())) {
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);
        }


        // Validate OTP

        OTP otp = otpService.findByMobileAndCode(currentAccount.getMobile(), accountResource.getOtp());
        if (otp == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "otp404", operationLanguage.getCode(), null);
        if (otp != null && otp.getStatus().getCode().equalsIgnoreCase("OTP_INACTIVE"))
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "otp302", operationLanguage.getCode(), null);
        if (!otp.getMobile().equalsIgnoreCase(currentAccount.getMobile()))
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "otp409", operationLanguage.getCode(), null);
        if (otp != null && otp.getExpiryDate().isBefore(LocalDateTime.now())) {
            otpService.deActive(otp);
            throw new ResourceException(applicationContext, HttpStatus.REQUEST_TIMEOUT, "otp408", operationLanguage.getCode(), null);
        }
        otp.setOperationLanguage(operationLanguage);
        otpService.deActive(otp);


        currentAccount.setOperationLanguage(operationLanguage);
        customerAccountService.changePassword(currentAccount, accountResource.getNewPassword());

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption =
                systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(
                MessageHandler.setMessageBody(systemMessageCaption, null), HttpStatus.OK);
    }


    @RequestMapping(value = "/forgetPassword/sendOtp", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> forgetPasswordCustomerSendOtp(HttpServletRequest request, @RequestBody CustomerAccountResources accountResource) {
        /*
         *Find operation language if null default english
         */
        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");
        /*
         *Check null and empty values from client
         */
        if (accountResource.getUsername() == null
                || accountResource.getUsername().isEmpty()) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }

        String countryCode = accountResource.getCountryCode();

        CustomerAccount currentAccount = null;

        if (countryCode == null || countryCode.isEmpty()) {
            //username is email
            if (!Utils.isEmailCorrect(accountResource.getUsername()))
                throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email302", operationLanguage.getCode(), null);

            currentAccount = customerAccountService.findByEmail(accountResource.getUsername());
            if (currentAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "email404", operationLanguage.getCode(), null);
            }


        } else {
            //username is mobile
            String mobile = Utils.StandardPhoneFormat(accountResource.getCountryCode(), accountResource.getUsername());
            accountResource.setMobile(mobile);

            currentAccount = customerAccountService.findByMobile(mobile);
            if (currentAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mobile404", operationLanguage.getCode(), null);
            }

        }

        Status status = statusService.findByCode(currentAccount.getStatus().getCode());

        if (status.getCode() == "CUS_INACTIVE") {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        /*
         *Create OTP object to pass throw service and persist in database
         */
        OTP otp = new OTP();
        otp.setMobile(currentAccount.getMobile());

        otp.setEmail(accountResource.getEmail());
        otp = otpService.createOTP(otp);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageCaption =
                systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);

        return new ResponseEntity<>(
                MessageHandler.setMessageBody(systemMessageCaption, new OTPResources().toResources(otp)), HttpStatus.CREATED);
    }


    @RequestMapping(value = "/promo/filter", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> customerFilterPromo(HttpServletRequest request,
                                                           @RequestParam(name = "industryId", required = false) Long industryId,
                                                           @RequestParam(name = "merchantId", required = false) Long merchantId,
                                                           @RequestParam(name = "name", required = false) String name,
                                                           @RequestParam(name = "trendFlag", required = false) Integer trendFlag,
                                                           @RequestParam(name = "longitude", required = false) String longitude,
                                                           @RequestParam(name = "latitude", required = false) String latitude,
                                                           @RequestParam(name = "page", required = false) Integer page,
                                                           @RequestParam(name = "size", required = false) Integer size) {

        String lang = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(lang);
        if (operationLanguage == null) {

            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;
        if (!isCustomerAccount) {

            mediaAccount = null;/*mediaAccountService.findById((userId))*/

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", lang, null);
            }
        }


        Industry industry = industryService.findById(industryId);

        Merchant merchant = merchantService.findById(merchantId);

        Status status = statusService.findByCode("PRO_ACTIVE");

        LocalDateTime startDate = LocalDate.now().atStartOfDay();

        LocalDateTime expiaryDate = LocalDate.now().atStartOfDay();

        Page<Promo> promoPage = promoService.filterCustomerPromoAll(
                (String) Utils.handleEmptyOrNullObject(name),
                industry,
                merchant,
                status,
                trendFlag,
                startDate,
                expiaryDate,
                Utils.parseDouble(longitude),
                Utils.parseDouble(latitude),
                (1D),
                page,
                size);

        List<PromoResources> promoResources = PromoResources.toResources(promoPage.getContent());

        OperationLanguage finalOperationLanguage = operationLanguage;
        promoResources.forEach(promoResourceObject -> {
            promoResourceObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoPage.getTotalElements());
        pageResource.setTotalPages(promoPage.getTotalPages());
        pageResource.setPageList(promoResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
    }


    @RequestMapping(value = "/promoItem/filter", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> customerFilterPromoItem(HttpServletRequest request,
                                                               @RequestParam(name = "industryId", required = false) Long industryId,
                                                               @RequestParam(name = "merchantId", required = false) Long merchantId,
                                                               @RequestParam(name = "categoryId", required = false) Long categoryId,
                                                               @RequestParam(name = "name", required = false) String name,
                                                               @RequestParam(name = "priceFrom", required = false) Double newPriceFrom,
                                                               @RequestParam(name = "priceTo", required = false) Double newPriceTo,
                                                               @RequestParam(name = "trendFlag", required = false) Integer trendFlag,
                                                               @RequestParam(name = "watchListFlag", required = false) Integer watchListFlag,
                                                               @RequestParam(name = "wishListFlag", required = false) Integer wishListFlag,
                                                               @RequestParam(name = "longitude", required = false) String longitude,
                                                               @RequestParam(name = "latitude", required = false) String latitude,
                                                               @RequestParam(name = "page", required = false) Integer page,
                                                               @RequestParam(name = "size", required = false) Integer size)  {

        String lang = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(lang);
        if (operationLanguage == null) {

            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }


        CustomerAccount account = null;
        MediaAccount mediaAccount = null;

        Page<PromoItem> promoItemPage = null;
        List<PromoItemResources> promoItemResources = null;

        Industry industry = industryService.findById(industryId);
        Merchant merchant = merchantService.findById(merchantId);
        ItemCategory itemCategory = itemCategoryService.findById(categoryId);
        Status status = statusService.findByCode("PIT_ACTIVE");
        LocalDateTime startDate = LocalDate.now().atStartOfDay();
        LocalDateTime expiaryDate = LocalDate.now().atStartOfDay();

        Double[] priceInterval = Utils.initPriceIntervalValue(newPriceFrom, newPriceTo);

        if (watchListFlag == null)
            watchListFlag = 0;

        if (wishListFlag == null)
            wishListFlag = 0;

        if (!isCustomerAccount) {

            mediaAccount = mediaAccountService.findById((userId));

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", lang, null);
            }

            if (watchListFlag == 1) {
                //find watch list
                promoItemPage = promoItemService.filterMediaPromoItemWatchList(
                        (String) Utils.handleEmptyOrNullObject(name),
                        industry,
                        merchant,
                        itemCategory,
                        status,
                        mediaAccount,
                        priceInterval[0],
                        priceInterval[1],
                        trendFlag,
                        startDate,
                        expiaryDate,
                        Utils.parseDouble(longitude),
                        Utils.parseDouble(latitude),
                        (1D),
                        page,
                        size);
            } else {
                if (wishListFlag == 1) {
                    //find wish list
                    wishListHandlerService.refreshMediaWishList(mediaAccount);
                    promoItemPage = promoItemService.filterMediaPromoItemWishList(
                            (String) Utils.handleEmptyOrNullObject(name),
                            industry,
                            merchant,
                            itemCategory,
                            status,
                            mediaAccount,
                            priceInterval[0],
                            priceInterval[1],
                            trendFlag,
                            startDate,
                            expiaryDate,
                            Utils.parseDouble(longitude),
                            Utils.parseDouble(latitude),
                            (1D),
                            page,
                            size);
                } else {
                    //find all
                    promoItemPage = promoItemService.filterCustomerPromoItemAll(
                            (String) Utils.handleEmptyOrNullObject(name),
                            industry,
                            merchant,
                            itemCategory,
                            status,
                            priceInterval[0],
                            priceInterval[1],
                            trendFlag,
                            startDate,
                            expiaryDate,
                            Utils.parseDouble(longitude),
                            Utils.parseDouble(latitude),
                            (1D),
                            page,
                            size);
                }
            }

            promoItemResources = PromoItemResources.toResources(promoItemPage.getContent());

            OperationLanguage finalOperationLanguage = operationLanguage;
            MediaAccount finalMediaAccount = mediaAccount;
            promoItemResources.forEach(promoItemResourcesObject -> {
                promoItemResourcesObject.initResourceCaption(applicationContext, finalOperationLanguage, null, finalMediaAccount, null, null);
            });
        } else {
            account = customerAccountService.findById((userId));

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", lang, null);
            }

            if (watchListFlag == 1) {
                //find watch
                promoItemPage = promoItemService.filterCustomerPromoItemWatchList(
                        (String) Utils.handleEmptyOrNullObject(name),
                        industry,
                        merchant,
                        itemCategory,
                        status,
                        account,
                        priceInterval[0],
                        priceInterval[1],
                        trendFlag,
                        startDate,
                        expiaryDate,
                        Utils.parseDouble(longitude),
                        Utils.parseDouble(latitude),
                        (1D),
                        page,
                        size);
            } else {
                if (wishListFlag == 1) {
                    //find wish
                    wishListHandlerService.refreshCustomerWishList(account);

                    promoItemPage = promoItemService.filterCustomerPromoItemWishList(
                            (String) Utils.handleEmptyOrNullObject(name),
                            industry,
                            merchant,
                            itemCategory,
                            status,
                            account,
                            priceInterval[0],
                            priceInterval[1],
                            trendFlag,
                            startDate,
                            expiaryDate,
                            Utils.parseDouble(longitude),
                            Utils.parseDouble(latitude),
                            (1D),
                            page,
                            size);
                } else {
                    //find all
                    promoItemPage = promoItemService.filterCustomerPromoItemAll(
                            (String) Utils.handleEmptyOrNullObject(name),
                            industry,
                            merchant,
                            itemCategory,
                            status,
                            priceInterval[0],
                            priceInterval[1],
                            trendFlag,
                            startDate,
                            expiaryDate,
                            Utils.parseDouble(longitude),
                            Utils.parseDouble(latitude),
                            (1D),
                            page,
                            size);
                }
            }

            promoItemResources = PromoItemResources.toResources(promoItemPage.getContent());

            OperationLanguage finalOperationLanguage = operationLanguage;
            CustomerAccount finalAccount = account;
            promoItemResources.forEach(promoItemResourcesObject -> {
                promoItemResourcesObject.initResourceCaption(applicationContext, finalOperationLanguage, finalAccount, null, null, null);
            });
        }

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoItemPage.getTotalElements());
        pageResource.setTotalPages(promoItemPage.getTotalPages());
        pageResource.setPageList(promoItemResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
    }

    @RequestMapping(value = "/getUserByToken", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getUserByToken(HttpServletRequest request) {

        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", languageCode, null);
        }

        if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", languageCode, null);
        }


        Long currentUser = account.getId();
        if (currentUser == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "user404", operationLanguage.getCode(), null);
        }


        CustomerAccountResources customerAccountResources = CustomerAccountResources.toResources(account);

        customerAccountResources.initResoucreCaption(applicationContext, operationLanguage);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, customerAccountResources), HttpStatus.OK);

    }


    @RequestMapping(value = "/updateUser", method = RequestMethod.PUT)
    public ResponseEntity<MessageBody> updateUserInformation(HttpServletRequest request, @RequestBody CustomerAccountResources customerAccountResources) {

        String languageCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(languageCode);
        if (operationLanguage == null)
            operationLanguage = operationLanguageService.findByCode("en");

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        boolean isCustomerAccount = true;
        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;
        if (!isCustomerAccount) {

            mediaAccount = null;/*mediaAccountService.findById((userId))*/

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", languageCode, null);
            }
        }

        if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", languageCode, null);
        }

        Long currentUser = account.getId();
        if (currentUser == null) {

            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "user404", operationLanguage.getCode(), null);
        }


        if (customerAccountResources.getGender() == null || customerAccountResources.getGender().isEmpty()) {
            customerAccountResources.setGender(account.getGender());
        }
        account.setGender(customerAccountResources.getGender());
        if (customerAccountResources.getBirthDate() == null) {
            customerAccountResources.setBirthDate(account.getBirthDate());
        }
        account.setBirthDate(customerAccountResources.getBirthDate());
        if (customerAccountResources.getFirstName() == null || customerAccountResources.getFirstName().isEmpty()) {
            customerAccountResources.setFirstName(account.getFirstName());
        }
        account.setFirstName(customerAccountResources.getFirstName());

        if (customerAccountResources.getLastName() == null || customerAccountResources.getLastName().isEmpty()) {
            customerAccountResources.setLastName(account.getLastName());
        }
        account.setLastName(customerAccountResources.getLastName());

        if (customerAccountResources.getApartmentNumber() == null) {
            customerAccountResources.setApartmentNumber(account.getApartmentNumber());
        }
        account.setApartmentNumber(customerAccountResources.getApartmentNumber());


        if (customerAccountResources.getCountryId() == null) {
            customerAccountResources.setCountryId(customerAccountResources.getCountryId());
        } else {
            Country country = countryService.findById(customerAccountResources.getCountryId());
            account.setCountry(country);
        }

        CustomerAccountResources customerAccountRes = CustomerAccountResources.toResources(account);
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, customerAccountRes), HttpStatus.OK);

    }


    @RequestMapping(value = "/wish/create", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> createWish(@RequestBody CustomerWishListResources wishListResources,
                                                  HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (wishListResources.getAliasName() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "404", operationLanguage.getCode(), null);

//        if ((wishListResources.getCategoryId() == null || wishListResources.getCategoryId() != 0)
//                && (wishListResources.getMerchantId() == null || wishListResources.getMerchantId() != 0)
//                && (wishListResources.getTags() == null || wishListResources.getTags().isEmpty())) {
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "404", operationLanguage.getCode(), null);
//        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }

        CustomerAccount account = null;
        MediaAccount mediaAccount = null;

        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById(userId);

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!mediaAccount.getStatus().getCode().equals("MAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            Status mediaStatus = statusService.findByCode("CWI_ACTIVE");
            if (mediaStatus == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", LangCode, null);

            Merchant merchant = null;
            if (wishListResources.getMerchantId() != null && wishListResources.getMerchantId() != 0) {
                merchant = merchantService.findById(wishListResources.getMerchantId());
                if (merchant == null)
                    throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", LangCode, null);
            }

            ItemCategory itemCategory = null;

            if (wishListResources.getCategoryId() != null && wishListResources.getCategoryId() != 0) {
                itemCategory = itemCategoryService.findById(wishListResources.getCategoryId());
                if (itemCategory == null)
                    throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "cat404", LangCode, null);
            }


            MediaWishList mediaWishList = new MediaWishList();
            mediaWishList.setAliasName(wishListResources.getAliasName());
            mediaWishList.setCreateDate(LocalDateTime.now());
            mediaWishList.setTags(wishListResources.getTags());
            mediaWishList.setMediaAccount(mediaAccount);
            mediaWishList.setMerchant(merchant);
            mediaWishList.setCategory(itemCategory);
            mediaWishList.setStatus(mediaStatus);
            mediaWishList = mediaWishListService.create(mediaWishList);
            MediaWishListResources resources = MediaWishListResources.toResources(mediaWishList);

            resources.initResoucreCaption(applicationContext, operationLanguage);

            SystemMessage message = systemMessageService.findByCode("201");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.CREATED);


        } else {
            account = customerAccountService.findById(userId);

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            Status accountStatus = statusService.findByCode("CWI_ACTIVE");
            if (accountStatus == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", LangCode, null);

            Merchant merchant = null;
            if (wishListResources.getMerchantId() != null && wishListResources.getMerchantId() != 0) {
                merchant = merchantService.findById(wishListResources.getMerchantId());
                if (merchant == null)
                    throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", LangCode, null);
            }

            ItemCategory itemCategory = null;

            if (wishListResources.getCategoryId() != null && wishListResources.getCategoryId() != 0) {
                itemCategory = itemCategoryService.findById(wishListResources.getCategoryId());
                if (itemCategory == null)
                    throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", LangCode, null);
            }

            CustomerWishList customerWishList = new CustomerWishList();
            customerWishList.setCategory(itemCategory);
            customerWishList.setCreateDate(LocalDateTime.now());
            customerWishList.setCustomerAccount(account);
            customerWishList.setMerchant(merchant);
            customerWishList.setAliasName(wishListResources.getAliasName());
            customerWishList.setTags(wishListResources.getTags());
            customerWishList.setStatus(accountStatus);
            customerWishList = customerWishListService.create(customerWishList);
            CustomerWishListResources resources = CustomerWishListResources.toResources(customerWishList);

            resources.initResoucreCaption(applicationContext, operationLanguage);

            SystemMessage message = systemMessageService.findByCode("201");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.CREATED);

        }

    }

    @RequestMapping(value = "/wish/delete", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> deleteWish(@RequestBody CustomerWishListResources wishListResources,
                                                  HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (wishListResources.getId() == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", LangCode, null);

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }

        CustomerAccount account = null;
        MediaAccount mediaAccount = null;

        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById(userId);

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!mediaAccount.getStatus().getCode().equals("MAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            Status mediaStatus = statusService.findByCode("MWI_DELETE");
            if (mediaStatus == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", LangCode, null);

            MediaWishList mediaWishList = mediaWishListService.findById(wishListResources.getId());
            if (mediaWishList == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "wish404", LangCode, null);

            mediaWishList.setStatus(mediaStatus);
            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.OK);

        } else {
            account = customerAccountService.findById(userId);

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            Status accountStatus = statusService.findByCode("CWI_DELETE");
            if (accountStatus == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", LangCode, null);


            CustomerWishList customerWishList = customerWishListService.findById(wishListResources.getId());
            if (customerWishList == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "wish404", LangCode, null);
            customerWishList.setStatus(accountStatus);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.OK);

        }


    }


    @RequestMapping(value = "/wish/findAll", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAll(HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }

        CustomerAccount account = null;
        MediaAccount mediaAccount = null;

        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById(userId);

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!mediaAccount.getStatus().getCode().equals("MAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            Status mediaStatus = statusService.findByCode("CWI_ACTIVE");
            if (mediaStatus == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", LangCode, null);

            List<MediaWishList> wishList = mediaWishListService.findAll(mediaAccount, mediaStatus);
            List<MediaWishListResources> resources = MediaWishListResources.toResources(wishList);
            OperationLanguage finalOperationLanguage = operationLanguage;
            resources.forEach(mediaWishListResources -> {
                mediaWishListResources.initResoucreCaption(applicationContext, finalOperationLanguage);
            });
            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

        } else {
            account = customerAccountService.findById(userId);

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            Status accountStatus = statusService.findByCode("CWI_ACTIVE");
            if (accountStatus == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", LangCode, null);

            List<CustomerWishList> wishLists = customerWishListService.findAll(account, accountStatus);
            List<CustomerWishListResources> resources = CustomerWishListResources.toResources(wishLists);
            OperationLanguage finalOperationLanguage1 = operationLanguage;
            resources.forEach(wishListResources -> {
                wishListResources.initResoucreCaption(applicationContext, finalOperationLanguage1);
            });

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

        }

    }


    @RequestMapping(value = "/changeLang", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> changeCustomerLanguage(@RequestBody CustomerAccountResources customerAccountResources, HttpServletRequest request) {

        String langCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(langCode);

        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }

        CustomerAccount account = null;
        MediaAccount mediaAccount = null;

        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById(userId);

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (!mediaAccount.getStatus().getCode().equals("MAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (customerAccountResources.getLanguageCode() == null)
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", langCode, null);

            /*
             *Find operation language if null default english
             */
            OperationLanguage language = operationLanguageService.findByCode(customerAccountResources.getLanguageCode());
            if (language == null)
                language = operationLanguageService.findByCode("en");

            mediaAccount.setOperationLanguage(language);
            mediaAccountService.changeLanguage(mediaAccount, language);


            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.OK);


        } else {
            account = customerAccountService.findById(userId);

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (customerAccountResources.getLanguageCode() == null)
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", langCode, null);

            OperationLanguage language = operationLanguageService.findByCode(customerAccountResources.getLanguageCode());
            if (language == null)
                language = operationLanguageService.findByCode("en");

//            account.setOperationLanguage(language);
            customerAccountService.changeLanguage(account, language);

            CustomerAccountResources accountResources = CustomerAccountResources.toResources(account);
            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, language);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, accountResources), HttpStatus.OK);

        }
    }


    @RequestMapping(value = "/updateLocation", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> updateLocation(@RequestBody CustomerAccountResources customerAccountResources, HttpServletRequest request) {

        String langCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(langCode);

        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }

        CustomerAccount account = null;
        MediaAccount mediaAccount = null;

        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById(userId);

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (!mediaAccount.getStatus().getCode().equals("MAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (customerAccountResources.getLatitude() == null ||
                    customerAccountResources.getLongitude() == null) {
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", langCode, null);
            }

            mediaAccount.setLatitude(customerAccountResources.getLatitude());
            mediaAccount.setLongitude(customerAccountResources.getLongitude());
            MediaAccount accountInfo = mediaAccountService.updateAccountInfo(mediaAccount);

            MediaAccountResources accountResources = MediaAccountResources.toResources(mediaAccount);
            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, accountResources), HttpStatus.OK);

        } else {
            account = customerAccountService.findById(userId);

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }


            if (customerAccountResources.getLatitude() == null ||
                    customerAccountResources.getLongitude() == null) {
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", langCode, null);
            }

            account.setLatitude(customerAccountResources.getLatitude());
            account.setLongitude(customerAccountResources.getLongitude());
            CustomerAccount customerAccount = customerAccountService.updateAccountInfo(account);

            CustomerAccountResources accountResources = CustomerAccountResources.toResources(account);
            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, accountResources), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/compare", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> comparePromoItem(@RequestParam("itemId") Long id,
                                                        @RequestParam(name = "page", required = false) Integer page,
                                                        @RequestParam(name = "size", required = false) Integer size,
                                                        @RequestParam(name = "longitude", required = false) String longitude,
                                                        @RequestParam(name = "latitude", required = false) String latitude,
                                                        HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (id == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;
        if (isCustomerAccount) {
            PromoItem promoItem = promoItemService.findById(id);
            if (promoItem == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", operationLanguage.getCode(), null);

            PromoItemCaption promoItemCaption = promoItemCaptionService.find(promoItem, languageService.findByCode("en"));
            String name = promoItemCaption.getName();

            Page<PromoItem> promoItemList = promoItemService.findByCompare(promoItem,
                    Utils.parseDouble(longitude),
                    Utils.parseDouble(latitude),
                    (1D),
                    page,
                    size);
            List<PromoItemResources> resources = PromoItemResources.toResources(promoItemList.getContent());
            OperationLanguage finalOperationLanguage = operationLanguage;
            resources.forEach(promoItemResources -> {
                promoItemResources.initResourceCaption(applicationContext, finalOperationLanguage, account, null, null, null);
            });
            PageResource pageResource = new PageResource();
            pageResource.setCount(promoItemList.getTotalElements());
            pageResource.setTotalPages(promoItemList.getTotalPages());
            pageResource.setPageList(resources);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);

        } else {
            mediaAccount = mediaAccountService.findById((userId));
            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }
            PromoItem promoItem = promoItemService.findById(id);
            if (promoItem == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", operationLanguage.getCode(), null);

            Page<PromoItem> promoItemList = promoItemService.findByCompare(promoItem,
                    Utils.parseDouble(longitude),
                    Utils.parseDouble(latitude),
                    (1D),
                    page,
                    size);
            List<PromoItemResources> resources = PromoItemResources.toResources(promoItemList.getContent());
            OperationLanguage finalOperationLanguage = operationLanguage;
            MediaAccount finalMediaAccount = mediaAccount;
            resources.forEach(promoItemResources -> {
                promoItemResources.initResourceCaption(applicationContext, finalOperationLanguage, null, finalMediaAccount, null, null);
            });
            PageResource pageResource = new PageResource();
            pageResource.setCount(promoItemList.getTotalElements());
            pageResource.setTotalPages(promoItemList.getTotalPages());
            pageResource.setPageList(resources);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);

        }

    }


    @RequestMapping(value = "/updateNotification/status", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> updateNotificationStatus(@RequestParam(name = "promoId") Long promoId,
                                                                @RequestParam(name = "NotificationId") Long NotificationId,
                                                                HttpServletRequest request) {

        String langCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(langCode);

        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }


        CustomerAccount account = null;
        MediaAccount mediaAccount = null;
        Status status = null;

        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById(userId);

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (!mediaAccount.getStatus().getCode().equals("MAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }

            if (promoId == null ||
                    NotificationId == null) {
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", langCode, null);
            }

            Promo promo = promoService.findById(promoId);
            if (promo == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pro404", operationLanguage.getCode(), null);
            }

            promo.setViewNo(promo.getViewNo() + 1L);
            MediaNotification mediaNotification = mediaNotificationService.findById(NotificationId);

            if (mediaNotification == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "not404", operationLanguage.getCode(), null);
            }


            status = statusService.findByCode("NOT_SEEN");
            if (status == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
            }

            mediaNotification.setStatus(status);

            PromoResources promoResources = PromoResources.toResources(promo);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, promoResources), HttpStatus.OK);
        } else {
            account = customerAccountService.findById((userId));

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", langCode, null);
            }
            Promo promo = promoService.findById(promoId);
            if (promo == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", operationLanguage.getCode(), null);
            }

            promo.setViewNo(promo.getViewNo() + 1L);
            CustomerNotification customerNotification = customerNotificationService.findById(NotificationId);

            if (customerNotification == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", operationLanguage.getCode(), null);
            }


            status = statusService.findByCode("NOT_SEEN");
            /*
             Get segment request
            */
            SegmentNotificationRequest segmentNotificationRequest = customerNotification.getSegmentNotificationRequest();
            /*
             update segment request view counter

            */
            segmentNotificationRequest.setTotalView(segmentNotificationRequest.getTotalView() + 1);
            if (status == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", operationLanguage.getCode(), null);
            }

            customerNotification.setStatus(status);

            PromoResources promoResources = PromoResources.toResources(promo);

            promoResources.initResoucreCaption(applicationContext, operationLanguage);

            List<PromoResources> promoItemResourcesList = new ArrayList<>();
            promoItemResourcesList.add(promoResources);

            PageResource pageResource = new PageResource();
            pageResource.setCount(1L);
            pageResource.setTotalPages(1);
            pageResource.setPageList(promoItemResourcesList);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/wish/findbyId", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findWishById(@RequestParam(name = "wishId") Long id,
                                                    HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        if (id == null || id == 0)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;

        if (isCustomerAccount) {

            account.setOperationLanguage(operationLanguage);
            CustomerWishList customerWishList = customerWishListService.find(account, id);
            if (customerWishList == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "wish404", operationLanguage.getCode(), null);

            CustomerWishListResources resources = CustomerWishListResources.toResources(customerWishList);
            resources.initResoucreCaption(applicationContext, operationLanguage);
            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

        } else {
            mediaAccount = mediaAccountService.findById((userId));
            mediaAccount.setOperationLanguage(operationLanguage);
            MediaWishList mediaWishList = mediaWishListService.find(mediaAccount, id);
            if (mediaWishList == null)
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

            MediaWishListResources resources = MediaWishListResources.toResources(mediaWishList);
            resources.initResoucreCaption(applicationContext, operationLanguage);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/wish/update", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> updateWish(@RequestBody CustomerWishListResources wishListResources,
                                                  HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (wishListResources.getAliasName() == null
                || wishListResources.getId() == null
                || wishListResources.getId() == 0)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "404", operationLanguage.getCode(), null);

//        if ((wishListResources.getCategoryId() == null || wishListResources.getCategoryId() == 0)
//                && (wishListResources.getMerchantId() == null || wishListResources.getMerchantId() == 0)
//                && (wishListResources.getTags() == null || wishListResources.getTags().isEmpty())) {
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "404", operationLanguage.getCode(), null);
//        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;

        if (isCustomerAccount) {

            account.setOperationLanguage(operationLanguage);
            CustomerWishList customerWishList = customerWishListService.find(account, wishListResources.getId());
            if (customerWishList == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "wish404", operationLanguage.getCode(), null);

            customerWishList.setAliasName(wishListResources.getAliasName());
            customerWishList.setTags(wishListResources.getTags());

            if (wishListResources.getCategoryId() != null && wishListResources.getCategoryId() != 0) {
                ItemCategory category = itemCategoryService.findById(wishListResources.getCategoryId());

                if (category == null)
                    throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "cat404", operationLanguage.getCode(), null);

                customerWishList.setCategory(category);
            }

            if (wishListResources.getMerchantId() != null && wishListResources.getMerchantId() != 0) {
                Merchant merchant = merchantService.findById(wishListResources.getMerchantId());

                if (merchant == null)
                    throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

                customerWishList.setMerchant(merchant);
            }

            CustomerWishListResources resources = CustomerWishListResources.toResources(customerWishList);
            resources.initResoucreCaption(applicationContext, operationLanguage);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

        } else {
            mediaAccount = mediaAccountService.findById((userId));
            mediaAccount.setOperationLanguage(operationLanguage);
            MediaWishList mediaWishList = mediaWishListService.find(mediaAccount, wishListResources.getId());
            if (mediaWishList == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "wish404", operationLanguage.getCode(), null);

            mediaWishList.setAliasName(wishListResources.getAliasName());
            mediaWishList.setTags(wishListResources.getTags());

            if (wishListResources.getCategoryId() != null && wishListResources.getCategoryId() != 0) {
                ItemCategory category = itemCategoryService.findById(wishListResources.getCategoryId());

                if (category == null)
                    throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "cat404", operationLanguage.getCode(), null);

                mediaWishList.setCategory(category);
            }
            if (wishListResources.getMerchantId() != null && wishListResources.getMerchantId() != 0) {
                Merchant merchant = merchantService.findById(wishListResources.getMerchantId());

                if (merchant == null)
                    throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "mer404", operationLanguage.getCode(), null);

                mediaWishList.setMerchant(merchant);
            }

            MediaWishListResources resources = MediaWishListResources.toResources(mediaWishList);
            resources.initResoucreCaption(applicationContext, operationLanguage);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
        }


    }


    @RequestMapping(value = "/allCategory", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAllCategory(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        List<ItemCategoryCaption> captionList = itemCategoryCaptionService.findAllByLang(operationLanguage);

        List<ItemCategoryCaptionResources> resources = ItemCategoryCaptionResources.toResources(captionList);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);


    }


    @RequestMapping(value = "/compare/new", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> comparePromoItemNew(@RequestParam("itemId") Long id,
                                                           @RequestParam("type") String type,
                                                           @RequestParam(name = "page", required = false) Integer page,
                                                           @RequestParam(name = "size", required = false) Integer size,
                                                           @RequestParam(name = "longitude", required = false) String longitude,
                                                           @RequestParam(name = "latitude", required = false) String latitude,
                                                           HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (id == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        CustomerAccount account = customerAccountService.findById((userId));
        boolean isCustomerAccount = true;
        if (account == null) {
            isCustomerAccount = false;
        }
        MediaAccount mediaAccount = null;

        PromoItem promoItem = promoItemService.findById(id);

        Double lng = 0.0;
        Double lat = 0.0;

        if (latitude != null && !latitude.isEmpty()) {
            lat = Double.parseDouble(longitude);
        }
        if (longitude != null && !longitude.isEmpty()) {
            lng = Double.parseDouble(latitude);
        }


        if (isCustomerAccount) {
            List<PromoItem> promoItemList = compareHandlerService.compare(promoItem, type, lat, lng);
            List<PromoItemResources> resources = PromoItemResources.toResources(promoItemList);

            OperationLanguage finalOperationLanguage = operationLanguage;
            resources.forEach(promoItemResourcesObject -> {
                promoItemResourcesObject.initResourceCaption(applicationContext, finalOperationLanguage, account, null, null, null);
            });

            PageResource pageResource = new PageResource();
            pageResource.setCount(Long.valueOf(promoItemList.size()));
            pageResource.setTotalPages(1);
            pageResource.setPageList(resources);
            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
        } else {
            mediaAccount = mediaAccountService.findById((userId));
            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }
            List<PromoItem> promoItemList = compareHandlerService.compare(promoItem, type, Double.parseDouble(longitude), Double.parseDouble(latitude));
            List<PromoItemResources> resources = PromoItemResources.toResources(promoItemList);

            OperationLanguage finalOperationLanguage = operationLanguage;
            MediaAccount finalMediaAccount = mediaAccount;
            resources.forEach(promoItemResourcesObject -> {
                promoItemResourcesObject.initResourceCaption(applicationContext, finalOperationLanguage, null, finalMediaAccount, null, null);
            });

            PageResource pageResource = new PageResource();
            pageResource.setCount(Long.valueOf(promoItemList.size()));
            pageResource.setTotalPages(1);
            pageResource.setPageList(resources);
            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
        }

    }

}