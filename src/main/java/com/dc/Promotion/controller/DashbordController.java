package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.*;
import com.dc.Promotion.security.AccountUserDetails;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/dashboard")
public class DashbordController {

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private PromoService promoService;

    @Autowired
    private PromoItemService promoItemService;

    @Autowired
    private MerchantUserService merchantUserService;

    @Autowired
    private ItemCategoryService itemCategoryService;

    @Autowired
    private MerchantCustomerSegmentService merchantCustomerSegmentService;

    @Autowired
    private SegmentNotificationRequestService segmentNotificationRequestService;

    @RequestMapping(value = "/allData", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getData(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);


        List<Promo> promoList = promoService.top3Promo(merchantUser.getMerchant());
        List<PromoResources> promoResourcesList = PromoResources.toResources(promoList);


        OperationLanguage finalOperationLanguage = operationLanguage;
        List<Promo> promos = promoList;
        promoResourcesList.forEach(promoResourceObject -> {
            promoResourceObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });


        Long promoNumber = promoService.totalPromo(merchantUser.getMerchant());
        Long promoView = promoService.sumOfView(merchantUser.getMerchant());
        if(promoView==null)
            promoView=0L;


        List<PromoItem> promoItemList = promoItemService.top5PromoItem(merchantUser.getMerchant());
        List<PromoItemResources> promoItemResources = PromoItemResources.toResources(promoItemList);
        Long promoItemNumber = promoItemService.totalPromoItem(merchantUser.getMerchant());


        OperationLanguage language = operationLanguage;
        List<PromoItem> promoItems = promoItemList;
        promoItemResources.forEach(promoResourceObject -> {
            promoResourceObject.initResourceCaption(applicationContext, finalOperationLanguage,null,null,null,null);
        });


        List<ItemCategory> itemCategoryList = itemCategoryService.findTopViewCategory(merchantUser.getMerchant());
        List<ItemCategoryResources> itemCategoryResourcesList = ItemCategoryResources.toResources(itemCategoryList);

        Long totalViews = Utils.findTotalCategoryViews(itemCategoryList);

        OperationLanguage finalOperationLanguage1 = operationLanguage;
        itemCategoryResourcesList.forEach(itemCategoryResources -> {
            itemCategoryResources.initResourceCaption(applicationContext, finalOperationLanguage1,totalViews);
        });


        //static for completing dashboard service from client
        List<MerchantCustomerSegment> merchantCustomerSegmentList = merchantCustomerSegmentService.findTopSegmant(merchantUser.getMerchant());

        merchantCustomerSegmentList.forEach(merchantCustomerSegment -> {
            System.out.println("merchantCustomerSegment: "+merchantCustomerSegment.getId());
            Long totalCustomer = segmentNotificationRequestService.findTotalCustomer(merchantCustomerSegment);
            Long totalNotification = segmentNotificationRequestService.findTotalNotification(merchantCustomerSegment);
            Long totalView = segmentNotificationRequestService.findTotalViews(merchantCustomerSegment);

            merchantCustomerSegment.setTotalCustomer(totalCustomer);
            merchantCustomerSegment.setTotalNotification(totalNotification);
            merchantCustomerSegment.setTotalViews(totalView);
        });

        Collections.sort(merchantCustomerSegmentList, new Comparator<MerchantCustomerSegment>() {
            @Override
            public int compare(MerchantCustomerSegment u1, MerchantCustomerSegment u2) {
                return u2.getTotalCustomer().compareTo(u1.getTotalCustomer());
            }
        });


        List<MerchantCustomerSegmentResources> merchantCustomerSegmentResources = MerchantCustomerSegmentResources.toResources(merchantCustomerSegmentList);

        DashbordResources resources = new DashbordResources();
        resources.setTopPromo(promoResourcesList);
        resources.setTopPromoItem(promoItemResources);
        resources.setTotalPromo(promoNumber);
        resources.setTotalPromoItem(promoItemNumber);
        resources.setTotalView(promoView);
        resources.setTopItemCategory(itemCategoryResourcesList);
        resources.setTopSegment(merchantCustomerSegmentResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

}
