package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.PageResource;
import com.dc.Promotion.resources.PromoItemResources;
import com.dc.Promotion.security.AccountUserDetails;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/item")
public class WishListController {


    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private WishListHandlerService wishListHandlerService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MediaAccountService mediaAccountService;

    @Autowired
    private CustomerAccountService customerAccountService;

    @Autowired
    private CustomerWishListService customerWishListService;

    @Autowired
    private MediaWishListService mediaWishListService;


    @RequestMapping(value = "/getwish", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getWishList(@RequestParam("wishId") Long id,
                                                   @RequestParam(name = "longitude", required = false) String longitude,
                                                   @RequestParam(name = "latitude", required = false) String latitude,
                                                   HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }

        CustomerAccount account = null;
        MediaAccount mediaAccount = null;

        if (!isCustomerAccount) {

            mediaAccount = mediaAccountService.findById(userId);

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!mediaAccount.getStatus().getCode().equals("MAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (id == null || id == 0)
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "404", operationLanguage.getCode(), null);

            MediaWishList mediaWishList = mediaWishListService.findById(id);
            if (mediaWishList == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", LangCode, null);

            List<PromoItem> promoItemList = wishListHandlerService.findPromoItemByMediaWish(mediaWishList);
            List<PromoItemResources> resources = PromoItemResources.toResources(promoItemList);
            MediaAccount finalMediaAccount1 = mediaAccount;
            OperationLanguage finalOperationLanguage1 = operationLanguage;
            resources.forEach(promoItemResources -> {
                promoItemResources.initResourceCaption(applicationContext, finalOperationLanguage1,
                        null,
                        finalMediaAccount1,
                        Utils.parseDouble(longitude),
                        Utils.parseDouble(latitude));
            });


//            PageResource pageResource = new PageResource();
//            pageResource.setCount(promoPage.getTotalElements());
//            pageResource.setTotalPages(promoPage.getTotalPages());
//            pageResource.setPageList(promoResources);


            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
        } else {


            account = customerAccountService.findById(userId);

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (!account.getStatus().getCode().equals("CAC_ACTIVE")) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            if (id == null || id == 0)
                throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "404", operationLanguage.getCode(), null);

            CustomerWishList customerWishList = customerWishListService.findById(id);
            if (customerWishList == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", LangCode, null);

            List<PromoItem> promoItemList = wishListHandlerService.findPromoItemByCustomerWish(customerWishList);
            List<PromoItemResources> resources = PromoItemResources.toResources(promoItemList);
            MediaAccount finalMediaAccount = mediaAccount;
            OperationLanguage finalOperationLanguage = operationLanguage;
            resources.forEach(promoItemResources -> {
                promoItemResources.initResourceCaption(applicationContext,
                        finalOperationLanguage,
                        null,
                        finalMediaAccount,
                        Utils.parseDouble(longitude),
                        Utils.parseDouble(latitude));
            });


//            PageResource pageResource = new PageResource();
//            pageResource.setCount(promoPage.getTotalElements());
//            pageResource.setTotalPages(promoPage.getTotalPages());
//            pageResource.setPageList(promoResources);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
        }

    }


}
