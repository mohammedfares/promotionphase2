//package com.dc.Promotion.controller;
//
//import com.dc.Promotion.MessageBody;
//import com.dc.Promotion.entities.*;
//import com.dc.Promotion.exception.ResourceException;
//import com.dc.Promotion.resources.IndustryCaptionResources;
//import com.dc.Promotion.resources.MerchantRegistrationRequestResources;
//import com.dc.Promotion.resources.MerchantUserResources;
//import com.dc.Promotion.resources.StatusCaptionResources;
//import com.dc.Promotion.service.*;
//import com.dc.Promotion.utils.MessageHandler;
//import com.dc.Promotion.utils.SessionHelper;
//import com.dc.Promotion.utils.Utils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.ApplicationContext;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
//@CrossOrigin
//@RestController
//@RequestMapping("/merchant")
//public class RegistrationRequestController {
//
//    @Autowired
//    private SystemMessageService systemMessageService;
//    @Autowired
//    private SystemMessageCaptionService systemMessageCaptionService;
//    @Autowired
//    private ApplicationContext applicationContext;
//    @Autowired
//    private OperationLanguageService languageService;
//    @Autowired
//    private MerchantRegistrationRequestService registrationRequestService;
//    @Autowired
//    private MerchantUserService merchantUserService;
//    @Autowired
//    private StatusCaptionService statusCaptionService;
//    @Autowired
//    private StatusService statusService;
//    @Autowired
//    private IndustryCaptionService industryCaptionService;
//
//    @RequestMapping(value = "/register", method = RequestMethod.POST)
//    public ResponseEntity<MessageBody> createMerchantRequest(@RequestBody MerchantRegistrationRequestResources requestResources, HttpServletRequest request) {
//
//        String LangCode = request.getHeader("lng");
//        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
//        if (operationLanguage == null) {
//            operationLanguage = languageService.findByCode("en");
//        }
//
//        if (requestResources.getCrn() == null
//                || requestResources.getEmail() == null
//                || requestResources.getMobile() == null
//                || requestResources.getPassword() == null
//                || requestResources.getConfirmPassword() == null
//                || requestResources.getMerchantName() == null
//                || requestResources.getIndustryList() == null
//                || requestResources.getIndustryList().isEmpty()
//                || requestResources.getCountryCode() == null) {
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "MRR400", operationLanguage.getCode(), null);
//        }
//
//        String mobile = Utils.StandardPhoneFormat(requestResources.getCountryCode(), requestResources.getMobile());
//        if (mobile == null)
//            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "mobile406", operationLanguage.getCode(), null);
//
//        if (!Utils.isEmailCorrect(requestResources.getEmail()))
//            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "email406", operationLanguage.getCode(), null);
//
//        if (!requestResources.getPassword().equalsIgnoreCase(requestResources.getConfirmPassword()))
//            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);
//
//        requestResources.setMobile(mobile);
//        MerchantRegistrationRequest registrationRequest = requestResources.toMerchantRegistrationRequest();
//        registrationRequest.setOperationLanguage(operationLanguage);
//        registrationRequest = registrationRequestService.create(registrationRequest, requestResources.getIndustryList());
//
//        MerchantRegistrationRequestResources resources = MerchantRegistrationRequestResources.toResources(registrationRequest);
//        SystemMessage message = systemMessageService.findByCode("200");
//        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
//        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
//    }
//
//    @RequestMapping(value = "/approve", method = RequestMethod.POST)
//    public ResponseEntity<MessageBody> approveMerchantRequest(@RequestBody MerchantRegistrationRequestResources requestResources, HttpServletRequest request) {
//
//        String LangCode = request.getHeader("lng");
//        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
//        if (operationLanguage == null) {
//            operationLanguage = languageService.findByCode("en");
//        }
//
//        if (requestResources.getId() == null) {
//            //error code for not found data
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "MRR400", operationLanguage.getCode(), null);
//        }
//        MerchantRegistrationRequest registrationRequest = registrationRequestService.approveRequest(requestResources.getId(), operationLanguage);
//        MerchantRegistrationRequestResources resources = MerchantRegistrationRequestResources.toResources(registrationRequest);
//
//        SystemMessage message = systemMessageService.findByCode("200");
//        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
//        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
//    }
//
//    @RequestMapping(value = "/reject", method = RequestMethod.POST)
//    public ResponseEntity<MessageBody> rejectMerchantRequest(@RequestBody MerchantRegistrationRequestResources requestResources, HttpServletRequest request) {
//
//        String LangCode = request.getHeader("lng");
//        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
//        if (operationLanguage == null) {
//            operationLanguage = languageService.findByCode("en");
//        }
//
//        if (requestResources.getId() == null) {
//            //error code for not found data
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "MRR400", operationLanguage.getCode(), null);
//        }
//
//        MerchantRegistrationRequest registrationRequest = registrationRequestService.rejectRequest(requestResources.getId(), operationLanguage);
//        MerchantRegistrationRequestResources resources = MerchantRegistrationRequestResources.toResources(registrationRequest);
//
//        SystemMessage message = systemMessageService.findByCode("200");
//        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
//        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
//
//    }
//
//    @RequestMapping(value = "/login", method = RequestMethod.POST)
//    public ResponseEntity<MessageBody> merchantLogin(@RequestBody MerchantUserResources userResources, HttpServletRequest request) {
//
//        String LangCode = request.getHeader("lng");
//        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
//        if (operationLanguage == null) {
//            operationLanguage = languageService.findByCode("en");
//        }
//
//        if(userResources.getLoginUsername()==null
//        || userResources.getPassword()==null)
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
//
//        MerchantUser merchantUser = null;
//
//        // find Merchant User by email
//        if(Utils.isEmailCorrect(userResources.getEmail()))
//        {
//            merchantUser = merchantUserService.findByEmail(userResources.getEmail());
//            if (merchantUser == null)
//                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", operationLanguage.getCode(), null);
//
//            if (!userResources.getPassword().equalsIgnoreCase(merchantUser.getPassword()))
//                throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);
//        }
//        // find Merchant User by username
//        else
//        {
//            merchantUser=merchantUserService.findByUsername(userResources.getLoginUsername());
//            if (merchantUser == null)
//                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", operationLanguage.getCode(), null);
//
//            if (!userResources.getPassword().equalsIgnoreCase(merchantUser.getPassword()))
//                throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", operationLanguage.getCode(), null);
//        }
//        userResources.setToken(request.getSession().getId());
//        SessionHelper.insertSession(userResources.getToken(), userResources.getId() + "");
//        MerchantUserResources resources = MerchantUserResources.toResources(merchantUser);
//        resources.setToken(userResources.getToken());
//        SystemMessage message = systemMessageService.findByCode("200");
//        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
//        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
//
//    }
//
//    @RequestMapping(value = "/status", method = RequestMethod.GET)
//    public ResponseEntity<MessageBody> getStatus(HttpServletRequest request) {
//        String LangCode = request.getHeader("lng");
//        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
//        if (operationLanguage == null) {
//            operationLanguage = languageService.findByCode("en");
//        }
//
//        List<StatusCaption> statusCaptionList = statusCaptionService.find(operationLanguage);
//        List<StatusCaptionResources> resources = StatusCaptionResources.toResources(statusCaptionList);
//
//        SystemMessage message = systemMessageService.findByCode("200");
//        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
//        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
//
//    }
//
//    @RequestMapping(value = "/allReq", method = RequestMethod.POST)
//    public ResponseEntity<MessageBody> getAllMerchantRegistrationRequest(@RequestBody Status status, HttpServletRequest request) {
//        String LangCode = request.getHeader("lng");
//        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
//        if (operationLanguage == null) {
//            operationLanguage = languageService.findByCode("en");
//        }
//
//        Status sts = statusService.findById(status.getId());
//        if (status == null)
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "MRR400", operationLanguage.getCode(), null);
//
//        List<MerchantRegistrationRequest> requestList = registrationRequestService.findAllByStatus(sts);
//        List<MerchantRegistrationRequestResources> resources = MerchantRegistrationRequestResources.toResources(requestList);
//
//
//        for (MerchantRegistrationRequestResources resources1 : resources) {
//            for (MerchantRegistrationRequest registrationRequest : requestList)
//                resources1.setIndustryCaptionResources(IndustryCaptionResources.toResources(industryCaptionService.findByMRR(operationLanguage, registrationRequest)));
//
//            resources1.setStatusCaption(statusCaptionService.findByStatusAndLanguage(sts, operationLanguage).getName());
//        }
//
//        SystemMessage message = systemMessageService.findByCode("200");
//        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
//        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
//    }
//
//    @RequestMapping(value = "/deleteReq" ,method = RequestMethod.DELETE)
//    public ResponseEntity<MessageBody> getAllMerchantRegistrationRequest(@RequestBody MerchantRegistrationRequestResources requestResources, HttpServletRequest request) {
//        String LangCode = request.getHeader("lng");
//        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
//        if (operationLanguage == null) {
//            operationLanguage = languageService.findByCode("en");
//        }
//
//        if(requestResources.getId()==null)
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "MRR400", operationLanguage.getCode(), null);
//
//        MerchantRegistrationRequest registrationRequest=registrationRequestService.findById(requestResources.getId());
//        if(registrationRequest==null)
//            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "MRR400", operationLanguage.getCode(), null);
//       String DeleteStatus= registrationRequestService.deleteRequest(registrationRequest.getId());
//
//        SystemMessage message = systemMessageService.findByCode("200");
//        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
//        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, DeleteStatus), HttpStatus.OK);
//
//    }
//}
//
