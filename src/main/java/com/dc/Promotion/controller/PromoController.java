package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.PromoRepo;
import com.dc.Promotion.resources.ItemCategoryCaptionResources;
import com.dc.Promotion.resources.ItemSpecificationCaptionResources;
import com.dc.Promotion.resources.PromoCaptionResources;
import com.dc.Promotion.resources.PromoResources;
import com.dc.Promotion.resources.*;
//import com.dc.Promotion.security.SessionHelper;
import com.dc.Promotion.security.AccountUserDetails;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.method.P;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.rmi.CORBA.Util;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/promo")
public class PromoController {

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private MediaItemViewService mediaItemViewService;


    @Autowired
    private MediaAccountService mediaAccountService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private PromoService promoService;

    @Autowired
    private PromoCaptionService promoCaptionService;

    @Autowired
    private SpecificationCaptionService specificationCaptionService;

    @Autowired
    private ItemCategoryCaptionService itemCategoryCaptionService;

    @Autowired
    private MerchantUserService merchantUserService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private MerchantBranchService merchantBranchService;

    @Autowired
    private PromoBranchService promoBranchService;

    @Autowired
    private PromoItemService promoItemService;

    @Autowired
    private PromoItemCaptionService promoItemCaptionService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private CustomerAccountService customerAccountService;

    @Autowired
    private CustomerItemViewService customerItemViewService;

    @Autowired
    private CustomerPromoViewService customerPromoViewService;

    @Autowired
    private ItemCategoryService itemCategoryService;


    @RequestMapping(value = "/uploadPromo", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> uploadPromo(@RequestBody PromoResources promoResources,
                                                   HttpServletRequest request) {
        // find Language
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        // find Merchant User By Token
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);
        //end

        if (
                promoResources.getStartDate() == null ||
                        promoResources.getExpiryDate() == null ||
                        promoResources.getCoverPhoto() == null ||
                        promoResources.getBranchList() == null ||
                        promoResources.getBranchList().isEmpty() ||
                        promoResources.getCaptionList() == null ||
                        promoResources.getCaptionList().isEmpty()

        ) {
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        }
        if (promoResources.getExpiryDate().isBefore(promoResources.getStartDate())) {
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "date409", operationLanguage.getCode(), null);
        }
        //  create Promo

        Promo promo = promoResources.toPromo();
        promo.setExpiryDate(promoResources.getExpiryDate());
        promo.setMerchant(merchantUser.getMerchant());
        promo = promoService.createPromo(promo);

        promoCaptionService.deleteAllByPromo(promo);

        //end Create Promo
        //create Promo Caption
        List<PromoCaptionResources> promoCaptionResources = promoResources.getCaptionList();
        for (PromoCaptionResources promoCaptionResource : promoCaptionResources) {
            PromoCaption promoCaption = promoCaptionResource.toPromoCaption();
            promoCaption.setOperationLanguage(languageService.findById(promoCaptionResource.getLanguageId()));
            promoCaption.setPromo(promo);
            promoCaption = promoCaptionService.create(promoCaption);
        }

        //end Create Promo Caption
        promoItemService.deletePromoItemByPromo(promo);
        //delete promo items

        // create  promo item
        List<PromoItemResources> promoItemResourceList = promoResources.getPromoItemList();
        for (PromoItemResources promoItemResource : promoItemResourceList) {
            PromoItem promoItem = promoItemResource.toPromoItem();
            promoItem.setPromo(promo);
            promoItem.setExpiryDate(promo.getExpiryDate());
            promoItemService.create(promoItem, promoItemResource.getImageList(), promoItemResource.getCaptionList(), promoItemResource.getSpecificationList());
        }

        promoBranchService.deleteAllByPromo(promo);

        // save promo branch
        for (MerchantBranchResources merchantBranchResources : promoResources.getBranchList()) {
            MerchantBranch merchantBranch = merchantBranchService.findById(merchantBranchResources.getId());
            PromoBranch promoBranch = new PromoBranch();
            promoBranch.setBranch(merchantBranch);
            promoBranch.setPromo(promo);
            promoBranchService.create(promoBranch);
        }


        SystemMessage message = systemMessageService.findByCode("201");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/uploadTemp", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> uploadTempPromo(@RequestBody PromoResources promoResources,
                                                       HttpServletRequest request) {
        // find Language
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        Merchant merchant = merchantService.findById(merchantUser.getMerchant().getId());
        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        //save Promo Temp
        promoResources.setMerchantId(merchant.getId());
        Promo promo = promoResources.toPromo();
        promo.setOperationLanguage(operationLanguage);
        promo = promoService.createTemp(promo);
        //end

        promoCaptionService.deleteAllByPromo(promo);
        //save Promo Temp Caption
        for (PromoCaptionResources promoCaptionResources : promoResources.getCaptionList()) {
            PromoCaption promoCaption = promoCaptionResources.toPromoCaption();
            promoCaption.setOperationLanguage(languageService.findById(promoCaptionResources.getLanguageId()));
            promoCaption.setPromo(promo);
            promoCaption.setName(promoCaptionResources.getName());
            promoCaption.setDescription(promoCaptionResources.getDescription());
            promoCaptionService.create(promoCaption);
        }
        //end

        // save promo branch

        promoBranchService.deleteAllByPromo(promo);

        for (MerchantBranchResources merchantBranchResources : promoResources.getBranchList()) {
            MerchantBranch merchantBranch = merchantBranchService.findById(merchantBranchResources.getId());
            PromoBranch promoBranch = new PromoBranch();
            promoBranch.setBranch(merchantBranch);
            promoBranch.setPromo(promo);
            promoBranchService.create(promoBranch);
        }
        //end

        promoItemService.deletePromoItemByPromo(promo);

        if (promoResources.getPromoItemList() != null && !promoResources.getPromoItemList().isEmpty()) {
            for (PromoItemResources itemResources : promoResources.getPromoItemList()) {
                PromoItem promoItem = itemResources.toPromoItem();
                promoItem.setPromo(promo);
                promoItem.setOperationLanguage(operationLanguage);
                promoItemService.createTemp(promoItem, itemResources.getImageList(), itemResources.getCaptionList(), itemResources.getSpecificationList());
            }
        }

        SystemMessage message = systemMessageService.findByCode("201");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, systemMessageStatusCaption.getText()), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/allSpecification", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAllSpecification(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

//        List<SpecificationCaption> captionList = specificationCaptionService.allSpecification();
//
//        List<ItemSpecificationCaptionResources> resources = ItemSpecificationCaptionResources.toResources(captionList);


        // List<ItemSpecificationCaptionResources> resources = ItemSpecificationCaptionResources.toResources(captionList);

        List<OperationLanguage> languageList = languageService.findAll();
        List<OperationLanguageSpecsResources> resources = OperationLanguageSpecsResources.toResources(languageList);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/allCategory", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAllCategory(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        List<ItemCategory> categories = itemCategoryService.findAll();


        List<ItemCategoryResources> resources = ItemCategoryResources.toResources(categories);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);


    }

    @RequestMapping(value = "/item", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getPromoItem(@RequestParam("promoId") Long id, HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        /*
        find promo by promo Id
         */
        Promo promo = promoService.findById(id);
        if (promo == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pro404", operationLanguage.getCode(), null);

        /*
        find promoItem Caption by promo
         */

        List<PromoItemCaption> promoItemCaptionList = promoItemCaptionService.findAllByPromo(promo);
        List<PromoItemCaptionResources> resources = null;
        if (promoItemCaptionList != null)
            resources = PromoItemCaptionResources.toResources(promoItemCaptionList);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);


    }

    @RequestMapping(value = "/customer/promos", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getCustomerPromos(HttpServletRequest request,
                                                         @RequestParam(name = "merchantId", required = false) Long merchantId,
                                                         @RequestParam(name = "statusCode", required = false) String code,
                                                         @RequestParam(name = "page", required = false) Integer page,
                                                         @RequestParam(name = "size", required = false) Integer size) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;
        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById((userId));
            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }
        }

        Merchant merchant = merchantService.findById(merchantId);
        Status status = statusService.findByCode("PRO_ACTIVE");

        Promo searchCriteriaPromo = new Promo();
        searchCriteriaPromo.setStatus(status);
        searchCriteriaPromo.setMerchant(merchant);
        searchCriteriaPromo.setExpiryDate(LocalDateTime.now());

        Page<Promo> promoPage = promoService.findAll(searchCriteriaPromo, page, size);

        List<PromoResources> promoResources = PromoResources.toResources(promoPage.getContent());

        OperationLanguage finalOperationLanguage = operationLanguage;
        promoResources.forEach(promoResourceObject -> {
            promoResourceObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoPage.getTotalElements());
        pageResource.setTotalPages(promoPage.getTotalPages());
        pageResource.setPageList(promoResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
    }


    @RequestMapping(value = "/merchant/promos", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getMerchantPromos(HttpServletRequest request,
                                                         @RequestParam(name = "statusCode", required = false) String code,
                                                         @RequestParam(name = "isExpired", required = false) Integer isExpired,
                                                         @RequestParam(name = "page", required = false) Integer page,
                                                         @RequestParam(name = "size", required = false) Integer size) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        Merchant merchant = merchantUser.getMerchant();

        Status status = statusService.findByCode(code);

        Promo searchCriteriaPromo = new Promo();
        searchCriteriaPromo.setStatus(status);
        searchCriteriaPromo.setMerchant(merchant);
        searchCriteriaPromo.setExpiryDate((isExpired == null || isExpired == 0) ? null : LocalDateTime.now());
        Page<Promo> promoPage = promoService.findAll(searchCriteriaPromo, page, size);

        List<PromoResources> promoResources = PromoResources.toResources(promoPage.getContent());

        OperationLanguage finalOperationLanguage = operationLanguage;
        promoResources.forEach(promoResourceObject -> {
            promoResourceObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoPage.getTotalElements());
        pageResource.setTotalPages(promoPage.getTotalPages());
        pageResource.setPageList(promoResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
    }


    @RequestMapping(value = "/promos", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getPromos(HttpServletRequest request,
                                                 @RequestParam(name = "merchantId", required = false) Long merchantId,
                                                 @RequestParam(name = "statusCode", required = false) String code,
                                                 @RequestParam(name = "page", required = false) Integer page,
                                                 @RequestParam(name = "size", required = false) Integer size) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        Merchant merchant = merchantService.findById(merchantId);
        Status status = statusService.findByCode(code);

        Promo searchCriteriaPromo = new Promo();
        searchCriteriaPromo.setStatus(status);
        searchCriteriaPromo.setMerchant(merchant);
        searchCriteriaPromo.setExpiryDate(LocalDateTime.now());

        Page<Promo> promoPage = promoService.findAll(searchCriteriaPromo, page, size);

        List<PromoResources> promoResources = PromoResources.toResources(promoPage.getContent());

        OperationLanguage finalOperationLanguage = operationLanguage;
        promoResources.forEach(promoResourceObject -> {
            promoResourceObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoPage.getTotalElements());
        pageResource.setTotalPages(promoPage.getTotalPages());
        pageResource.setPageList(promoResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
    }


    @RequestMapping(value = "/items", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getItems(HttpServletRequest request,
                                                @RequestParam(name = "promoId", required = false) Long promoId,
                                                @RequestParam(name = "statusCode", required = false) String code,
                                                @RequestParam(name = "categoryId", required = false) Long categoryId,
                                                @RequestParam(name = "page", required = false) Integer page,
                                                @RequestParam(name = "size", required = false) Integer size) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }
        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;
        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById((userId));
            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }
            Promo promo = promoService.findById(promoId);
            if (promo == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
            }
            Status status = statusService.findByCode(code);
            PromoItem itemSearchCriteria = new PromoItem();
            itemSearchCriteria.setStatus(status);
            itemSearchCriteria.setPromo(promo);

            Page<PromoItem> promoItemPage = promoItemService.findAll(itemSearchCriteria, page, size);
            List<PromoItemResources> promoItemResources = PromoItemResources.toResources(promoItemPage.getContent());

            OperationLanguage finalOperationLanguage = operationLanguage;
            MediaAccount finalMediaAccount = mediaAccount;
            promoItemResources.forEach(promoResourceObject -> {
                promoResourceObject.initResourceCaption(applicationContext, finalOperationLanguage, null, finalMediaAccount, null, null);
            });

            //add view to Promo
            CustomerPromoView customerPromoView = new CustomerPromoView();
            customerPromoView.setPromo(promo);
            customerPromoView.setCustomerAccount(account);
            customerPromoViewService.create(customerPromoView);


            PageResource pageResource = new PageResource();
            pageResource.setCount(promoItemPage.getTotalElements());
            pageResource.setTotalPages(promoItemPage.getTotalPages());
            pageResource.setPageList(promoItemResources);


            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
        } else {
            Promo promo = promoService.findById(promoId);
            if (promo == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
            }
            Status status = statusService.findByCode(code);
            PromoItem itemSearchCriteria = new PromoItem();
            itemSearchCriteria.setStatus(status);
            itemSearchCriteria.setPromo(promo);

            Page<PromoItem> promoItemPage = promoItemService.findAll(itemSearchCriteria, page, size);
            List<PromoItemResources> promoItemResources = PromoItemResources.toResources(promoItemPage.getContent());

            OperationLanguage finalOperationLanguage = operationLanguage;
            promoItemResources.forEach(promoResourceObject -> {
                promoResourceObject.initResourceCaption(applicationContext, finalOperationLanguage, account, null, null, null);
            });

            //add view to Promo
            CustomerPromoView customerPromoView = new CustomerPromoView();
            customerPromoView.setPromo(promo);
            customerPromoView.setCustomerAccount(account);
            customerPromoViewService.create(customerPromoView);


            PageResource pageResource = new PageResource();
            pageResource.setCount(promoItemPage.getTotalElements());
            pageResource.setTotalPages(promoItemPage.getTotalPages());
            pageResource.setPageList(promoItemResources);


            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/activePromo", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> activePromo(@RequestBody PromoResources promoResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (promoResources.getId() == null
                || promoResources.getPoint() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);
        promoResources.setOperationLanguage(operationLanguage);


        Status activePromo = statusService.findByCode("PRO_ACTIVE");
        if (activePromo == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);

        Status activePromoItem = statusService.findByCode("PIT_ACTIVE");
        if (activePromoItem == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);

        Promo promo = promoService.changePromoStatus(promoResources, activePromo, activePromoItem);
        PromoResources resources = PromoResources.toResources(promo);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/rejectPromo", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> rejectPromo(@RequestBody PromoResources promoResources, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        if (promoResources.getId() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        promoResources.setOperationLanguage(operationLanguage);
        Status rejectPromo = statusService.findByCode("PRO_REJECT");
        if (rejectPromo == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);

        Status rejectPromoItem = statusService.findByCode("PIT_REJECT");
        if (rejectPromoItem == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        Promo promo = promoService.changePromoStatus(promoResources, rejectPromo, rejectPromoItem);
        PromoResources resources = PromoResources.toResources(promo);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

    }

    @RequestMapping(value = "/deletPromo", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> deletePromo(@RequestBody PromoResources promoResources, HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        if (promoResources.getId() == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "400", operationLanguage.getCode(), null);

        promoResources.setOperationLanguage(operationLanguage);
        Status deletePromo = statusService.findByCode("PRO_DELETE");
        if (deletePromo == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);

        Status deletePromoItem = statusService.findByCode("PIT_DELETE");
        if (deletePromoItem == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        Promo promo = promoService.changePromoStatus(promoResources, deletePromo, deletePromoItem);
        PromoResources resources = PromoResources.toResources(promo);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }


    @RequestMapping(value = "/item/detail", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findPromoByMerchant(@RequestParam(name = "id") Long id,
                                                           @RequestParam(name = "longitude", required = false) String longitude,
                                                           @RequestParam(name = "latitude", required = false) String latitude,
                                                           HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        String userType = Utils.fetchUserType(accountUserDetails.getUsername());

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        if (userType == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        if (!userType.equals("CUS")) {
            isCustomerAccount = false;
        }


        CustomerAccount account = null;
        MediaAccount mediaAccount = null;


        if (!isCustomerAccount) {
            mediaAccount = mediaAccountService.findById((userId));

            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            PromoItem promoItem = promoItemService.findById(id);
            if (promoItem == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", operationLanguage.getCode(), null);


            //Add media view to Promo Item
            MediaItemView itemView = new MediaItemView();
            itemView.setPromoItem(promoItem);
            itemView.setMediaAccount(mediaAccount);
            itemView.setOperationLanguage(operationLanguage);
            mediaItemViewService.create(itemView);

            PromoItemResources promoItemResources = PromoItemResources.toResources(promoItem);

            promoItemResources.initResourceCaption(applicationContext,
                    operationLanguage,
                    null,
                    mediaAccount,
                    Utils.parseDouble(longitude),
                    Utils.parseDouble(latitude));

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, promoItemResources), HttpStatus.OK);
        } else {

            account = customerAccountService.findById((userId));

            if (account == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            PromoItem promoItem = promoItemService.findById(id);
            if (promoItem == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", operationLanguage.getCode(), null);

            //Add view to Promo Item
            CustomerItemView itemView = new CustomerItemView();
            itemView.setPromoItem(promoItem);
            itemView.setCustomerAccount(account);
            itemView.setOperationLanguage(operationLanguage);
            customerItemViewService.create(itemView);

            PromoItemResources promoItemResources = PromoItemResources.toResources(promoItem);

            promoItemResources.initResourceCaption(applicationContext,
                    operationLanguage,
                    account,
                    null,
                    Utils.parseDouble(longitude),
                    Utils.parseDouble(latitude));

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, promoItemResources), HttpStatus.OK);
        }
    }


    @RequestMapping(value = "/getPromo", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getPromoById(HttpServletRequest request,
                                                    @RequestParam(name = "promoId") Long promoId) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);


        Promo promo = promoService.findById(promoId);
        if (promo == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pro404", operationLanguage.getCode(), null);

        PromoResources resources = PromoResources.toResources(promo);
        List<PromoItem> promoItemList = promoItemService.findAllByPromo(promo);
        List<PromoItemResources> promoItemResources = PromoItemResources.toResources(promoItemList);
        for (PromoItemResources itemResources : promoItemResources)
            itemResources.initResourceCaption(applicationContext, operationLanguage, null, null, null, null);
        List<PromoCaption> promoCaptionList = promoCaptionService.findAllByPromo(promo);
        resources.initResoucreCaption(applicationContext, operationLanguage);
        resources.setCaptionList(PromoCaptionResources.toResources(promoCaptionList));
        resources.setPromoItemList(promoItemResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }


    @RequestMapping(value = "/allTemp", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAllPromoTemp(HttpServletRequest request,
                                                       @RequestParam(name = "page", required = false) Integer page,
                                                       @RequestParam(name = "size", required = false) Integer size) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        Merchant merchant = merchantService.findById(merchantUser.getMerchant().getId());
        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        merchant.setOperationLanguage(operationLanguage);

        Status status = statusService.findByCode("PRO_TEMP");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", operationLanguage.getCode(), null);
        Promo searchCriteriaPromo = new Promo();
        searchCriteriaPromo.setStatus(status);

        Page<Promo> promoPage = promoService.findAll(searchCriteriaPromo, page, size);
        List<PromoResources> promoResources = PromoResources.toResources(promoPage.getContent());
        OperationLanguage finalOperationLanguage = operationLanguage;
        promoResources.forEach(promoResourceObject -> {
            promoResourceObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoPage.getTotalElements());
        pageResource.setTotalPages(promoPage.getTotalPages());
        pageResource.setPageList(promoResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
    }


    @RequestMapping(value = "/tempById", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAllPromoTempById(@RequestParam("promoId") Long id, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

//        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        Long userId = accountUserDetails.getUser().getId();
//
//        if (userId == null) {
//            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
//        }
//
//        MerchantUser merchantUser = merchantUserService.findById((userId));
//
//        if (merchantUser == null)
//            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "MER404", operationLanguage.getCode(), null);
//
//        if (id == null || id == 0L)
//            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", operationLanguage.getCode(), null);

        Promo promo = promoService.findById(id);
        PromoResources resources = PromoResources.toResources(promo);
        List<PromoItem> promoItemList = promoItemService.findAllByPromo(promo);
        List<PromoItemResources> promoItemResources = PromoItemResources.toResources(promoItemList);
        for (PromoItemResources itemResources : promoItemResources) {
            itemResources.initResourceCaption(applicationContext, operationLanguage, null, null, null, null);
        }
        List<PromoCaption> promoCaptionList = promoCaptionService.findAllByPromo(promo);
//        resources.initResoucreCaption(applicationContext, operationLanguage);
        resources.setCaptionList(PromoCaptionResources.toResources(promoCaptionList));
        resources.setPromoItemList(promoItemResources);
        resources.initResoucreCaption(applicationContext, operationLanguage);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }


    @RequestMapping(value = "/trendPromo", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findTrendPromo(@RequestParam(name = "page", required = false) Integer page,
                                                      @RequestParam(name = "size", required = false) Integer size,
                                                      HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;
        if (!isCustomerAccount) {
            mediaAccount = null;/*mediaAccountService.findById((userId))*/
            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }
        }
        Promo promo = new Promo();
        Status status = statusService.findByCode("PRO_ACTIVE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "sts404", operationLanguage.getCode(), null);
        promo.setStatus(status);
        promo.setExpiryDate(LocalDateTime.now());
        Page<Promo> promoList = promoService.findTrend(promo, page, size);

        List<PromoResources> promoResources = PromoResources.toResources(promoList.getContent());

        OperationLanguage finalOperationLanguage = operationLanguage;
        promoResources.forEach(promoResourcesObject -> {
            promoResourcesObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoList.getTotalElements());
        pageResource.setTotalPages(promoList.getTotalPages());
        pageResource.setPageList(promoResources);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
    }

    @RequestMapping(value = "/trendItem", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findTrendItem(@RequestParam(name = "page", required = false) Integer page,
                                                     @RequestParam(name = "size", required = false) Integer size,
                                                     HttpServletRequest request) {

        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        boolean isCustomerAccount = true;

        CustomerAccount account = customerAccountService.findById((userId));

        if (account == null) {
            isCustomerAccount = false;
        }

        MediaAccount mediaAccount = null;
        if (!isCustomerAccount) {
            mediaAccount = null;/*mediaAccountService.findById((userId))*/
            if (mediaAccount == null) {
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", LangCode, null);
            }

            PromoItem promoItem = new PromoItem();
            Status status = statusService.findByCode("PIT_ACTIVE");
            if (status == null)
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "sts404", operationLanguage.getCode(), null);
            promoItem.setStatus(status);
            promoItem.setExpiryDate(LocalDateTime.now());
            Page<PromoItem> itemPage = promoItemService.findTrendItem(promoItem, page, size);

            List<PromoItemResources> promoItemResources = PromoItemResources.toResources(itemPage.getContent());

            OperationLanguage finalOperationLanguage = operationLanguage;
            MediaAccount finalMediaAccount = mediaAccount;
            promoItemResources.forEach(promoResourcesObject -> {
                promoResourcesObject.initResourceCaption(applicationContext, finalOperationLanguage, null, finalMediaAccount, null, null);
            });

            PageResource pageResource = new PageResource();
            pageResource.setCount(itemPage.getTotalElements());
            pageResource.setTotalPages(itemPage.getTotalPages());
            pageResource.setPageList(promoItemResources);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);

        } else {
            PromoItem promoItem = new PromoItem();
            Status status = statusService.findByCode("PIT_ACTIVE");
            if (status == null)
                throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "sts404", operationLanguage.getCode(), null);
            promoItem.setStatus(status);
            promoItem.setExpiryDate(LocalDateTime.now());
            Page<PromoItem> itemPage = promoItemService.findTrendItem(promoItem, page, size);

            List<PromoItemResources> promoItemResources = PromoItemResources.toResources(itemPage.getContent());

            OperationLanguage finalOperationLanguage = operationLanguage;
            promoItemResources.forEach(promoResourcesObject -> {
                promoResourcesObject.initResourceCaption(applicationContext, finalOperationLanguage, account, null, null, null);
            });

            PageResource pageResource = new PageResource();
            pageResource.setCount(itemPage.getTotalElements());
            pageResource.setTotalPages(itemPage.getTotalPages());
            pageResource.setPageList(promoItemResources);

            SystemMessage message = systemMessageService.findByCode("200");
            SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
            return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);
        }

    }


    @RequestMapping(value = "/filter/promo/merchant", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> merchantFilterPromo(HttpServletRequest request,
                                                           @RequestParam(name = "name", required = false) String name,
                                                           @RequestParam(name = "description", required = false) String description,
                                                           @RequestParam(name = "creationDate", required = false) String creationDate,
                                                           @RequestParam(name = "expiryDate", required = false) String expiryDate,
                                                           @RequestParam(name = "startDate", required = false) String startDate,
                                                           @RequestParam(name = "statusCode", required = false) String statusCode,
                                                           @RequestParam(name = "page", required = false) Integer page,
                                                           @RequestParam(name = "size", required = false) Integer size) {


        String lang = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(lang);
        if (operationLanguage == null) {

            operationLanguage = languageService.findByCode("en");
        }

        AccountUserDetails accountUserDetails = (AccountUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = accountUserDetails.getUser().getId();

        if (userId == null) {
            throw new ResourceException(applicationContext, HttpStatus.UNAUTHORIZED, "401", operationLanguage.getCode(), null);
        }

        MerchantUser merchantUser = merchantUserService.findById((userId));

        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", operationLanguage.getCode(), null);

        Status status = statusService.findByCode(statusCode);

        LocalDate localCreateDate = Utils.parseLocalDate(creationDate, "yyyy-MM-dd");
        LocalDate localStartDate = Utils.parseLocalDate(startDate, "yyyy-MM-dd");
        LocalDate localExpiryDate = Utils.parseLocalDate(expiryDate, "yyyy-MM-dd");

        LocalDateTime createDateStart = Utils.startOfDay(localCreateDate);
        LocalDateTime createDateEnd = Utils.endOfDay(localCreateDate);

        LocalDateTime startDateStart = Utils.startOfDay(localStartDate);
        LocalDateTime startDateEnd = Utils.endOfDay(localStartDate);

        LocalDateTime expiaryDateStart = Utils.startOfDay(localExpiryDate);
        LocalDateTime expiaryDateEnd = Utils.endOfDay(localExpiryDate);

        Page<Promo> promoPage = promoService.search(
                (String) Utils.handleEmptyOrNullObject(name),
                (String) Utils.handleEmptyOrNullObject(description),
                createDateStart,
                createDateEnd,
                startDateStart,
                startDateEnd,
                expiaryDateStart,
                expiaryDateEnd,
                merchantUser.getMerchant(),
                status,
                page,
                size);

        List<PromoResources> promoResources = PromoResources.toResources(promoPage.getContent());

        OperationLanguage finalOperationLanguage = operationLanguage;
        promoResources.forEach(promoResourceObject -> {
            promoResourceObject.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        PageResource pageResource = new PageResource();
        pageResource.setCount(promoPage.getTotalElements());
        pageResource.setTotalPages(promoPage.getTotalPages());
        pageResource.setPageList(promoResources);


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, pageResource), HttpStatus.OK);


    }

    @RequestMapping(value = "/deleteOurPromo", method = RequestMethod.DELETE)
    public ResponseEntity<MessageBody> deletePromo(@RequestParam(name = "promoId") Long promoId,
                                                   HttpServletRequest request) {
        String lang = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(lang);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        Promo promo = promoService.findById(promoId);
        if (promo == null) {

            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "promo404", null, null);
        }

        try{
            promoItemService.deletePromoItemByPromo(promo);
        } catch (Exception e){
            e.printStackTrace();
        }

        try{
        promoService.deletePromo(promo.getId());
        } catch (Exception e){
            e.printStackTrace();
        }


        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, null), HttpStatus.OK);
    }


}




