//package com.dc.Promotion.controller;
//
//import com.dc.Promotion.MessageBody;
//import com.dc.Promotion.entities.*;
//import com.dc.Promotion.resources.NationalityCaptionResources;
//import com.dc.Promotion.resources.NationalityResources;
//import com.dc.Promotion.service.*;
//import com.dc.Promotion.utils.MessageHandler;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletRequest;
//import java.util.List;
//
//@RestController
//@RequestMapping(value = "/nationality")
//public class NationalityController {
//    @Autowired
//    private NationalityService nationalityService;
//    @Autowired
//    private OperationLanguageService operationLanguageService;
//    @Autowired
//    private NationalityCaptionService nationalityCaptionService;
//    @Autowired
//    private SystemMessageService systemMessageService;
//    @Autowired
//    private SystemMessageCaptionService systemMessageCaptionService;
//
//    @RequestMapping(value = "/nationalities", method = RequestMethod.GET)
//    public ResponseEntity<MessageBody> allNationality(HttpServletRequest request) {
//
//        String lang = request.getHeader("lng");
//        String LangCode = request.getHeader("lng");
//        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
//        if (operationLanguage == null) {
//            operationLanguage = operationLanguageService.findByCode("en");
//        }
//        List<NationalityCaption> nationalities = nationalityCaptionService.findAllByOperationLanguage(operationLanguage);
//
//        List<NationalityCaptionResources> resources = NationalityCaptionResources.toResources(nationalities);
//
//        SystemMessage message = systemMessageService.findByCode("200");
//        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
//        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
//
//
//    }
//
//
//}
