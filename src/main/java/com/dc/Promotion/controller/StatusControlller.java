package com.dc.Promotion.controller;


import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.resources.*;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/status")
public class StatusControlller {

    @Autowired
    private OperationLanguageService languageService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private StatusCaptionService statusCaptionService;

    @RequestMapping(value = "/nonDeleted", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> findAllOperationCountry(HttpServletRequest request,
                                                               @RequestParam(name = "code") String code) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }

        List<StatusCaption> statusCaptionList = statusCaptionService.findWithoutDeleted(operationLanguage,code);
        List<StatusCaptionResources> resources = StatusCaptionResources.toResources(statusCaptionList);
        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getStatus(@RequestParam(name = "code") String code, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = languageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = languageService.findByCode("en");
        }


        List<StatusCaption> statusCaptionList = statusCaptionService.find(operationLanguage, code);
        List<StatusCaptionResources> resources = StatusCaptionResources.toResources(statusCaptionList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

    }

}
