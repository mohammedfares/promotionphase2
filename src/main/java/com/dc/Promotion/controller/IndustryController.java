package com.dc.Promotion.controller;

import com.dc.Promotion.MessageBody;
import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.resources.IndustryCaptionResources;
import com.dc.Promotion.resources.MerchantCaptionResources;
import com.dc.Promotion.resources.MerchantIndustryResources;
import com.dc.Promotion.resources.MerchantResources;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping(value = "/industry")
public class IndustryController {

    @Autowired
    private OperationLanguageService operationLanguageService;

    @Autowired
    private SystemMessageService systemMessageService;

    @Autowired
    private SystemMessageCaptionService systemMessageCaptionService;

    @Autowired
    private IndustryCaptionService industryCaptionService;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private IndustryService industryService;

    @Autowired
    private MerchantCaptionService merchantCaptionService;

    // All Industry with Caption
    @RequestMapping(value = "/findAll", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getAllIndustry(HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        List<IndustryCaption> industryCaptionList = industryCaptionService.findAllByLanguageId(operationLanguage);
        List<IndustryCaptionResources> resources = IndustryCaptionResources.toResources(industryCaptionList);
        OperationLanguage finalOperationLanguage = operationLanguage;
        resources.forEach(industryCaptionResources -> {
                    industryCaptionResources.initResoucreCaption(applicationContext, finalOperationLanguage);
                }
        );

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }

    // All Industry By Merchant
    @RequestMapping(value = "/byMerchant", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getInduestryByMerchant(@RequestParam(name = "merchantId") Long id, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }
        if (id == null || id.longValue() == 0)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        Merchant merchant = merchantService.findById(id);
        merchant.setOperationLanguage(operationLanguage);

        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", operationLanguage.getCode(), null);

        List<IndustryCaption> industryCaptionList = industryCaptionService.find(merchant);
        List<IndustryCaptionResources> resources = IndustryCaptionResources.toResources(industryCaptionList);

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);
    }


    @RequestMapping(value = "/merchant", method = RequestMethod.GET)
    public ResponseEntity<MessageBody> getMerchantByIndustry(@RequestParam(name = "industryId", required = false) Long id, HttpServletRequest request) {
        String LangCode = request.getHeader("lng");
        OperationLanguage operationLanguage = operationLanguageService.findByCode(LangCode);
        if (operationLanguage == null) {
            operationLanguage = operationLanguageService.findByCode("en");
        }

        Industry industry = industryService.findById(id);
        List<Merchant> merchantList=null;
        if(industry==null)
        {
            merchantList=merchantService.findAll();
        }
        else
        {
            merchantList=merchantService.findByIndustry(industry);
        }
        List<MerchantResources> resources=MerchantResources.toResource(merchantList);
        OperationLanguage finalOperationLanguage = operationLanguage;
        resources.forEach(merchantResources -> {
            merchantResources.initResoucreCaption(applicationContext, finalOperationLanguage);
        });

        SystemMessage message = systemMessageService.findByCode("200");
        SystemMessageCaption systemMessageStatusCaption = systemMessageCaptionService.findByMessageAndLanguage(message, operationLanguage);
        return new ResponseEntity<>(MessageHandler.setMessageBody(systemMessageStatusCaption, resources), HttpStatus.OK);

    }


}
