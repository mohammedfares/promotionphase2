package com.dc.Promotion.config;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.repository.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@Component
public class DatabaseSeeder {

    @Autowired
    private OperationLanguageRepo languageRepo;
    @Autowired
    private CountryCaptionRepo countryCaptionRepo;
    @Autowired
    private CountryRepo countryRepo;
    @Autowired
    private CityRepo cityRepo;
    @Autowired
    private DistrictRepo districtRepo;
    @Autowired
    private StatusRepo statusRepo;
    @Autowired
    private OperationCountryRepo operationCountryRepo;
    @Autowired
    private CityCaptionRepo cityCaptionRepo;
    @Autowired
    private IndustryRepo industryRepo;
    @Autowired
    private IndustryCaptionRepo industryCaptionRepo;
    @Autowired
    private SystemMessageCaptionRepo systemMessageCaptionRepo;
    @Autowired
    private SystemMessageRepo systemMessageRepo;
    @Autowired
    private DistrictCaptionRepo districtCaptionRepo;

    public void seed() {
      //  seedLangTable();
        //seedCountry();
       // seedCountryOperation();
       // seedCity();
        //seedIndustry();
        //seedSystemMessage();
        //seedDistrict();
    }

    private void seedLangTable() {
        //Arabic Language
        OperationLanguage arabicOperationLanguage = new OperationLanguage();
        arabicOperationLanguage.setCode("ar");
        arabicOperationLanguage.setName("العربية");
        arabicOperationLanguage.setDirection("rtl");

        languageRepo.save(arabicOperationLanguage);

        //English Language
        OperationLanguage englishOperationLanguage = new OperationLanguage();
        englishOperationLanguage.setCode("en");
        englishOperationLanguage.setName("English");
        englishOperationLanguage.setDirection("ltr");

        languageRepo.save(englishOperationLanguage);

        //French Language
        OperationLanguage frenchOperationLanguage = new OperationLanguage();
        frenchOperationLanguage.setCode("fr");
        frenchOperationLanguage.setName("French");
        frenchOperationLanguage.setDirection("ltr");

        languageRepo.save(frenchOperationLanguage);


    }

    private void seedCountry() {
        File file = new File("file1.txt");
        String contents = null;
        try {
            contents = new String(Files.readAllBytes(file.toPath()));
            JSONObject object = new JSONObject(contents);
            JSONArray countryList = object.getJSONArray("countryList");
            Status status = statusRepo.findByCode("OLA_ACTIVE");


            for (int i = 0; i < countryList.length(); i++) {
                Country country = new Country();

                country.setCode(countryList.getJSONObject(i).get("countryCode").toString());
                country.setKey(countryList.getJSONObject(i).get("countrykey").toString());
                country.setFlag(countryList.getJSONObject(i).get("countryFlag").toString());
                countryRepo.save(country);

                CountryCaption countryCaptionEn = new CountryCaption();
                countryCaptionEn.setLanguage(languageRepo.findByCode("en"));
                countryCaptionEn.setCountry(country);
                countryCaptionEn.setCountryName(countryList.getJSONObject(i).getString("countryNameEn"));
                countryCaptionEn.setNationalityName(countryList.getJSONObject(i).getString("nationalityNameEn"));
                countryCaptionRepo.save(countryCaptionEn);


                CountryCaption countryCaptionAr = new CountryCaption();
                countryCaptionAr.setLanguage(languageRepo.findByCode("ar"));
                countryCaptionAr.setCountry(country);
                countryCaptionAr.setCountryName(countryList.getJSONObject(i).getString("countryNameAr"));
                countryCaptionAr.setNationalityName(countryList.getJSONObject(i).getString("nationalityNameAr"));
                countryCaptionRepo.save(countryCaptionAr);


                CountryCaption countryCaptionFr = new CountryCaption();
                countryCaptionFr.setLanguage(languageRepo.findByCode("fr"));
                countryCaptionFr.setCountry(country);
                countryCaptionFr.setCountryName(countryList.getJSONObject(i).getString("countryNameFr"));
                countryCaptionFr.setNationalityName(countryList.getJSONObject(i).getString("nationalityNameFr"));
                countryCaptionRepo.save(countryCaptionFr);

            }


        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void seedCountryOperation() {
        OperationCountry operationCountry = new OperationCountry();
        Country country = countryRepo.findByCode("sa");
        operationCountry.setCountry(country);
        operationCountryRepo.save(operationCountry);
    }

    private void seedCity() {
        String saudiCities[] = {"Riyadh:الرياض:Riyad", "Jeddah:جدة:Djeddah",
                "Mecca:مكه:Mecque", "Medina:المدينة:Médina", "Al-Ahsa:الحسا:Al-Ahsa",
                "Ta'if:الطائف:Ta'if", "Dammam:الدمام:Dammam", "Buraidah:بريده:Buraidah",
                "Khobar:الخبر:Khobar", "Tabuk:تبوك:Tabuk", "Qatif:القطيف:Qatif",
                "Khamis Mushait:خميس مشيط:Khamis Mushait", "Ha'il:حائل:Saluer", "Hafar Al-Batin:حفر الباطن:Hafar Al-Batin",
                "Jubail:الجبيل:Jubail", "Kharj:الخرج:Kharj", "Abha:أبها:Abha",
                "Najran:نجران:Najran", "Yanbu:ينبع:Yanbu", "Al Qunfudhah:القنفذة:Al Qunfudhah"};

        OperationLanguage languageAr = languageRepo.findByCode("ar");
        OperationLanguage languageFr = languageRepo.findByCode("fr");
        OperationLanguage languageEn = languageRepo.findByCode("en");

        OperationCountry country = operationCountryRepo.findByCountry_Code("sa");
        int index = 1;
        for (String cityName : saudiCities) {
            String englishCityName = cityName.split(":")[0];
            String arabicCityName = cityName.split(":")[1];
            String frenchCityName = cityName.split(":")[2];
            City city = new City();
            city.setCode(index + "");
            city.setCountry(country);
            cityRepo.save(city);

            CityCaption cityCaptionEn = new CityCaption();
            cityCaptionEn.setName(englishCityName);
            cityCaptionEn.setLanguage(languageEn);
            cityCaptionEn.setCity(city);
            cityCaptionRepo.save(cityCaptionEn);

            CityCaption cityCaptionFr = new CityCaption();
            cityCaptionFr.setName(frenchCityName);
            cityCaptionFr.setLanguage(languageFr);
            cityCaptionFr.setCity(city);
            cityCaptionRepo.save(cityCaptionFr);

            CityCaption cityCaptionAr = new CityCaption();
            cityCaptionAr.setName(arabicCityName);
            cityCaptionAr.setLanguage(languageAr);
            cityCaptionAr.setCity(city);
            cityCaptionRepo.save(cityCaptionAr);

            index++;

        }
    }

    private void seedIndustry() {


        OperationLanguage languageAr = languageRepo.findByCode("ar");
        OperationLanguage languageFr = languageRepo.findByCode("fr");
        OperationLanguage languageEn = languageRepo.findByCode("en");

        // Furniture Industry
        Industry industryFurniture = new Industry();
        industryFurniture.setImage("https://cdn.shopify.com/s/files/1/2660/5106/files/LR-2-Main_159cda8c-8447-4d3b-888b-0bc8b8999dd2_960x.jpg?v=1546552038");
        industryRepo.save(industryFurniture);

        IndustryCaption industryCaptionFurnitureEN = new IndustryCaption();
        industryCaptionFurnitureEN.setIndustry(industryFurniture);
        industryCaptionFurnitureEN.setLanguageId(languageEn);
        industryCaptionFurnitureEN.setName("Furniture");
        industryCaptionRepo.save(industryCaptionFurnitureEN);

        IndustryCaption industryCaptionFurnitureAr = new IndustryCaption();
        industryCaptionFurnitureAr.setIndustry(industryFurniture);
        industryCaptionFurnitureAr.setLanguageId(languageAr);
        industryCaptionFurnitureAr.setName("أثاث المنزل");
        industryCaptionRepo.save(industryCaptionFurnitureAr);

        IndustryCaption industryCaptionFurnitureFr = new IndustryCaption();
        industryCaptionFurnitureFr.setIndustry(industryFurniture);
        industryCaptionFurnitureFr.setLanguageId(languageFr);
        industryCaptionFurnitureFr.setName("Meubles");
        industryCaptionRepo.save(industryCaptionFurnitureFr);


        // Electronics Industry
        Industry industryElectronics = new Industry();
        industryElectronics.setImage("https://onyourdesks.com/wp-content/uploads/2019/08/Consumer-Electronics-and-Home-Appliances.jpg");
        industryRepo.save(industryElectronics);

        IndustryCaption industryCaptionElectronicsEN = new IndustryCaption();
        industryCaptionElectronicsEN.setIndustry(industryElectronics);
        industryCaptionElectronicsEN.setLanguageId(languageEn);
        industryCaptionElectronicsEN.setName("Electronics");
        industryCaptionRepo.save(industryCaptionElectronicsEN);

        IndustryCaption industryCaptionElectronicsAr = new IndustryCaption();
        industryCaptionElectronicsAr.setIndustry(industryElectronics);
        industryCaptionElectronicsAr.setLanguageId(languageAr);
        industryCaptionElectronicsAr.setName("إلكترونيات");
        industryCaptionRepo.save(industryCaptionElectronicsAr);

        IndustryCaption industryCaptionElectronicsFr = new IndustryCaption();
        industryCaptionElectronicsFr.setIndustry(industryElectronics);
        industryCaptionElectronicsFr.setLanguageId(languageFr);
        industryCaptionElectronicsFr.setName("Électronique");
        industryCaptionRepo.save(industryCaptionElectronicsAr);


        // Food Industry
        Industry industryFood = new Industry();
        industryFood.setImage("https://static.independent.co.uk/s3fs-public/thumbnails/image/2019/07/18/15/istock-855098134.jpg?w968h681");
        industryRepo.save(industryFood);

        IndustryCaption industryCaptionFoodEN = new IndustryCaption();
        industryCaptionFoodEN.setIndustry(industryFood);
        industryCaptionFoodEN.setLanguageId(languageEn);
        industryCaptionFoodEN.setName("Food");
        industryCaptionRepo.save(industryCaptionFoodEN);

        IndustryCaption industryCaptionFoodAr = new IndustryCaption();
        industryCaptionFoodAr.setIndustry(industryFood);
        industryCaptionFoodAr.setLanguageId(languageAr);
        industryCaptionFoodAr.setName("غذاء");
        industryCaptionRepo.save(industryCaptionFoodAr);

        IndustryCaption industryCaptionFoodFr = new IndustryCaption();
        industryCaptionFoodFr.setIndustry(industryFood);
        industryCaptionFoodFr.setLanguageId(languageFr);
        industryCaptionFoodFr.setName("Aliments");
        industryCaptionRepo.save(industryCaptionFoodFr);


        // Clothes Industry
        Industry industryClothes = new Industry();
        industryClothes.setImage("https://www.bigissuenorth.com/wp-content/uploads/2019/05/lyc-campaigns-1600_0_bigissuenorth.jpg");
        industryRepo.save(industryClothes);
        IndustryCaption industryCaptionClothesEN = new IndustryCaption();
        industryCaptionClothesEN.setIndustry(industryClothes);
        industryCaptionClothesEN.setLanguageId(languageEn);
        industryCaptionClothesEN.setName("Clothes");
        industryCaptionRepo.save(industryCaptionClothesEN);

        IndustryCaption industryCaptionClothesAr = new IndustryCaption();
        industryCaptionClothesAr.setIndustry(industryClothes);
        industryCaptionClothesAr.setLanguageId(languageAr);
        industryCaptionClothesAr.setName("ملابس");
        industryCaptionRepo.save(industryCaptionClothesAr);

        IndustryCaption industryCaptionClothesFr = new IndustryCaption();
        industryCaptionClothesFr.setIndustry(industryClothes);
        industryCaptionClothesFr.setLanguageId(languageFr);
        industryCaptionClothesFr.setName("vêtements");
        industryCaptionRepo.save(industryCaptionClothesAr);


    }

    private void seedSystemMessage() {
        OperationLanguage arabicLanguage = languageRepo.findByCode("ar");

        OperationLanguage englishLanguage = languageRepo.findByCode("en");

        OperationLanguage languageFr = languageRepo.findByCode("fr");

        //---------------------------------------------------------------

        SystemMessage successMessage = new SystemMessage();
        successMessage.setCode("200");
        systemMessageRepo.save(successMessage);

        SystemMessageCaption successAr = new SystemMessageCaption();
        successAr.setMessage(successMessage);
        successAr.setLanguage(arabicLanguage);
        successAr.setText("مقبول");
        systemMessageCaptionRepo.save(successAr);


        SystemMessageCaption successEn = new SystemMessageCaption();
        successEn.setMessage(successMessage);
        successEn.setLanguage(englishLanguage);
        successEn.setText("Success");
        systemMessageCaptionRepo.save(successEn);

        SystemMessageCaption successFr = new SystemMessageCaption();
        successFr.setMessage(successMessage);
        successFr.setLanguage(languageFr);
        successFr.setText("Succès");
        systemMessageCaptionRepo.save(successFr);
    }

    private void seedDistrict() {
        OperationLanguage languageAr = languageRepo.findByCode("ar");
        OperationLanguage languageEn = languageRepo.findByCode("en");
        OperationLanguage languageFr = languageRepo.findByCode("fr");

        //riyadh City
        City city = cityRepo.findById(1);
        District districtSulaymaniyah = new District();
        districtSulaymaniyah.setCity(city);
        districtRepo.save(districtSulaymaniyah);

        DistrictCaption captionAr = new DistrictCaption();
        captionAr.setName("السليمانية");
        captionAr.setLanguage(languageAr);
        captionAr.setDistrict(districtSulaymaniyah);
        captionAr.setDescription("السليمانية");
        districtCaptionRepo.save(captionAr);

        DistrictCaption captionEn = new DistrictCaption();
        captionEn.setName("Sulaymaniyah");
        captionEn.setLanguage(languageEn);
        captionEn.setDistrict(districtSulaymaniyah);
        captionEn.setDescription("Sulaymaniyah");
        districtCaptionRepo.save(captionEn);

        DistrictCaption captionFr = new DistrictCaption();
        captionFr.setName("Sulaymaniyah");
        captionFr.setLanguage(languageFr);
        captionFr.setDistrict(districtSulaymaniyah);
        captionFr.setDescription("Sulaymaniyah");
        districtCaptionRepo.save(captionFr);


        District districtalOlayya = new District();
        districtalOlayya.setCity(city);
        districtRepo.save(districtalOlayya);

        DistrictCaption captionOlayyaAr = new DistrictCaption();
        captionOlayyaAr.setName("حي العليا");
        captionOlayyaAr.setLanguage(languageAr);
        captionOlayyaAr.setDistrict(districtalOlayya);
        captionOlayyaAr.setDescription("حي العليا");
        districtCaptionRepo.save(captionOlayyaAr);

        DistrictCaption captionOlayyaEn = new DistrictCaption();
        captionOlayyaEn.setName("Al-'Olayya");
        captionOlayyaEn.setLanguage(languageEn);
        captionOlayyaEn.setDistrict(districtalOlayya);
        captionOlayyaEn.setDescription("Al-'Olayya");
        districtCaptionRepo.save(captionOlayyaEn);

        DistrictCaption captionOlayyaFr = new DistrictCaption();
        captionOlayyaFr.setName("Al-'Olayya");
        captionOlayyaFr.setLanguage(languageFr);
        captionOlayyaFr.setDistrict(districtalOlayya);
        captionOlayyaFr.setDescription("Al-'Olayya");
        districtCaptionRepo.save(captionOlayyaFr);


        District districtalAlBatha = new District();
        districtalAlBatha.setCity(city);
        districtRepo.save(districtalAlBatha);

        DistrictCaption captionAlBathaAr = new DistrictCaption();
        captionAlBathaAr.setName("البطحاء");
        captionAlBathaAr.setLanguage(languageAr);
        captionAlBathaAr.setDistrict(districtalAlBatha);
        captionAlBathaAr.setDescription("البطحاء");
        districtCaptionRepo.save(captionAlBathaAr);

        DistrictCaption captionAlBathaEn = new DistrictCaption();
        captionAlBathaEn.setName("Al-Batha");
        captionAlBathaEn.setLanguage(languageEn);
        captionAlBathaEn.setDistrict(districtalAlBatha);
        captionAlBathaEn.setDescription("Al-Batha");
        districtCaptionRepo.save(captionAlBathaEn);

        DistrictCaption captionAlBathaFr = new DistrictCaption();
        captionAlBathaFr.setName("Al-Batha");
        captionAlBathaFr.setLanguage(languageFr);
        captionAlBathaFr.setDistrict(districtalAlBatha);
        captionAlBathaFr.setDescription("Al-Batha");
        districtCaptionRepo.save(captionAlBathaFr);


        District districtalAlMalaz = new District();
        districtalAlMalaz.setCity(city);
        districtRepo.save(districtalAlMalaz);

        DistrictCaption captionAlMalazAr = new DistrictCaption();
        captionAlMalazAr.setName("البطحاء");
        captionAlMalazAr.setLanguage(languageAr);
        captionAlMalazAr.setDistrict(districtalAlMalaz);
        captionAlMalazAr.setDescription("البطحاء");
        districtCaptionRepo.save(captionAlMalazAr);

        DistrictCaption captionAlMalazEn = new DistrictCaption();
        captionAlMalazEn.setName("Al-Batha");
        captionAlMalazEn.setLanguage(languageEn);
        captionAlMalazEn.setDistrict(districtalAlMalaz);
        captionAlMalazEn.setDescription("Al-Batha");
        districtCaptionRepo.save(captionAlMalazEn);

        DistrictCaption captionAlMalazFr = new DistrictCaption();
        captionAlMalazFr.setName("Al-Batha");
        captionAlMalazFr.setLanguage(languageFr);
        captionAlMalazFr.setDistrict(districtalAlMalaz);
        captionAlMalazFr.setDescription("Al-Batha");
        districtCaptionRepo.save(captionAlMalazFr);


    }


}
