package com.dc.Promotion.service;

import com.dc.Promotion.entities.MediaNotification;

public interface MediaNotificationService {

    public MediaNotification findById(Long id);

    public  MediaNotification save(MediaNotification notification);

}
