package com.dc.Promotion.service;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.SystemMessage;
import com.dc.Promotion.entities.SystemMessageCaption;

public interface SystemMessageCaptionService {
    public SystemMessageCaption findById(long id);
    public SystemMessageCaption findByMessageAndLanguage(SystemMessage message, OperationLanguage language);
}
