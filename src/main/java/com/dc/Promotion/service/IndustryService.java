package com.dc.Promotion.service;

import com.dc.Promotion.entities.Industry;

public interface IndustryService {

    public Industry findById(Long id);
}
