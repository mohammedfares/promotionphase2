package com.dc.Promotion.service;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Specification;
import com.dc.Promotion.entities.SpecificationCaption;

import java.util.List;

public interface SpecificationCaptionService {

    public SpecificationCaption findById(long id);
    public List<SpecificationCaption> allSpecification();
    public SpecificationCaption find(Long specificationId , OperationLanguage operationLanguage);
    public List<SpecificationCaption> find(Long specificationId);
}
