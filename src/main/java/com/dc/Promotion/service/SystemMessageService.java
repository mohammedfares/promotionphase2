package com.dc.Promotion.service;

import com.dc.Promotion.entities.SystemMessage;

public interface SystemMessageService {
    public SystemMessage findById(long id);
    public  SystemMessage findByCode(String code);
}
