package com.dc.Promotion.service;

import com.dc.Promotion.entities.Country;

import java.util.List;

public interface CountryService {
    public Country findById(long id);
    public List<Country> findAll();
    public Country findByCode(String code);
}
