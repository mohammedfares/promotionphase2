package com.dc.Promotion.service;

import com.dc.Promotion.entities.CustomerItemView;

public interface CustomerItemViewService {
    public CustomerItemView findById(long id);

    public CustomerItemView create(CustomerItemView customerItemView);
}
