package com.dc.Promotion.service;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantCustomerSegment;
import com.dc.Promotion.entities.Status;

import java.util.List;

public interface MerchantCustomerSegmentService {

    public MerchantCustomerSegment findById(Long id);

    public MerchantCustomerSegment create(MerchantCustomerSegment merchantCustomerSegment);

    public MerchantCustomerSegment ChangeStatus(MerchantCustomerSegment segment, Status status);

    public List<MerchantCustomerSegment> findAllByMerchant(Merchant merchant);

    public List<MerchantCustomerSegment> findTopSegmant(Merchant merchant);
}
