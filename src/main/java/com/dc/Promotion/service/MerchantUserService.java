package com.dc.Promotion.service;

import com.dc.Promotion.entities.*;

import java.util.List;

public interface MerchantUserService {
    public MerchantUser findById(long id);

    public MerchantUser create(MerchantUser merchantUser);

    public MerchantUser findByEmail(String email);

    public MerchantUser findByMobile(String mobile);

    public MerchantUser findByIdAndMerchantUserType(long id, String userType);

    public List<MerchantUser> findAllByMerchant_IdAndStatus_Code(long id, String code);

    public MerchantUser changePassword(MerchantUser user, String NewPassword);

    public MerchantUser updateUser(MerchantUser user);

    public MerchantUser resetMerchantUserPassword(MerchantUser user);

    public List<MerchantUser> findByParam(MerchantUser user, Status status, OperationLanguage language);

    public List<MerchantUser> findAll(MerchantUser merchantUserSearchCriteria);

    public MerchantUser findByUsername(String username);

    public MerchantUser findMerchantByUsernameOrEmail(String usernameOrEmail);
    public MerchantUser changePassword(MerchantUser merchantUser);

}
