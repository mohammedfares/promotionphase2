package com.dc.Promotion.service;

import com.dc.Promotion.entities.MerchantUserType;

public interface MerchantUserTypeService {
    public MerchantUserType findById(Long id);
    public MerchantUserType findByCode(String code);
}
