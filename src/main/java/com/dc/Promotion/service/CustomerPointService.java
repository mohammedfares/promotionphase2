package com.dc.Promotion.service;

import com.dc.Promotion.entities.CustomerPoint;

public interface CustomerPointService {

    public CustomerPoint findById(long id);
}
