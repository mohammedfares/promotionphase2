package com.dc.Promotion.service;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoCaption;

import java.util.List;

public interface PromoCaptionService {
    public PromoCaption findById(long id);

    public PromoCaption create(PromoCaption promoCaption);

    public PromoCaption find(Promo promo , OperationLanguage operationLanguage);

    public List<PromoCaption> findAllByPromo(Promo promo);

    public void deleteAllByPromo(Promo promo);
}
