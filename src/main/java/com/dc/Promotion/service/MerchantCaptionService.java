package com.dc.Promotion.service;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.List;

public interface MerchantCaptionService {
    public MerchantCaption findById(long id);

    public MerchantCaption create(MerchantCaption merchantCaption);

    public List<MerchantCaption> findByIndustry(Industry industry);

    public List<MerchantCaption>  findAll();

    public List<MerchantCaption>  findByMerchantId(Long id);

    public MerchantCaption findByIdAndLanguage(Long id,OperationLanguage language);

    public MerchantCaption findByMerchant(Merchant merchant,OperationLanguage language);
}
