package com.dc.Promotion.service;

import com.dc.Promotion.entities.*;

import java.util.List;

public interface MerchantBranchCaptionService {
    public MerchantBranchCaption findById(long id);

    public List<MerchantBranchCaption> findByMerchant(Merchant merchant, OperationLanguage language, Status status);

    public MerchantBranchCaption create(MerchantBranchCaption merchantBranchCaption);

    public List<MerchantBranchCaption> updateBranchCaption(List<MerchantBranchCaption> merchantBranchCaption);

    public MerchantBranchCaption find(MerchantBranch merchantBranch , OperationLanguage language);


    public List<MerchantBranchCaption> find(MerchantBranch merchantBranch);

    public List<MerchantBranchCaption> findAllByPromo(Promo promo);

    public List<MerchantBranchCaption> findAll(MerchantBranchCaption merchantBranchCaptionSearchCriteria);



}
