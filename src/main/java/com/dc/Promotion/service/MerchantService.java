package com.dc.Promotion.service;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantIndustry;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.List;

public interface MerchantService  {
    public Merchant findById(Long id);
    public Merchant create(Merchant merchant);
    public List<Merchant> findAll();
    public List<Merchant> findByIndustry(Industry industry);
}
