package com.dc.Promotion.service;

import com.dc.Promotion.entities.ItemSpecification;

public interface ItemSpecificationService {

    public ItemSpecification findById(long id);
}
