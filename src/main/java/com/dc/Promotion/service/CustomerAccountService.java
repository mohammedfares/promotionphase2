package com.dc.Promotion.service;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.OperationLanguage;

public interface CustomerAccountService {
    public CustomerAccount findById(long id);

    public CustomerAccount findByMobile(String mobile);

    public  CustomerAccount findByEmail(String email);

    public  CustomerAccount save(CustomerAccount account);

    public CustomerAccount changeLanguage(CustomerAccount customerAccount, OperationLanguage operationLanguage);

    public CustomerAccount updateAccountInfo(CustomerAccount customerAccount);

    public CustomerAccount changePassword(CustomerAccount user, String NewPassword);
}
