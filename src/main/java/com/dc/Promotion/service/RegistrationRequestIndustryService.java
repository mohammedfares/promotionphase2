package com.dc.Promotion.service;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.MerchantRegistrationRequest;
import com.dc.Promotion.entities.RegistrationRequestIndustry;

import java.util.List;

public interface RegistrationRequestIndustryService {

    public RegistrationRequestIndustry findById(long id);

    public List<Industry> findByMerchantReq(MerchantRegistrationRequest registrationRequest);
}
