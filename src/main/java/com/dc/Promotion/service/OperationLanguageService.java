package com.dc.Promotion.service;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;

import java.util.List;

public interface OperationLanguageService {
    public OperationLanguage findById(Long id);
    public OperationLanguage findByCode(String code);
    public List<OperationLanguage> findAll();
}
