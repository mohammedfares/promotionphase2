package com.dc.Promotion.service;

import com.dc.Promotion.entities.AgeRange;

public interface AgeRangeService {

    public AgeRange findById(long id);
}
