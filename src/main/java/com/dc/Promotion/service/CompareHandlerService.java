package com.dc.Promotion.service;

import com.dc.Promotion.entities.PromoItem;

import java.util.List;

public interface CompareHandlerService {
    public List<PromoItem> compare(PromoItem promoItem, String type, Double longitude, Double latitude);

}
