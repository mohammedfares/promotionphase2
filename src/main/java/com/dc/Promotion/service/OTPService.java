package com.dc.Promotion.service;

import com.dc.Promotion.entities.OTP;
import com.dc.Promotion.entities.Status;

public interface OTPService {
    public OTP findById(long id);

    public OTP findByMobileAndCode(String mobile,String code);
    public OTP createOTP(OTP otp);
    public OTP  deActive (OTP code);
    public OTP findActiveOtp(String mobile, String status );
}
