package com.dc.Promotion.service;

import com.dc.Promotion.entities.MerchantBranch;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoBranch;

import java.util.List;

public interface PromoBranchService {

    public PromoBranch findById(long id);

    public PromoBranch create(PromoBranch promoBranch);

    public List<PromoBranch> findAllbyPromo(Promo promo);

    public void deleteAllByPromo(Promo promo);
}
