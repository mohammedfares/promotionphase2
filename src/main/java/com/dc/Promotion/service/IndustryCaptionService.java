package com.dc.Promotion.service;

import com.dc.Promotion.entities.IndustryCaption;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantRegistrationRequest;
import com.dc.Promotion.entities.OperationLanguage;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IndustryCaptionService {

    public IndustryCaption findById(long id);

    public List<IndustryCaption> findAllByLanguageId(OperationLanguage language);

    public List<IndustryCaption> find(Merchant merchant);

    public List<IndustryCaption>findByMRR(OperationLanguage language, MerchantRegistrationRequest registrationRequest);
}
