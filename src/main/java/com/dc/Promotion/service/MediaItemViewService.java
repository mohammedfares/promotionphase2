package com.dc.Promotion.service;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaItemView;
import com.dc.Promotion.entities.PromoItem;

public interface MediaItemViewService {

    public MediaItemView findById(Long id);

    public MediaItemView create(MediaItemView itemView);

    public MediaItemView findByMediaAccountAndPromoItem(PromoItem promoItem, MediaAccount mediaAccount);
}
