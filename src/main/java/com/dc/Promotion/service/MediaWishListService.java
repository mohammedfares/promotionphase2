package com.dc.Promotion.service;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaWishList;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.Status;

import java.util.List;

public interface MediaWishListService {

    public MediaWishList findById(long id);

    public MediaWishList create(MediaWishList wishList);

    public List<MediaWishList> findAll(MediaAccount mediaAccount, Status status);

    public MediaWishList find(MediaAccount account,Long id);
}
