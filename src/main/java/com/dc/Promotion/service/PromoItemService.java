package com.dc.Promotion.service;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.resources.PromoItemCaptionResources;
import com.dc.Promotion.resources.PromoItemImageResources;
import com.dc.Promotion.resources.SpecificationResources;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface PromoItemService {
    public PromoItem findById(long id);

    public PromoItem create(PromoItem promoItem, List<PromoItemImageResources> imageList, List<PromoItemCaptionResources> captionList, List<SpecificationResources> specificationList);

    public PromoItem createTemp(PromoItem promoItem, List<PromoItemImageResources> imageList, List<PromoItemCaptionResources> captionList, List<SpecificationResources> specificationList);


    public Page<PromoItem> findAll(PromoItem itemSearchCriteria, Integer page, Integer size);

    public List<PromoItem> findAllByPromo(Promo promo);

    public List<PromoItem> findWatchListCustomer(CustomerAccount account);

    public List<PromoItem> findWatchListMedia(MediaAccount account);

    public Page<PromoItem> findTrendItem(PromoItem promoItem, Integer page, Integer size);

    public Page<PromoItem> filterCustomerPromoItemAll(
            String name,
            Industry industry,
            Merchant merchant,
            ItemCategory itemCategory,
            Status status,
            Double newPriceFrom,
            Double newPriceTo,
            Integer trendFlag,
            LocalDateTime startDate,
            LocalDateTime expiryDate,
            Double longitude,
            Double latitude,
            Double radius,
            Integer page,
            Integer size);

    public Page<PromoItem> filterCustomerPromoItemWatchList(
            String name,
            Industry industry,
            Merchant merchant,
            ItemCategory itemCategory,
            Status status,
            CustomerAccount customerAccount,
            Double newPriceFrom,
            Double newPriceTo,
            Integer trendFlag,
            LocalDateTime startDate,
            LocalDateTime expiryDate,
            Double longitude,
            Double latitude,
            Double radius,
            Integer page,
            Integer size);

    public Page<PromoItem> filterMediaPromoItemWatchList(
            String name,
            Industry industry,
            Merchant merchant,
            ItemCategory itemCategory,
            Status status,
            MediaAccount mediaAccount,
            Double newPriceFrom,
            Double newPriceTo,
            Integer trendFlag,
            LocalDateTime startDate,
            LocalDateTime expiryDate,
            Double longitude,
            Double latitude,
            Double radius,
            Integer page,
            Integer size);


    public List<PromoItem> top5PromoItem(Merchant merchant);

    public Long totalPromoItem(Merchant merchant);

    public void deletePromoItemByPromo(Promo promo);

    public Page<PromoItem> findByCompare(PromoItem promoItem,Double longitude,Double latitude,
                                         Double radius,Integer page, Integer size);


    public Page<PromoItem> filterCustomerPromoItemWishList(
            String name,
            Industry industry,
            Merchant merchant,
            ItemCategory itemCategory,
            Status status,
            CustomerAccount customerAccount,
            Double newPriceFrom,
            Double newPriceTo,
            Integer trendFlag,
            LocalDateTime startDate,
            LocalDateTime expiryDate,
            Double longitude,
            Double latitude,
            Double radius,
            Integer page,
            Integer size);

    public Page<PromoItem> filterMediaPromoItemWishList(
            String name,
            Industry industry,
            Merchant merchant,
            ItemCategory itemCategory,
            Status status,
            MediaAccount mediaAccount,
            Double newPriceFrom,
            Double newPriceTo,
            Integer trendFlag,
            LocalDateTime startDate,
            LocalDateTime expiryDate,
            Double longitude,
            Double latitude,
            Double radius,
            Integer page,
            Integer size);

    public Page<PromoItem> itemReport(LocalDateTime startDate,
                                      LocalDateTime endDate,
                                      Promo promo,
                                      Merchant merchant,
                                      Integer page,
                                      Integer size);
}
