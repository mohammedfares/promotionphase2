package com.dc.Promotion.service;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaType;
import com.dc.Promotion.entities.OperationLanguage;

public interface MediaAccountService {

    public MediaAccount findById(Long id);

    public MediaAccount findByUsernameAndType(String username , MediaType mediaType);

    public MediaAccount createFaceBookAccount(MediaAccount mediaAccount);

    public MediaAccount updateAccountInfo(MediaAccount mediaAccount);

    public MediaAccount changeLanguage(MediaAccount account, OperationLanguage operationLanguage);


}
