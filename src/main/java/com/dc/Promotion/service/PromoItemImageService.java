package com.dc.Promotion.service;

import com.dc.Promotion.entities.PromoItemImage;

public interface PromoItemImageService {
    public PromoItemImage findById(long id);
}
