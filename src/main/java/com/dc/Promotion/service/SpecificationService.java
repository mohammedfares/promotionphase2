package com.dc.Promotion.service;

import com.dc.Promotion.entities.Specification;

public interface SpecificationService {

    public Specification findById(long id);
}
