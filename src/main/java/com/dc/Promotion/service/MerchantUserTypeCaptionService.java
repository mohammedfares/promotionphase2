package com.dc.Promotion.service;

import com.dc.Promotion.entities.MerchantUserType;
import com.dc.Promotion.entities.MerchantUserTypeCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.List;

public interface MerchantUserTypeCaptionService {
    public MerchantUserTypeCaption findById(long id);

    public List<MerchantUserTypeCaption> findByOperationLanguage( OperationLanguage language);

    public MerchantUserTypeCaption findByUserTypeAndLanguage(MerchantUserType userType,OperationLanguage language);

}
