package com.dc.Promotion.service;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerPromoView;
import com.dc.Promotion.entities.Promo;

public interface CustomerPromoViewService {
    public CustomerPromoView findById(long id);

    public CustomerPromoView create(CustomerPromoView customerPromoView);
}
