package com.dc.Promotion.service;

import com.dc.Promotion.entities.NotificationTemplate;

public interface NotificationTemplateService {

    public NotificationTemplate findByCode(String code);


}
