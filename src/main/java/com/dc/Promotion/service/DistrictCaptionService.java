package com.dc.Promotion.service;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.District;
import com.dc.Promotion.entities.DistrictCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.List;

public interface DistrictCaptionService {

    public DistrictCaption findById(long id);
    public DistrictCaption findByDistrictAndLanguage(District district, OperationLanguage language);

    public List<DistrictCaption> findAllByLanguageAndDistrict_City(OperationLanguage language, City city);
}
