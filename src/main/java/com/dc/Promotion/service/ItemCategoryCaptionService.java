package com.dc.Promotion.service;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.ItemCategoryCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.List;

public interface ItemCategoryCaptionService {

    public ItemCategoryCaption findById(long id);

    public List<ItemCategoryCaption> findAll();

    public List<ItemCategoryCaption> findAllByLang(OperationLanguage language);

    public ItemCategoryCaption find(Long itemCategoryId, OperationLanguage operationLanguage);

    public ItemCategoryCaption findByCategoryAndLanguage(ItemCategory category,OperationLanguage language);
}
