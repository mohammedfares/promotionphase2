package com.dc.Promotion.service;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantBranch;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.Status;

import java.util.List;

import java.util.List;

public interface MerchantBranchService {
    public MerchantBranch findById(Long id);

    public MerchantBranch create(MerchantBranch merchantBranch);

    public MerchantBranch updateBranch(MerchantBranch branch);


    public List<MerchantBranch> findAllByPromo(Promo promo);

    public List<MerchantBranch> findAll(String name,
                                        String address,
                                        String contactNumber,
                                        Merchant merchant,
                                        Status status);


}
