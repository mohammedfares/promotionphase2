package com.dc.Promotion.service;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.OperationCountry;

import java.util.List;

public interface OperationCountryService {

    public OperationCountry findById(long id);

    public List<OperationCountry> findAll();

    public OperationCountry findByCountry(Country country);

}
