package com.dc.Promotion.service;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaWatchList;
import com.dc.Promotion.entities.PromoItem;

public interface MediaWatchListService {
    public MediaWatchList findById(long id);


    public MediaWatchList create(MediaWatchList  mediaWatchList);

    public MediaWatchList find(PromoItem promoItem, MediaAccount mediaAccount);

}
