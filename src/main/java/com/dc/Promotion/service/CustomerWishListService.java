package com.dc.Promotion.service;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerWishList;
import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.Status;

import java.util.List;

public interface CustomerWishListService {

    public CustomerWishList findById(long id);

    public CustomerWishList create(CustomerWishList customerWishList);

    public List<CustomerWishList> findAll(CustomerAccount customerAccount, Status status);

    public CustomerWishList find(CustomerAccount account ,Long id);
}
