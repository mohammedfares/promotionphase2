package com.dc.Promotion.service;

import com.dc.Promotion.entities.CustomerNotification;

public interface CustomerNotificationService {

    public CustomerNotification findById(Long id);
}
