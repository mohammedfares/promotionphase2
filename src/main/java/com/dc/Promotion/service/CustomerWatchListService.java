package com.dc.Promotion.service;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerWatchList;
import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.repository.CustomerWatchListRepo;
import com.dc.Promotion.resources.CustomerWatchListResources;

public interface CustomerWatchListService {

    public CustomerWatchList findById(long id);

    public CustomerWatchList create(CustomerWatchList customerWatchList);

    public CustomerWatchList find(PromoItem promoItem, CustomerAccount account);
}
