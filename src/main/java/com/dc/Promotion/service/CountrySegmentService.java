package com.dc.Promotion.service;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.CountrySegment;
import com.dc.Promotion.entities.MerchantCustomerSegment;

import java.util.List;

public interface CountrySegmentService {
    public CountrySegment findById(Long id);
    public CountrySegment create(CountrySegment countrySegment);

    public List<Country> findAllCountry(MerchantCustomerSegment segment);
}
