package com.dc.Promotion.service;

import com.dc.Promotion.entities.MediaType;

public interface MediaTypeService {
    public MediaType findById(Long id);
    public MediaType findByCode(String code);
}
