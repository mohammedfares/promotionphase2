package com.dc.Promotion.service;

import com.dc.Promotion.entities.*;

import java.util.List;

public interface CityCaptionService  {

    public CityCaption findById(long id);

    public CityCaption findByCityAndLanguage(City city, OperationLanguage language);

    public List<CityCaption> findByLanguageAndCity_Country(OperationLanguage language, OperationCountry country);
}
