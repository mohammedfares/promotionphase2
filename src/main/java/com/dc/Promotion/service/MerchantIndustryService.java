package com.dc.Promotion.service;

import com.dc.Promotion.entities.MerchantIndustry;

public interface MerchantIndustryService {
    public MerchantIndustry findById(long id);
}
