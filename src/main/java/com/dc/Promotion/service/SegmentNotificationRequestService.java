package com.dc.Promotion.service;

import com.dc.Promotion.entities.MerchantCustomerSegment;
import com.dc.Promotion.entities.SegmentNotificationRequest;

public interface SegmentNotificationRequestService {


    public SegmentNotificationRequest findById(Long id);

    public SegmentNotificationRequest createRequest(SegmentNotificationRequest request);

    public void notifyCustomers(SegmentNotificationRequest request);

    public void notifyMedia(SegmentNotificationRequest request);

    public Long findTotalCustomer(MerchantCustomerSegment customerSegment);

    public Long findTotalNotification(MerchantCustomerSegment customerSegment);

    public Long findTotalViews(MerchantCustomerSegment customerSegment);
}
