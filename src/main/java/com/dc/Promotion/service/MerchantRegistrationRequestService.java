package com.dc.Promotion.service;

import com.dc.Promotion.entities.*;

import java.util.List;

public interface MerchantRegistrationRequestService {
    public MerchantRegistrationRequest findById(long id);

    public MerchantRegistrationRequest create(MerchantRegistrationRequest registrationRequest, List<Long> industryList);

    public MerchantRegistrationRequest rejectRequest(Long requestId, OperationLanguage language);

    public MerchantRegistrationRequest approveRequest(Long requestId, OperationLanguage language);

    public List<MerchantRegistrationRequest> findAllByStatus(Status status);

    public String deleteRequest(long id, OperationLanguage language);

    public MerchantRegistrationRequest findByEmail(String email);

    public MerchantRegistrationRequest findByUsername(String username);

    public List<MerchantRegistrationRequest> findAll();

    public MerchantRegistrationRequest findByCRN(String crn);

}
