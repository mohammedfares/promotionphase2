package com.dc.Promotion.service;

import com.dc.Promotion.entities.Status;

import java.util.List;

public interface StatusService {
    public Status findById(long id);

    public List<Status> getAllRegistrationRequestStatus(String code);
    public Status findByCode(String code);

}
