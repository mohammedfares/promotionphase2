package com.dc.Promotion.service;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.PromoItemCaption;

import java.util.List;

public interface PromoItemCaptionService {
    public PromoItemCaption findById(long id);

    public List<PromoItemCaption> findAllByPromo(Promo promo);


    public PromoItemCaption find(PromoItem promoItem, OperationLanguage operationLanguage);

    public List<PromoItemCaption> find(PromoItem promoItem);
}
