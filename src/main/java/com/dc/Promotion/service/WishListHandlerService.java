package com.dc.Promotion.service;

import com.dc.Promotion.entities.*;

import java.util.List;

public interface WishListHandlerService {
    public void refreshCustomerWishList(CustomerAccount customerAccount);

    public void refreshMediaWishList(MediaAccount mediaAccount);

    public List<PromoItem> findPromoItemByCustomerWish(CustomerWishList customerWishList);

    public List<PromoItem> findPromoItemByMediaWish(MediaWishList mediaWishList);
}
