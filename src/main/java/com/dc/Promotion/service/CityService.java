package com.dc.Promotion.service;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.Country;

import java.util.List;

public interface CityService {

    public City findById(long id);
    public List<City> findAllByCountry(Country country);
}
