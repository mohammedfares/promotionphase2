package com.dc.Promotion.service;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.District;
import com.dc.Promotion.entities.Status;

import java.util.List;

public interface DistrictService {

    public District findById(long id);

    public List<District> findAllByCity(City city);
}
