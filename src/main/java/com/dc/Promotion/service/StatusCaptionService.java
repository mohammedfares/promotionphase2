package com.dc.Promotion.service;

import com.dc.Promotion.entities.OperationCountry;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.entities.StatusCaption;

import java.util.List;

public interface StatusCaptionService {
    public StatusCaption findById(long id);

    public List<StatusCaption> find(OperationLanguage language,String code);

    public List<StatusCaption> findWithoutDeleted(OperationLanguage language,String code);

    public StatusCaption findByStatusAndLanguage(Status status, OperationLanguage language);


}
