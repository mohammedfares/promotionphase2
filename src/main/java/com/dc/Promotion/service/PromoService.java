package com.dc.Promotion.service;

import com.dc.Promotion.entities.*;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;
import java.util.List;

import com.dc.Promotion.resources.PromoResources;

public interface PromoService {
    public Promo findById(Long id);

    public Promo createTemp(Promo promo);

    public Promo createPromo(Promo promo);

    public Page<Promo> findAll(Promo promoSearchCriteria, Integer page, Integer size);

    public Page<Promo> findTrend(Promo promo, Integer page, Integer size);

    public Promo changePromoStatus(PromoResources promoResources, Status activePromoStatus, Status activePromoItem);

    public List<Promo> findAllTempByMerchant(Merchant merchant);

    public Page<Promo> search(String name,
                              String description,
                              LocalDateTime creationDateStart,
                              LocalDateTime creationDateEnd,
                              LocalDateTime expirationDateStart,
                              LocalDateTime expirationDateEnd,
                              LocalDateTime startDateStart,
                              LocalDateTime startDateEnd,
                              Merchant merchant,
                              Status status,
                              Integer page, Integer size);


    public Page<Promo> filterNearMe(String name,
                                    Double longitude,
                                    Double latitude,
                                    Double radius,
                                    Integer page,
                                    Integer size);

    public Page<Promo> filterCustomerPromoAll(
            String name,
            Industry industry,
            Merchant merchant,
            Status status,
            Integer trendFlag,
            LocalDateTime startDate,
            LocalDateTime expiryDate,
            Double longitude,
            Double latitude,
            Double radius,
            Integer page,
            Integer size);


    public List<Promo> top3Promo(Merchant merchant);

    public Long totalPromo(Merchant merchant);

    public Long sumOfView(Merchant merchant);

    public Page<Promo> promoReport(LocalDateTime startDate,
                                   LocalDateTime expiryDate,
                                   Long branchId,
                                   Merchant merchant,
                                   Integer page,
                                   Integer size);

    public void uploadPromosExcelFile(Workbook workbook, Merchant merchant);

    public void deletePromo(Long promoId);
}
