package com.dc.Promotion.service;

import com.dc.Promotion.entities.NotificationTemplate;
import com.dc.Promotion.entities.NotificationTemplateCaption;
import com.dc.Promotion.entities.OperationLanguage;

public interface NotificationTemplateCaptionService {


    public NotificationTemplateCaption findByNotificationTemplateAndOperationLanguage(NotificationTemplate template, OperationLanguage operationLanguage);



}
