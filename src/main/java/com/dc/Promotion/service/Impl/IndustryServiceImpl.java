package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.repository.IndustryRepo;
import com.dc.Promotion.service.IndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class IndustryServiceImpl implements IndustryService {

    @Autowired
    private IndustryRepo industryRepo;

    @Override
    public Industry findById(Long id) {
        Industry industry = industryRepo.findById(id);
        return industry;
    }
}
