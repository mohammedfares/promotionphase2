package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.repository.ItemCategoryRepo;
import com.dc.Promotion.service.ItemCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class ItemCategoryServiceImpl implements ItemCategoryService {

    @Autowired
    private ItemCategoryRepo itemCategoryRepo;
    @Override
    public ItemCategory findById(Long id) {
        ItemCategory category=itemCategoryRepo.findById(id);
        return category;
    }

    @Override
    public List<ItemCategory> findTopViewCategory(Merchant merchant) {
        return itemCategoryRepo.findTotalViewCategory(merchant, LocalDateTime.now());
    }

    @Override
    public List<ItemCategory> findAll() {
        return itemCategoryRepo.findAll();
    }
}
