package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.OTP;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.OTPRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.OTPService;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class OTPServiceImpl implements OTPService {
    @Autowired
    private OTPRepo otpRepo;
    @Autowired
    private StatusRepo statusRepo;
    @Autowired
    private ApplicationContext applicationContext;


    public OTP findById(long id) {
        OTP otp=otpRepo.findById(id);
        return otp;
    }

    @Override
    public OTP findByMobileAndCode(String mobile, String code) {

        OTP otp=otpRepo.findByMobileAndCode(mobile,code);

        return otp;
    }

    @Override
    public OTP createOTP(OTP otp) {
        otp.setCreationDate(LocalDateTime.now());
        otp.setCode(Utils.randomNumber(4));
        Status Active=statusRepo.findByCode("OTP_ACTIVE");
        otp.setStatus(Active);
        otp.setExpiryDate(LocalDateTime.now().plusMinutes(2));
        return otpRepo.save( otp);
    }

    @Override
    public OTP deActive(OTP otp) {
        OperationLanguage operationLanguage = otp.getOperationLanguage();
        OTP verificationOTP=otpRepo.findByCode(otp.getCode());
        if(verificationOTP==null){
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "otp404", operationLanguage.getCode(), null);
        }
        Status deActive=statusRepo.findByCode("OTP_INACTIVE");
        verificationOTP.setStatus(deActive);
        return verificationOTP;
    }

    @Override
    public OTP findActiveOtp(String mobile, String status) {
        OTP otp=otpRepo.findByMobileAndStatus_Code(mobile,status);
        return otp;
    }
}
