package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantBranch;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MerchantBranchRepo;
import com.dc.Promotion.repository.MerchantRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.MerchantBranchService;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MerchantBranchServiceImpl implements MerchantBranchService {
    @Autowired
    private MerchantBranchRepo merchantBranchRepo;
    @Autowired
    private StatusRepo statusRepo;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private MerchantRepo merchantRepo;

    public MerchantBranch findById(Long id) {
        MerchantBranch merchantBranch = merchantBranchRepo.findById(id);
        return merchantBranch;
    }

    @Override
    public MerchantBranch create(MerchantBranch merchantBranch) {

        //active branch status code
        Status status = statusRepo.findByCode("BRA_ACTIVE");
        if (status == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", merchantBranch.getOperationLanguage().getCode(), null);
        }

        Merchant merchant = merchantRepo.findById(merchantBranch.getMerchant().getId());
        if (merchant == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mer404", merchantBranch.getOperationLanguage().getCode(), null);

        merchantBranch.setMerchant(merchant);
        merchantBranch.setCreateDate(LocalDateTime.now());
        merchantBranch.setExpiryDate(LocalDateTime.now().plusYears(2));
        merchantBranch.setStatus(status);
        merchantBranch = merchantBranchRepo.save(merchantBranch);
        return merchantBranch;
    }

    @Override
    public MerchantBranch updateBranch(MerchantBranch branch) {

        MerchantBranch merchantBranch = merchantBranchRepo.findById(branch.getId());
        if (merchantBranch == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mbr404", branch.getOperationLanguage().getCode(), null);
        merchantBranch.setContactNumber(branch.getContactNumber());
        merchantBranch.setCreateDate(branch.getCreateDate());
        merchantBranch.setMerchant(branch.getMerchant());
        merchantBranch.setAddress(branch.getAddress());
        merchantBranch.setLatitude(branch.getLatitude());
        merchantBranch.setLongitude(branch.getLongitude());


        return merchantBranch;

    }

    @Override

    public List<MerchantBranch> findAllByPromo(Promo promo) {
        return merchantBranchRepo.findByQuery(promo);
    }

    public List<MerchantBranch> findAll(String name,
                                        String address,
                                        String contactNumber,
                                        Merchant merchant,
                                        Status status) {


        List<MerchantBranch> merchantBranchList = merchantBranchRepo.findByQuery(
                Utils.appendLikeOperator(name) ,
                Utils.appendLikeOperator(address) ,
                Utils.appendLikeOperator(contactNumber)
                ,merchant
                ,status);

//        List<MerchantBranch> merchantBranch = merchantBranchRepo.findAll(new Specification<MerchantBranch>() {
//            @Override
//            public Predicate toPredicate(Root<MerchantBranch> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
//
//                List<Predicate> predicateList = new ArrayList<>();
//
//                if (branches.getContactNumber() != null && !branches.getContactNumber().isEmpty()) {
//                    predicateList.add(criteriaBuilder.like(root.get("contactNumber"), "%" + branches.getContactNumber() + "%"));
//                }
//
//                if (branches.getMerchant() != null) {
//                    predicateList.add(criteriaBuilder.equal(root.get("merchant"), branches.getMerchant()));
//                }
//
//                predicateList.add(criteriaBuilder.notEqual(root.get("status"), statusRepo.findByCode("BRA_DELETED")));
//
//                return criteriaBuilder.and(predicateList.toArray(new Predicate[0]));
//            }
//        });

        return merchantBranchList;
    }
}
