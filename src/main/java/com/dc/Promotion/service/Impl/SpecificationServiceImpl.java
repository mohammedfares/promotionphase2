package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Specification;
import com.dc.Promotion.repository.SpecificationRepo;
import com.dc.Promotion.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class SpecificationServiceImpl implements SpecificationService {

    @Autowired
    private SpecificationRepo specificationRepo;

    @Override
    public Specification findById(long id) {

        Specification specification = specificationRepo.findById(id);
        return specification;
    }
}
