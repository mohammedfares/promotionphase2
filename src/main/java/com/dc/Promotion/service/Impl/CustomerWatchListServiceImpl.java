package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.CustomerAccountRepo;
import com.dc.Promotion.repository.CustomerWatchListRepo;
import com.dc.Promotion.repository.PromoItemRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.resources.CustomerWatchListResources;
import com.dc.Promotion.service.CustomerWatchListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@Transactional
public class CustomerWatchListServiceImpl implements CustomerWatchListService {

    @Autowired
    private CustomerWatchListRepo customerWatchListRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private PromoItemRepo itemRepo;

    @Autowired
    private CustomerAccountRepo accountRepo;

    @Override
    public CustomerWatchList findById(long id) {
        CustomerWatchList customerWatchList = customerWatchListRepo.findById(id);
        return customerWatchList;
    }

    @Override
    public CustomerWatchList create(CustomerWatchList customerWatchList) {
        PromoItem promoItem = itemRepo.findById(customerWatchList.getPromoItem().getId());
        if (promoItem == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", customerWatchList.getOperationLanguage().getCode(), null);
        CustomerAccount account = accountRepo.findById(customerWatchList.getCustomerAccount().getId());
        if (account == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "user404", customerWatchList.getOperationLanguage().getCode(), null);


        Status status = statusRepo.findByCode("CWA_ACTIVE");
        CustomerWatchList newCustomerWatchList = customerWatchListRepo.findByPromoItemAndCustomerAccount(promoItem, account);
        if (newCustomerWatchList == null) {
            newCustomerWatchList = new CustomerWatchList();
            newCustomerWatchList.setStatus(status);
            newCustomerWatchList.setPromoItem(promoItem);
            newCustomerWatchList.setCustomerAccount(account);
            newCustomerWatchList.setCreateDate(LocalDateTime.now());
            customerWatchListRepo.save(newCustomerWatchList);
        } else if (newCustomerWatchList != null && newCustomerWatchList.getStatus().getCode().equals(status.getCode())) {
            status = statusRepo.findByCode("CWA_INACTIVE");
            newCustomerWatchList.setStatus(status);
        } else if (newCustomerWatchList != null && !newCustomerWatchList.getStatus().getCode().equals(status.getCode())) {
            newCustomerWatchList.setStatus(status);
        }

        return newCustomerWatchList;
    }

    @Override
    public CustomerWatchList find(PromoItem promoItem, CustomerAccount account) {
        return customerWatchListRepo.findByPromoItemAndCustomerAccountAndStatus(promoItem, account, statusRepo.findByCode("CWA_ACTIVE"));
    }
}
