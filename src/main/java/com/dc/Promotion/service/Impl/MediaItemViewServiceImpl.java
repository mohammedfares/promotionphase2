package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaItemView;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MediaAccountRepo;
import com.dc.Promotion.repository.MediaItemViewRepo;
import com.dc.Promotion.repository.PromoItemRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.MediaItemViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class MediaItemViewServiceImpl implements MediaItemViewService {

    @Autowired
    private MediaItemViewRepo mediaItemViewRepo;

    @Autowired
    private MediaAccountRepo mediaAccountRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private PromoItemRepo promoItemRepo;

    @Override
    public MediaItemView findById(Long id) {
        MediaItemView mediaItemView = mediaItemViewRepo.findById(id);
        return mediaItemView;
    }

    @Override
    public MediaItemView create(MediaItemView itemView) {

        MediaAccount mediaAccount = mediaAccountRepo.findById(itemView.getMediaAccount().getId());

        PromoItem promoItem=promoItemRepo.findById(itemView.getPromoItem().getId());

        if(promoItem==null){

            throw new ResourceException(applicationContext,HttpStatus.NOT_FOUND,"pit404",itemView.getOperationLanguage().getCode(), null);
        }

        MediaItemView mediaItemView=mediaItemViewRepo.findByPromoItemAndMediaAccount(promoItem,mediaAccount);

            if(mediaItemView==null) {
                itemView.setMediaAccount(mediaAccount);
                itemView.setPromoItem(promoItem);

                Status status = statusRepo.findByCode("MIV_ACTIVE");
                if (status == null) {
                    throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", itemView.getOperationLanguage().getCode(), null);

                }

                itemView.setStatus(status);
                itemView.setCreateDate(LocalDateTime.now());
                promoItem.setViewNo(promoItem.getViewNo()+1L);
                mediaItemViewRepo.save(itemView);


            }
        return itemView;
    }

    @Override
    public MediaItemView findByMediaAccountAndPromoItem(PromoItem promoItem, MediaAccount mediaAccount) {

        MediaItemView mediaItemView=mediaItemViewRepo.findByPromoItemAndMediaAccount(promoItem,mediaAccount);
        return mediaItemView;
    }
}
