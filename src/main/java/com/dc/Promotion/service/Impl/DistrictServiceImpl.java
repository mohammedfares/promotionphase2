package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.District;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.repository.DistrictRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DistrictServiceImpl implements DistrictService {

    @Autowired
    private DistrictRepo districtRepo;
    @Autowired
    private StatusRepo statusRepo;

    @Override
    public District findById(long id) {
        District  district=districtRepo.findById(id);
        return district;
    }

    @Override
    public List<District> findAllByCity(City city) {

        //code for active district
        Status status=statusRepo.findByCode("");
        List<District> districts=districtRepo.findAllByCityAndStatus(city,status);
        return districts;
    }
}
