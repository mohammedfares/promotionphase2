package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.MediaNotification;
import com.dc.Promotion.repository.MediaNotificationRepo;
import com.dc.Promotion.service.MediaNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class MediaNotificationServiceImpl implements MediaNotificationService {
    @Autowired
    private MediaNotificationRepo mediaNotificationRepo;


    @Override
    public MediaNotification findById(Long id) {
        MediaNotification mediaNotification=mediaNotificationRepo.findById(id);
        return mediaNotification;
    }

    @Override
    public MediaNotification save(MediaNotification notification) {
        MediaNotification mediaNotification=mediaNotificationRepo.save(notification);

        return mediaNotification;
    }
}
