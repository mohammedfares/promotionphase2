package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.MerchantRegistrationRequest;
import com.dc.Promotion.entities.RegistrationRequestIndustry;
import com.dc.Promotion.repository.RegistrationRequestIndustryRepo;
import com.dc.Promotion.service.RegistrationRequestIndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RegistrationRequestIndustryServiceImpl implements RegistrationRequestIndustryService {

    @Autowired
    private RegistrationRequestIndustryRepo registrationRequestIndustryRepo;
    @Override
    public RegistrationRequestIndustry findById(long id) {
        RegistrationRequestIndustry registrationRequestIndustry=registrationRequestIndustryRepo.findById(id);
        return registrationRequestIndustry;
    }

    @Override
    public List<Industry> findByMerchantReq(MerchantRegistrationRequest registrationRequest) {
        List<Industry> industryList=registrationRequestIndustryRepo.findByQuery(registrationRequest);
        return industryList;
    }
}
