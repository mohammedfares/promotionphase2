package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.SystemMessage;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.SystemMessageRepo;
import com.dc.Promotion.service.SystemMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SystemMessageServiceImpl implements SystemMessageService {
    @Autowired
    private SystemMessageRepo systemMessageRepo;

    public SystemMessage findById(long id) {
        SystemMessage systemMessage = systemMessageRepo.findById(id);
        return systemMessage;
    }

    @Override
    public SystemMessage findByCode(String code) {
        SystemMessage systemMessage=systemMessageRepo.findByCode(code);
        return systemMessage;
    }
}
