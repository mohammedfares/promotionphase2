package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.NotificationTemplate;
import com.dc.Promotion.entities.NotificationTemplateCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.repository.NotificationTemplateCaptionRepo;
import com.dc.Promotion.service.NotificationTemplateCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class NotificationTemplateCaptionServiceImpl implements NotificationTemplateCaptionService {
    @Autowired
    private NotificationTemplateCaptionRepo notificationTemplateCaptionRepo;

    @Override
    public NotificationTemplateCaption findByNotificationTemplateAndOperationLanguage(NotificationTemplate template, OperationLanguage operationLanguage) {

        NotificationTemplateCaption notificationTemplateCaption=notificationTemplateCaptionRepo.findByNotificationTemplateAndOperationLanguage(template,operationLanguage);




        return notificationTemplateCaption;
    }
}
