package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MerchantRepo;
import com.dc.Promotion.repository.MerchantUserRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.security.SECCriptoRsa;
import com.dc.Promotion.service.MerchantService;
import com.dc.Promotion.service.MerchantUserService;
import com.dc.Promotion.service.StatusService;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MerchantUserServiceImpl implements MerchantUserService {

    @Autowired
    private MerchantUserRepo merchantUserRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MerchantRepo merchantRepo;

    public MerchantUser findById(long id) {
        MerchantUser merchantUser = merchantUserRepo.findMerchantUserById(id);
        return merchantUser;
    }

    @Override
    public MerchantUser create(MerchantUser merchantUser) {
        //status code for active merchant User
        Status status = statusRepo.findByCode("MUS_ACTIVE");

        if (status == null)
            //error code for not found data
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts400", merchantUser.getOperationLanguage().getCode(), null);

        merchantUser.setCreateDate(LocalDateTime.now());
        merchantUser.setExpireDate(LocalDateTime.now().plusYears(2));
        merchantUser.setStatus(status);
        merchantUser = merchantUserRepo.save(merchantUser);
        return merchantUser;
    }

    @Override
    public MerchantUser findByEmail(String email) {

        MerchantUser merchantUser = merchantUserRepo.findByEmail(email);
        return merchantUser;
    }

    @Override
    public MerchantUser findByMobile(String mobile) {
        MerchantUser merchantUser = merchantUserRepo.findByMobile(mobile);
        return merchantUser;
    }

    @Override
    public MerchantUser findByIdAndMerchantUserType(long id, String userType) {
        MerchantUser byId = merchantUserRepo.findMerchantUserById(id);
        userType = "MER_ADMIN";
        MerchantUser merchantUser = merchantUserRepo.findByIdAndMerchantUserType_Code(id, userType);

        return merchantUser;
    }

    @Override
    public List<MerchantUser> findAllByMerchant_IdAndStatus_Code(long id, String code) {
        Merchant merchant = merchantRepo.findById(id);
        List<MerchantUser> merchantUsers = merchantUserRepo.findAllByMerchant_IdAndStatus_Code(merchant.getId(), code);
        return merchantUsers;
    }

    @Override
    public MerchantUser changePassword(MerchantUser user, String newPassword) {
        MerchantUser merchantUser = merchantUserRepo.findMerchantUserById(user.getId());
        if (merchantUser.getPassword().equals(newPassword))
            throw new ResourceException(applicationContext, HttpStatus.CONFLICT, "pass409", user.getOperationLanguage().getCode(), null);

        user.setPassword(newPassword);
        return user;
    }

    @Override
    public MerchantUser updateUser(MerchantUser user) {

        MerchantUser merchantUser = merchantUserRepo.findMerchantUserById(user.getId());
        if (merchantUser == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mus404", user.getOperationLanguage().getCode(), null);
        merchantUser.setEmail(user.getEmail());
        merchantUser.setMobile(user.getMobile());
        merchantUser.setMerchantUserType(user.getMerchantUserType());
        merchantUser.setFirstName(user.getFirstName());
        merchantUser.setLastName(user.getLastName());
        return merchantUser;
    }

    @Override
    public MerchantUser resetMerchantUserPassword(MerchantUser user) {
        MerchantUser merchantUser = merchantUserRepo.findMerchantUserById(user.getId());
        merchantUser.setPassword(Utils.randomNumber(6));
        merchantUser.setPassword(SECCriptoRsa.encrypt(user.getPassword()));
        return merchantUser;
    }

    @Override
    public List<MerchantUser> findByParam(MerchantUser user, Status status, OperationLanguage language) {


//        List<MerchantUser> merchantUsers=


        return null;
    }

    @Override
    public List<MerchantUser> findAll(MerchantUser merchantUserSearchCriteria) {

        List<MerchantUser> merchantUserList = merchantUserRepo.findAll(new Specification<MerchantUser>() {
            @Override
            public Predicate toPredicate(Root<MerchantUser> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder cb) {

                List<Predicate> predicateList = new ArrayList<>();

                if (merchantUserSearchCriteria.getFirstName() != null && !merchantUserSearchCriteria.getFirstName().isEmpty()) {
                    predicateList.add(cb.like(cb.lower(root.get("firstName")), "%" + merchantUserSearchCriteria.getFirstName().toLowerCase().trim() + "%"));
                }

                if (merchantUserSearchCriteria.getLastName() != null && !merchantUserSearchCriteria.getLastName().isEmpty()) {
                    predicateList.add(cb.like(cb.lower(root.get("lastName")), "%" + merchantUserSearchCriteria.getLastName().toLowerCase().trim() + "%"));
                }

                if (merchantUserSearchCriteria.getMerchantUserType() != null) {
                    predicateList.add(cb.equal(root.get("merchantUserType"), merchantUserSearchCriteria.getMerchantUserType()));
                }
                if (merchantUserSearchCriteria.getUserName() != null && !merchantUserSearchCriteria.getUserName().isEmpty()) {
                    predicateList.add(cb.like(cb.lower(root.get("userName")), "%" + merchantUserSearchCriteria.getUserName().toLowerCase().trim() + "%"));
                }
                if (merchantUserSearchCriteria.getEmail() != null && !merchantUserSearchCriteria.getEmail().isEmpty()) {
                    predicateList.add(cb.like(cb.lower(root.get("email")), "%" + merchantUserSearchCriteria.getEmail().toLowerCase().trim() + "%"));
                }
                if (merchantUserSearchCriteria.getMobile() != null && !merchantUserSearchCriteria.getMobile().isEmpty()) {
                    predicateList.add(cb.like(cb.lower(root.get("mobile")), "%" + merchantUserSearchCriteria.getMobile().trim() + "%"));
                }

                if (merchantUserSearchCriteria.getStatus() != null) {
                    predicateList.add(cb.equal(root.get("status"), merchantUserSearchCriteria.getStatus()));
                }
                if (merchantUserSearchCriteria.getMerchant() != null) {
                    predicateList.add(cb.equal(root.get("merchant"), merchantUserSearchCriteria.getMerchant()));
                }

                predicateList.add(cb.notEqual(root.get("status"), statusRepo.findByCode("MUS_DELETE")));

                return cb.and(predicateList.toArray(new Predicate[0]));
            }
        });
        return merchantUserList;
    }

    @Override
    public MerchantUser findByUsername(String username) {

        MerchantUser merchantUser = merchantUserRepo.findByUserName(username);
        return merchantUser;
    }

    @Override
    public MerchantUser findMerchantByUsernameOrEmail(String usernameOrEmail) {
        return merchantUserRepo.findMerchantByUsernameOrEmail(usernameOrEmail);
    }

    @Override
    public MerchantUser changePassword(MerchantUser merchantUser) {

        return merchantUser;
    }


}
