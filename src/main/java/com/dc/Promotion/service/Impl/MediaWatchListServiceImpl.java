package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaWatchList;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MediaAccountRepo;
import com.dc.Promotion.repository.MediaWatchListRepo;
import com.dc.Promotion.repository.PromoItemRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.MediaWatchListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@Transactional
public class MediaWatchListServiceImpl implements MediaWatchListService {

    @Autowired
    private MediaWatchListRepo mediaWatchListRepo;

    @Autowired
    private PromoItemRepo promoItemRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MediaAccountRepo mediaAccountRepo;



    @Autowired
    private StatusRepo statusRepo;
    @Override
    public MediaWatchList findById(long id) {
        MediaWatchList mediaWatchList=mediaWatchListRepo.findById(id);
        return mediaWatchList;
    }

    @Override
    public MediaWatchList create(MediaWatchList mediaWatchList) {
        PromoItem promoItem = promoItemRepo.findById(mediaWatchList.getPromoItem().getId());
        if (promoItem == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", mediaWatchList.getOperationLanguage().getCode(), null);
        MediaAccount account = mediaAccountRepo.findById(mediaWatchList.getMediaAccount().getId());

        if (account == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "user404", mediaWatchList.getOperationLanguage().getCode(), null);

        Status status = statusRepo.findByCode("MWA_ACTIVE");
        MediaWatchList newMediaWatchList = mediaWatchListRepo.findByPromoItemAndMediaAccount(promoItem, account);
        if (newMediaWatchList == null) {
            newMediaWatchList = new MediaWatchList();
            newMediaWatchList.setStatus(status);
            newMediaWatchList.setPromoItem(promoItem);
            newMediaWatchList.setMediaAccount(account);
            newMediaWatchList.setCreateDate(LocalDateTime.now());
            mediaWatchListRepo.save(newMediaWatchList);
        } else if (newMediaWatchList != null && newMediaWatchList.getStatus().getCode().equals(status.getCode())) {
            status = statusRepo.findByCode("MWA_INACTIVE");
            newMediaWatchList.setStatus(status);
        } else if (newMediaWatchList != null && !newMediaWatchList.getStatus().getCode().equals(status.getCode())) {
            newMediaWatchList.setStatus(status);
        }

        return newMediaWatchList;

    }

    @Override
    public MediaWatchList find(PromoItem promoItem, MediaAccount mediaAccount) {
        MediaWatchList mediaWatchList=mediaWatchListRepo.findByPromoItemAndMediaAccount(promoItem,mediaAccount);
        return mediaWatchList;
    }
}
