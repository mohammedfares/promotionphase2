package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.ItemCategoryCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.repository.ItemCategoryCaptionRepo;
import com.dc.Promotion.service.ItemCategoryCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ItemCategoryCaptionServiceImpl implements ItemCategoryCaptionService {

    @Autowired
    private ItemCategoryCaptionRepo itemCategoryCaptionRepo;

    @Override
    public ItemCategoryCaption findById(long id) {
        ItemCategoryCaption itemCategoryCaption = itemCategoryCaptionRepo.findById(id);
        return itemCategoryCaption;
    }

    @Override
    public List<ItemCategoryCaption> findAll() {
        List<ItemCategoryCaption> captions = itemCategoryCaptionRepo.findAll();
        return captions;
    }

    @Override
    public List<ItemCategoryCaption> findAllByLang(OperationLanguage language) {

        List<ItemCategoryCaption> categoryCaptionList = itemCategoryCaptionRepo.findAllByLanguageId(language);
        return categoryCaptionList;
    }

    @Override
    public ItemCategoryCaption find(Long itemCategoryId, OperationLanguage operationLanguage) {
        return itemCategoryCaptionRepo.findByCategory_IdAndLanguageId(itemCategoryId, operationLanguage);
    }

    @Override
    public ItemCategoryCaption findByCategoryAndLanguage(ItemCategory category, OperationLanguage language) {
        ItemCategoryCaption categoryCaption=itemCategoryCaptionRepo.findByCategoryAndLanguageId(category,language);
        return categoryCaption;
    }
}
