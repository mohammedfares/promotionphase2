package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MerchantBranchCaptionRepo;
import com.dc.Promotion.repository.OperationLanguageRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.MerchantBranchCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MerchantBranchCaptionServiceImpl implements MerchantBranchCaptionService {

    @Autowired
    private MerchantBranchCaptionRepo merchantBranchCaptionRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private OperationLanguageRepo operationLanguageRepo;

    public MerchantBranchCaption findById(long id) {
        MerchantBranchCaption merchantBranchCaption = merchantBranchCaptionRepo.findById(id);
        return merchantBranchCaption;
    }

    @Override
    public List<MerchantBranchCaption> findByMerchant(Merchant merchant, OperationLanguage language, Status status) {
        List<MerchantBranchCaption> captionList = merchantBranchCaptionRepo.findAllByMerchantBranch_MerchantAndOperationLanguageAndMerchantBranch_Status(merchant, language, status);
        return captionList;
    }

    @Override
    public MerchantBranchCaption create(MerchantBranchCaption merchantBranchCaption) {
        MerchantBranchCaption branchCaption = merchantBranchCaptionRepo.save(merchantBranchCaption);

        return branchCaption;
    }

    @Override
    public List<MerchantBranchCaption> updateBranchCaption(List<MerchantBranchCaption> newMerchantBranchCaption) {
        List<MerchantBranchCaption> branchCaptionList = merchantBranchCaptionRepo.findAllByMerchantBranch(newMerchantBranchCaption.get(0).getMerchantBranch());

        branchCaptionList.forEach(merchantBranchCaptionObject -> {
            merchantBranchCaptionRepo.delete(merchantBranchCaptionObject.getId());
        });

        newMerchantBranchCaption.forEach(merchantBranchCaption -> {
            merchantBranchCaptionRepo.save(merchantBranchCaption);
        });

        return newMerchantBranchCaption;
    }

    @Override
    public MerchantBranchCaption find(MerchantBranch merchantBranch, OperationLanguage language) {
        return merchantBranchCaptionRepo.findMerchantBranchCaptionByMerchantBranchAndOperationLanguage(merchantBranch, language);
    }

    @Override
    public List<MerchantBranchCaption> findAll(MerchantBranchCaption merchantBranchCaptionSearchCriteria) {
        List<MerchantBranchCaption> merchantBranchCaptionList = merchantBranchCaptionRepo.findAll(new Specification<MerchantBranchCaption>() {
            @Override
            public Predicate toPredicate(Root<MerchantBranchCaption> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

                List<Predicate> predicateList = new ArrayList<>();

                if (merchantBranchCaptionSearchCriteria.getMerchantBranch() != null) {
                    predicateList.add(criteriaBuilder.equal((root.get("merchantBranch")),  merchantBranchCaptionSearchCriteria.getMerchantBranch()));
                }

                if (merchantBranchCaptionSearchCriteria.getName() != null && !merchantBranchCaptionSearchCriteria.getName().isEmpty()) {
                    predicateList.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), merchantBranchCaptionSearchCriteria.getName()));
                }

                if (merchantBranchCaptionSearchCriteria.getAddress() != null && !merchantBranchCaptionSearchCriteria.getAddress().isEmpty()) {
                    predicateList.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("address")), merchantBranchCaptionSearchCriteria.getAddress()));
                }

                return criteriaBuilder.and(predicateList.toArray(new Predicate[0]));
            }
        });

        return merchantBranchCaptionList;
    }

    @Override
    public List<MerchantBranchCaption> find(MerchantBranch merchantBranch) {

        return merchantBranchCaptionRepo.findAllByMerchantBranch(merchantBranch);
    }

    @Override
    public List<MerchantBranchCaption> findAllByPromo(Promo promo) {
        return null;
    }
}
