//package com.dc.Promotion.service.Impl;
//
//import com.dc.Promotion.entities.NationalityCaption;
//import com.dc.Promotion.entities.OperationLanguage;
//import com.dc.Promotion.repository.NationalityCaptionRepo;
//import com.dc.Promotion.service.NationalityCaptionService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import java.util.List;
//
//@Service
//@Transactional
//public class NationalityCaptionServiceImpl implements NationalityCaptionService {
//    @Autowired
//    private NationalityCaptionRepo nationalityCaptionRepo;
//
//    public NationalityCaption findById(long id) {
//        NationalityCaption nationalityCaption = nationalityCaptionRepo.findById(id);
//        return nationalityCaption;
//    }
//
//    @Override
//    public List<NationalityCaption> findAllByOperationLanguage(OperationLanguage language) {
//        List<NationalityCaption> nationalityCaptions=nationalityCaptionRepo.findAllByOperationLanguage_Id(language.getId());
//
//
//
//        return nationalityCaptions;
//    }
//}
