package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.SystemMessage;
import com.dc.Promotion.entities.SystemMessageCaption;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.SystemMessageCaptionRepo;
import com.dc.Promotion.service.SystemMessageCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SystemMessageCaptionServiceImpl implements SystemMessageCaptionService {
    @Autowired
    private SystemMessageCaptionRepo systemMessageCaptionRepo;
    public SystemMessageCaption findById(long id) {
        SystemMessageCaption systemMessageCaption=systemMessageCaptionRepo.findById(id);
        return systemMessageCaption;
    }

    @Override
    public SystemMessageCaption findByMessageAndLanguage(SystemMessage message, OperationLanguage language) {
        SystemMessageCaption systemMessageCaption=systemMessageCaptionRepo.findByMessageAndLanguage(message,language);
        if(systemMessageCaption==null){
            throw new ResourceException(HttpStatus.NOT_FOUND,"Message Not Found");
        }
        return systemMessageCaption;

    }
}
