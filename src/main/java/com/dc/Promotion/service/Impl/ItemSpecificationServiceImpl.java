package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.ItemSpecification;
import com.dc.Promotion.repository.ItemSpecificationRepo;
import com.dc.Promotion.service.ItemSpecificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class ItemSpecificationServiceImpl implements ItemSpecificationService {

    @Autowired
    private ItemSpecificationRepo itemSpecificationRepo;

    @Override
    public ItemSpecification findById(long id) {

        ItemSpecification itemSpecification =itemSpecificationRepo.findById(id);
        return itemSpecification;
    }
}
