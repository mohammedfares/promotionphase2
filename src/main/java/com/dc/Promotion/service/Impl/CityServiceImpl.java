package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.Country;
import com.dc.Promotion.repository.CityRepo;
import com.dc.Promotion.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepo cityRepo;

    @Override
    public City findById(long id) {
        City city=cityRepo.findById(id);
        return city;
    }

    @Override
    public List<City> findAllByCountry(Country country) {
        List<City> cityList=cityRepo.findAllByCountry(country);
        return cityList;
    }
}
