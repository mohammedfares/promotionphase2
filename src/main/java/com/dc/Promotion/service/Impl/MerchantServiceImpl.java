package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MerchantRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MerchantServiceImpl implements MerchantService {
    @Autowired
    private MerchantRepo merchantRepo;
    @Autowired
    StatusRepo statusRepo;
    @Autowired
    ApplicationContext applicationContext;

    public Merchant findById(Long id) {
        Merchant merchant = merchantRepo.findById(id);
        return merchant;
    }

    @Override
    public Merchant create(Merchant merchant) {

        //status code for active merchant
        Status status = statusRepo.findByCode("MER_ACTIVE");
        if (status == null)
            //error code for status not found
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts400", merchant.getOperationLanguage().getCode(), null);

        merchant.setStatus(status);
        //save active merchant
        merchant = merchantRepo.save(merchant);
        return merchant;
    }

    @Override
    public List<Merchant> findAll() {

        List<Merchant> merchantList=merchantRepo.findAll();
        return merchantList;
    }

    @Override
    public List<Merchant> findByIndustry(Industry industry) {
        List<Merchant> merchantList=merchantRepo.findByIndustry(industry);
        return merchantList;
    }
}
