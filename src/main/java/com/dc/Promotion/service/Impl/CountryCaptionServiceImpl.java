package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.CountryCaption;
import com.dc.Promotion.entities.OperationCountry;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.repository.CountryCaptionRepo;
import com.dc.Promotion.service.CountryCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CountryCaptionServiceImpl implements CountryCaptionService {

    @Autowired
    private CountryCaptionRepo countryCaptionRepo;
    @Override
    public CountryCaption findById(long id) {
        CountryCaption countryCaption=countryCaptionRepo.findById(id);
        return countryCaption;
    }

    @Override
    public CountryCaption findByCountryAndLanguage(Country country, OperationLanguage language) {
        CountryCaption caption=countryCaptionRepo.findByCountryAndLanguage(country,language);
        return caption;
    }

    @Override
    public List<CountryCaption> find( OperationLanguage language) {

        return countryCaptionRepo.findByQuery(language);
    }

    @Override
    public List<CountryCaption> findAllCountry(OperationLanguage language) {
        List<CountryCaption> countryCaptionList=countryCaptionRepo.findByQueryCountry(language);
        return countryCaptionList;
    }
}
