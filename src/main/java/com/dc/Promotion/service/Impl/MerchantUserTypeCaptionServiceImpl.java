package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.MerchantUserType;
import com.dc.Promotion.entities.MerchantUserTypeCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.repository.MerchantUserTypeCaptionRepo;
import com.dc.Promotion.service.MerchantUserTypeCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MerchantUserTypeCaptionServiceImpl implements MerchantUserTypeCaptionService {
    @Autowired
    private MerchantUserTypeCaptionRepo merchantUserTypeCaptionRepo;

    public MerchantUserTypeCaption findById(long id) {
        MerchantUserTypeCaption merchantUserTypeCaption=merchantUserTypeCaptionRepo.findById(id);
        return merchantUserTypeCaption;
    }

    @Override
    public List<MerchantUserTypeCaption> findByOperationLanguage( OperationLanguage language)
    {
        List<MerchantUserTypeCaption> typeCaptionList=merchantUserTypeCaptionRepo.findByOperationLanguage(language);
        return typeCaptionList;
    }

    @Override
    public MerchantUserTypeCaption findByUserTypeAndLanguage(MerchantUserType userType, OperationLanguage language) {
        MerchantUserTypeCaption merchantUserTypeCaption=
                merchantUserTypeCaptionRepo.findByMerchantUserTypeAndOperationLanguage(userType,language);
        return merchantUserTypeCaption;
    }
}
