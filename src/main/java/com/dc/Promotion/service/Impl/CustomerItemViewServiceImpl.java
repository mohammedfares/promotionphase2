package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerItemView;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.CustomerAccountRepo;
import com.dc.Promotion.repository.CustomerItemViewRepo;
import com.dc.Promotion.repository.PromoItemRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.CustomerItemViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@Transactional
public class CustomerItemViewServiceImpl implements CustomerItemViewService {

    @Autowired
    private CustomerItemViewRepo customerItemViewRepo;

    @Autowired
    private CustomerAccountRepo accountRepo;

    @Autowired
    private PromoItemRepo itemRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private StatusRepo statusRepo;

    @Override
    public CustomerItemView findById(long id) {
        CustomerItemView customerItemView=customerItemViewRepo.findById(id);
        return customerItemView;
    }

    @Override
    public CustomerItemView create(CustomerItemView customerItemView) {

        CustomerAccount account=accountRepo.findById(customerItemView.getCustomerAccount().getId());
        if(account==null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "user404", customerItemView.getOperationLanguage().getCode(), null);

        PromoItem promoItem=itemRepo.findById(customerItemView.getPromoItem().getId());
        if(promoItem==null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", customerItemView.getOperationLanguage().getCode(), null);

        Status status=statusRepo.findByCode("CIV_ACTIVE");
        if(status==null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", customerItemView.getOperationLanguage().getCode(), null);

        CustomerItemView itemView=customerItemViewRepo.findByPromoItemAndCustomerAccount(promoItem,account);
        if(itemView==null)
        {
            itemView=new CustomerItemView();
            itemView.setStatus(status);
            itemView.setCustomerAccount(account);
            itemView.setCreateDate(LocalDateTime.now());
            itemView.setPromoItem(promoItem);
            promoItem.setViewNo(promoItem.getViewNo()+1L);
            customerItemViewRepo.save(itemView);
        }

        return itemView;
    }
}
