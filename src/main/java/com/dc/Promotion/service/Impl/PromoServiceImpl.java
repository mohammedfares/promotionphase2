package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.*;
import com.dc.Promotion.resources.PromoResources;
import com.dc.Promotion.service.PromoService;
import com.dc.Promotion.utils.Utils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class PromoServiceImpl implements PromoService {

    @Autowired
    private PromoRepo promoRepo;


    @Autowired
    private StatusRepo statusRepo;
    @Autowired
    private SegmentNotificationRequestRepo segmentNotificationRequestRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private PromoItemRepo itemRepo;

    @Autowired
    private PromoBranchRepo promoBranchRepo;

    @Autowired
    private CustomerPromoViewRepo customerPromoViewRepo;

    @Autowired
    private PromoCaptionRepo promoCaptionRepo;

    @Autowired
    private OperationLanguageRepo operationLanguageRepo;

    @Autowired
    private ItemCategoryRepo itemCategoryRepo;

    @Autowired
    private PromoItemCaptionRepo promoItemCaptionRepo;

    @Autowired
    private PromoItemImageRepo promoItemImageRepo;

    @Autowired
    private SpecificationRepo specificationRepo;

    @Autowired
    private ItemSpecificationRepo itemSpecificationRepo;

    @Autowired
    private PromoSourceRepo promoSourceRepo;

    public Promo findById(Long id) {
        Promo promo = promoRepo.findById(id);
        return promo;
    }

    @Override
    public Promo createTemp(Promo promo) {

        if (promo.getId() == null || promo.getId() == 0) {
            Status status = statusRepo.findByCode("PRO_TEMP");
            if (status == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", promo.getOperationLanguage().getCode(), null);
            promo.setCreateDate(LocalDateTime.now());
            promo.setStatus(status);
            promo.setViewNo(0L);
            promo.setPromoSource(promoSourceRepo.findByCode("WEB"));
            //convertImage
            String imageUrlPromo = Utils.ConvertImage(promo.getCoverPhoto());
            if (imageUrlPromo != null) {
                promo.setCoverPhoto(imageUrlPromo);
            }
            promo = promoRepo.save(promo);
            return promo;
        } else {
            Promo currentPromo = promoRepo.findById(promo.getId());
            currentPromo.setPromoSource(promoSourceRepo.findByCode("WEB"));

            if (!currentPromo.getStatus().getCode().equals("PRO_TEMP")) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "proTemp404", promo.getOperationLanguage().getCode(), null);
            }

            Status status = statusRepo.findByCode("PRO_TEMP");
            if (status == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", promo.getOperationLanguage().getCode(), null);
            }
            currentPromo.setStatus(status);
            currentPromo.setStartDate(promo.getStartDate());
            currentPromo.setExpiryDate(promo.getExpiryDate());
            String imageUrlPromo = Utils.ConvertImage(promo.getCoverPhoto());
            if (imageUrlPromo != null) {
                currentPromo.setCoverPhoto(imageUrlPromo);
            }
            return currentPromo;
        }
    }

    @Override
    public Promo createPromo(Promo promo) {

        if (promo.getId() == null || promo.getId() == 0) {
            //create promo
            Status status = statusRepo.findByCode("PRO_NEW");
            if (status == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pro404", promo.getOperationLanguage().getCode(), null);
            }
            promo.setStatus(status);
            promo.setCreateDate(LocalDateTime.now());
            promo.setViewNo(0L);
            promo.setBarcode(Utils.randomNumber(10));
            promo.setPromoSource(promoSourceRepo.findByCode("WEB"));

            String imageUrlPromo = Utils.ConvertImage(promo.getCoverPhoto());
            if (imageUrlPromo != null) {
                promo.setCoverPhoto(imageUrlPromo);
            }
            Promo savePromo = promoRepo.save(promo);
            return savePromo;
        } else {
            //update promo
            Promo currentPromo = promoRepo.findById(promo.getId());

            currentPromo.setStartDate(promo.getStartDate());
            currentPromo.setExpiryDate(promo.getExpiryDate());
            Status status = statusRepo.findByCode("PRO_NEW");
            if (status == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", promo.getOperationLanguage().getCode(), null);
            }
            currentPromo.setStatus(status);
            currentPromo.setBarcode(Utils.randomNumber(10));
            currentPromo.setPromoSource(promoSourceRepo.findByCode("WEB"));

            String imageUrlPromo = Utils.ConvertImage(promo.getCoverPhoto());
            if (imageUrlPromo != null) {
                currentPromo.setCoverPhoto(imageUrlPromo);
            }
            return currentPromo;
        }
    }

    @Override
    public Page<Promo> findAll(Promo promoSearchCriteria, Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");

        Page<Promo> promoPage = promoRepo.findAll(new Specification<Promo>() {

            @Override
            public Predicate toPredicate(Root<Promo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (promoSearchCriteria.getStatus() != null) {
                    predicates.add(cb.equal(root.get("status"), promoSearchCriteria.getStatus()));
                }

                if (promoSearchCriteria.getMerchant() != null) {
                    predicates.add(cb.equal(root.get("merchant"), promoSearchCriteria.getMerchant()));
                }

                if (promoSearchCriteria.getExpiryDate() != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("expiryDate"), promoSearchCriteria.getExpiryDate()));
                }

                return cb.and(predicates.toArray(new Predicate[0]));
            }
        }, pageable);

        return promoPage;
    }

    @Override
    public Promo changePromoStatus(PromoResources promoResources, Status promoStatus, Status promoItemStatus) {
        Promo promo = promoRepo.findById(promoResources.getId());
        if (promo == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pro404", promoResources.getOperationLanguage().getCode(), null);
        promo.setPoint(promoResources.getPoint());
        promo.setStatus(promoStatus);
        List<PromoItem> promoItemList = itemRepo.findAllByPromo(promo);
        for (PromoItem promoItem : promoItemList)
            promoItem.setStatus(promoItemStatus);
        return promo;
    }


    @Override
    public Page<Promo> findTrend(Promo promo, Integer page, Integer size) {

        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "viewNo");

        Page<Promo> promoPage = promoRepo.findAll(new Specification<Promo>() {

            @Override
            public Predicate toPredicate(Root<Promo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (promo.getStatus() != null) {
                    predicates.add(cb.equal(root.get("status"), promo.getStatus()));
                }

                if (promo.getExpiryDate() != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("expiryDate"), promo.getExpiryDate()));
                }

                predicates.add(cb.greaterThan(root.get("viewNo").as(String.class), "0"));

                return cb.and(predicates.toArray(new Predicate[0]));
            }
        }, pageable);

        return promoPage;


    }

    @Override
    public List<Promo> findAllTempByMerchant(Merchant merchant) {

        Status status = statusRepo.findByCode("PRO_TEMP");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", merchant.getOperationLanguage().getCode(), null);

        List<Promo> promoList = promoRepo.findAllByMerchantAndStatus(merchant, status);
        return promoList;
    }

    @Override
    public Page<Promo> search(String name,
                              String description,
                              LocalDateTime creationDateStart,
                              LocalDateTime creationDateEnd,
                              LocalDateTime expirationDateStart,
                              LocalDateTime expirationDateEnd,
                              LocalDateTime startDateStart,
                              LocalDateTime startDateEnd,
                              Merchant merchant,
                              Status status,
                              Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");


        Page<Promo> promoPage = promoRepo.findByQuery(
                Utils.appendLikeOperator(name)
                , Utils.appendLikeOperator(description)
                , creationDateStart,
                creationDateEnd,
                expirationDateStart,
                expirationDateEnd,
                startDateStart,
                startDateEnd,
                merchant,
                status,
                pageable);


        return promoPage;
    }

    @Override
    public Page<Promo> filterNearMe(String name, Double longitude, Double latitude, Double radius, Integer page, Integer size) {

        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");

        return promoRepo.findNearMe(name, longitude, latitude, radius, pageable);
    }

    @Override
    public Page<Promo> filterCustomerPromoAll(String name,
                                              Industry industry,
                                              Merchant merchant,
                                              Status status,
                                              Integer trendFlag,
                                              LocalDateTime startDate,
                                              LocalDateTime expiryDate,
                                              Double longitude,
                                              Double latitude,
                                              Double radius,
                                              Integer page,
                                              Integer size) {

        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = null;

        if (trendFlag == null) {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        } else {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "viewNo");
        }

        return promoRepo.filterCustomerPromoAll(Utils.appendLikeOperator(name), industry, merchant, status, trendFlag, startDate, expiryDate, longitude, latitude, radius, pageable);
    }

    @Override
    public List<Promo> top3Promo(Merchant merchant) {
        Status status = statusRepo.findByCode("PRO_DELETE");
        List<Promo> promoList = promoRepo.findTop(new PageRequest(0, 3), merchant, LocalDateTime.now());

        return promoList;
    }

    @Override
    public Long totalPromo(Merchant merchant) {
        Long total = promoRepo.totalPromo(merchant);
        return total;
    }

    @Override
    public Long sumOfView(Merchant merchant) {
        Long sum = promoRepo.sumOfView(merchant, LocalDateTime.now());
        return sum;
    }

    @Override
    public Page<Promo> promoReport(LocalDateTime startDate, LocalDateTime expiryDate, Long branchId, Merchant merchant, Integer page, Integer size) {

        if (page == null) page = 0;
        if (size == null) size = 10;
        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        Page<Promo> promoList = promoRepo.promoReport(
                startDate,
                expiryDate,
                branchId,
                merchant,
                pageable
        );


        return promoList;
    }

    @Override
    public void uploadPromosExcelFile(Workbook workbook, Merchant merchant) {

        DataFormatter dataFormatter = new DataFormatter();

        //iterate sheet promos;
        for (int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++) {
            Sheet sheet = workbook.getSheetAt(sheetIndex);

            //iterate over rows;

            Promo promo = new Promo();
            promo.setMerchant(merchant);
            promo.setBarcode(Utils.randomNumber(10));
            promo.setCreateDate(LocalDateTime.now());
            promo.setStatus(statusRepo.findByCode("PRO_NEW"));
            promo.setPromoSource(promoSourceRepo.findByCode("FILE"));

            //parse promo master data
            Row promoMasterDataRow = sheet.getRow(3);

            Cell startDateCell = promoMasterDataRow.getCell(0);

            Cell endDateCell = promoMasterDataRow.getCell(1);

            Cell selectBranchCell = promoMasterDataRow.getCell(2);

            String startDateCellValue = dataFormatter.formatCellValue(startDateCell);
            promo.setStartDate(Utils.parseLocalDate(startDateCellValue, "yyyy-MM-dd").atStartOfDay());

            String endDateCellValue = dataFormatter.formatCellValue(endDateCell);
            promo.setExpiryDate(Utils.parseLocalDate(endDateCellValue, "yyyy-MM-dd").atStartOfDay());

            //get promo cover photo:
            String coverPhoto = Utils.ConvertImageExcel(sheet, 8);
            promo.setCoverPhoto(coverPhoto);
            promo.setViewNo(0L);

            promo = promoRepo.save(promo);

            String selectBranchCellValue = dataFormatter.formatCellValue(selectBranchCell);
            List<MerchantBranch> merchantBranchList = Utils.parseMerchantBranchExcel(selectBranchCellValue, applicationContext);

            for (MerchantBranch merchantBranch : merchantBranchList) {
                PromoBranch promoBranch = new PromoBranch();
                promoBranch.setBranch(merchantBranch);
                promoBranch.setPromo(promo);
                promoBranchRepo.save(promoBranch);
            }

            //parse promo english caption
            Row promoEnglishCaptionRow = sheet.getRow(5);

            Cell englishNameCell = promoEnglishCaptionRow.getCell(1);

            Cell englishDescriptionCell = promoEnglishCaptionRow.getCell(2);

            String englishNameCellValue = dataFormatter.formatCellValue(englishNameCell);

            String englishDescriptionValue = dataFormatter.formatCellValue(englishDescriptionCell);

            if (englishNameCellValue != null && !englishNameCellValue.isEmpty()) {
                PromoCaption promoCaptionEn = new PromoCaption();
                promoCaptionEn.setName(englishNameCellValue);
                promoCaptionEn.setDescription(englishDescriptionValue);
                promoCaptionEn.setOperationLanguage(operationLanguageRepo.findByCode("en"));
                promoCaptionEn.setPromo(promo);
                promoCaptionRepo.save(promoCaptionEn);
            }

            //parse promo arabic caption
            Row promoArabicCaptionRow = sheet.getRow(6);

            Cell arabicNameCell = promoArabicCaptionRow.getCell(1);

            Cell arabicDescriptionCell = promoArabicCaptionRow.getCell(2);

            String arabicNameCellValue = dataFormatter.formatCellValue(arabicNameCell);

            String arabicDescriptionValue = dataFormatter.formatCellValue(arabicDescriptionCell);

            if (arabicNameCellValue != null && !arabicNameCellValue.isEmpty()) {
                PromoCaption promoCaptionEn = new PromoCaption();
                promoCaptionEn.setName(arabicNameCellValue);
                promoCaptionEn.setDescription(arabicDescriptionValue);
                promoCaptionEn.setOperationLanguage(operationLanguageRepo.findByCode("ar"));
                promoCaptionEn.setPromo(promo);
                promoCaptionRepo.save(promoCaptionEn);
            }

            //parse promo arabic caption
            Row promoFranceCaptionRow = sheet.getRow(7);

            Cell franceNameCell = promoFranceCaptionRow.getCell(1);

            Cell franceDescriptionCell = promoFranceCaptionRow.getCell(2);

            String franceNameCellValue = dataFormatter.formatCellValue(franceNameCell);

            String franceDescriptionValue = dataFormatter.formatCellValue(franceDescriptionCell);

            if (franceNameCellValue != null && !franceNameCellValue.isEmpty()) {
                PromoCaption promoCaptionEn = new PromoCaption();
                promoCaptionEn.setName(franceNameCellValue);
                promoCaptionEn.setDescription(franceDescriptionValue);
                promoCaptionEn.setOperationLanguage(operationLanguageRepo.findByCode("fr"));
                promoCaptionEn.setPromo(promo);
                promoCaptionRepo.save(promoCaptionEn);
            }

            //iterate over items//
            Row firstSheetRow = sheet.getRow(0);
            Cell numberOfItemCell = firstSheetRow.getCell(46);
            String numberOfItemCellValue = dataFormatter.formatCellValue(numberOfItemCell);

            //iterate over items//
            Row lastSheetRow = sheet.getRow(0);
            Cell lastSheetCell = lastSheetRow.getCell(45);
            String lastSheetCellValue = dataFormatter.formatCellValue(lastSheetCell);

            Integer numberOfItems = Integer.parseInt(numberOfItemCellValue);
            System.out.println("numberOfItems: " + numberOfItems);
            Integer lastRowValue = Integer.parseInt(lastSheetCellValue);
            System.out.println("lastRowValue: " + lastRowValue);
            if (numberOfItems > 0) {
                for (int itemRowIndex = 20; itemRowIndex <= lastRowValue; itemRowIndex = itemRowIndex + 18) {
                    System.out.println("itemRowIndex: " + itemRowIndex);
                    Row itemMasterInfoRow = sheet.getRow(itemRowIndex + 1);

                    if (itemMasterInfoRow == null) {
                        continue;
                    }

                    PromoItem promoItem = new PromoItem();

                    Cell oldPriceCell = itemMasterInfoRow.getCell(0);

                    if (oldPriceCell == null) {
                        return;
                    }


                    Cell newPriceCell = itemMasterInfoRow.getCell(1);

                    if (newPriceCell == null) {
                        continue;
                    }

                    Cell categoryCell = itemMasterInfoRow.getCell(2);

                    if (categoryCell == null) {
                        continue;
                    }

                    String oldPriceCellValue = dataFormatter.formatCellValue(oldPriceCell);

                    String newPriceCellValue = dataFormatter.formatCellValue(newPriceCell);

                    String categoryCellValue = dataFormatter.formatCellValue(categoryCell);
                    System.out.println("categoryCellValue: " + categoryCellValue);
                    Long categoryId = Long.parseLong(categoryCellValue.split("-")[0].trim());

                    promoItem.setPromo(promo);

//                    String coverPhotoItem = Utils.ConvertImageExcel(sheet, itemRowIndex + 7);
//                    promoItem.setCoverPhoto(coverPhotoItem);

                    promoItem.setOldPrice(oldPriceCellValue);
                    promoItem.setNewPrice(newPriceCellValue);
                    promoItem.setStatus(statusRepo.findByCode("PIT_INACTIVE"));
                    promoItem.setExpiryDate(promo.getExpiryDate());
                    promoItem.setViewNo(0L);
                    promoItem.setCreateDate(LocalDateTime.now());

                    ItemCategory itemCategory = itemCategoryRepo.findById(categoryId);
                    promoItem.setCategory(itemCategory);

                    promoItem = itemRepo.save(promoItem);

                    //parse promo item english caption
                    Row promoItemEnglishCaptionRow = sheet.getRow(itemRowIndex + 3);

                    Cell englishItemNameCell = promoItemEnglishCaptionRow.getCell(1);

                    Cell englishItemDescriptionCell = promoItemEnglishCaptionRow.getCell(2);

                    String englishItemNameCellValue = dataFormatter.formatCellValue(englishItemNameCell);

                    String englishItemDescriptionValue = dataFormatter.formatCellValue(englishItemDescriptionCell);

                    if (englishItemNameCellValue != null && !englishItemNameCellValue.isEmpty()) {
                        PromoItemCaption promoItemCaptionEn = new PromoItemCaption();
                        promoItemCaptionEn.setName(englishItemNameCellValue);
                        promoItemCaptionEn.setDescription(englishItemDescriptionValue);
                        promoItemCaptionEn.setLanguage(operationLanguageRepo.findByCode("en"));
                        promoItemCaptionEn.setPromoItem(promoItem);
                        promoItemCaptionRepo.save(promoItemCaptionEn);
                    }

                    //parse promo item arabic caption
                    Row promoItemArabicCaptionRow = sheet.getRow(itemRowIndex + 4);

                    Cell arabicItemNameCell = promoItemArabicCaptionRow.getCell(1);

                    Cell arabicItemDescriptionCell = promoItemArabicCaptionRow.getCell(2);

                    String arabicItemNameCellValue = dataFormatter.formatCellValue(arabicItemNameCell);

                    String arabicItemDescriptionValue = dataFormatter.formatCellValue(arabicItemDescriptionCell);

                    if (arabicItemNameCellValue != null && !arabicItemNameCellValue.isEmpty()) {
                        PromoItemCaption promoItemCaptionAr = new PromoItemCaption();
                        promoItemCaptionAr.setName(arabicItemNameCellValue);
                        promoItemCaptionAr.setDescription(arabicItemDescriptionValue);
                        promoItemCaptionAr.setLanguage(operationLanguageRepo.findByCode("ar"));
                        promoItemCaptionAr.setPromoItem(promoItem);
                        promoItemCaptionRepo.save(promoItemCaptionAr);
                    }

                    //parse promo item france caption
                    Row promoItemFranceCaptionRow = sheet.getRow(itemRowIndex + 5);

                    Cell franceItemNameCell = promoItemFranceCaptionRow.getCell(1);

                    Cell franceItemDescriptionCell = promoItemFranceCaptionRow.getCell(2);

                    String franceItemNameCellValue = dataFormatter.formatCellValue(franceItemNameCell);

                    String franceItemDescriptionValue = dataFormatter.formatCellValue(franceItemDescriptionCell);

                    if (franceItemNameCellValue != null && !franceItemNameCellValue.isEmpty()) {
                        PromoItemCaption promoItemCaptionFr = new PromoItemCaption();
                        promoItemCaptionFr.setName(franceItemNameCellValue);
                        promoItemCaptionFr.setDescription(franceItemDescriptionValue);
                        promoItemCaptionFr.setLanguage(operationLanguageRepo.findByCode("fr"));
                        promoItemCaptionFr.setPromoItem(promoItem);
                        promoItemCaptionRepo.save(promoItemCaptionFr);
                    }



                    List<String> imageStringList = Utils.ConvertImageItemExcel(sheet, itemRowIndex + 7);

//                    List<String> imageStringList = Utils.ConvertImageItemExcel(sheet,itemRowIndex + 7);
//                    System.out.println("item>>>>: "+imageStringList);


                    for (String itemImageStr : imageStringList) {

                        PromoItemImage promoItemImage = new PromoItemImage();
                        promoItemImage.setImageUrl(itemImageStr);
                        promoItemImage.setPromoItemId(promoItem);
                        promoItemImage.setStatus(statusRepo.findByCode("PII_ACTIVE"));
                        promoItemImageRepo.save(promoItemImage);
                    }

                    promoItem.setCoverPhoto(imageStringList.get(0));

                    //insert pre define specs
                    Row promoItemPreDefinedSpecsKeyRow = sheet.getRow(itemRowIndex + 9);
                    Row promoItemPreDefinedSpecsEnRow = sheet.getRow(itemRowIndex + 10);
                    Row promoItemPreDefinedSpecsArRow = sheet.getRow(itemRowIndex + 11);
                    Row promoItemPreDefinedSpecsFrRow = sheet.getRow(itemRowIndex + 12);

                    for (int cellSpecsIndex = 1; cellSpecsIndex <= promoItemPreDefinedSpecsKeyRow.getLastCellNum(); cellSpecsIndex++) {
                        System.out.println("pre specs loop: " + cellSpecsIndex);

                        Cell keySpecsCell = promoItemPreDefinedSpecsKeyRow.getCell(cellSpecsIndex);

                        String keySpecsCellValue = dataFormatter.formatCellValue(keySpecsCell);
                        System.out.println("keySpecsCellValue: " + keySpecsCellValue);

                        if (keySpecsCellValue == null && keySpecsCellValue.isEmpty()) {
                            continue;
                        }

                        System.out.println("keySpecsCellValue: " + keySpecsCellValue);
                        com.dc.Promotion.entities.Specification specification = specificationRepo.findByKey(keySpecsCellValue.trim());

                        Cell valueSpecsCellEn = promoItemPreDefinedSpecsEnRow.getCell(cellSpecsIndex);
                        String valueSpecsCellEnValue = dataFormatter.formatCellValue(valueSpecsCellEn);

                        if (valueSpecsCellEnValue != null && !valueSpecsCellEnValue.isEmpty()) {
                            System.out.println("valueSpecsCellEnValue: " + valueSpecsCellEnValue);
                            OperationLanguage operationLanguage = operationLanguageRepo.findByCode("en");
                            ItemSpecification itemSpecification = new ItemSpecification();
                            itemSpecification.setKey(null);
                            itemSpecification.setValue(valueSpecsCellEnValue);
                            itemSpecification.setPromoItem(promoItem);
                            itemSpecification.setSpecification(specification);
                            itemSpecification.setOperationLanguage(operationLanguage);
                            itemSpecificationRepo.save(itemSpecification);
                        }

                        Cell valueSpecsCellAr = promoItemPreDefinedSpecsArRow.getCell(cellSpecsIndex);
                        String valueSpecsCellArValue = dataFormatter.formatCellValue(valueSpecsCellAr);

                        if (valueSpecsCellArValue != null && !valueSpecsCellArValue.isEmpty()) {
                            System.out.println("valueSpecsCellArValue: " + valueSpecsCellArValue);
                            OperationLanguage operationLanguage = operationLanguageRepo.findByCode("ar");
                            ItemSpecification itemSpecification = new ItemSpecification();
                            itemSpecification.setKey(null);
                            itemSpecification.setValue(valueSpecsCellArValue);
                            itemSpecification.setPromoItem(promoItem);
                            itemSpecification.setSpecification(specification);
                            itemSpecification.setOperationLanguage(operationLanguage);
                            itemSpecificationRepo.save(itemSpecification);
                        }

                        Cell valueSpecsCellFr = promoItemPreDefinedSpecsFrRow.getCell(cellSpecsIndex);
                        String valueSpecsCellFrValue = dataFormatter.formatCellValue(valueSpecsCellFr);

                        if (valueSpecsCellFrValue != null && !valueSpecsCellFrValue.isEmpty()) {
                            System.out.println("valueSpecsCellFrValue: " + valueSpecsCellFrValue);
                            OperationLanguage operationLanguage = operationLanguageRepo.findByCode("fr");
                            ItemSpecification itemSpecification = new ItemSpecification();
                            itemSpecification.setKey(null);
                            itemSpecification.setValue(valueSpecsCellFrValue);
                            itemSpecification.setPromoItem(promoItem);
                            itemSpecification.setSpecification(specification);
                            itemSpecification.setOperationLanguage(operationLanguage);
                            itemSpecificationRepo.save(itemSpecification);
                        }
                    }

                    //insert other specs
                    Row promoItemOtherSpecsRow = sheet.getRow(itemRowIndex + 14);
                    System.out.println("promoItemOtherSpecsRow: " + promoItemOtherSpecsRow);
                    if (promoItemOtherSpecsRow == null) {
                        continue;
                    }

                    System.out.println("last cell other: " + promoItemOtherSpecsRow.getLastCellNum());
                    for (int cellSpecsIndex = 1; cellSpecsIndex <= promoItemOtherSpecsRow.getLastCellNum(); cellSpecsIndex++) {

                        System.out.println("other specs loop: " + cellSpecsIndex);

                        Cell specsLangCell = promoItemOtherSpecsRow.getCell(cellSpecsIndex);
                        System.out.println("specsLangCell: " + specsLangCell);

                        if (specsLangCell == null) {
                            continue;
                        }

                        String specsLangCellValue = dataFormatter.formatCellValue(specsLangCell);
                        System.out.println("specsLangCellValue: " + specsLangCellValue);

                        if (specsLangCellValue == null && specsLangCellValue.isEmpty()) {
                            continue;
                        }

                        System.out.println("specsLangCellValue: " + specsLangCellValue);
                        OperationLanguage operationLanguage = operationLanguageRepo.findByCode(specsLangCellValue.split("-")[0].toLowerCase().trim());
                        System.out.println("operationLanguage: " + operationLanguage);

                        if (operationLanguage == null) {
                            continue;
                        }

                        Row specsKeyRow = sheet.getRow(itemRowIndex + 15);
                        System.out.println("specsKeyRow: " + specsKeyRow);


                        if (specsKeyRow == null) {
                            continue;
                        }

                        Cell specsKeyCell = specsKeyRow.getCell(cellSpecsIndex);
                        System.out.println("specsKeyCell: " + specsKeyCell);

                        if (specsKeyCell == null) {
                            continue;
                        }

                        String specsKeyCellValue = dataFormatter.formatCellValue(specsKeyCell);
                        System.out.println("specsKeyCellValue: " + specsKeyCellValue);

                        if (specsKeyCellValue == null && specsKeyCellValue.isEmpty()) {
                            continue;
                        }

                        Row specsValueRow = sheet.getRow(itemRowIndex + 16);
                        System.out.println("specsValueRow: " + specsValueRow);

                        if (specsValueRow == null) {
                            continue;
                        }

                        Cell specsValueCell = specsValueRow.getCell(cellSpecsIndex);
                        System.out.println("specsValueCell: " + specsValueCell);

                        if (specsValueCell == null) {
                            continue;
                        }

                        String specsValueCellValue = dataFormatter.formatCellValue(specsValueCell);
                        System.out.println("specsValueCellValue: " + specsValueCellValue);

                        if (specsValueCellValue == null && specsValueCellValue.isEmpty()) {
                            continue;
                        }

                        ItemSpecification otherSpecs = new ItemSpecification();
                        otherSpecs.setKey(specsKeyCellValue);
                        otherSpecs.setValue(specsValueCellValue);
                        otherSpecs.setPromoItem(promoItem);
                        otherSpecs.setSpecification(null);
                        otherSpecs.setOperationLanguage(operationLanguage);
                        itemSpecificationRepo.save(otherSpecs);
                    }
                }
            }
        }
    }

    @Override
    public void deletePromo(Long promoId) {


        Promo promo = promoRepo.findById(promoId);
        try {

            List<PromoCaption> promoCaptions = promoCaptionRepo.findAllByPromo(promo);

            for (PromoCaption promoCaption : promoCaptions) {

                if (promoCaptions != null && !promoCaptions.isEmpty()) {
                    promoCaptionRepo.deleteAllByPromo(promo);
                }
            }

            List<PromoBranch> promoBra = promoBranchRepo.findAllByPromo(promo);

            for (PromoBranch promoBranch : promoBra) {

                if (promoBra != null && !promoBra.isEmpty()) {
                    promoBranchRepo.deleteAllByPromo(promo);
                }
            }
            List<CustomerPromoView> customerPromoViews = customerPromoViewRepo.findAllByPromo(promo);

            for (CustomerPromoView promoView : customerPromoViews) {

                if (customerPromoViews != null && !customerPromoViews.isEmpty()) {
                    customerPromoViewRepo.deleteAllByPromo(promo);
                }
            }



            promoRepo.deleteById(promo.getId());


        } catch (Exception ex) {
            System.out.println(">>>>: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
}