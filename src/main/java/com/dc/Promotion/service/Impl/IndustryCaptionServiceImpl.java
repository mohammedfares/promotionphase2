package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.IndustryCaption;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantRegistrationRequest;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.repository.IndustryCaptionRepo;
import com.dc.Promotion.service.IndustryCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class IndustryCaptionServiceImpl implements IndustryCaptionService {

    @Autowired
    private IndustryCaptionRepo industryCaptionRepo;
    @Override
    public IndustryCaption findById(long id) {
        IndustryCaption industryCaption=industryCaptionRepo.findById(id);
        return industryCaption;
    }

    @Override
    public List<IndustryCaption> findAllByLanguageId(OperationLanguage language) {
        List<IndustryCaption> captionList=industryCaptionRepo.findAllByLanguageId(language);
        return captionList;
    }

    @Override
    public List<IndustryCaption> find(Merchant merchant) {
        List<IndustryCaption> industryCaptionList=industryCaptionRepo.findByQuery(merchant.getOperationLanguage(),merchant);
        return industryCaptionList;
    }

    @Override
    public List<IndustryCaption> findByMRR(OperationLanguage language, MerchantRegistrationRequest registrationRequest) {
        List<IndustryCaption> industryCaptionList=industryCaptionRepo.findByMRR(language,registrationRequest);
        return industryCaptionList;
    }
}
