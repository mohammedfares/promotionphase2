package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.CountrySegment;
import com.dc.Promotion.entities.MerchantCustomerSegment;
import com.dc.Promotion.repository.CountrySegmentRepo;
import com.dc.Promotion.service.CountrySegmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CountrySegmentServiceImpl implements CountrySegmentService {

    @Autowired
    private CountrySegmentRepo countrySegmentRepo;

    @Override
    public CountrySegment findById(Long id) {
        return countrySegmentRepo.findById(id);
    }

    @Override
    public CountrySegment create(CountrySegment countrySegment) {
        return countrySegmentRepo.save(countrySegment) ;
    }

    @Override
    public List<Country> findAllCountry(MerchantCustomerSegment segment) {
        List<Country> countryList=countrySegmentRepo.findByQuery(segment);

        return countryList;
    }
}
