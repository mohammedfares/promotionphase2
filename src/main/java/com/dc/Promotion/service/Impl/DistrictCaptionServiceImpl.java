package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.District;
import com.dc.Promotion.entities.DistrictCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.repository.DistrictCaptionRepo;
import com.dc.Promotion.service.DistrictCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class DistrictCaptionServiceImpl  implements DistrictCaptionService  {

   @Autowired
   private DistrictCaptionRepo districtCaptionRepo;

    @Override
    public DistrictCaption findById(long id) {
        DistrictCaption districtCaption=districtCaptionRepo.findById(id);
        return districtCaption;
    }

    @Override
    public DistrictCaption findByDistrictAndLanguage(District district, OperationLanguage language) {


        DistrictCaption caption=districtCaptionRepo.findByDistrictAndLanguage(district,language);
        return caption;
    }

    @Override
    public List<DistrictCaption> findAllByLanguageAndDistrict_City(OperationLanguage language, City city) {
        List<DistrictCaption> districtCaptionList=districtCaptionRepo.findAllByLanguageAndDistrict_City(language,city);
        return districtCaptionList;
    }
}
