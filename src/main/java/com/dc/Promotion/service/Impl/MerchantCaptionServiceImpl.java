package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.repository.MerchantCaptionRepo;
import com.dc.Promotion.service.MerchantCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class MerchantCaptionServiceImpl implements MerchantCaptionService {

    @Autowired
    private MerchantCaptionRepo merchantCaptionRepo;


    public MerchantCaption findById(long id) {
        MerchantCaption merchantCaption = merchantCaptionRepo.findById(id);
        return merchantCaption;
    }

    @Override
    public MerchantCaption create(MerchantCaption merchantCaption) {

        merchantCaption = merchantCaptionRepo.save(merchantCaption);
        return merchantCaption;
    }

    @Override
    public List<MerchantCaption> findByIndustry(Industry industry) {

        List<MerchantCaption> merchantCaptionList = merchantCaptionRepo.findByIndustry(industry);
        return merchantCaptionList;
    }

    @Override
    public List<MerchantCaption> findAll() {
        List<MerchantCaption> merchantCaptionList = merchantCaptionRepo.findAll();
        return merchantCaptionList;
    }

    @Override
    public List<MerchantCaption> findByMerchantId(Long id) {
        return merchantCaptionRepo.findMerchantCaptionByMerchant_Id(id);
    }

    @Override
    public MerchantCaption findByIdAndLanguage(Long id, OperationLanguage language) {
        MerchantCaption merchantCaption = merchantCaptionRepo.findByIdAndOperationLanguage(id, language);
        return merchantCaption;
    }

    @Override
    public MerchantCaption findByMerchant(Merchant merchant,OperationLanguage language) {

        MerchantCaption merchantCaption=merchantCaptionRepo.findByMerchantAndOperationLanguage(merchant,language);
        return merchantCaption;
    }
}
