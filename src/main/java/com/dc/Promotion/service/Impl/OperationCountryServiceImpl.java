package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.OperationCountry;
import com.dc.Promotion.repository.OperationCountryRepo;
import com.dc.Promotion.service.OperationCountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OperationCountryServiceImpl implements OperationCountryService {

    @Autowired
    private OperationCountryRepo operationCountryRepo;

    @Override
    public OperationCountry findById(long id) {
        OperationCountry operationCountry=operationCountryRepo.findById(id);
        return operationCountry;
    }

    @Override
    public List<OperationCountry> findAll() {

        List<OperationCountry> countryList=operationCountryRepo.findAll();
        return countryList;
    }

    @Override
    public OperationCountry findByCountry(Country country) {

        OperationCountry  operationCountry=operationCountryRepo.findByCountry(country);
        return operationCountry;
    }
}
