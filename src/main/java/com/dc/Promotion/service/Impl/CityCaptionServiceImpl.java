package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.repository.CityCaptionRepo;
import com.dc.Promotion.service.CityCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CityCaptionServiceImpl implements CityCaptionService {

    @Autowired
    private CityCaptionRepo cityCaptionRepo;

    @Override
    public CityCaption findById(long id) {
        CityCaption cityCaption=cityCaptionRepo.findById(id);
        return cityCaption;
    }

    @Override
    public CityCaption findByCityAndLanguage(City city, OperationLanguage language) {

        CityCaption caption=cityCaptionRepo.findByCityAndLanguage(city,language);

        return caption;
    }

    @Override
    public List<CityCaption> findByLanguageAndCity_Country(OperationLanguage language, OperationCountry country) {
        List<CityCaption> cityCaptionList=cityCaptionRepo.findByLanguageAndCity_Country(language,country);
        return cityCaptionList;
    }
}
