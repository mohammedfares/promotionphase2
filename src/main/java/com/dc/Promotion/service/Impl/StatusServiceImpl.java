package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Status;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class StatusServiceImpl implements StatusService {
    @Autowired
    private StatusRepo statusRepo;

    public Status findById(long id) {
        Status status = statusRepo.findById(id);

        return status;

    }

    @Override
    public List<Status> getAllRegistrationRequestStatus(String code) {

        List<Status> statusList=statusRepo.findAllByCode( code);


        return null;
    }

    @Override
    public Status findByCode(String code) {
        Status status=statusRepo.findByCode(code);
        return status;
    }
}
