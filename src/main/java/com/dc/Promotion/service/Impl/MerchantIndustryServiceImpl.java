package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.MerchantIndustry;
import com.dc.Promotion.repository.MerchantIndustryRepo;
import com.dc.Promotion.service.MerchantIndustryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MerchantIndustryServiceImpl implements MerchantIndustryService {
    @Autowired
    private MerchantIndustryRepo merchantIndustryRepo;
    public MerchantIndustry findById(long id) {
        MerchantIndustry merchantIndustry=merchantIndustryRepo.findById(id);
        return merchantIndustry;
    }
}
