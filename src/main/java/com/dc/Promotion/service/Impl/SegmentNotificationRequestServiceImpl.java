package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.*;
import com.dc.Promotion.service.MediaNotificationService;
import com.dc.Promotion.service.SegmentNotificationRequestService;
import com.dc.Promotion.utils.PushNotificationHandler;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class SegmentNotificationRequestServiceImpl implements SegmentNotificationRequestService {


    @Autowired
    private SegmentNotificationRequestRepo segmentNotificationRequestRepo;

    @Autowired
    private CustomerAccountRepo customerAccountRepo;

    @Autowired
    private CustomerNotificationRepo customerNotificationRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MediaAccountRepo mediaAccountRepo;

    @Autowired
    private MediaNotificationService mediaNotificationService;

    @Autowired
    private NotificationTemplateRepo notificationTemplateRepo;

    @Autowired
    private NotificationTemplateCaptionRepo notificationTemplateCaptionRepo;

    @Autowired
    private MerchantCustomerSegmentRepo merchantCustomerSegmentRepo;

    @Autowired
    private MerchantCaptionRepo merchantCaptionRepo;


    @Override
    public SegmentNotificationRequest findById(Long id) {

        SegmentNotificationRequest notificationRequest = segmentNotificationRequestRepo.findById(id);


        return notificationRequest;
    }

    @Override
    public SegmentNotificationRequest createRequest(SegmentNotificationRequest request) {

        SegmentNotificationRequest notificationRequest = new SegmentNotificationRequest();

        notificationRequest.setCustomerSegment(merchantCustomerSegmentRepo.findById(request.getCustomerSegment().getId()));
        notificationRequest.setPromo(request.getPromo());
        notificationRequest.setTotalNotification(0L);
        notificationRequest.setTotalView(0L);
        notificationRequest.setTotalCustomer(0L);
        notificationRequest.setCreationTime(LocalDateTime.now());

        SegmentNotificationRequest segmentNotificationRequest = segmentNotificationRequestRepo.save(notificationRequest);


        return segmentNotificationRequest;
    }

    @Override
    public void notifyCustomers(SegmentNotificationRequest request
    ) {
        /*
         * Find Customers based on segment
         */
        Status status = statusRepo.findByCode("CAC_ACTIVE");
        if (status == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", null, null);
        }

        MerchantCustomerSegment merchantCustomerSegment = request.getCustomerSegment();
        List<CustomerAccount> customerAccountList = customerAccountRepo.findByQuery(
        merchantCustomerSegment.getGender(),
                merchantCustomerSegment.getAgeFrom(),
                merchantCustomerSegment.getAgeTo(),
                (merchantCustomerSegment.getDistrict() != null) ? (merchantCustomerSegment.getDistrict().getId()) : (null),
                (merchantCustomerSegment.getCity() != null) ? (merchantCustomerSegment.getCity().getId()) : (null),
                status,
                (merchantCustomerSegment.getCountrySegmentList() != null && !merchantCustomerSegment.getCountrySegmentList().isEmpty()) ? (merchantCustomerSegment.getId()):(null),
                merchantCustomerSegment.getLongitude(),
                merchantCustomerSegment.getLatitude(),
                merchantCustomerSegment.getRadius()
                ); // edit

        for (CustomerAccount customerAccount : customerAccountList) {

            // check account active

            NotificationTemplate notificationTemplate = notificationTemplateRepo.findByCode("PRO_NOTIFICATION");

            NotificationTemplateCaption notificationTemplateCaption = notificationTemplateCaptionRepo.findByNotificationTemplateAndOperationLanguage(notificationTemplate, customerAccount.getOperationLanguage());

            String title = notificationTemplateCaption.getCaptionTitle();

            Merchant merchant = merchantCustomerSegment.getMerchant();

            MerchantCaption merchantCaption = merchantCaptionRepo.findByMerchantAndOperationLanguage(merchant,customerAccount.getOperationLanguage());

            if(merchantCaption == null){
                merchantCaption = merchantCaptionRepo.findMerchantCaptionByMerchant_Id(merchant.getId()).get(0);
            }

            String body = notificationTemplateCaption.getCaptionBody().replace("MER_NAME",merchantCaption.getName());
            //save notification on database
            CustomerNotification customerNotification = new CustomerNotification();
            customerNotification.setStatus(statusRepo.findByCode("NOT_INSEEN"));
            customerNotification.setSendDate(LocalDateTime.now());
            customerNotification.setTitle(title);
            customerNotification.setSegmentNotificationRequest(request);
            customerNotification.setBody(body);
            customerNotification.setCustomerAccount(customerAccount);

            customerNotification = customerNotificationRepo.save(customerNotification);

            String data = Utils.initPromoNotificationData(request.getPromo().getId(), customerNotification.getId());

            PushNotificationHandler.sendNotification(title, body, data, customerAccount.getDeviceToken(), applicationContext);
        }

        //update total customer and notification for request

        SegmentNotificationRequest notificationRequest = segmentNotificationRequestRepo.findById(request.getId());

        Long totalCustomer = notificationRequest.getTotalCustomer();
        Long totalNotification = notificationRequest.getTotalNotification();

        notificationRequest.setTotalCustomer(totalCustomer + customerAccountList.size());
        notificationRequest.setTotalNotification(totalNotification + customerAccountList.size());

    }

    @Override
    public void notifyMedia(SegmentNotificationRequest request) {

        Status status = statusRepo.findByCode("MAC_ACTIVE");
        if (status == null) {
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", null, null);
        }

        MerchantCustomerSegment merchantCustomerSegment = request.getCustomerSegment();
        List<MediaAccount> mediaAccounts = mediaAccountRepo.findByQuery(
                merchantCustomerSegment.getGender(),
                merchantCustomerSegment.getAgeFrom(),
                merchantCustomerSegment.getAgeTo(),
                status,
                merchantCustomerSegment.getLongitude(),
                merchantCustomerSegment.getLatitude(),
                merchantCustomerSegment.getRadius());


        // edit



        for (MediaAccount mediaAccount : mediaAccounts) {

            NotificationTemplate notificationTemplate = notificationTemplateRepo.findByCode("PRO_NOTIFICATION");

            NotificationTemplateCaption notificationTemplateCaption = notificationTemplateCaptionRepo.findByNotificationTemplateAndOperationLanguage(notificationTemplate, mediaAccount.getOperationLanguage());

            String title = notificationTemplateCaption.getCaptionTitle();

            Merchant merchant = merchantCustomerSegment.getMerchant();

            MerchantCaption merchantCaption = merchantCaptionRepo.findByMerchantAndOperationLanguage(merchant,mediaAccount.getOperationLanguage());

            if(merchantCaption == null){
                merchantCaption = merchantCaptionRepo.findMerchantCaptionByMerchant_Id(merchant.getId()).get(0);
            }

            String body = notificationTemplateCaption.getCaptionBody().replace("MER_NAME",merchantCaption.getName());

            //save notification on database
            MediaNotification mediaNotification = new MediaNotification();
            mediaNotification.setStatus(statusRepo.findByCode("NOT_INSEEN"));
            mediaNotification.setSendDate(LocalDateTime.now());
            mediaNotification.setTitle(title);
            mediaNotification.setSegmentNotificationRequest(request);
            mediaNotification.setBody(body);
            mediaNotification.setMediaAccount(mediaAccount);
            MediaNotification notification = mediaNotificationService.save(mediaNotification);

            String data = Utils.initPromoNotificationData(request.getPromo().getId(), notification.getId());

            PushNotificationHandler.sendNotification(title, body, data, mediaAccount.getDeviceToken(), applicationContext);

        }


        //update total customer and notification for request
        SegmentNotificationRequest notificationRequest = segmentNotificationRequestRepo.findById(request.getId());
        Long totalCustomer = notificationRequest.getTotalCustomer();
        Long totalNotification = notificationRequest.getTotalNotification();

        notificationRequest.setTotalCustomer(totalCustomer + mediaAccounts.size());
        notificationRequest.setTotalNotification(totalNotification + mediaAccounts.size());

    }

    @Override
    public Long findTotalCustomer(MerchantCustomerSegment customerSegment) {
        return segmentNotificationRequestRepo.findTotalCustomer(customerSegment);
    }

    @Override
    public Long findTotalNotification(MerchantCustomerSegment customerSegment) {
        return segmentNotificationRequestRepo.findTotalNotification(customerSegment);
    }

    @Override
    public Long findTotalViews(MerchantCustomerSegment customerSegment) {
        return segmentNotificationRequestRepo.findTotalViews(customerSegment);
    }
}
