package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoCaption;
import com.dc.Promotion.repository.PromoCaptionRepo;
import com.dc.Promotion.repository.PromoRepo;
import com.dc.Promotion.service.PromoCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PromoCaptionServiceImpl implements PromoCaptionService {

    @Autowired
    private PromoCaptionRepo promoCaptionRepo;

    @Autowired
    private PromoRepo promoRepo;

    public PromoCaption findById(long id) {
        PromoCaption promoCaption = promoCaptionRepo.findById(id);
        return promoCaption;
    }

    @Override
    public PromoCaption create(PromoCaption caption) {

        if (caption.getOperationLanguage() != null) {
            PromoCaption currentCaption = promoCaptionRepo.findPromoCaptionByPromoAndOperationLanguage(caption.getPromo(), caption.getOperationLanguage());

            if (currentCaption != null) {
                if (caption.getName().isEmpty()) {
                    return currentCaption;
                }
                currentCaption.setName(caption.getName());
                currentCaption.setDescription(caption.getDescription());
            } else {
                if (caption.getName().isEmpty()) {
                    return caption;
                }
                currentCaption = promoCaptionRepo.save(caption);
            }
            return currentCaption;
        } else {
            return caption;
        }
    }

    @Override
    public PromoCaption find(Promo promo, OperationLanguage operationLanguage) {

        PromoCaption promoCaption = promoCaptionRepo.findPromoCaptionByPromoAndOperationLanguage(promo, operationLanguage);

        if (promoCaption == null) {
            List<PromoCaption> promoCaptionList = promoCaptionRepo.findPromoCaptionByPromo(promo);

            if (promoCaptionList != null && !promoCaptionList.isEmpty()) {
                promoCaption = promoCaptionList.get(0);
            }
        }
        return promoCaption;
    }

    @Override
    public List<PromoCaption> findAllByPromo(Promo promo) {
        List<PromoCaption> promoCaptionList = promoCaptionRepo.findAllByPromo(promo);
        return promoCaptionList;
    }

    @Override
    public void deleteAllByPromo(Promo promo) {

        Promo currentPromo = promoRepo.findById(promo.getId());
        promoCaptionRepo.deleteAllByPromo(currentPromo);
    }
}
