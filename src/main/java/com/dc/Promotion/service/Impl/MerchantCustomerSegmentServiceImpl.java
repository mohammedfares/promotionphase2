package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantCustomerSegment;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MerchantCustomerSegmentRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.MerchantCustomerSegmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class MerchantCustomerSegmentServiceImpl implements MerchantCustomerSegmentService {

    @Autowired
    private MerchantCustomerSegmentRepo merchantCustomerSegmentRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public MerchantCustomerSegment findById(Long id) {
        MerchantCustomerSegment merchantCustomerSegment = merchantCustomerSegmentRepo.findById(id);
        return merchantCustomerSegment;
    }

    @Override
    public MerchantCustomerSegment create(MerchantCustomerSegment merchantCustomerSegment) {

        Status status = statusRepo.findByCode("MCS_ACTIVE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", merchantCustomerSegment.getOperationLanguage().getCode(), null);

        merchantCustomerSegment.setCreateDate(LocalDateTime.now());
        merchantCustomerSegment.setStatus(status);
        merchantCustomerSegment = merchantCustomerSegmentRepo.save(merchantCustomerSegment);
        return merchantCustomerSegment;
    }

    @Override
    public MerchantCustomerSegment ChangeStatus(MerchantCustomerSegment segment, Status status) {

        return null;
    }

    @Override
    public List<MerchantCustomerSegment> findAllByMerchant(Merchant merchant) {

        Status status = statusRepo.findByCode("MCS_DELETE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", merchant.getOperationLanguage().getCode(), null);
        List<MerchantCustomerSegment> segmentList = merchantCustomerSegmentRepo.findByQuery(merchant);
        return segmentList;
    }

    @Override
    public List<MerchantCustomerSegment> findTopSegmant(Merchant merchant) {

        return merchantCustomerSegmentRepo.findTopSegment(merchant);
    }
}
