package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoBranch;
import com.dc.Promotion.repository.PromoBranchRepo;
import com.dc.Promotion.service.PromoBranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PromoBranchServiceImpl implements PromoBranchService {

    @Autowired
    private PromoBranchRepo promoBranchRepo;
    
    @Override
    public PromoBranch findById(long id) {
        return promoBranchRepo.findById(id);
    }

    @Override
    public PromoBranch create(PromoBranch promoBranch) {
        PromoBranch branch=promoBranchRepo.save(promoBranch);
        return branch;
    }

    @Override
    public List<PromoBranch> findAllbyPromo(Promo promo) {
        List<PromoBranch> promoBranchList=promoBranchRepo.findAllByPromo(promo);
        return promoBranchList;
    }

    @Override
    public void deleteAllByPromo(Promo promo) {
        promoBranchRepo.deleteAllByPromo(promo);
    }
}
