package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.AgeRange;
import com.dc.Promotion.repository.AgeRangeRepo;
import com.dc.Promotion.service.AgeRangeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AgeRangeServiceImpl implements AgeRangeService {

    @Autowired
    private AgeRangeRepo ageRangeRepo;

    @Override
    public AgeRange findById(long id) {
        AgeRange ageRange=ageRangeRepo.findById(id);
        return ageRange;
    }
}
