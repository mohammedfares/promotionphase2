package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.entities.StatusCaption;
import com.dc.Promotion.repository.StatusCaptionRepo;
import com.dc.Promotion.service.StatusCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class StatusCaptionServiceImpl implements StatusCaptionService {

    @Autowired
    private StatusCaptionRepo statusCaptionRepo;

    public StatusCaption findById(long id) {
        StatusCaption statusCaption = statusCaptionRepo.findById(id);
        return statusCaption;
    }

    @Override
    public List<StatusCaption> find(OperationLanguage language,String code) {

        List<StatusCaption> statusCaptionList=statusCaptionRepo.findByQuery(language,code);
        return statusCaptionList;
    }

    @Override
    public List<StatusCaption> findWithoutDeleted(OperationLanguage language, String code) {
        return statusCaptionRepo.findWithoutDeleted(language,code);
    }

    @Override
    public StatusCaption findByStatusAndLanguage(Status status, OperationLanguage language) {
        StatusCaption caption=statusCaptionRepo.findByStatusAndLanguage(status,language);
        return caption;
    }
}
