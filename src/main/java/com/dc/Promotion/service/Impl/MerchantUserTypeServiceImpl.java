package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.MerchantUserType;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MerchantUserTypeRepo;
import com.dc.Promotion.service.MerchantUserTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MerchantUserTypeServiceImpl implements MerchantUserTypeService {
    @Autowired
    private MerchantUserTypeRepo merchantUserTypeRepo;
    @Autowired
    private ApplicationContext applicationContext;

    public MerchantUserType findById(Long id) {
        MerchantUserType merchantUserType = merchantUserTypeRepo.findById(id);
        return merchantUserType;
    }

    @Override
    public MerchantUserType findByCode(String code) {
        MerchantUserType merchantUserType = merchantUserTypeRepo.findByCode(code);
        return merchantUserType;
    }
}
