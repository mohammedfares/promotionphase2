package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.PromoItemImage;
import com.dc.Promotion.repository.PromoItemImageRepo;
import com.dc.Promotion.service.PromoItemImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PromoItemImageServiceImpl implements PromoItemImageService {
    @Autowired
    private PromoItemImageRepo promoItemImageRepo;
    public PromoItemImage findById(long id) {
        PromoItemImage promoItemImage=promoItemImageRepo.findById(id);
        return promoItemImage;
    }
}
