package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.CustomerPoint;
import com.dc.Promotion.repository.CustomerPointRepo;
import com.dc.Promotion.service.CustomerPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CustomerPointServiceImpl implements CustomerPointService {

    @Autowired
    private CustomerPointRepo customerPointRepo;

    @Override
    public CustomerPoint findById(long id) {
        CustomerPoint customerPoint=customerPointRepo.findById(id);
        return customerPoint;
    }
}
