package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.CustomerNotification;
import com.dc.Promotion.repository.CustomerNotificationRepo;
import com.dc.Promotion.service.CustomerNotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CustomerNotificationServiceImpl implements CustomerNotificationService {
    @Autowired
    private CustomerNotificationRepo customerNotificationRepo;

    @Override
    public CustomerNotification findById(Long id) {
        CustomerNotification customerNotification=customerNotificationRepo.findById(id);

        return customerNotification;
    }
}
