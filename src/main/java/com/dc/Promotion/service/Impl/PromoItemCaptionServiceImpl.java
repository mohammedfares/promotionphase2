package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.PromoItemCaption;
import com.dc.Promotion.repository.PromoItemCaptionRepo;
import com.dc.Promotion.service.PromoItemCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PromoItemCaptionServiceImpl implements PromoItemCaptionService {
    @Autowired
    private PromoItemCaptionRepo promoItemCaptionRepo;

    public PromoItemCaption findById(long id) {
        PromoItemCaption promoItemCaption=promoItemCaptionRepo.findById(id);
        return promoItemCaption;
    }

    @Override
    public List<PromoItemCaption> findAllByPromo(Promo promo) {
        return promoItemCaptionRepo.findAllByPromoItem_Promo(promo);
    }


    @Override
    public PromoItemCaption find(PromoItem promoItem, OperationLanguage operationLanguage) {
        PromoItemCaption promoItemCaption=promoItemCaptionRepo.findByPromoItemAndLanguage(promoItem,operationLanguage);
        return promoItemCaption;
    }

    @Override
    public List<PromoItemCaption> find(PromoItem promoItem) {
        return promoItemCaptionRepo.findPromoItemCaptionByPromoItem(promoItem);
    }
}
