package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.CustomerAccountRepo;
import com.dc.Promotion.security.SECCriptoRsa;
import com.dc.Promotion.service.CustomerAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class CustomerAccountServiceImpl implements CustomerAccountService {

    @Autowired
    private CustomerAccountRepo accountRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public CustomerAccount findById(long id) {
        CustomerAccount account = accountRepo.findById(id);
        return account;
    }

    @Override
    public CustomerAccount findByMobile(String mobile) {
        CustomerAccount account = accountRepo.findByMobile(mobile);
        return account;
    }

    @Override
    public CustomerAccount findByEmail(String email) {
        CustomerAccount account = accountRepo.findByEmail(email);
        return account;
    }

    @Override
    public CustomerAccount save(CustomerAccount account) {
        account = accountRepo.save(account);
        return account;
    }

    @Override
    public CustomerAccount changeLanguage(CustomerAccount customerAccount, OperationLanguage operationLanguage) {

        CustomerAccount account=accountRepo.findById(customerAccount.getId());

        account.setOperationLanguage(operationLanguage);


        return account;
    }

    @Override
    public CustomerAccount updateAccountInfo(CustomerAccount customerAccount) {

        CustomerAccount currentCustomerAccount = accountRepo.findById(customerAccount.getId());

        if(customerAccount.getDeviceToken() != null && !customerAccount.getDeviceToken().isEmpty()){
            currentCustomerAccount.setDeviceToken(customerAccount.getDeviceToken());
        }

        if(customerAccount.getDevicePlatform() != null && !customerAccount.getDeviceToken().isEmpty()){
            currentCustomerAccount.setDevicePlatform(customerAccount.getDevicePlatform());
        }

        if(customerAccount.getOperationLanguage() != null){
            currentCustomerAccount.setOperationLanguage(customerAccount.getOperationLanguage());
        }

        if(customerAccount.getLatitude() != null && customerAccount.getLatitude() != 0.0){
            currentCustomerAccount.setLatitude(customerAccount.getLatitude());
        }

        if(customerAccount.getLongitude() != null && customerAccount.getLongitude() != 0.0){
            currentCustomerAccount.setLatitude(customerAccount.getLongitude());
        }

        return currentCustomerAccount;
    }

    @Override
    public CustomerAccount changePassword(CustomerAccount user, String NewPassword) {
        CustomerAccount merchantUser = accountRepo.findById(user.getId());
        if (merchantUser.getPassword().equals(NewPassword))
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "pass406", user.getOperationLanguage().getCode(), null);

        user.setPassword(SECCriptoRsa.encrypt(NewPassword));
        return user;
    }
}
