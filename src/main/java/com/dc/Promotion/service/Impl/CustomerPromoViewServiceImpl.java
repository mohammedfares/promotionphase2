package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.*;
import com.dc.Promotion.service.CustomerPromoViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@Service
@Transactional
public class CustomerPromoViewServiceImpl implements CustomerPromoViewService {

    @Autowired
    CustomerPromoViewRepo customerPromoViewRepo;

    @Autowired
    private CustomerAccountRepo accountRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private PromoRepo promoRepo;


    @Override
    public CustomerPromoView findById(long id) {
        CustomerPromoView customerPromoView=customerPromoViewRepo.findById(id);
        return customerPromoView;
    }

    @Override
    public CustomerPromoView create(CustomerPromoView customerPromoView) {

        Promo promo=promoRepo.findById(customerPromoView.getPromo().getId());
        if(promo==null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pro404", customerPromoView.getOperationLanguage().getCode(), null);

        CustomerAccount account=accountRepo.findById(customerPromoView.getCustomerAccount().getId());
        if(account==null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "user404", customerPromoView.getOperationLanguage().getCode(), null);


        Status status=statusRepo.findByCode("CPV_ACTIVE");
        if(status==null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", customerPromoView.getOperationLanguage().getCode(), null);
        CustomerPromoView promoView=customerPromoViewRepo.findByPromoAndCustomerAccount(promo,account);
        if(promoView==null)
        {
            promoView=new CustomerPromoView();
            promoView.setStatus(status);
            promoView.setCustomerAccount(account);
            promoView.setCreateDate(LocalDateTime.now());
            promoView.setPromo(promo);
            promo.setViewNo(promo.getViewNo()+1L);
            customerPromoViewRepo.save(promoView);
        }


        return promoView;
    }
}
