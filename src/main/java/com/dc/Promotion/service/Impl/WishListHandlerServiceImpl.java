package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.repository.*;
import com.dc.Promotion.service.StatusService;
import com.dc.Promotion.service.WishListHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class WishListHandlerServiceImpl implements WishListHandlerService {

    @Autowired
    private WishListHandlerRepo wishListHandlerRepo;

    @Autowired
    private CustomerWishListViewRepo customerWishListViewRepo;

    @Autowired
    private CustomerWishListRepo customerWishListRepo;

    @Autowired
    private MediaWishListViewRepo mediaWishListViewRepo;

    @Autowired
    private MediaWishListRepo mediaWishListRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Override
    public void refreshCustomerWishList(CustomerAccount customerAccount) {

        customerWishListViewRepo.deleteAllByCustomerAccount(customerAccount);

        List<CustomerWishList> customerWishLists = customerWishListRepo.findAllByCustomerAccountAndStatus(customerAccount, statusRepo.findByCode("CWI_ACTIVE"));

        List<CustomerWishListView> customerWishListViews = new ArrayList<>();

        for (CustomerWishList customerWishList : customerWishLists) {

            List<PromoItem> promoItemList = wishListHandlerRepo.findPromoItemByCustomerWish(customerWishList);

            for (PromoItem promoItem : promoItemList) {

                CustomerWishListView customerWishListView = new CustomerWishListView();
                customerWishListView.setCustomerAccount(customerAccount);
                customerWishListView.setPromoItem(promoItem);
                customerWishListView.setCustomerWishList(customerWishList);
                try {
                    if(customerWishListViewRepo.findByPromoItem(promoItem) == null){
                        customerWishListViewRepo.save(customerWishListView);
                    }
                } catch (Exception ex) {
                    System.out.println("item exist: " + ex.getMessage());
                }
            }
        }
    }

    @Override
    public void refreshMediaWishList(MediaAccount mediaAccount) {
        mediaWishListViewRepo.deleteAllByMediaAccount(mediaAccount);

        List<MediaWishList> mediaWishLists = mediaWishListRepo.findAllByMediaAccountAndStatus(mediaAccount, statusRepo.findByCode("CWI_ACTIVE"));

        List<MediaWishListView> mediaWishListViews = new ArrayList<>();

        for (MediaWishList mediaWishList : mediaWishLists) {

            List<PromoItem> promoItemList = wishListHandlerRepo.findPromoItemByMediaWish(mediaWishList);

            for (PromoItem promoItem : promoItemList) {

                MediaWishListView mediaWishListView = new MediaWishListView();
                mediaWishListView.setMediaAccount(mediaAccount);
                mediaWishListView.setPromoItem(promoItem);
                mediaWishListView.setMediaWishList(mediaWishList);
                try {
                    if(mediaWishListViewRepo.findByPromoItem(promoItem) == null){
                        mediaWishListViewRepo.save(mediaWishListView);
                    }
                } catch (Exception ex) {
                    System.out.println("item exist: " + ex.getMessage());
                }

            }
        }
    }

    @Override
    public List<PromoItem> findPromoItemByCustomerWish(CustomerWishList customerWishList) {

        List<PromoItem> promoItemList=wishListHandlerRepo.findPromoItemByCustomerWish(customerWishList);
        return promoItemList;
    }

    @Override
    public List<PromoItem> findPromoItemByMediaWish(MediaWishList mediaWishList) {

        List<PromoItem> promoItemList=wishListHandlerRepo.findPromoItemByMediaWish(mediaWishList);
        return promoItemList;
    }


}
