package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerWishList;
import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.CustomerWishListRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.CustomerWishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CustomerWishListServiceImpl implements CustomerWishListService {

    @Autowired
    private CustomerWishListRepo customerWishListRepo;
    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private StatusRepo statusRepo;

    @Override
    public CustomerWishList findById(long id) {
        CustomerWishList customerWishList=customerWishListRepo.findById(id);
        return customerWishList;
    }

    @Override
    public CustomerWishList create(CustomerWishList customerWishList) {
        CustomerWishList wishList=customerWishListRepo.save(customerWishList);
        return wishList;
    }

    @Override
    public List<CustomerWishList> findAll(CustomerAccount customerAccount, Status status) {
        List<CustomerWishList> wishLists=customerWishListRepo.findAllByCustomerAccountAndStatus(customerAccount,status);
        return wishLists;
    }

    @Override
    public CustomerWishList find(CustomerAccount account, Long id) {

        Status status=statusRepo.findByCode("CWI_ACTIVE");
        if(status==null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "sts400", account.getOperationLanguage().getCode(), null);

        CustomerWishList wishList=customerWishListRepo.findByCustomerAccountAndIdAndStatus(account,id,status);
        return wishList;
    }

}
