package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.repository.CountryRepo;
import com.dc.Promotion.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryRepo countryRepo;
    @Override
    public Country findById(long id) {
        Country country=countryRepo.findById(id);
        return country;
    }

    @Override
    public List<Country> findAll() {
        List<Country> countries=countryRepo.findAll();
        return countries;
    }

    @Override
    public Country findByCode(String code) {
        Country country=countryRepo.findByCode(code);
        return country;
    }
}
