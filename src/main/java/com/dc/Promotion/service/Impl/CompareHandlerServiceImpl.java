package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.repository.CompareHandlerRepo;
import com.dc.Promotion.service.CompareHandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CompareHandlerServiceImpl implements CompareHandlerService {

    @Autowired
    private CompareHandlerRepo compareHandlerRepo;

    @Override
    public List<PromoItem> compare(PromoItem promoItem, String type, Double longitude, Double latitude) {
        return compareHandlerRepo.compare(promoItem,type,longitude,latitude) ;
    }
}
