package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.MediaType;
import com.dc.Promotion.repository.MediaTypeRepo;
import com.dc.Promotion.service.MediaTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MediaTypeServiceImpl implements MediaTypeService {

    @Autowired
    private MediaTypeRepo mediaTypeRepo;

    @Override
    public MediaType findById(Long id) {
        MediaType type= mediaTypeRepo.findById(id);
        return type;
    }


    @Override
    public MediaType findByCode(String code) {
        return mediaTypeRepo.findMediaTypeByCode(code);
    }
}
