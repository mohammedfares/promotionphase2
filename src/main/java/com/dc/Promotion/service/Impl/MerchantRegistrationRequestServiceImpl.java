package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.*;
import com.dc.Promotion.security.SECCriptoRsa;
import com.dc.Promotion.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class MerchantRegistrationRequestServiceImpl implements MerchantRegistrationRequestService {
    @Autowired
    private MerchantRegistrationRequestRepo merchantRegistrationRequestRepo;
    @Autowired
    private StatusRepo statusRepo;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private OperationLanguageService languageService;
    @Autowired
    private MerchantService merchantService;
    @Autowired
    private MerchantRepo merchantRepo;
    @Autowired
    private MerchantCaptionService merchantCaptionService;
    @Autowired
    private MerchantCaptionRepo merchantCaptionRepo;
    @Autowired
    private MerchantUserTypeService merchantUserTypeService;
    @Autowired
    private MerchantUserTypeRepo merchantUserTypeRepo;
    @Autowired
    private MerchantUserService merchantUserService;
    @Autowired
    private MerchantUserRepo merchantUserRepo;
    @Autowired
    private IndustryRepo industryRepo;
    @Autowired
    private RegistrationRequestIndustryRepo registrationRequestIndustryRepo;
    @Autowired
    private MerchantIndustryRepo merchantIndustryRepo;

    public MerchantRegistrationRequest findById(long id) {
        MerchantRegistrationRequest merchantRegistrationRequest = merchantRegistrationRequestRepo.findById(id);
        return merchantRegistrationRequest;
    }

    @Override
    public MerchantRegistrationRequest create(MerchantRegistrationRequest registrationRequest, List<Long> industryList) {
        //status code for NEW Merchant Request
        Status status = statusRepo.findByCode("MRR_NEW");
        if (status == null)
            //status not found Error
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", registrationRequest.getOperationLanguage().getCode(), null);

        registrationRequest.setCreateDate(LocalDateTime.now());
        registrationRequest.setExpiryDate(LocalDateTime.now().plusYears(2));
        registrationRequest.setStatus(status);
        //save merchant request
        registrationRequest = merchantRegistrationRequestRepo.save(registrationRequest);

        //save List of Industry for Registration Request
        for (Long industryId : industryList) {

            if (industryId == null) {
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "industry404", registrationRequest.getOperationLanguage().getCode(), null);
            }

            Industry industry = industryRepo.findById(industryId.longValue());
            //check industry
            if (industry == null)
                throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "industry404", registrationRequest.getOperationLanguage().getCode(), null);

            RegistrationRequestIndustry registrationRequestIndustry = new RegistrationRequestIndustry();
            registrationRequestIndustry.setIndustry(industry);
            registrationRequestIndustry.setMerchantRegistrationRequest(registrationRequest);
            registrationRequestIndustry = registrationRequestIndustryRepo.save(registrationRequestIndustry);
        }
        // end save Industry List

        return registrationRequest;
    }

    @Override
    public MerchantRegistrationRequest rejectRequest(Long requestId, OperationLanguage language) {

        MerchantRegistrationRequest registrationRequest = merchantRegistrationRequestRepo.findById(requestId.longValue());
        if (registrationRequest == null)
            //error code for not found data
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mrr404", language.getCode(), null);

        //status code for reject merchant Request
        Status status = statusRepo.findByCode("MRR_REJECT");
        if (status == null)
            //error code for not found data
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", language.getCode(), null);

        if (registrationRequest.getStatus().equals(status))
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "mrr302", language.getCode(), null);

        registrationRequest.setStatus(status);
        return registrationRequest;
    }

    @Override
    public MerchantRegistrationRequest approveRequest(Long requestId, OperationLanguage language) {
        MerchantRegistrationRequest registrationRequest = merchantRegistrationRequestRepo.findById(requestId.longValue());
        if (registrationRequest == null)
            //error code for not found data
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "mrr404", language.getCode(), null);

        //status code for approve merchant Request
        Status status = statusRepo.findByCode("MRR_APPROVE");
        if (status == null)
            //error code for not found data
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", language.getCode(), null);

        //request already approved
        if (registrationRequest.getStatus().getCode().equals(status.getCode()))
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "mrr302", language.getCode(), null);

        registrationRequest.setStatus(status);

        Merchant merchant = new Merchant();
        merchant.setOperationLanguage(language);
        merchant = merchantService.create(merchant);

        MerchantCaption merchantCaption = new MerchantCaption();
        merchantCaption.setMerchant(merchant);
        merchantCaption.setName(registrationRequest.getMerchantName());
        merchantCaption.setOperationLanguage(language);
        merchantCaptionRepo.save(merchantCaption);

        //merchant user type code for
        MerchantUserType merchantUserType = merchantUserTypeRepo.findByCode("MER_ADMIN");
        if (merchantUserType == null)
            //error code for not found data
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", language.getCode(), null);

        if (merchantUserService.findByEmail(registrationRequest.getEmail()) != null)
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "email302", language.getCode(), null);

        if (merchantUserService.findByUsername(registrationRequest.getUsername()) != null)
            throw new ResourceException(applicationContext, HttpStatus.FOUND, "username302", language.getCode(), null);

        MerchantUser merchantUser = new MerchantUser();
        merchantUser.setCrn(registrationRequest.getCrn());
        merchantUser.setEmail(registrationRequest.getEmail());
        merchantUser.setMerchant(merchant);
        merchantUser.setMerchantUserType(merchantUserType);
        merchantUser.setMobile(registrationRequest.getMobile());
        merchantUser.setPassword(registrationRequest.getPassword());
        merchantUser.setUserName(registrationRequest.getUsername());
        merchantUser.setOperationLanguage(language);
        merchantUserService.create(merchantUser);

        Status merchantIndustryStatus = statusRepo.findByCode("MIN_ACTIVE");
        if (merchantIndustryStatus == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", language.getCode(), null);

        List<Industry> industryList = registrationRequestIndustryRepo.findByQuery(registrationRequest);
        for (Industry industry : industryList) {
            MerchantIndustry merchantIndustry = new MerchantIndustry();
            merchantIndustry.setIndustry(industry);
            merchantIndustry.setMerchant(merchant);
            merchantIndustry.setStatus(merchantIndustryStatus);
            merchantIndustryRepo.save(merchantIndustry);
        }
        return registrationRequest;
    }

    @Override
    public List<MerchantRegistrationRequest> findAllByStatus(Status status) {

        List<MerchantRegistrationRequest> requestList = merchantRegistrationRequestRepo.findAllByStatus(status);
        return requestList;
    }

    @Override
    public String deleteRequest(long id, OperationLanguage language) {

//        Status status = statusRepo.findByCode("MRR_REJECT");
//        if (status == null)
//            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", language.getCode(), null);
//
//        MerchantRegistrationRequest registrationRequest = merchantRegistrationRequestRepo.findById(id);
//        if (registrationRequest == null)
//            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", language.getCode(), null);
//
//        if (!registrationRequest.getStatus().equals(status))
//            throw new ResourceException(applicationContext, HttpStatus.NOT_ACCEPTABLE, "del406", language.getCode(), null);

        registrationRequestIndustryRepo.deleteAllByMerchantRegistrationRequest_Id(id);
        merchantRegistrationRequestRepo.deleteById(id);
        return "deleted";
    }

    @Override
    public MerchantRegistrationRequest findByEmail(String email) {

        MerchantRegistrationRequest request = merchantRegistrationRequestRepo.findByEmail(email);
        return request;
    }

    @Override
    public MerchantRegistrationRequest findByUsername(String username) {
        MerchantRegistrationRequest request = merchantRegistrationRequestRepo.findByUsername(username);
        return request;
    }

    @Override
    public List<MerchantRegistrationRequest> findAll() {
        return merchantRegistrationRequestRepo.findAll();
    }

    @Override
    public MerchantRegistrationRequest findByCRN(String crn) {
        MerchantRegistrationRequest registrationRequest = merchantRegistrationRequestRepo.findByCrn(crn);
        return registrationRequest;
    }


}
