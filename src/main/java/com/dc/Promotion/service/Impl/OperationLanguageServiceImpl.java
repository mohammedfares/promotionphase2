package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.OperationLanguageRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.OperationLanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OperationLanguageServiceImpl implements OperationLanguageService {
    @Autowired
    private OperationLanguageRepo operationLanguageRepo;
    @Autowired
    private StatusRepo statusRepo;

    public OperationLanguage findById(Long id) {
        OperationLanguage operationLanguage=operationLanguageRepo.findById(id);
        return operationLanguage;
    }

    @Override
    public OperationLanguage findByCode(String code) {
        OperationLanguage language=operationLanguageRepo.findByCode(code);
        return language;
    }

    @Override
    public List<OperationLanguage> findAll() {

        //status code for Language
        Status status=statusRepo.findByCode("OLA_ACTIVE");
        List<OperationLanguage> languageList=operationLanguageRepo.findAllByStatus(status);
        return languageList;
    }
}
