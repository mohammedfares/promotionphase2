package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.repository.*;

import com.dc.Promotion.exception.ResourceException;

import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.repository.PromoItemRepo;
import com.dc.Promotion.repository.StatusRepo;

import com.dc.Promotion.resources.PromoItemCaptionResources;
import com.dc.Promotion.resources.PromoItemImageResources;
import com.dc.Promotion.resources.SpecificationResources;
import com.dc.Promotion.service.PromoItemService;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class PromoItemServiceImpl implements PromoItemService {

    @Autowired
    private PromoItemRepo promoItemRepo;

    @Autowired
    private PromoItemCaptionRepo promoItemCaptionRepo;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private PromoItemImageRepo promoItemImageRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Autowired
    private OperationLanguageRepo operationLanguageRepo;

    @Autowired
    private ItemSpecificationRepo itemSpecificationRepo;

    @Autowired
    private SpecificationRepo specificationRepo;

    @Autowired
    private OperationLanguageRepo languageRepo;

    @Autowired
    private CustomerWatchListRepo customerWatchListRepo;

    @Autowired
    private MediaWatchListRepo mediaWatchListRepo;

    @Autowired
    private CustomerItemViewRepo customerItemViewRepo;

    @Autowired
    private MediaItemViewRepo mediaItemViewRepo;

    @Override
    public PromoItem findById(long id) {
        PromoItem promoItem = promoItemRepo.findById(id);
        return promoItem;
    }

    @Override
    public PromoItem create(PromoItem promoItem, List<PromoItemImageResources> imageList, List<PromoItemCaptionResources> captionList, List<SpecificationResources> specificationList) {
        // create  promo item
        promoItem.setCreateDate(LocalDateTime.now());
        promoItem.setViewNo((long) 0);
        Status itemstatus = statusRepo.findByCode("PIT_INACTIVE");
        if (itemstatus == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", promoItem.getOperationLanguage().getCode(), null);
        promoItem.setStatus(itemstatus);

        String imageUrl = Utils.ConvertImage(imageList.get(0).getImageUrl());
        if (imageUrl != null) {
            promoItem.setCoverPhoto(imageUrl);
        }

        PromoItem item = promoItemRepo.save(promoItem);

        // create  promo item Caption
        for (PromoItemCaptionResources promoItemCaptionResource : captionList) {
            PromoItemCaption promoItemCaption = promoItemCaptionResource.toPromoItemCaption();
            OperationLanguage operationLanguage = operationLanguageRepo.findById(promoItemCaptionResource.getLanguageId());
            if (operationLanguage != null) {
                PromoItemCaption currentCaption = promoItemCaptionRepo.findByPromoItemAndLanguage(item, operationLanguage);
                if (currentCaption != null) {
                    if (!promoItemCaptionResource.getName().isEmpty()) {
                        promoItemCaption.setPromoItem(item);
                        promoItemCaption.setName(promoItemCaptionResource.getName());
                        promoItemCaption.setLanguage(operationLanguage);
                    }
                } else {
                    if (!promoItemCaptionResource.getName().isEmpty()) {
                        promoItemCaption.setPromoItem(item);
                        promoItemCaption.setName(promoItemCaptionResource.getName());
                        promoItemCaption.setLanguage(operationLanguage);
                        promoItemCaption = promoItemCaptionRepo.save(promoItemCaption);
                    }
                }
            }
        }
        // create  promo item image
        for (PromoItemImageResources promoItemImageResources : imageList) {
            PromoItemImage promoItemImage = promoItemImageResources.toPromoItemImage();
            promoItemImage.setPromoItemId(promoItem);
            promoItemImage.setStatus(statusRepo.findByCode("PII_ACTIVE"));

            String imageUrlPromoItem = Utils.ConvertImage(promoItemImage.getImageUrl());
            if (imageUrlPromoItem != null) {
                promoItemImage.setImageUrl(imageUrlPromoItem);
            }
            PromoItemImage itemImage = promoItemImageRepo.save(promoItemImage);
        }
        // create  promo item specs
        if (!(specificationList.isEmpty() || specificationList == null))
            for (SpecificationResources specificationResources : specificationList) {

                ItemSpecification itemSpecification = new ItemSpecification();
                Specification specification = specificationRepo.findByKey(specificationResources.getKey());
                if (specification != null) {
                    itemSpecification.setKey(null);
                    itemSpecification.setValue(specificationResources.getValue());
                    itemSpecification.setPromoItem(promoItem);
                    itemSpecification.setSpecification(specification);
                    OperationLanguage operationLanguage = operationLanguageRepo.findById(specificationResources.getLanguageId());
                    itemSpecification.setOperationLanguage(operationLanguage);
                    itemSpecificationRepo.save(itemSpecification);
                } else {
                    itemSpecification.setKey(specificationResources.getKey());
                    itemSpecification.setValue(specificationResources.getValue());
                    itemSpecification.setPromoItem(promoItem);
                    itemSpecification.setSpecification(null);
                    OperationLanguage operationLanguage = operationLanguageRepo.findById(specificationResources.getLanguageId());
                    itemSpecification.setOperationLanguage(operationLanguage);
                    itemSpecificationRepo.save(itemSpecification);
                }
            }

        return item;
    }

    @Override
    public PromoItem createTemp(PromoItem promoItem, List<PromoItemImageResources> imageList, List<PromoItemCaptionResources> captionList, List<SpecificationResources> specificationList) {

        Status itemTempstatus = statusRepo.findByCode("PIT_TEMP");
        if (itemTempstatus == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "pit404", promoItem.getOperationLanguage().getCode(), null);

        promoItem.setExpiryDate(promoItem.getPromo().getExpiryDate());
        promoItem.setViewNo(0L);
        // get First Image as cover Photo
        if (imageList != null && !imageList.isEmpty()) {
            String imageUrl = Utils.ConvertImage(imageList.get(0).getImageUrl());
            if (imageUrl != null) {
                promoItem.setCoverPhoto(imageUrl);
            }
        }

        promoItem.setStatus(itemTempstatus);
        promoItem.setCreateDate(LocalDateTime.now());
        promoItem = promoItemRepo.save(promoItem);
        //end save promo Item

        //save promo Item Image
        if (imageList != null || !imageList.isEmpty())
            for (PromoItemImageResources imageResources : imageList) {
                PromoItemImage itemImage = imageResources.toPromoItemImage();
                Status itemImagestatus = statusRepo.findByCode("PII_ACTIVE");
                if (itemImagestatus == null)
                    throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", promoItem.getOperationLanguage().getCode(), null);
                //convert Image to URL
                String imageUrlPromoItem = Utils.ConvertImage(itemImage.getImageUrl());
                if (imageUrlPromoItem != null) {
                    itemImage.setImageUrl(imageUrlPromoItem);
                }
                itemImage.setStatus(itemImagestatus);
                itemImage.setPromoItemId(promoItem);
                promoItemImageRepo.save(itemImage);
            }

        //End

        //save Promo Item Caption

        for (PromoItemCaptionResources itemCaptionResources : captionList) {
            PromoItemCaption promoItemCaption = itemCaptionResources.toPromoItemCaption();
            OperationLanguage operationLanguage = operationLanguageRepo.findById(itemCaptionResources.getLanguageId());
            if (operationLanguage != null) {
                PromoItemCaption currentCaption = promoItemCaptionRepo.findByPromoItemAndLanguage(promoItem, operationLanguage);
                if (currentCaption != null) {
                    if (!itemCaptionResources.getName().isEmpty()) {
                        promoItemCaption.setPromoItem(promoItem);
                        promoItemCaption.setName(itemCaptionResources.getName());
                        promoItemCaption.setLanguage(operationLanguage);
                    }
                } else {
                    if (!itemCaptionResources.getName().isEmpty()) {
                        promoItemCaption.setPromoItem(promoItem);
                        promoItemCaption.setName(itemCaptionResources.getName());
                        promoItemCaption.setLanguage(operationLanguage);
                        promoItemCaption = promoItemCaptionRepo.save(promoItemCaption);
                    }
                }
            }
        }

//        for (PromoItemCaptionResources itemCaptionResources : captionList) {
//            PromoItemCaption itemCaption = itemCaptionResources.toPromoItemCaption();
//            itemCaption.setPromoItem(promoItem);
//            itemCaption.setLanguage(operationLanguageRepo.findById(itemCaptionResources.getLanguageId()));
//            itemCaption.setName(itemCaptionResources.getName());
//            promoItemCaptionRepo.save(itemCaption);
//        }

        //save Item Specification
        for (SpecificationResources specificationResources : specificationList) {

            ItemSpecification itemSpecification = new ItemSpecification();
            Specification specification = specificationRepo.findByKey(specificationResources.getKey());
            if (specification != null) {
                itemSpecification.setKey(null);
                itemSpecification.setValue(specificationResources.getValue());
                itemSpecification.setOperationLanguage(languageRepo.findById(specificationResources.getLanguageId()));
                itemSpecification.setPromoItem(promoItem);
                itemSpecification.setSpecification(specification);
                itemSpecificationRepo.save(itemSpecification);
            } else {
                itemSpecification.setKey(null);
                itemSpecification.setKey(specificationResources.getKey());
                itemSpecification.setValue(specificationResources.getValue());
                itemSpecification.setOperationLanguage(languageRepo.findById(specificationResources.getLanguageId()));
                itemSpecification.setPromoItem(promoItem);
                itemSpecification.setSpecification(null);
                itemSpecificationRepo.save(itemSpecification);
            }
        }
        //end
        return null;
    }

    @Override
    public Page<PromoItem> findAll(PromoItem itemSearchCriteria, Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");

        Page<PromoItem> itemPage = promoItemRepo.findAll(new org.springframework.data.jpa.domain.Specification() {
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> predicates = new ArrayList<>();

                if (itemSearchCriteria.getStatus() != null) {

                    predicates.add(criteriaBuilder.equal(root.get("status"), itemSearchCriteria.getStatus()));
                }
                if (itemSearchCriteria.getPromo() != null) {
                    predicates.add(criteriaBuilder.equal(root.get("promo"), itemSearchCriteria.getPromo()));
                }
//                if (itemSearchCriteria.getCategory() != null) {
//                    predicates.add(criteriaBuilder.equal(root.get("category"), itemSearchCriteria.getCategory()));
//
//                }
                return criteriaBuilder.and(predicates.toArray(new Predicate[0]));


            }
        }, pageable);
        return itemPage;
    }

    @Override
    public List<PromoItem> findAllByPromo(Promo promo) {
        return promoItemRepo.findAllByPromo(promo);
    }

    @Override
    public List<PromoItem> findWatchListCustomer(CustomerAccount account) {
        Status status = statusRepo.findByCode("CWA_ACTIVE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", account.getOperationLanguage().getCode(), null);

        List<PromoItem> promoItemList = promoItemRepo.findByQuery(account, status);
        return promoItemList;
    }

    @Override
    public List<PromoItem> findWatchListMedia(MediaAccount account) {
        Status status = statusRepo.findByCode("MWA_ACTIVE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "sts404", account.getOperationLanguage().getCode(), null);

        List<PromoItem> promoItemList = promoItemRepo.findByQueryMedia(account, status);
        return promoItemList;
    }


//    @Override
//    public List<PromoItem> findWatchList(CustomerAccount account) {
//        Status status = statusRepo.findByCode("CWA_ACTIVE");
//        if (status == null)
//            throw new ResourceException(applicationContext, HttpStatus.NOT_FOUND, "404", account.getOperationLanguage().getCode(), null);
//
//        List<PromoItem> promoItemList = promoItemRepo.findByQuery(account, status);
//        return promoItemList;
//    }

    @Override
    public Page<PromoItem> findTrendItem(PromoItem promoItem, Integer page, Integer size) {

        if (page == null) page = 0;
        if (size == null) size = 10;


        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "viewNo");

        Page<PromoItem> promoPage = promoItemRepo.findAll(new org.springframework.data.jpa.domain.Specification<PromoItem>() {

            @Override
            public Predicate toPredicate(Root<PromoItem> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

                List<Predicate> predicates = new ArrayList<>();

                if (promoItem.getStatus() != null) {
                    predicates.add(cb.equal(root.get("status"), promoItem.getStatus()));
                }

                if (promoItem.getExpiryDate() != null) {
                    predicates.add(cb.greaterThanOrEqualTo(root.get("expiryDate"), promoItem.getExpiryDate()));
                }

                predicates.add(cb.greaterThan(root.get("viewNo").as(String.class), "0"));

                return cb.and(predicates.toArray(new Predicate[0]));
            }
        }, pageable);

        return promoPage;


    }

    @Override
    public Page<PromoItem> filterCustomerPromoItemAll(
            String name,
            Industry industry,
            Merchant merchant,
            ItemCategory itemCategory,
            Status status,
            Double newPriceFrom,
            Double newPriceTo,
            Integer trendFlag,
            LocalDateTime startDate,
            LocalDateTime expiryDate,
            Double longitude,
            Double latitude,
            Double radius,
            Integer page,
            Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = null;

        if (trendFlag == null) {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        } else {
            pageable = new PageRequest(page, size);
        }

        return promoItemRepo.filterCustomerPromoItemAll(Utils.appendLikeOperator(name), industry, merchant, itemCategory, status, newPriceFrom, newPriceTo, trendFlag, startDate, expiryDate, longitude, latitude, radius, pageable);
    }

    @Override
    public Page<PromoItem> filterCustomerPromoItemWatchList(String name, Industry industry, Merchant merchant, ItemCategory itemCategory, Status status, CustomerAccount customerAccount, Double newPriceFrom, Double newPriceTo, Integer trendFlag, LocalDateTime startDate, LocalDateTime expiryDate, Double longitude, Double latitude, Double radius, Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = null;

        if (trendFlag == null) {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        } else {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "viewNo");
        }

        return promoItemRepo.filterCustomerPromoItemWatchList(Utils.appendLikeOperator(name), industry, merchant, itemCategory, status, customerAccount, newPriceFrom, newPriceTo, trendFlag, startDate, expiryDate, longitude, latitude, radius, pageable);
    }

    @Override
    public Page<PromoItem> filterMediaPromoItemWatchList(String name, Industry industry, Merchant merchant, ItemCategory itemCategory, Status status, MediaAccount mediaAccount, Double newPriceFrom, Double newPriceTo, Integer trendFlag, LocalDateTime startDate, LocalDateTime expiryDate, Double longitude, Double latitude, Double radius, Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = null;

        if (trendFlag == null) {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        } else {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "viewNo");
        }

        return promoItemRepo.filterMediaPromoItemWatchList(Utils.appendLikeOperator(name), industry, merchant, itemCategory, status, mediaAccount, newPriceFrom, newPriceTo, trendFlag, startDate, expiryDate, longitude, latitude, radius, pageable);
    }

    @Override
    public List<PromoItem> top5PromoItem(Merchant merchant) {
        List<PromoItem> promoItemList = promoItemRepo.findTop(new PageRequest(0, 5), merchant, LocalDateTime.now());
        return promoItemList;
    }

    @Override
    public Long totalPromoItem(Merchant merchant) {

        Long total = promoItemRepo.totalPromoItem(merchant);
        return total;
    }

    @Override
    public void deletePromoItemByPromo(Promo promo) {

        try {
            List<PromoItem> promoItemList = promoItemRepo.findAllByPromo(promo);

            for (PromoItem promoItem : promoItemList) {

                promoItemImageRepo.deleteAllByPromoItemId(promoItem);

                promoItemCaptionRepo.deleteAllByPromoItem(promoItem);

                itemSpecificationRepo.deleteAllByPromoItem(promoItem);

                System.out.println("currentPromoItem: " + promoItem.getId());

                customerWatchListRepo.deleteCustomerWatchListByPromoItem(promoItem);

                mediaWatchListRepo.deleteMediaWatchListByPromoItem(promoItem);

                customerItemViewRepo.deleteAllByPromoItem(promoItem);

                mediaItemViewRepo.deleteAllByPromoItem(promoItem);
            }

            if (promoItemList != null && !promoItemList.isEmpty()) {
                promoItemRepo.deleteAllByPromo(promo);
            }
        } catch (Exception ex) {
            System.out.println(">>>>: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Override
    public Page<PromoItem> findByCompare(PromoItem promoItem, Double longitude, Double latitude,
                                         Double radius, Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;
        Pageable pageable = null;
        pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        Page<PromoItem> promoItemList =
                promoItemRepo.findByCompareItem(
                        LocalDateTime.now(),
                        promoItem.getStatus(),
                        promoItem.getId(),
                        promoItem.getCategory(),
                        promoItem,
                        Double.parseDouble(promoItem.getNewPrice()),
                        longitude,
                        latitude,
                        radius,
                        pageable);

        return promoItemList;
    }

    @Override
    public Page<PromoItem> filterCustomerPromoItemWishList(String name, Industry industry, Merchant merchant, ItemCategory itemCategory, Status status, CustomerAccount customerAccount, Double newPriceFrom, Double newPriceTo, Integer trendFlag, LocalDateTime startDate, LocalDateTime expiryDate, Double longitude, Double latitude, Double radius, Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = null;

        if (trendFlag == null) {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        } else {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "viewNo");
        }

        return promoItemRepo.filterCustomerPromoItemWishList(Utils.appendLikeOperator(name), industry, merchant, itemCategory, status, customerAccount, newPriceFrom, newPriceTo, trendFlag, startDate, expiryDate, longitude, latitude, radius, pageable);
    }

    @Override
    public Page<PromoItem> filterMediaPromoItemWishList(String name, Industry industry, Merchant merchant, ItemCategory itemCategory, Status status, MediaAccount mediaAccount, Double newPriceFrom, Double newPriceTo, Integer trendFlag, LocalDateTime startDate, LocalDateTime expiryDate, Double longitude, Double latitude, Double radius, Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;

        Pageable pageable = null;

        if (trendFlag == null) {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        } else {
            pageable = new PageRequest(page, size, Sort.Direction.DESC, "viewNo");
        }

        return promoItemRepo.filterMediaPromoItemWishList(Utils.appendLikeOperator(name), industry, merchant, itemCategory, status, mediaAccount, newPriceFrom, newPriceTo, trendFlag, startDate, expiryDate, longitude, latitude, radius, pageable);
    }

    @Override
    public Page<PromoItem> itemReport(LocalDateTime startDate, LocalDateTime endDate, Promo promo, Merchant merchant ,Integer page, Integer size) {
        if (page == null) page = 0;
        if (size == null) size = 10;
        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "id");
        Page<PromoItem> promoItemList = promoItemRepo.findItemReport(startDate,
                endDate,
                promo,
                merchant,
                pageable);

        return promoItemList;
    }


}
