package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.repository.MediaAccountRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.security.SECCriptoRsa;
import com.dc.Promotion.service.MediaAccountService;
import com.dc.Promotion.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class MediaAccountServiceImpl implements MediaAccountService {

    @Autowired
    private MediaAccountRepo mediaAccountRepo;

    @Autowired
    private StatusRepo statusRepo;

    @Override
    public MediaAccount findById(Long id) {

        MediaAccount mediaAccount = mediaAccountRepo.findById(id);
        return mediaAccount;
    }

    @Override
    public MediaAccount findByUsernameAndType(String username, MediaType mediaType) {
        return mediaAccountRepo.findByUserNameAndMediaType(username, mediaType);
    }

    @Override
    public MediaAccount createFaceBookAccount(MediaAccount mediaAccount) {

        MediaAccount account = new MediaAccount();
        account.setUserName(mediaAccount.getUserName());
        account.setMediaType(mediaAccount.getMediaType());
        Status status = statusRepo.findByCode("MAC_ACTIVE");
        account.setStatus(status);
        account.setPassword(SECCriptoRsa.encrypt("12345"));
        account.setBarCode(Long.valueOf(Utils.randomNumber(10)));

       account= mediaAccountRepo.save(account);

        return account;
    }

    @Override
    public MediaAccount updateAccountInfo(MediaAccount mediaAccount) {
        MediaAccount currentMediaAccount = mediaAccountRepo.findById(mediaAccount.getId());

        if (mediaAccount.getDeviceToken() != null && !mediaAccount.getDeviceToken().isEmpty()) {
            currentMediaAccount.setDeviceToken(mediaAccount.getDeviceToken());
        }

        if (mediaAccount.getDevicePlatform() != null && !mediaAccount.getDevicePlatform().isEmpty()) {
            currentMediaAccount.setDevicePlatform(mediaAccount.getDevicePlatform());
        }

        if (mediaAccount.getOperationLanguage() != null) {
            currentMediaAccount.setOperationLanguage(mediaAccount.getOperationLanguage());
        }

        if (mediaAccount.getLatitude() != null && mediaAccount.getLatitude() != 0.0) {
            currentMediaAccount.setLatitude(mediaAccount.getLatitude());
        }

        if (mediaAccount.getLongitude() != null && mediaAccount.getLongitude() != 0.0) {
            currentMediaAccount.setLatitude(mediaAccount.getLongitude());
        }

        return currentMediaAccount;
    }

    @Override
    public MediaAccount changeLanguage(MediaAccount account, OperationLanguage operationLanguage) {

        MediaAccount mediaAccount=mediaAccountRepo.findById(account.getId());
        mediaAccount.setOperationLanguage(operationLanguage);
        return mediaAccount;
    }
}
