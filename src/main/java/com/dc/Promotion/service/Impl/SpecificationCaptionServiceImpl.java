package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Specification;
import com.dc.Promotion.entities.SpecificationCaption;
import com.dc.Promotion.repository.SpecificationCaptionRepo;
import com.dc.Promotion.service.SpecificationCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SpecificationCaptionServiceImpl implements SpecificationCaptionService {

    @Autowired
    private SpecificationCaptionRepo specificationCaptionRepo;

    @Override
    public SpecificationCaption findById(long id) {

        SpecificationCaption specificationCaption = specificationCaptionRepo.findById(id);
        return specificationCaption;
    }

    @Override
    public List<SpecificationCaption> allSpecification() {

        List<SpecificationCaption> captionList=specificationCaptionRepo.findAllByOrderBySpecificationId();


        return captionList;
    }

    @Override
    public SpecificationCaption find(Long specificationId, OperationLanguage operationLanguage) {
        return specificationCaptionRepo.findSpecificationCaptionBySpecification_IdAndLanguage(specificationId,operationLanguage);
    }

    @Override
    public List<SpecificationCaption> find(Long specificationId) {
        return specificationCaptionRepo.findSpecificationCaptionBySpecification_Id(specificationId);
    }
}
