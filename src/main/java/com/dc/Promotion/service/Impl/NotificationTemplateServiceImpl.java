package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.NotificationTemplate;
import com.dc.Promotion.repository.NotificationTemplateRepo;
import com.dc.Promotion.service.NotificationTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class NotificationTemplateServiceImpl implements NotificationTemplateService {
    @Autowired
    private NotificationTemplateRepo notificationTemplateRepo;


    @Override
    public NotificationTemplate findByCode(String code) {
        NotificationTemplate notificationTemplate=notificationTemplateRepo.findByCode(code);


        return notificationTemplate;
    }
}
