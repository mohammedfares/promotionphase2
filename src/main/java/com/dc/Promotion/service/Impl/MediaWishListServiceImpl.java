package com.dc.Promotion.service.Impl;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaWishList;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.exception.ResourceException;
import com.dc.Promotion.repository.MediaWishListRepo;
import com.dc.Promotion.repository.StatusRepo;
import com.dc.Promotion.service.MediaWishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class MediaWishListServiceImpl implements MediaWishListService {
    @Autowired
    private MediaWishListRepo mediaWishListRepo;
    @Autowired
    private StatusRepo statusRepo;
    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public MediaWishList findById(long id) {
        MediaWishList mediaWishList = mediaWishListRepo.findById(id);
        return mediaWishList;
    }

    @Override
    public MediaWishList create(MediaWishList wishList) {
        MediaWishList mediaWishList = mediaWishListRepo.save(wishList);
        return mediaWishList;
    }

    @Override
    public List<MediaWishList> findAll(MediaAccount mediaAccount, Status status) {
        List<MediaWishList> wishLists = mediaWishListRepo.findAllByMediaAccountAndStatus(mediaAccount, status);
        return wishLists;
    }

    @Override
    public MediaWishList find(MediaAccount account, Long id) {

        Status status = statusRepo.findByCode("CWI_ACTIVE");
        if (status == null)
            throw new ResourceException(applicationContext, HttpStatus.BAD_REQUEST, "sts400", account.getOperationLanguage().getCode(), null);

        MediaWishList mediaWishList = mediaWishListRepo.findByMediaAccountAndStatusAndId(account, status, id);
        return mediaWishList;
    }
}
