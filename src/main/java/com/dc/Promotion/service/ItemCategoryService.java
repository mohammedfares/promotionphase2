package com.dc.Promotion.service;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.Merchant;

import java.util.List;

public interface ItemCategoryService {

    public ItemCategory findById(Long id);

    public List<ItemCategory> findTopViewCategory(Merchant merchant);

    public List<ItemCategory> findAll();
}
