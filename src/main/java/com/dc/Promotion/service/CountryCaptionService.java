package com.dc.Promotion.service;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.CountryCaption;
import com.dc.Promotion.entities.OperationCountry;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.List;

public interface CountryCaptionService {
    public CountryCaption findById(long id);
    public CountryCaption findByCountryAndLanguage(Country country, OperationLanguage language);
    public List<CountryCaption> find( OperationLanguage language);
    public List<CountryCaption> findAllCountry(OperationLanguage language);
}
