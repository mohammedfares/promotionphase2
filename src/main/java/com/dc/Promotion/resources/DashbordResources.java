package com.dc.Promotion.resources;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.PromoItem;

import java.util.List;

public class DashbordResources {

    private List<PromoItemResources> topPromoItem;
    private List<PromoResources> topPromo;
    private List<ItemCategoryResources> topItemCategory;
    private List<MerchantCustomerSegmentResources> topSegment;
    private Long totalPromo;
    private Long totalPromoItem;
    private Long totalView;

    public List<PromoItemResources> getTopPromoItem() {
        return topPromoItem;
    }

    public void setTopPromoItem(List<PromoItemResources> topPromoItem) {
        this.topPromoItem = topPromoItem;
    }

    public List<PromoResources> getTopPromo() {
        return topPromo;
    }

    public void setTopPromo(List<PromoResources> topPromo) {
        this.topPromo = topPromo;
    }

    public Long getTotalPromo() {
        return totalPromo;
    }

    public void setTotalPromo(Long totalPromo) {
        this.totalPromo = totalPromo;
    }

    public Long getTotalPromoItem() {
        return totalPromoItem;
    }

    public void setTotalPromoItem(Long totalPromoItem) {
        this.totalPromoItem = totalPromoItem;
    }

    public Long getTotalView() {
        return totalView;
    }

    public void setTotalView(Long totalView) {
        this.totalView = totalView;
    }

    public List<ItemCategoryResources> getTopItemCategory() {
        return topItemCategory;
    }

    public void setTopItemCategory(List<ItemCategoryResources> topItemCategory) {
        this.topItemCategory = topItemCategory;
    }

    public List<MerchantCustomerSegmentResources> getTopSegment() {
        return topSegment;
    }

    public void setTopSegment(List<MerchantCustomerSegmentResources> topSegment) {
        this.topSegment = topSegment;
    }
}
