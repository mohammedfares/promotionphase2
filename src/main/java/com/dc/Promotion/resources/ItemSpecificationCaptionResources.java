package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Specification;
import com.dc.Promotion.entities.SpecificationCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.ArrayList;
import java.util.List;

public class ItemSpecificationCaptionResources {

    private Long id;

    private String name;

    private Long specificationId;

    private Long languageId;

    private String key;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSpecificationId() {
        return specificationId;
    }

    public void setSpecificationId(Long specificationId) {
        this.specificationId = specificationId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static ItemSpecificationCaptionResources toResources(SpecificationCaption caption) {
        ItemSpecificationCaptionResources resources = new ItemSpecificationCaptionResources();
        resources.setId(caption.getId());
        resources.setLanguageId(caption.getLanguage().getId());
        resources.setName(caption.getKey());
        resources.setSpecificationId(caption.getSpecification().getId());
        resources.setKey(caption.getSpecification().getKey());
        return resources;
    }

    public static List<ItemSpecificationCaptionResources> toResources(List<SpecificationCaption> captionList) {
        List<ItemSpecificationCaptionResources> resourcesList = new ArrayList<>();
        captionList.forEach(caption -> {
            ItemSpecificationCaptionResources resources = toResources(caption);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public SpecificationCaption toItemSpecificationCaption() {
        SpecificationCaption caption = new SpecificationCaption();
        caption.setId(this.id);
        caption.setKey(this.name);

        if (specificationId != null) {
            Specification specification = new Specification();
            specification.setId(this.specificationId);
            caption.setSpecification(specification);
        }

        if (languageId != null) {
            OperationLanguage language = new OperationLanguage();
            language.setId(this.languageId);
            caption.setLanguage(language);
        }
        return caption;
    }
}
