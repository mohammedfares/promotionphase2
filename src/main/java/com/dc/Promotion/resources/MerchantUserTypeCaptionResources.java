package com.dc.Promotion.resources;

import com.dc.Promotion.entities.MerchantUserType;
import com.dc.Promotion.entities.MerchantUserTypeCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.ArrayList;
import java.util.List;

public class MerchantUserTypeCaptionResources {
    private Long id;
    private String name;
    private String description;
    private Long merchantUserType;
    private Long languageId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMerchantUserType() {
        return merchantUserType;
    }

    public void setMerchantUserType(Long merchantUserType) {
        this.merchantUserType = merchantUserType;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public static MerchantUserTypeCaptionResources toResource(MerchantUserTypeCaption merchantUserTypeCaption) {

        MerchantUserTypeCaptionResources resources = new MerchantUserTypeCaptionResources();
        resources.setId(merchantUserTypeCaption.getId());
        resources.setName(merchantUserTypeCaption.getName());
        resources.setDescription(merchantUserTypeCaption.getDescription());
        resources.setLanguageId(merchantUserTypeCaption.getOperationLanguage().getId());
        resources.setMerchantUserType(merchantUserTypeCaption.getMerchantUserType().getId());
        return resources;

    }

    public static List<MerchantUserTypeCaptionResources> toResource(List<MerchantUserTypeCaption> merchantUserTypeCaption) {
        List<MerchantUserTypeCaptionResources> resources = new ArrayList<>();
        merchantUserTypeCaption.forEach(merchantUserTypeCaption1 -> {
            MerchantUserTypeCaptionResources resources1 = toResource(merchantUserTypeCaption1);
            resources.add(resources1);
        });
        return resources;
    }

    public MerchantUserTypeCaption toMerchantUserTypeCaption(){

        MerchantUserTypeCaption merchantUserTypeCaption1=new MerchantUserTypeCaption();
        merchantUserTypeCaption1.setId(this.id);
        merchantUserTypeCaption1.setName(this.name);
        merchantUserTypeCaption1.setDescription(this.description);
        if(merchantUserType==null){

            MerchantUserType merchantUserType=new MerchantUserType();
            merchantUserType.setId(this.merchantUserType);
            merchantUserTypeCaption1.setMerchantUserType(merchantUserType);
        }
        if(languageId==null){

            OperationLanguage operationLanguage=new OperationLanguage();
            operationLanguage.setId(languageId);
            merchantUserTypeCaption1.setOperationLanguage(operationLanguage);
        }
        return  merchantUserTypeCaption1;
    }
}
