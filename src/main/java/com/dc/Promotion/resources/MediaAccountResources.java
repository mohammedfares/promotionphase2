package com.dc.Promotion.resources;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaType;
import com.dc.Promotion.entities.Status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MediaAccountResources {

    private Long id;

    private Long mediaTypeId;

    private int age;

    private Long statusId;

    private String userName;

    private String token;

    private String deviceToken;

    private String devicePlatform;

    private Double longitude;

    private Double latitude;

    private Long languageId;

    private String languageCode;

    private Long barCode;

    private String gender;


    private String birthdate;


    private String firstName;


    private String lastName;

    public Long getBarCode() {
        return barCode;
    }

    public void setBarCode(Long barCode) {
        this.barCode = barCode;
    }

    public Long getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMediaTypeId() {
        return mediaTypeId;
    }

    public void setMediaTypeId(Long mediaTypeId) {
        this.mediaTypeId = mediaTypeId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDevicePlatform() {
        return devicePlatform;
    }

    public void setDevicePlatform(String devicePlatform) {
        this.devicePlatform = devicePlatform;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static MediaAccountResources toResources(MediaAccount mediaAccount) {
        MediaAccountResources resources = new MediaAccountResources();
        resources.setId(mediaAccount.getId());
        resources.setMediaTypeId(mediaAccount.getMediaType().getId());
        resources.setUserName(mediaAccount.getUserName());
        resources.setStatusId(mediaAccount.getStatus().getId());
        resources.setAge(mediaAccount.getAge());
        resources.setBirthdate(mediaAccount.getBirthdate());
        resources.setFirstName(mediaAccount.getFirstName());
        resources.setLastName(mediaAccount.getLastName());
        resources.setGender(mediaAccount.getGender());
        resources.setBarCode(mediaAccount.getBarCode());
        return resources;
    }

    public static List<MediaAccountResources> toResources(List<MediaAccount> accountList) {
        List<MediaAccountResources> resourcesList = new ArrayList<>();
        accountList.forEach(mediaAccount -> {
            MediaAccountResources resources = toResources(mediaAccount);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public MediaAccount toMediaAccount() {
        MediaAccount mediaAccount = new MediaAccount();
        mediaAccount.setId(this.id);
        mediaAccount.setUserName(this.userName);
        mediaAccount.setBirthdate(this.birthdate);
        mediaAccount.setFirstName(this.firstName);
        mediaAccount.setLastName(this.lastName);
        mediaAccount.setGender(this.gender);
        mediaAccount.setDeviceToken(this.deviceToken);
        mediaAccount.setDevicePlatform(this.devicePlatform);
        mediaAccount.setLatitude(this.latitude);
        mediaAccount.setLongitude(this.longitude);
        mediaAccount.setBarCode(this.barCode);

        if (mediaTypeId != null) {
            MediaType mediaType = new MediaType();
            mediaType.setId(this.mediaTypeId);
            mediaAccount.setMediaType(mediaType);
        }
        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            mediaAccount.setStatus(status);
        }
        return mediaAccount;
    }
}
