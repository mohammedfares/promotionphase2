package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class StatusResources {

    private Long id;
    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static StatusResources toResources(Status status)
    {
        StatusResources resources=new StatusResources();
        resources.setCode(status.getCode());
        resources.setId(resources.id);
        return resources;
    }

    public static List<StatusResources> toResources(List<Status> statusList)
    {
        List<StatusResources> resourcesList=new ArrayList<>();
        statusList.forEach(status -> {
            StatusResources resources=toResources(status);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public Status toStatus()
    {
        Status status=new Status();
        status.setId(this.id);
        status.setCode(this.code);
        return status;
    }

}
