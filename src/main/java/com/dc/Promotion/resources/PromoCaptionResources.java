package com.dc.Promotion.resources;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.PromoCaption;

import java.util.ArrayList;
import java.util.List;

public class PromoCaptionResources {
    private Long id;
    private String name;
    private String description;
    private Long promoId;
    private Long languageId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getPromoId() {
        return promoId;
    }

    public void setPromoId(Long promoId) {
        this.promoId = promoId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public static PromoCaptionResources toResources(PromoCaption promoCaption) {
        PromoCaptionResources resources = new PromoCaptionResources();
        resources.setId(promoCaption.getId());
        resources.setName(promoCaption.getName());
        resources.setDescription(promoCaption.getDescription());
        resources.setPromoId(promoCaption.getPromo().getId());
        resources.setLanguageId(promoCaption.getOperationLanguage().getId());
        return resources;
    }

    public static List<PromoCaptionResources> toResources(List<PromoCaption> promoCaption) {
        List<PromoCaptionResources> resources = new ArrayList<>();

        promoCaption.forEach(promoCaption1 -> {
            PromoCaptionResources captionResources = toResources(promoCaption1);
            resources.add(captionResources);


        });
        return resources;


    }
    public PromoCaption toPromoCaption(){
        PromoCaption promoCaption=new PromoCaption();
        promoCaption.setId(this.id);
        promoCaption.setName(this.name);
        promoCaption.setDescription(this.description);
        if(languageId!=null){
            OperationLanguage language=new OperationLanguage();
            language.setId(languageId);
            promoCaption.setOperationLanguage(language);

        }
        if(promoId!=null){
            Promo promo=new Promo();
            promo.setId(promoId);
            promoCaption.setPromo(promo);
        }
        return promoCaption;

    }
}
