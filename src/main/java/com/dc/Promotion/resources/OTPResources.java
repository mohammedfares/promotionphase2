package com.dc.Promotion.resources;

import com.dc.Promotion.entities.OTP;
import com.dc.Promotion.entities.Status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class OTPResources {
    private Long id;
    private String mobile;
    private String email;
    private String code;
    private LocalDateTime creationDate;
    private LocalDateTime expiryDate;
    private Long status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }
    public  OTPResources toResources(OTP otp){
        OTPResources resources=new OTPResources();
        resources.setId(otp.getId());
        resources.setMobile(otp.getMobile());
        resources.setCode(otp.getCode());
        resources.setCreationDate(otp.getCreationDate());
        resources.setExpiryDate(otp.getExpiryDate());
        resources.setStatus(otp.getStatus().getId());
        resources.setEmail(otp.getEmail());
        return  resources;
    }
    public List<OTPResources> toResources(List<OTP> otp){
        List<OTPResources> resources=new ArrayList<>();
        otp.forEach(otp1 -> {
            OTPResources otpResources=toResources(otp1);
            resources.add(otpResources);
        });
        return  resources;
    }
    public OTP toOtp(){
        OTP otp1=new OTP();
        otp1.setId(this.id);
        otp1.setCode(this.code);
        otp1.setEmail(this.email);
        otp1.setCreationDate(this.creationDate);
        otp1.setExpiryDate(this.expiryDate);
        otp1.setMobile(this.mobile);
        if(status==null){
            Status status=new Status();
            status.setId(this.status);
            otp1.setStatus(status);
        }
        return otp1;
    }
}
