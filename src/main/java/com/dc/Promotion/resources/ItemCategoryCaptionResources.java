package com.dc.Promotion.resources;


import com.dc.Promotion.entities.*;
import java.util.ArrayList;
import java.util.List;

public class ItemCategoryCaptionResources {

    private Long id;

    private String name;

    private String description;

    private Long categoryId;

    private Long statusId;

    private String code;

    private Long languageId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public static ItemCategoryCaptionResources toResources(ItemCategoryCaption caption) {
        ItemCategoryCaptionResources resources = new ItemCategoryCaptionResources();
        resources.setStatusId(caption.getStatus().getId());
        resources.setCategoryId(caption.getCategory().getId());
        resources.setDescription(caption.getDescription());
        resources.setLanguageId(caption.getLanguageId().getId());
        resources.setCode(caption.getLanguageId().getCode());
        resources.setId(caption.getId());
        resources.setName(caption.getName());
        return resources;
    }

    public static List<ItemCategoryCaptionResources> toResources(List<ItemCategoryCaption> captionList) {
        List<ItemCategoryCaptionResources> resourcesList = new ArrayList<>();
        captionList.forEach(caption -> {
            ItemCategoryCaptionResources resources = toResources(caption);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public ItemCategoryCaption toItemCategoryCaption() {
        ItemCategoryCaption caption = new ItemCategoryCaption();

        if (categoryId != null) {
            ItemCategory category = new ItemCategory();
            category.setId(this.categoryId);
            caption.setCategory(category);
        }

        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            caption.setStatus(status);
        }
        caption.setDescription(this.description);
        caption.setId(this.id);
        caption.setName(this.name);
        return caption;
    }
}
