package com.dc.Promotion.resources;

import com.dc.Promotion.entities.CustomerPoint;

import java.util.ArrayList;
import java.util.List;

public class CustomerPointResources {

    private Long id;

    private Long custId;

    private Long custType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustId() {
        return custId;
    }

    public void setCustId(Long custId) {
        this.custId = custId;
    }

    public Long getCustType() {
        return custType;
    }

    public void setCustType(Long custType) {
        this.custType = custType;
    }

    public static CustomerPointResources toResources(CustomerPoint customerPoint)
    {
        CustomerPointResources resources=new CustomerPointResources();
        resources.setCustId(customerPoint.getCustId());
        resources.setCustType(customerPoint.getCustType());
        resources.setId(customerPoint.getId());
        return resources;
    }

    public static List<CustomerPointResources> toResources(List<CustomerPoint> customerPointList)
    {
        List<CustomerPointResources> resourcesList=new ArrayList<>();
        customerPointList.forEach(customerPoint -> {
            CustomerPointResources resources=toResources(customerPoint);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public CustomerPoint toCustomerPoint()
    {
        CustomerPoint customerPoint=new CustomerPoint();
        customerPoint.setCustId(this.custId);
        customerPoint.setCustType(this.custType);
        customerPoint.setId(this.id);
        return customerPoint;
    }
}
