package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CustomerWatchListResources {
    private Long id;

    private LocalDateTime createDate;

    private Long customerAccountId;

    private Long promoItemId;

    private Long statusId;

    private OperationLanguage operationLanguage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getCustomerAccountId() {
        return customerAccountId;
    }

    public void setCustomerAccountId(Long customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    public Long getPromoItemId() {
        return promoItemId;
    }

    public void setPromoItemId(Long promoItemId) {
        this.promoItemId = promoItemId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public static CustomerWatchListResources toResources(CustomerWatchList watchList) {
        CustomerWatchListResources resources = new CustomerWatchListResources();
        resources.setCreateDate(watchList.getCreateDate());
        resources.setCustomerAccountId(watchList.getCustomerAccount().getId());
        resources.setId(watchList.getId());
        resources.setPromoItemId(watchList.getPromoItem().getId());
        resources.setStatusId(watchList.getStatus().getId());
        return resources;
    }

    public static List<CustomerWatchListResources> toResources(List<CustomerWatchList> watchListList) {
        List<CustomerWatchListResources> resourcesList = new ArrayList<>();
        watchListList.forEach(watchList -> {
            CustomerWatchListResources resources = toResources(watchList);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public CustomerWatchList toCustomerWatchList() {
        CustomerWatchList watchList = new CustomerWatchList();
        watchList.setCreateDate(this.createDate);

        watchList.setId(this.id);
        if (promoItemId != null) {
            PromoItem promoItem = new PromoItem();
            promoItem.setId(this.promoItemId);
            watchList.setPromoItem(promoItem);
        }

        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            watchList.setStatus(status);
        }

        if (customerAccountId != null) {
            CustomerAccount account = new CustomerAccount();
            account.setId(this.customerAccountId);
            watchList.setCustomerAccount(account);
        }

        return watchList;

    }
}
