package com.dc.Promotion.resources;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.District;
import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class DistrictResources {

    private Long id;

    private String code;

    private Long statusId;

    private Long cityId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public static DistrictResources toResources(District district)
    {
        DistrictResources resources=new DistrictResources();
        resources.setCode(district.getCode());
        resources.setCityId(district.getCity().getId());
        resources.setId(district.getId());
        resources.setStatusId(district.getStatus().getId());
        return resources;
    }

    public static List<DistrictResources> toResources(List<District> districtList)
    {
        List<DistrictResources> resourcesList=new ArrayList<>();
        districtList.forEach(district -> {
            DistrictResources resources=toResources(district);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public District toDistrict()
    {
        District district=new District();
        district.setId(this.id);
        district.setCode(this.code);
        if(cityId!=null)
        {
            City city=new City();
            city.setId(this.cityId);
            district.setCity(city);
        }

        if(statusId!=null)
        {
            Status status=new Status();
            status.setId(this.statusId);
            district.setStatus(status);
        }
        return district;

    }

}
