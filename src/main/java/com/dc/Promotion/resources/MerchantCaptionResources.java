package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.service.MerchantService;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class MerchantCaptionResources {
    private Long id;
    private String name;
    private String description;
    private Long merchantId;
    private Long languageId;
    private String merchantImg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getMerchantImg() {
        return merchantImg;
    }

    public void setMerchantImg(String merchantImg) {
        this.merchantImg = merchantImg;
    }

    public static MerchantCaptionResources toResources(MerchantCaption merchantCaption) {
        MerchantCaptionResources merchantCaptionResources = new MerchantCaptionResources();
        merchantCaptionResources.setId(merchantCaption.getId());
        merchantCaptionResources.setName(merchantCaption.getName());
        merchantCaptionResources.setDescription(merchantCaption.getDescription());
        merchantCaptionResources.setMerchantId(merchantCaption.getMerchant().getId());
        merchantCaptionResources.setLanguageId(merchantCaption.getOperationLanguage().getId());
        return merchantCaptionResources;
    }

    public static List<MerchantCaptionResources> toResources(List<MerchantCaption> merchantCaption) {
        List<MerchantCaptionResources> resources = new ArrayList<>();
        merchantCaption.forEach(merchantCaption1 -> {
            MerchantCaptionResources captionResources = toResources(merchantCaption1);
            resources.add(captionResources);
        });
        return resources;

    }

    public MerchantCaption toMerchantCaption() {
        MerchantCaption merchantCaption = new MerchantCaption();
        merchantCaption.setId(this.id);
        merchantCaption.setDescription(this.description);
        merchantCaption.setName(this.name);
        if (languageId == null) {

            OperationLanguage language = new OperationLanguage();
            language.setId(languageId);
            merchantCaption.setOperationLanguage(language);
        }
        if (merchantId == null) {

            Merchant merchant = new Merchant();
            merchant.setId(merchantId);
            merchantCaption.setMerchant(merchant);
        }
        return merchantCaption;
    }

    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        Merchant merchant=applicationContext.getBean(MerchantService.class).findById(this.merchantId);
        this.setMerchantImg(merchant.getImage());

    }

}

