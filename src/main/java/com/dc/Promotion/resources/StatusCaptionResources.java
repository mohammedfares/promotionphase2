package com.dc.Promotion.resources;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.entities.StatusCaption;

import java.util.ArrayList;
import java.util.List;

public class StatusCaptionResources {

    private Long id;

    private String name;

    private String description;

    private Long statusId;

    private Long languageId;

    private String code;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static StatusCaptionResources toResources(StatusCaption caption)
    {
        StatusCaptionResources resources=new StatusCaptionResources();
        resources.setDescription(caption.getDescription());
        resources.setId(caption.getId());
        resources.setLanguageId(caption.getLanguage().getId());
        resources.setName(caption.getName());
        resources.setStatusId(caption.getStatus().getId());
        resources.setCode(caption.getStatus().getCode());
        return resources;
    }

    public static List<StatusCaptionResources> toResources(List<StatusCaption> captionList)
    {
        List<StatusCaptionResources> resourcesList=new ArrayList<>();
        captionList.forEach(caption -> {
            StatusCaptionResources resources=toResources(caption);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public StatusCaption toStatusCaption()
    {
        StatusCaption caption=new StatusCaption();
        caption.setDescription(this.description);
        caption.setId(this.id);
        caption.setName(this.name);
        if(languageId!=null)
        {
            OperationLanguage language=new OperationLanguage();
            language.setId(this.languageId);
            caption.setLanguage(language);
        }

        if(statusId!=null)
        {
            Status status=new Status();
            status.setId(this.statusId);
            caption.setStatus(status);
        }
        return caption;
    }
}
