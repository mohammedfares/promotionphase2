package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.IndustryCaptionService;
import com.dc.Promotion.service.MerchantRegistrationRequestService;
import com.dc.Promotion.service.StatusCaptionService;
import com.dc.Promotion.service.StatusService;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MerchantRegistrationRequestResources {
    private Long id;
    private String email;
    private String username;
    private String password;
    private String mobile;
    private LocalDateTime createDate;
    private LocalDateTime expiryDate;
    private String crn;
    private Long statusId;
    private String statusCaption;
    private String confirmPassword;
    private String merchantName;
    private List<Long> industryList;
    private List<IndustryCaptionResources> industryCaptionResources;
    private String countryCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCrn() {
        return crn;
    }

    public void setCrn(String crn) {
        this.crn = crn;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public List<Long> getIndustryList() {
        return industryList;
    }

    public void setIndustryList(List<Long> industryList) {
        this.industryList = industryList;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getStatusCaption() {
        return statusCaption;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setStatusCaption(String statusCaption) {
        this.statusCaption = statusCaption;
    }

    public List<IndustryCaptionResources> getIndustryCaptionResources() {
        return industryCaptionResources;
    }

    public void setIndustryCaptionResources(List<IndustryCaptionResources> industryCaptionResources) {
        this.industryCaptionResources = industryCaptionResources;
    }

    public static MerchantRegistrationRequestResources toResources(MerchantRegistrationRequest registrationRequest) {

        MerchantRegistrationRequestResources resources = new MerchantRegistrationRequestResources();
        resources.setId(registrationRequest.getId());
        resources.setEmail(registrationRequest.getEmail());
        resources.setMobile(registrationRequest.getMobile());
        resources.setCreateDate(registrationRequest.getCreateDate());
        resources.setExpiryDate(registrationRequest.getExpiryDate());
        resources.setCrn(registrationRequest.getCrn());
        resources.setPassword(registrationRequest.getPassword());
        resources.setStatusId(registrationRequest.getStatus().getId());
        resources.setMerchantName(registrationRequest.getMerchantName());
        return resources;
    }

    public static List<MerchantRegistrationRequestResources> toResources(List<MerchantRegistrationRequest> registrationRequest) {

        List<MerchantRegistrationRequestResources> resources = new ArrayList<>();
        registrationRequest.forEach(registrationRequest1 -> {
            MerchantRegistrationRequestResources resources1 = toResources(registrationRequest1);
            resources.add(resources1);

        });
        return resources;

    }

    public MerchantRegistrationRequest toMerchantRegistrationRequest() {

        MerchantRegistrationRequest registrationRequest = new MerchantRegistrationRequest();
        registrationRequest.setId(this.id);
        registrationRequest.setEmail(this.email);
        registrationRequest.setPassword(this.password);
        registrationRequest.setCrn(this.crn);
        registrationRequest.setCreateDate(this.createDate);
        registrationRequest.setExpiryDate(this.expiryDate);
        registrationRequest.setMobile(this.mobile);
        registrationRequest.setMerchantName(this.merchantName);
        registrationRequest.setUsername(this.username);
        if (statusId == null) {

            Status status = new Status();
            status.setId(statusId);
            registrationRequest.setStatus(status);
        }
        return registrationRequest;
    }

    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        Status status = applicationContext.getBean(StatusService.class).findById(this.statusId);

        StatusCaption statusCaption = applicationContext.getBean(StatusCaptionService.class).findByStatusAndLanguage(status, operationLanguage);

        this.setStatusCaption(statusCaption.getName());
        MerchantRegistrationRequest registrationRequest = applicationContext.getBean(MerchantRegistrationRequestService.class).findById(this.id);

        List<IndustryCaption> industryCaptionList = applicationContext.getBean(IndustryCaptionService.class).findByMRR(operationLanguage, registrationRequest);
        industryCaptionResources = IndustryCaptionResources.toResources(industryCaptionList);


    }


}
