//package com.dc.Promotion.resources;
//
//import com.dc.Promotion.entities.Nationality;
//import com.dc.Promotion.entities.NationalityCaption;
//import com.dc.Promotion.entities.OperationLanguage;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class NationalityCaptionResources {
//
//    private Long id;
//    private String name;
//    private String description;
//    private Long nationalityId;
//    private Long operationLanguage;
//
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public Long getNationalityId() {
//        return nationalityId;
//    }
//
//    public void setNationalityId(Long nationalityId) {
//        this.nationalityId = nationalityId;
//    }
//
//    public Long getOperationLanguage() {
//        return operationLanguage;
//    }
//
//    public void setOperationLanguage(Long operationLanguage) {
//        this.operationLanguage = operationLanguage;
//    }
//
//    public static NationalityCaptionResources toResources(NationalityCaption caption) {
//        NationalityCaptionResources resources = new NationalityCaptionResources();
//        resources.setId(caption.getId());
//        resources.setName(caption.getName());
//        resources.setDescription(caption.getDescription());
//        resources.setNationalityId(caption.getCountry().getId());
//        resources.setOperationLanguage(caption.getOperationLanguage().getId());
//        return resources;
//    }
//
//    public static List<NationalityCaptionResources> toResources(List<NationalityCaption> caption) {
//        List<NationalityCaptionResources> resources = new ArrayList<>();
//        caption.forEach(caption1 -> {
//
//            NationalityCaptionResources nationalityCaptionResource = toResources(caption1);
//            resources.add(nationalityCaptionResource);
//        });
//        return resources;
//
//    }
//
//public NationalityCaption toNationalityCaption(){
//
//    NationalityCaption nationalityCaption=new NationalityCaption();
//
//    nationalityCaption.setId(this.id);
//    nationalityCaption.setName(this.name);
//   nationalityCaption.setDescription(this.description);
//   if(operationLanguage==null){
//
//       OperationLanguage operationLanguage=new OperationLanguage();
//       operationLanguage.setId(this.operationLanguage);
//       nationalityCaption.setOperationLanguage(operationLanguage);
//   }
//        if(nationalityId==null){
//
//            Nationality nationality=new Nationality();
//           nationality.setId(nationalityId);
//           nationalityCaption.setCountry(nationality);
//
//        }
//        return  nationalityCaption;
//
//}
//}
