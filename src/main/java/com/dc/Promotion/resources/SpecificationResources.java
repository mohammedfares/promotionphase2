package com.dc.Promotion.resources;


import com.dc.Promotion.entities.ItemSpecification;
import com.dc.Promotion.entities.Specification;
import com.dc.Promotion.entities.Status;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class SpecificationResources {

    private Long id;

    private String key;

    private Long statusId;

    private String value;

    private Long languageId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static SpecificationResources toResources(Specification specification) {

        //   resources.setCode(specification.getCode());
        SpecificationResources resources = new SpecificationResources();
        resources.setKey(specification.getKey());
        resources.setId(specification.getId());
        resources.setStatusId(specification.getStatus().getId());
        return resources;
    }

    public static List<SpecificationResources> toResources(List<Specification> specificationList) {
        List<SpecificationResources> resourcesList = new ArrayList<>();
        specificationList.forEach(itemSpecification -> {
            SpecificationResources resources = toResources(itemSpecification);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public static List<SpecificationResources> toItemSpecificationResources(List<ItemSpecification> specificationList) {
        List<SpecificationResources> resourcesList = new ArrayList<>();
        specificationList.forEach(itemSpecification -> {
            SpecificationResources resources = toResources(itemSpecification.getSpecification());
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public Specification toItemSpecification() {
        Specification specification = new Specification();
        specification.setId(this.id);
        // specification.setCode(this.code);
        specification.setKey(this.key);
        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            specification.setStatus(status);
        }
        return specification;
    }
}
