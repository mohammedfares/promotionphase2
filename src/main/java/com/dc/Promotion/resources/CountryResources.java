package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class CountryResources {


    private Long id;
    private String code;
    private String key;
    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static CountryResources toResources(Country country)
    {
        CountryResources resources=new CountryResources();
        resources.setKey(country.getKey());
        resources.setId(country.getId());
        resources.setCode(country.getCode());
//        resources.setStatusId(country.getStatus().getId());
        return resources;
    }

    public static List<CountryResources> toResources(List<Country> countryList){
        List<CountryResources> resourcesList=new ArrayList<>();
        countryList.forEach(country -> {
            CountryResources resources=toResources(country);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public Country toCountry()
    {
        Country country=new Country();
        country.setId(this.id);
        country.setKey(this.key);
        country.setCode(this.code);
        if(statusId!=null)
        {
            Status  status=new Status();
            status.setId(this.statusId);
            country.setStatus(status);

        }
        return country;
    }



}
