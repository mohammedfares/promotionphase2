package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.*;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MediaWishListResources {

    private Long id;
    private String aliasName;
    private String tags;
    private LocalDateTime createDate;
    private Long categoryId;
    private Long mediaAccountId;
    private Long merchantId;
    private Long statusId;
    private String merchantName;
    private String categoryName;

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getMediaAccountId() {
        return mediaAccountId;
    }

    public void setMediaAccountId(Long mediaAccountId) {
        this.mediaAccountId = mediaAccountId;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static MediaWishListResources toResources(MediaWishList wishList)
    {
        MediaWishListResources resources=new MediaWishListResources();
        resources.setCreateDate(wishList.getCreateDate());
        resources.setMediaAccountId(wishList.getMediaAccount().getId());
        resources.setId(wishList.getId());
        resources.setStatusId(wishList.getStatus().getId());
        resources.setTags(wishList.getTags());
        resources.setMediaAccountId(wishList.getMediaAccount().getId());
        resources.setCategoryId(wishList.getCategory().getId());
        resources.setAliasName(wishList.getAliasName());
        resources.setMerchantId(wishList.getMerchant().getId());
        return resources;
    }

    public static List<MediaWishListResources> toResources(List<MediaWishList> wishListList)
    {
        List<MediaWishListResources>resourcesList =new ArrayList<>();
        wishListList.forEach(wishList -> {
            MediaWishListResources resources=toResources(wishList);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public MediaWishList toMediaWishList()
    {
        MediaWishList mediaWishList=new MediaWishList();
        mediaWishList.setCreateDate(this.createDate);
        mediaWishList.setId(this.id);
        mediaWishList.setTags(this.tags);
        if(mediaAccountId!=null)
        {
            MediaAccount account=new MediaAccount();
            account.setId(this.mediaAccountId);
            mediaWishList.setMediaAccount(account);
        }

        if(statusId!=null)
        {
            Status status=new Status();
            status.setId(this.statusId);
            mediaWishList.setStatus(status);
        }
        if (merchantId != null) {
            Merchant merchant = new Merchant();
            merchant.setId(this.merchantId);
            mediaWishList.setMerchant(merchant);
        }
        if (categoryId != null) {
            ItemCategory itemCategory = new ItemCategory();
            itemCategory.setId(this.categoryId);
            mediaWishList.setCategory(itemCategory);
        }
        return mediaWishList;


    }


    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        if(this.merchantId!=null && merchantId!=0)
        {
            Merchant merchant= applicationContext.getBean(MerchantService.class).findById(this.merchantId);
            MerchantCaption merchantCaption= applicationContext.getBean(MerchantCaptionService.class).findByMerchant(merchant,operationLanguage);
            if(merchantCaption==null){
                OperationLanguage  languageEn=applicationContext.getBean(OperationLanguageService.class).findByCode("en");
                merchantCaption= applicationContext.getBean(MerchantCaptionService.class).findByMerchant(merchant,languageEn);
            }
            this.merchantName=merchantCaption.getName();
        }
        if(this.categoryId!=null && this.categoryId!=0)
        {
            ItemCategory category=applicationContext.getBean(ItemCategoryService.class).findById(this.categoryId);
            ItemCategoryCaption categoryCaption=applicationContext.getBean(ItemCategoryCaptionService.class).findByCategoryAndLanguage(category,operationLanguage);

            if(categoryCaption==null){
                OperationLanguage  languageEn=applicationContext.getBean(OperationLanguageService.class).findByCode("en");
                categoryCaption=applicationContext.getBean(ItemCategoryCaptionService.class).findByCategoryAndLanguage(category,languageEn);
            }

            this.categoryName=categoryCaption.getName();

        }

    }
}
