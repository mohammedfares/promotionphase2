package com.dc.Promotion.resources;
import com.dc.Promotion.entities.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class MediaWatchListResources {
    private Long id;

    private LocalDateTime createDate;

    private Long mediaAccountId;

    private Long promoItemId;

    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getMediaAccountId() {
        return mediaAccountId;
    }

    public void setMediaAccountId(Long mediaAccountId) {
        this.mediaAccountId = mediaAccountId;
    }

    public Long getPromoItemId() {
        return promoItemId;
    }

    public void setPromoItemId(Long promoItemId) {
        this.promoItemId = promoItemId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static MediaWatchListResources toResources(MediaWatchList watchList)
    {
        MediaWatchListResources resources=new MediaWatchListResources();
        resources.setCreateDate(watchList.getCreateDate());
        resources.setMediaAccountId(watchList.getMediaAccount().getId());
        resources.setId(watchList.getId());
        resources.setPromoItemId(watchList.getPromoItem().getId());
        resources.setStatusId(watchList.getStatus().getId());
        return resources;
    }

    public static List<MediaWatchListResources> toResources(List<MediaWatchList> watchListList)
    {
        List<MediaWatchListResources> resourcesList=new ArrayList<>();
        watchListList.forEach(watchList -> {
            MediaWatchListResources resources=toResources(watchList);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public  MediaWatchList toMediaWatchList()
    {
        MediaWatchList watchList=new MediaWatchList();
        watchList.setCreateDate(this.createDate);

        watchList.setId(this.id);
        if(promoItemId!=null)
        {
            PromoItem promoItem=new PromoItem();
            promoItem.setId(this.promoItemId);
            watchList.setPromoItem(promoItem);
        }

        if(statusId!=null)
        {
            Status status=new Status();
            status.setId(this.statusId);
            watchList.setStatus(status);
        }

        if(mediaAccountId!=null)
        {
            MediaAccount account=new MediaAccount();
            account.setId(this.mediaAccountId);
            watchList.setMediaAccount(account);
        }

        return watchList;

    }
}
