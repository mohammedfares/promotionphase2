package com.dc.Promotion.resources;

import com.dc.Promotion.entities.MediaType;
import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class MediaTypeResources {
    private Long id;
    private String code;
    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static MediaTypeResources toResource(MediaType mediaType) {
        MediaTypeResources mediaTypeResources = new MediaTypeResources();
        mediaTypeResources.setId(mediaType.getId());
        mediaTypeResources.setCode(mediaType.getCode());
        mediaTypeResources.setStatusId(mediaType.getStatus().getId());
        return mediaTypeResources;

    }

    public static List<MediaTypeResources> toResource(List<MediaType> mediaType) {
        List<MediaTypeResources> mediaTypeResources = new ArrayList<>();
        mediaType.forEach(mediaType1 -> {
            MediaTypeResources resources = toResource(mediaType1);
            mediaTypeResources.add(resources);

        });

        return mediaTypeResources;


    }

    public MediaType toMediaType() {
        MediaType type = new MediaType();

        type.setId(this.id);
        type.setCode(this.code);
        if (statusId == null) {
            Status status = new Status();
            status.setId(this.statusId);
            type.setStatus(status);
        }

        return type;
    }

}
