package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.security.SECCriptoRsa;
import com.dc.Promotion.service.*;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MerchantUserResources {
    private Long id;
    private String userName;
    private String email;
    private String password;
    private String mobile;
    private LocalDateTime createDate;
    private LocalDateTime expireDate;
    private String crn;
    private Long merchantId;
    private Long merchantUserType;
    private Long statusId;
    private String statusCaption;
    private String countryCode;
    private String newPassword;
    private String confirmPassword;
    private String token;
    private String firstName;
    private String lastName;
    private String loginUsername;
    private String statusCode;
    private String merchantCaption;
    private String merchantLogo;
    private String merchantUserTypeCaption;

    public String getMerchantUserTypeCaption() {
        return merchantUserTypeCaption;
    }

    public void setMerchantUserTypeCaption(String merchantUserTypeCaption) {
        this.merchantUserTypeCaption = merchantUserTypeCaption;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public String getCrn() {
        return crn;
    }

    public void setCrn(String crn) {
        this.crn = crn;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getMerchantUserType() {
        return merchantUserType;
    }

    public void setMerchantUserType(Long merchantUserType) {
        this.merchantUserType = merchantUserType;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getLoginUsername() {
        return loginUsername;
    }

    public void setLoginUsername(String loginUsername) {
        this.loginUsername = loginUsername;
    }

    public String getStatusCaption() {
        return statusCaption;
    }

    public void setStatusCaption(String statusCaption) {
        this.statusCaption = statusCaption;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMerchantCaption() {
        return merchantCaption;
    }

    public void setMerchantCaption(String merchantCaption) {
        this.merchantCaption = merchantCaption;
    }

    public String getMerchantLogo() {
        return merchantLogo;
    }

    public void setMerchantLogo(String merchantLogo) {
        this.merchantLogo = merchantLogo;
    }

    public static MerchantUserResources toResources(MerchantUser merchantUser) {

        MerchantUserResources resources = new MerchantUserResources();
        resources.setId(merchantUser.getId());
        resources.setMobile(merchantUser.getMobile());
        resources.setFirstName(merchantUser.getFirstName());
        resources.setLastName(merchantUser.getLastName());
        resources.setEmail(merchantUser.getEmail());
        resources.setPassword(SECCriptoRsa.decrypt(merchantUser.getPassword()));
        resources.setCrn(merchantUser.getCrn());
        resources.setCreateDate(merchantUser.getCreateDate());
        resources.setExpireDate(merchantUser.getExpireDate());
        resources.setMerchantId(merchantUser.getMerchant().getId());
        resources.setStatusId(merchantUser.getStatus().getId());
        resources.setMerchantUserType(merchantUser.getMerchantUserType().getId());
        resources.setUserName(merchantUser.getUserName());
        return resources;

    }

    public static List<MerchantUserResources> toResources(List<MerchantUser> merchantUser) {
        List<MerchantUserResources> resources = new ArrayList<>();
        merchantUser.forEach(merchantUser1 -> {
            MerchantUserResources resources1 = toResources(merchantUser1);
            resources.add(resources1);

        });
        return resources;

    }


    public MerchantUser toMerchantUser() {


        MerchantUser merchantUser = new MerchantUser();
        merchantUser.setId(this.id);
        merchantUser.setFirstName(this.firstName);
        merchantUser.setLastName(this.lastName);
        merchantUser.setUserName(this.userName);
        merchantUser.setEmail(this.email);
        merchantUser.setMobile(this.mobile);
        merchantUser.setPassword(this.password);
        merchantUser.setCreateDate(this.createDate);
        merchantUser.setExpireDate(this.expireDate);
        merchantUser.setCrn(this.crn);
        if (merchantId == null) {
            Merchant merchant = new Merchant();
            merchant.setId(merchantId);
            merchantUser.setMerchant(merchant);
        }
        if (statusId == null) {
            Status status = new Status();
            status.setId(statusId);
            merchantUser.setStatus(status);
        }
        if (merchantUserType == null) {
            MerchantUserType merchantUserType = new MerchantUserType();
            merchantUserType.setId(this.merchantUserType);
            merchantUser.setMerchantUserType(merchantUserType);
        }
        return merchantUser;
    }

    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        Status status = applicationContext.getBean(StatusService.class).findById(this.statusId);

        StatusCaption statusCaption = applicationContext.getBean(StatusCaptionService.class).findByStatusAndLanguage(status, operationLanguage);

        this.setStatusCaption(statusCaption.getName());

        this.setStatusCode(status.getCode());

        Merchant merchant = applicationContext.getBean(MerchantService.class).findById(this.merchantId);
        MerchantCaption merchantCaption = applicationContext.getBean(MerchantCaptionService.class).findByMerchant(merchant, operationLanguage);

        if(merchantCaption==null)
        {
            merchantCaption=applicationContext.getBean(MerchantCaptionService.class).findByMerchantId(this.merchantId).get(0);
        }
        this.setMerchantCaption(merchantCaption.getName());

        this.setMerchantLogo(merchant.getImage());

        MerchantUserType merchantUserType=applicationContext.getBean(MerchantUserTypeService.class).findById(this.merchantUserType);
        MerchantUserTypeCaption merchantUserTypeCaption=
                applicationContext.getBean(MerchantUserTypeCaptionService.class).findByUserTypeAndLanguage(merchantUserType,operationLanguage);
        this.setMerchantUserTypeCaption(merchantUserTypeCaption.getName());

    }
}
