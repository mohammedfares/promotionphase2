package com.dc.Promotion.resources;

import com.dc.Promotion.entities.MediaAccount;
import com.dc.Promotion.entities.MediaItemView;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.Status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MediaItemViewResources {

    private Long id;
    private LocalDateTime createDate;
    private Long mediaAccountId;
    private Long promoItemId;
    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getMediaAccountId() {
        return mediaAccountId;
    }

    public void setMediaAccountId(Long mediaAccountId) {
        this.mediaAccountId = mediaAccountId;
    }

    public Long getPromoItemId() {
        return promoItemId;
    }

    public void setPromoItemId(Long promoItemId) {
        this.promoItemId = promoItemId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }


    public static MediaItemViewResources toMediaItemViewResources(MediaItemView mediaItemView){

        MediaItemViewResources resources=new MediaItemViewResources();
        resources.setId( mediaItemView.getId());
        resources.setCreateDate(mediaItemView.getCreateDate());
        resources.setStatusId(mediaItemView.getStatus().getId());
        resources.setMediaAccountId(mediaItemView.getMediaAccount().getId());
        resources.setPromoItemId(mediaItemView.getPromoItem().getId());
        return  resources;

    }
    public static List<MediaItemViewResources> toMediaItemViewResources(List<MediaItemView> mediaItemView){
        List<MediaItemViewResources> resources=new ArrayList<>();
        mediaItemView.forEach(mediaItemViewResources -> {
            MediaItemViewResources viewResources=toMediaItemViewResources(mediaItemViewResources);
            resources.add(viewResources);

        });


        return  resources;


    }


    public  MediaItemView toMediaItemView(){

        MediaItemView mediaItemView=new MediaItemView();
        mediaItemView.setId(this.id);
        mediaItemView.setCreateDate(this.createDate);
        if(statusId==null) {
            Status status = new Status();
            status.setId(statusId);
            mediaItemView.setStatus(status);
        }
        if(mediaAccountId==null){

            MediaAccount mediaAccount=new MediaAccount();
            mediaAccount.setId(mediaAccountId);
            mediaItemView.setMediaAccount(mediaAccount);
        }
        if(promoItemId==null){

            PromoItem promoItem=new PromoItem();
            promoItem.setId(promoItemId);
            mediaItemView.setPromoItem(promoItem);
        }
        return  mediaItemView;


    }

}
