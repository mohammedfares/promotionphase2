package com.dc.Promotion.resources;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.SystemMessage;
import com.dc.Promotion.entities.SystemMessageCaption;

import java.util.ArrayList;
import java.util.List;

public class SystemMessageCaptionResources {

    private Long id;

    private String description;

    private String text;

    private Long messageId;

    private Long languageId;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }


    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public static SystemMessageCaptionResources toResources(SystemMessageCaption caption)
    {
        SystemMessageCaptionResources resources=new SystemMessageCaptionResources();
        resources.setDescription(caption.getDescription());
        resources.setId(caption.getId());
        resources.setMessageId(caption.getMessage().getId());
        resources.setText(caption.getText());
        resources.setLanguageId(caption.getLanguage().getId());
    return resources;
    }

    public static List<SystemMessageCaptionResources> toResources(List<SystemMessageCaption> captionList )
    {
        List<SystemMessageCaptionResources> resourcesList=new ArrayList<>();
        captionList.forEach(caption -> {
            SystemMessageCaptionResources resources=toResources(caption);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public SystemMessageCaption toSystemMessageCaption()
    {
        SystemMessageCaption caption=new SystemMessageCaption();
        caption.setId(this.id);
        caption.setDescription(this.description);
        caption.setText(this.text);

        if(messageId!=null)
        {
            SystemMessage message=new SystemMessage();
            message.setId(this.messageId);
            caption.setMessage(message);
        }
        if(languageId!=null)
        {
            OperationLanguage language=new OperationLanguage();
            language.setId(this.languageId);
            caption.setLanguage(language);
        }
        return caption;
    }
}
