package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.CountryCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.ArrayList;
import java.util.List;

public class CountryCaptionResources {


    private Long id;

    private String countryName;

    private String description;

    private Long countryId;

    private Long languageId;

    private String nationalityName;

    private String countryCode;

    private String countrykey;

    private String countryFlag;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountrykey() {
        return countrykey;
    }

    public void setCountrykey(String countrykey) {
        this.countrykey = countrykey;
    }

    public String getCountryFlag() {
        return countryFlag;
    }

    public void setCountryFlag(String countryFlag) {
        this.countryFlag = countryFlag;
    }

    public static CountryCaptionResources toResources(CountryCaption caption) {

        CountryCaptionResources resources = new CountryCaptionResources();
        resources.setCountryId(caption.getCountry().getId());
        resources.setDescription(caption.getDescription());
        resources.setId(caption.getId());
        resources.setLanguageId(caption.getLanguage().getId());
        resources.setCountryName(caption.getCountryName());
        resources.setNationalityName(caption.getNationalityName());
        resources.setCountryCode(caption.getCountry().getCode());
        resources.setCountryFlag(caption.getCountry().getFlag());
        resources.setCountrykey(caption.getCountry().getKey());
        return resources;

    }

    public static List<CountryCaptionResources> toResources(List<CountryCaption> captionList) {
        List<CountryCaptionResources> resourcesList = new ArrayList<>();
        captionList.forEach(caption -> {
            CountryCaptionResources resources = toResources(caption);
            resourcesList.add(resources);
        });
        return resourcesList;
    }


    public CountryCaption toCountryCaption() {
        CountryCaption caption = new CountryCaption();

        caption.setCountryName(this.countryName);
        caption.setDescription(this.description);
        caption.setNationalityName(this.nationalityName);
        caption.setId(this.id);
        if (languageId != null) {
            OperationLanguage language = new OperationLanguage();
            language.setId(this.languageId);
            caption.setLanguage(language);
        }

        if (countryId != null) {
            Country country = new Country();
            country.setId(this.countryId);
            caption.setCountry(country);
        }

        return caption;
    }
}
