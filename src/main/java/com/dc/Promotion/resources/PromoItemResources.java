package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.*;
import com.dc.Promotion.utils.Utils;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PromoItemResources {
    private Long id;
    private LocalDateTime createDate;
    private LocalDateTime expiryDate;
    private String coverPhoto;
    private String oldPrice;
    private String newPrice;
    private Long viewNo;
    private Long statusId;
    private String statusCaption;
    private Long promoId;
    private Long category;
    private List<PromoItemImageResources> imageList;
    private List<PromoItemCaptionResources> captionList;
    private List<SpecificationResources> specificationList;
    private List<ItemSpecificationResource> itemSpecificationResourceList;
    private String name;
    private String description;
    private Integer watchListFlag;
    private String merchantImage;
    private List<MerchantBranchResources> promoBranchList;
    private Double distance;

    public String getMerchantImage() {
        return merchantImage;
    }

    public void setMerchantImage(String merchantImage) {
        this.merchantImage = merchantImage;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public Long getViewNo() {
        return viewNo;
    }

    public void setViewNo(Long viewNo) {
        this.viewNo = viewNo;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getPromoId() {
        return promoId;
    }

    public void setPromoId(Long promoId) {
        this.promoId = promoId;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    public List<PromoItemImageResources> getImageList() {
        return imageList;
    }

    public void setImageList(List<PromoItemImageResources> imageList) {
        this.imageList = imageList;
    }

    public List<PromoItemCaptionResources> getCaptionList() {
        return captionList;
    }

    public void setCaptionList(List<PromoItemCaptionResources> captionList) {
        this.captionList = captionList;
    }

    public List<SpecificationResources> getSpecificationList() {
        return specificationList;
    }

    public void setSpecificationList(List<SpecificationResources> specificationList) {
        this.specificationList = specificationList;
    }

    public String getStatusCaption() {
        return statusCaption;
    }

    public void setStatusCaption(String statusCaption) {
        this.statusCaption = statusCaption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ItemSpecificationResource> getItemSpecificationResourceList() {
        return itemSpecificationResourceList;
    }

    public void setItemSpecificationResourceList(List<ItemSpecificationResource> itemSpecificationResourceList) {
        this.itemSpecificationResourceList = itemSpecificationResourceList;
    }

    public Integer getWatchListFlag() {
        return watchListFlag;
    }

    public void setWatchListFlag(Integer watchListFlag) {
        this.watchListFlag = watchListFlag;
    }

    public List<MerchantBranchResources> getPromoBranchList() {
        return promoBranchList;
    }

    public void setPromoBranchList(List<MerchantBranchResources> promoBranchList) {
        this.promoBranchList = promoBranchList;
    }

    public static PromoItemResources toResources(PromoItem promoItem) {
        PromoItemResources resources = new PromoItemResources();
        resources.setId(promoItem.getId());
        resources.setCoverPhoto(promoItem.getCoverPhoto());
        resources.setNewPrice(promoItem.getNewPrice());
        resources.setExpiryDate(promoItem.getExpiryDate());
        resources.setOldPrice(promoItem.getOldPrice());
        resources.setCategory((promoItem.getCategory() != null) ? (promoItem.getCategory().getId()) : (null));
        resources.setStatusId(promoItem.getStatus().getId());
        resources.setViewNo(promoItem.getViewNo());
        resources.setPromoId(promoItem.getPromo().getId());
        resources.setCreateDate(promoItem.getCreateDate());
        resources.setImageList(PromoItemImageResources.toResources(promoItem.getImageList()));
        resources.setItemSpecificationResourceList(ItemSpecificationResource.toResources(promoItem.getItemSpecificationList()));
        resources.setDistance(Utils.roundDouble(promoItem.getDistance()));
        resources.setMerchantImage(promoItem.getPromo().getMerchant().getImage());
        return resources;


    }

    public static List<PromoItemResources> toResources(List<PromoItem> promoItem) {
        List<PromoItemResources> resources = new ArrayList<>();
        promoItem.forEach(promoItem1 -> {
            PromoItemResources itemResources = toResources(promoItem1);
            resources.add(itemResources);
        });
        return resources;
    }

    public PromoItem toPromoItem() {
        PromoItem promoItem = new PromoItem();
        promoItem.setId(this.id);
        promoItem.setCoverPhoto(this.coverPhoto);
        promoItem.setNewPrice(this.newPrice);
        promoItem.setOldPrice(this.oldPrice);
        promoItem.setCreateDate(this.createDate);
        promoItem.setExpiryDate(this.expiryDate);
        promoItem.setViewNo(this.viewNo);
        if (category != null) {
            ItemCategory itemCategory = new ItemCategory();
            itemCategory.setId(category);
            promoItem.setCategory(itemCategory);
        }
        if (promoId != null) {
            Promo promo = new Promo();
            promo.setId(promoId);
            promoItem.setPromo(promo);
        }
        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            promoItem.setStatus(status);
        }
        return promoItem;

    }

    public void initResourceCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage, CustomerAccount customerAccount, MediaAccount mediaAccount,Double longitudeBranch,Double latitudeBranch) {

        PromoItem promoItem = applicationContext.getBean(PromoItemService.class).findById(this.id);

        PromoItemCaption promoItemCaption = applicationContext.getBean(PromoItemCaptionService.class).find(promoItem, operationLanguage);

        if (promoItemCaption == null) {

            List<PromoItemCaption> promoItemCaptionList = applicationContext.getBean(PromoItemCaptionService.class).find(promoItem);

            if (promoItemCaptionList != null && !promoItemCaptionList.isEmpty()) {
                promoItemCaption = applicationContext.getBean(PromoItemCaptionService.class).find(promoItem).get(0);
            }
        }

        Status status = applicationContext.getBean(StatusService.class).findById(this.statusId);

        StatusCaption statusCaption = applicationContext.getBean(StatusCaptionService.class).findByStatusAndLanguage(status, operationLanguage);

        if (customerAccount != null) {
            CustomerWatchList customerWatchList = applicationContext.getBean(CustomerWatchListService.class).find(promoItem, customerAccount);

            if (customerWatchList != null) {
                this.setWatchListFlag(1);
            } else {
                this.setWatchListFlag(0);
            }
        }

        if (mediaAccount != null) {
            MediaWatchList mediaWatchList = applicationContext.getBean(MediaWatchListService.class).find(promoItem, mediaAccount);
            if (mediaWatchList != null) {
                this.setWatchListFlag(1);
            } else {
                this.setWatchListFlag(0);
            }
        }

        this.setStatusCaption(statusCaption != null ? statusCaption.getName() : null);
        this.setName(promoItemCaption != null ? promoItemCaption.getName() : null);
        this.setDescription(promoItemCaption != null ? promoItemCaption.getDescription() : null);
        this.setCaptionList(PromoItemCaptionResources.toResources(applicationContext.getBean(PromoItemCaptionService.class).find(promoItem)));
//        this.setSpecificationList(SpecificationResources.toResources());

        for (ItemSpecificationResource itemSpecificationCaptionResourceObject : this.itemSpecificationResourceList) {
            itemSpecificationCaptionResourceObject.initResourceCaption(applicationContext, operationLanguage);
        }

        List<MerchantBranchResources> branchList = MerchantBranchResources.toResource(
                applicationContext.getBean(MerchantBranchService.class).findAllByPromo(promoItem.getPromo()));

        for (MerchantBranchResources merchantBranchResources : branchList) {
            merchantBranchResources.initResoucreCaption(applicationContext, operationLanguage,longitudeBranch,latitudeBranch);
        }

        this.setPromoBranchList(branchList);
    }

}
