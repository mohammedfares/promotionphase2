package com.dc.Promotion.resources;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerPromoView;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.Status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CustomerPromoViewResources {

    private Long id;

    private LocalDateTime createDate;

    private Long customerAccountId;

    private Long promoId;

    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getCustomerAccountId() {
        return customerAccountId;
    }

    public void setCustomerAccountId(Long customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    public Long getPromoId() {
        return promoId;
    }

    public void setPromoId(Long promoId) {
        this.promoId = promoId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static CustomerPromoViewResources toResources(CustomerPromoView promoView)
    {
        CustomerPromoViewResources resources=new CustomerPromoViewResources();
        resources.setCreateDate(promoView.getCreateDate());
        resources.setCustomerAccountId(promoView.getCustomerAccount().getId());
        resources.setId(promoView.getId());
        resources.setPromoId(promoView.getPromo().getId());
        resources.setStatusId(promoView.getStatus().getId());
        return resources;
    }

    public static List<CustomerPromoViewResources> toResources(List<CustomerPromoView> promoViewList)
    {
        List<CustomerPromoViewResources> resourcesList=new ArrayList<>();
        promoViewList.forEach(customerPromoView -> {
            CustomerPromoViewResources resources=toResources(customerPromoView);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public  CustomerPromoView toCustomerPromoView()
    {
        CustomerPromoView promoView=new CustomerPromoView();

        promoView.setCreateDate(this.createDate);

        promoView.setId(this.id);
        if(statusId!=null)
        {
            Status status=new Status();
            status.setId(this.statusId);
            promoView.setStatus(status);
        }
        if(promoId!=null)
        {
            Promo promo=new Promo();
            promo.setId(this.promoId);
            promoView.setPromo(promo);
        }

        if(customerAccountId!=null)
        {
            CustomerAccount account=new CustomerAccount();
            account.setId(this.customerAccountId);
            promoView.setCustomerAccount(account);
        }
        return promoView;
    }
}
