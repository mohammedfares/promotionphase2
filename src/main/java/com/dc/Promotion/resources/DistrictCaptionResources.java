package com.dc.Promotion.resources;

import com.dc.Promotion.entities.District;
import com.dc.Promotion.entities.DistrictCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.util.ArrayList;
import java.util.List;

public class DistrictCaptionResources {

    private Long id;

    private String name;

    private String description;

    private Long districtId;

    private Long languageId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public static DistrictCaptionResources toResources(DistrictCaption caption)
    {
        DistrictCaptionResources resources=new DistrictCaptionResources();
        resources.setDescription(caption.getDescription());
        resources.setDistrictId(caption.getDistrict().getId());
        resources.setId(caption.getId());
        resources.setLanguageId(caption.getLanguage().getId());
        resources.setName(caption.getName());
        return resources;
    }

    public static List<DistrictCaptionResources> toResources(List<DistrictCaption> captionList)
    {
        List<DistrictCaptionResources> resourcesList=new ArrayList<>();
        captionList.forEach(caption -> {
            DistrictCaptionResources resources=toResources(caption);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public  DistrictCaption toDistrictCaption()
    {
        DistrictCaption caption=new DistrictCaption();
        caption.setDescription(this.description);
        caption.setName(this.name);
        caption.setId(this.id);
        if(languageId!=null)
        {
            OperationLanguage language=new OperationLanguage();
            language.setId(this.languageId);
            caption.setLanguage(language);
        }

        if(districtId!=null)
        {
            District district=new District();
            district.setId(this.districtId);
            caption.setDistrict(district);

        }

        return caption;


    }
}
