package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.service.MerchantCaptionService;
import com.dc.Promotion.service.MerchantService;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class MerchantResources {
    private Long id;
    private String image;
    private String name;
    private String description;
    private Long statusId;
    private Long merchantId;
    private String merchantImg;

    public String getMerchantImg() {
        return merchantImg;
    }

    public void setMerchantImg(String merchantImg) {
        this.merchantImg = merchantImg;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static MerchantResources toResource(Merchant merchant){

        MerchantResources resources=new MerchantResources();
        resources.setId(merchant.getId());
        resources.setImage(merchant.getImage());
        resources.setStatusId(merchant.getStatus().getId());
        resources.setMerchantId(merchant.getId());
        resources.setMerchantImg(merchant.getImage());
        return resources;
    }
    public static List<MerchantResources> toResource(List<Merchant> merchant){

       List<MerchantResources> resources=new ArrayList<>();
       merchant.forEach(merchant1 -> {
           MerchantResources merchantResources=toResource(merchant1);
           resources.add(merchantResources);
       });
        return  resources;

    }
    public Merchant toMerchant(){
        Merchant merchant=new Merchant();
        merchant.setId(this.id);
        merchant.setImage(this.image);
        if(statusId==null){
            Status status=new Status();
            status.setId(statusId);
            merchant.setStatus(status);

        }
        return  merchant;
    }

    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {
        Merchant merchant=applicationContext.getBean(MerchantService.class).findById(this.id);
        MerchantCaption merchantCaption=applicationContext.getBean(MerchantCaptionService.class).findByMerchant(merchant,operationLanguage);
        if(merchantCaption==null)
        {
            merchantCaption=applicationContext.getBean(MerchantCaptionService.class).findByMerchantId(this.id).get(0);
        }
        this.setName(merchantCaption.getName());
        this.setDescription(merchantCaption.getDescription());
    }
}
