package com.dc.Promotion.resources;

import com.dc.Promotion.entities.ItemCategory;
import com.dc.Promotion.entities.ItemCategoryCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;
import com.dc.Promotion.service.ItemCategoryCaptionService;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class ItemCategoryResources {

    private Long id;

    private Long statusId;
    private Long  categoryId;

    private String name;

    private Long viewNo;

    private Double percentage;

    private List<ItemCategoryCaptionResources> itemCategoryCaptionResources;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }


    public List<ItemCategoryCaptionResources> getItemCategoryCaptionResources() {
        return itemCategoryCaptionResources;
    }

    public void setItemCategoryCaptionResources(List<ItemCategoryCaptionResources> itemCategoryCaptionResources) {
        this.itemCategoryCaptionResources = itemCategoryCaptionResources;
    }

    public static ItemCategoryResources toResources(ItemCategory category) {
        ItemCategoryResources resources = new ItemCategoryResources();
        resources.setId(category.getId());
        resources.setCategoryId(category.getId());
        resources.setViewNo(category.getTotalViews());

        if(category.getItemCategoryCaptions() != null){
            resources.setItemCategoryCaptionResources(ItemCategoryCaptionResources.toResources(category.getItemCategoryCaptions()));
        }

//        resources.setStatusId(category.getStatus().getId());
        return resources;

    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getViewNo() {
        return viewNo;
    }

    public void setViewNo(Long viewNo) {
        this.viewNo = viewNo;
    }

    public Double getPercentage() {
        return percentage;
    }

    public void setPercentage(Double percentage) {
        this.percentage = percentage;
    }

    public static List<ItemCategoryResources> toResources(List<ItemCategory> categoryList) {
        List<ItemCategoryResources> resourcesList = new ArrayList<>();
        categoryList.forEach(category -> {
            ItemCategoryResources resources = toResources(category);
            resourcesList.add(resources);

        });
        return resourcesList;
    }

    public ItemCategory toItemCategory() {
        ItemCategory category = new ItemCategory();

        category.setId(this.id);
        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            category.setStatus(status);
        }
        return category;
    }

    public void initResourceCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage, Long totalViews) {

        ItemCategoryCaption itemCategoryCaption = applicationContext.getBean(ItemCategoryCaptionService.class).find(this.id, operationLanguage);

        if (itemCategoryCaption == null) {
            itemCategoryCaption = applicationContext.getBean(ItemCategoryCaptionService.class).findAllByLang(operationLanguage).get(0);
        }

        this.setName(itemCategoryCaption.getName());


        Double percentage = (( Double.valueOf(this.viewNo) / Double.valueOf(totalViews)) * 100);
        if(Double.isNaN(percentage)){
            this.setPercentage(0D);
        }else{
            this.setPercentage(percentage);
        }

    }
}
