package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.OperationLanguageService;
import com.dc.Promotion.service.SpecificationCaptionService;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class ItemSpecificationResource {
    private Long id;
    private String specs;
    private Long merchantId;
    private Long promoId;
    private Long statusId;
    private Long promoItemId;
    private Long languageId;
    private String langCode;
    private String value;
    private String specsType;
    private Long specificationId;
    private String name;
    private String key;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpecs() {
        return specs;
    }

    public void setSpecs(String specs) {
        this.specs = specs;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getPromoId() {
        return promoId;
    }

    public void setPromoId(Long promoId) {
        this.promoId = promoId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getPromoItemId() {
        return promoItemId;
    }

    public void setPromoItemId(Long promoItemId) {
        this.promoItemId = promoItemId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getSpecsType() {
        return specsType;
    }

    public void setSpecsType(String specsType) {
        this.specsType = specsType;
    }

    public Long getSpecificationId() {
        return specificationId;
    }

    public void setSpecificationId(Long specificationId) {
        this.specificationId = specificationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static ItemSpecificationResource toResources(ItemSpecification itemSpecification) {
        ItemSpecificationResource resources = new ItemSpecificationResource();
        resources.setId(itemSpecification.getId());
//        resources.setStatusId(itemSpecification.getStatus().getId());
        if (itemSpecification.getPromoItem() != null)
            resources.setPromoItemId(itemSpecification.getPromoItem().getId());
        resources.setSpecs((itemSpecification.getSpecification() == null) ? (itemSpecification.getKey()) : itemSpecification.getSpecification().getKey());
        resources.setName((itemSpecification.getSpecification() == null) ? (itemSpecification.getKey()) : itemSpecification.getSpecification().getKey());
        if (itemSpecification.getOperationLanguage() != null) {
            resources.setLanguageId(itemSpecification.getOperationLanguage().getId());
            resources.setLangCode(itemSpecification.getOperationLanguage().getCode());
        }
        resources.setValue(itemSpecification.getValue());
        if (itemSpecification.getSpecification() != null)
            resources.setSpecificationId(itemSpecification.getSpecification().getId());
        resources.setSpecsType((itemSpecification.getSpecification() != null) ? "pre" : "other");
        resources.setKey((itemSpecification.getSpecification() == null) ? (itemSpecification.getKey()) : itemSpecification.getSpecification().getKey());

        return resources;
    }

    public static List<ItemSpecificationResource> toResources(List<ItemSpecification> itemSpecifications) {
        List<ItemSpecificationResource> resourcesList = new ArrayList<>();
        itemSpecifications.forEach(categorySpecification -> {
            ItemSpecificationResource resources = toResources(categorySpecification);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public ItemSpecification toItemCategorySpecification() {
        ItemSpecification itemSpecification = new ItemSpecification();


        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            itemSpecification.setStatus(status);
        }
        if (promoItemId != null) {
            PromoItem promoItem = new PromoItem();
            promoItem.setId(this.promoItemId);
            itemSpecification.setPromoItem(promoItem);
        }

        itemSpecification.setId(this.id);
        return itemSpecification;
    }

    public void initResourceCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        OperationLanguage specsOperationLanguage = applicationContext.getBean(OperationLanguageService.class).findByCode(this.langCode);
        if (specificationId != null && specificationId != 0) {
            SpecificationCaption specificationCaption = applicationContext.getBean(SpecificationCaptionService.class).find(this.specificationId, specsOperationLanguage);
            if (specificationCaption == null) {
                specificationCaption = applicationContext.getBean(SpecificationCaptionService.class).find(this.specificationId).get(0);
            }

            this.setKey(specificationCaption.getSpecification().getKey());
            this.setName(specificationCaption.getKey());
        }
    }
}

