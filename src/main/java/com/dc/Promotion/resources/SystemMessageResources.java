package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Status;
import com.dc.Promotion.entities.SystemMessage;

import java.util.ArrayList;
import java.util.List;

public class SystemMessageResources {

    private Long id;

    private String code;

    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static SystemMessageResources toResources(SystemMessage message)
    {
        SystemMessageResources resources=new SystemMessageResources();
        resources.setId(message.getId());
        resources.setCode(message.getCode());
        resources.setStatusId(message.getStatus().getId());
        return resources;
    }

    public static List<SystemMessageResources> toResources(List<SystemMessage> messageList)
    {
        List<SystemMessageResources> resourcesList =new ArrayList<>();
        messageList.forEach(message -> {
            SystemMessageResources resources=toResources(message);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public SystemMessage toSystemMessage()
    {
        SystemMessage message=new SystemMessage();
        message.setId(this.id);
        message.setCode(this.code);
        if(statusId!=null) {
            Status status=new Status();
            status.setId(this.statusId);
            message.setStatus(status);
        }
        return message;

    }
}
