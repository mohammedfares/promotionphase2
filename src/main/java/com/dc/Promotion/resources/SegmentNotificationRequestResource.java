package com.dc.Promotion.resources;

import com.dc.Promotion.entities.MerchantCustomerSegment;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.SegmentNotificationRequest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SegmentNotificationRequestResource {

    private Long id;


    private LocalDateTime creationTime;


    private Long totalNotification;


    private Long totalCustomer;


    private Long customerSegmentId;


    private Long promoId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

    public Long getTotalNotification() {
        return totalNotification;
    }

    public void setTotalNotification(Long totalNotification) {
        this.totalNotification = totalNotification;
    }

    public Long getTotalCustomer() {
        return totalCustomer;
    }

    public void setTotalCustomer(Long totalCustomer) {
        this.totalCustomer = totalCustomer;
    }

    public Long getCustomerSegmentId() {
        return customerSegmentId;
    }

    public void setCustomerSegmentId(Long customerSegmentId) {
        this.customerSegmentId = customerSegmentId;
    }

    public Long getPromoId() {
        return promoId;
    }

    public void setPromoId(Long promoId) {
        this.promoId = promoId;
    }


    public static SegmentNotificationRequestResource toResource(SegmentNotificationRequest segmentNotificationRequest) {

        SegmentNotificationRequestResource resource = new SegmentNotificationRequestResource();

        resource.setId(segmentNotificationRequest.getId());
        resource.setTotalCustomer(segmentNotificationRequest.getTotalCustomer());
        resource.setCreationTime(segmentNotificationRequest.getCreationTime());
        resource.setCustomerSegmentId(segmentNotificationRequest.getCustomerSegment().getId());
        resource.setPromoId(segmentNotificationRequest.getPromo().getId());
        resource.setTotalNotification(segmentNotificationRequest.getTotalNotification());
        return resource;


    }


    public static List<SegmentNotificationRequestResource> toResource(List<SegmentNotificationRequest> segmentNotificationRequest) {

        List<SegmentNotificationRequestResource> resources = new ArrayList<>();
        resources.forEach(segmentNotificationRequestResource -> {

            SegmentNotificationRequestResource.toResource(segmentNotificationRequest);
            resources.add(segmentNotificationRequestResource);
        });


        return resources;
    }


    public SegmentNotificationRequest toSegmentNotificationRequest() {

        SegmentNotificationRequest segmentNotificationRequest = new SegmentNotificationRequest();

        segmentNotificationRequest.setCreationTime(this.creationTime);
        segmentNotificationRequest.setId(this.id);
        segmentNotificationRequest.setTotalCustomer(this.totalCustomer);
        segmentNotificationRequest.setTotalNotification(this.totalNotification);

        if (promoId != null) {
            Promo promo = new Promo();
            promo.setId(this.promoId);
            segmentNotificationRequest.setPromo(promo);


        }

        if(customerSegmentId!=null){
            MerchantCustomerSegment merchantCustomerSegment=new MerchantCustomerSegment();
            merchantCustomerSegment.setId(this.customerSegmentId);
            segmentNotificationRequest.setCustomerSegment(merchantCustomerSegment);

        }
        return  segmentNotificationRequest;

    }


}
