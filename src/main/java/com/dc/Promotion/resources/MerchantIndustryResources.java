package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.MerchantIndustry;
import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class MerchantIndustryResources {
    private Long id;
    private Long merchantId;
    private Long industry;
    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getIndustry() {
        return industry;
    }

    public void setIndustry(Long industry) {
        this.industry = industry;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static MerchantIndustryResources toResources(MerchantIndustry merchantIndustry) {
        MerchantIndustryResources merchantIndustryResources = new MerchantIndustryResources();
        merchantIndustryResources.setId(merchantIndustry.getId());
        merchantIndustryResources.setIndustry(merchantIndustry.getIndustry().getId());
        merchantIndustryResources.setMerchantId(merchantIndustry.getMerchant().getId());
        merchantIndustryResources.setStatusId(merchantIndustry.getStatus().getId());
        return merchantIndustryResources;
    }

    public static List<MerchantIndustryResources> toResources(List<MerchantIndustry> merchantIndustry) {
        List<MerchantIndustryResources> resources = new ArrayList<>();
        merchantIndustry.forEach(merchantIndustry1 -> {
            MerchantIndustryResources industryResources = toResources(merchantIndustry1);
            resources.add(industryResources);

        });
        return resources;


    }

    public MerchantIndustry toMerchantIndustry() {
        MerchantIndustry merchantIndustry = new MerchantIndustry();
        merchantIndustry.setId(this.id);
        if (industry == null) {
            Industry industry = new Industry();
            industry.setId(this.industry);
            merchantIndustry.setIndustry(industry);

        }
        if (statusId == null) {
            Status status = new Status();
            status.setId(statusId);
            merchantIndustry.setStatus(status);
        }
        if (merchantId == null) {

            Merchant merchant = new Merchant();
            merchant.setId(merchantId);
            merchantIndustry.setMerchant(merchant);
        }
        return merchantIndustry;
    }
}
