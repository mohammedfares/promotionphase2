package com.dc.Promotion.resources;

import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.PromoItemImage;
import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class PromoItemImageResources {
    private Long id;
    private String imageUrl;
    private Long statusId;
    private Long promoItemId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getPromoItemId() {
        return promoItemId;
    }

    public void setPromoItemId(Long promoItemId) {
        this.promoItemId = promoItemId;
    }

    public static PromoItemImageResources toResources(PromoItemImage promoItemImage){

        PromoItemImageResources promoItemImageResources=new PromoItemImageResources();
        promoItemImageResources.setId(promoItemImage.getId());
        promoItemImageResources.setImageUrl(promoItemImage.getImageUrl());
        promoItemImageResources.setPromoItemId(promoItemImage.getPromoItemId().getId());
        promoItemImageResources.setStatusId(promoItemImage.getStatus().getId());
        return  promoItemImageResources;
    }
    public static List<PromoItemImageResources> toResources(List<PromoItemImage> promoItemImage){
        List<PromoItemImageResources> resources=new ArrayList<>();
        promoItemImage.forEach(promoItemImage1 -> {
            PromoItemImageResources imageResources=toResources(promoItemImage1);
            resources.add(imageResources);
        });
        return resources;

    }


    public PromoItemImage toPromoItemImage(){

        PromoItemImage promoItemImage=new PromoItemImage();
        promoItemImage.setId(this.id);
        promoItemImage.setImageUrl(this.imageUrl);
        if(promoItemId!=null){
            PromoItem promoItem=new PromoItem();
            promoItem.setId(promoItemId);
            promoItemImage.setPromoItemId(promoItem);
        }
        if(statusId!=null){
            Status status=new Status();
            status.setId(statusId);
            promoItemImage.setStatus(status);
        }
        return  promoItemImage;
    }
}
