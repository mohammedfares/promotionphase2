package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.IndustryCaption;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.service.IndustryService;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class IndustryCaptionResources {

    private Long id;

    private String name;

    private Long languageId;

    private Long industryId;

    private String industryImg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public Long getIndustryId() {
        return industryId;
    }

    public void setIndustryId(Long industryId) {
        this.industryId = industryId;
    }

    public String getIndustryImg() {
        return industryImg;
    }

    public void setIndustryImg(String industryImg) {
        this.industryImg = industryImg;
    }

    public static IndustryCaptionResources toResources(IndustryCaption caption)
    {
        IndustryCaptionResources resources=new IndustryCaptionResources();
        resources.setId(caption.getId());
        resources.setIndustryId(caption.getIndustry().getId());
        resources.setLanguageId(caption.getLanguageId().getId());
        resources.setName(caption.getName());
        return resources;

    }

    public static List<IndustryCaptionResources> toResources(List<IndustryCaption> captionList)
    {
        List<IndustryCaptionResources> resourcesList=new ArrayList<>();
        captionList.forEach(industryCaption -> {
            IndustryCaptionResources resources=toResources(industryCaption);
            resourcesList.add(resources);
        });
        return resourcesList;

    }

    public IndustryCaption toIndustryCaption()
    {
        IndustryCaption caption=new IndustryCaption();
        caption.setId(this.id);
        caption.setName(this.name);
        if(languageId!=null)
        {
            OperationLanguage language=new OperationLanguage();
            language.setId(this.languageId);
            caption.setLanguageId(language);
        }
        if(industryId!=null)
        {
            Industry industry=new Industry();
            industry.setId(this.industryId);
            caption.setIndustry(industry);
        }
        return caption;
    }

    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        Industry industry=applicationContext.getBean(IndustryService.class).findById(this.industryId);
        this.setIndustryImg(industry.getImage());
    }
}
