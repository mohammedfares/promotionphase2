package com.dc.Promotion.resources;

import com.dc.Promotion.entities.MerchantBranch;
import com.dc.Promotion.entities.MerchantBranchCaption;
import com.dc.Promotion.entities.OperationLanguage;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MerchantBranchCaptionResources {
    private Long id;
    private String name;
    private String description;
    private Long merchantBranch;
    private Long languageId;
    private String address;
    private String contactNumber;
    private LocalDateTime createDate;
    private LocalDateTime expiryDate;
    private Double longitude;
    private Double latitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMerchantBranch() {
        return merchantBranch;
    }

    public void setMerchantBranch(Long merchantBranch) {
        this.merchantBranch = merchantBranch;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public static MerchantBranchCaptionResources toResource(MerchantBranchCaption branchCaption) {
        MerchantBranchCaptionResources resources = new MerchantBranchCaptionResources();
        resources.setId(branchCaption.getId());
        resources.setName(branchCaption.getName());
        resources.setDescription(branchCaption.getDescription());
        resources.setMerchantBranch(branchCaption.getMerchantBranch().getId());
        resources.setLanguageId(branchCaption.getOperationLanguage().getId());
        resources.setLongitude(branchCaption.getMerchantBranch().getLongitude());
        resources.setLatitude(branchCaption.getMerchantBranch().getLatitude());
        resources.setAddress(branchCaption.getAddress());
        resources.setContactNumber(branchCaption.getMerchantBranch().getContactNumber());
        return resources;
    }
    public static List<MerchantBranchCaptionResources> toResource(List<MerchantBranchCaption> branchCaption)
    {
        List<MerchantBranchCaptionResources> resources=new ArrayList<>();
        branchCaption.forEach(branchCaption1 -> {
            MerchantBranchCaptionResources resources1=toResource(branchCaption1);
            resources.add(resources1);

        });
        return  resources;

    }

    public MerchantBranchCaption toMerchantBranchCaption(){
        MerchantBranchCaption merchantBranchCaption=new MerchantBranchCaption();
        merchantBranchCaption.setId(this.id);
        merchantBranchCaption.setName(this.name);
        merchantBranchCaption.setDescription(this.description);
        merchantBranchCaption.setAddress(this.address);
        if(languageId==null){
            OperationLanguage operationLanguage=new OperationLanguage();
            operationLanguage.setId(languageId);
            merchantBranchCaption.setOperationLanguage(operationLanguage);
        }
        if(merchantBranch==null){
            MerchantBranch merchantBranch=new MerchantBranch();
           merchantBranch.setId(merchantBranch.getId());
           merchantBranchCaption.setMerchantBranch(merchantBranch);

        }
        return merchantBranchCaption;

    }
}
