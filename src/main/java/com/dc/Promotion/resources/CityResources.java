package com.dc.Promotion.resources;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.Country;
import com.dc.Promotion.entities.OperationCountry;
import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class CityResources {



    private Long id;

    private String code;

    private Long countryId;

    private Long statusId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }


    public static CityResources toResources(City city){

        CityResources  resources=new CityResources();
        resources.setCode(city.getCode());
        resources.setCountryId(city.getCountry().getId());
        resources.setId(city.getId());
        resources.setStatusId(city.getStatus().getId());
        return resources;
    }

    public static List<CityResources> toResources(List<City> cityList)
    {
        List<CityResources> resourcesList=new ArrayList<>();
        cityList.forEach(city -> {
            CityResources resources=toResources(city);
            resourcesList.add(resources);

        });
        return resourcesList;
    }

    public City toCity()
    {
        City city=new City();
        city.setId(this.id);
        city.setCode(this.code);

        if(countryId!=null)
        {
            OperationCountry country=new OperationCountry();
            country.setId(this.countryId);
            city.setCountry(country);
        }

        if(statusId!=null)
        {
            Status status=new Status();
            status.setId(this.statusId);
            city.setStatus(status);
        }

        return city;
    }
}
