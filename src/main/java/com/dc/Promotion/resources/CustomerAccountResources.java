package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CustomerAccountResources {

    private Long id;

    private int age;

    private Long barCode;

    private LocalDateTime createDate;

    private LocalDateTime expireDate;

    private String email;

    private String mobile;

    private String image;

    private String gender;

    private Long nationalityId;

    private String nationalityName;

    private String password;

    private LocalDateTime birthDate;

    private Long buildingNumber;

    private Long apartmentNumber;

    private String firstName;

    private String lastName;

    private Long rangeId;

    private Long cityId;

    private String cityName;

    private Long districtId;

    private String districtName;

    private Long statusId;

    private String confirmPassword;

    private String countryCode;

    private String otp;

    private Long flag;

    private Long countryId;

    private String username;

    private String token;

    private String newPassword;

    private String deviceToken;

    private String devicePlatform;

    private Double longitude;

    private Double latitude;

    private Long languageId;

    private String languageCode;

    public int getAge() {
        return age;
    }

    public Long getBarCode() {
        return barCode;
    }

    public void setBarCode(Long barCode) {
        this.barCode = barCode;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public Long getNationalityId() {
        return nationalityId;
    }

    public void setNationalityId(Long nationalityId) {
        this.nationalityId = nationalityId;
    }

    public Long getFlag() {
        return flag;
    }

    public void setFlag(Long flag) {
        this.flag = flag;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpireDate() {
        return expireDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setExpireDate(LocalDateTime expireDate) {
        this.expireDate = expireDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getBirthDate() {
        return birthDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public Long getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(Long buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public Long getApartmentNumber() {
        return apartmentNumber;
    }

    public void setApartmentNumber(Long apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getRangeId() {
        return rangeId;
    }

    public void setRangeId(Long rangeId) {
        this.rangeId = rangeId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNationalityName() {
        return nationalityName;
    }

    public void setNationalityName(String nationalityName) {
        this.nationalityName = nationalityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getDevicePlatform() {
        return devicePlatform;
    }

    public void setDevicePlatform(String devicePlatform) {
        this.devicePlatform = devicePlatform;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public static CustomerAccountResources toResources(CustomerAccount account) {

        CustomerAccountResources resources = new CustomerAccountResources();
        resources.setLastName(account.getLastName());
        resources.setFirstName(account.getFirstName());
        resources.setPassword(account.getPassword());
        resources.setStatusId(account.getStatus().getId());
        resources.setMobile(account.getMobile());
        resources.setEmail(account.getEmail());
        resources.setApartmentNumber(account.getApartmentNumber());
        resources.setBirthDate(account.getBirthDate());
        resources.setBuildingNumber(account.getBuildingNumber());
        resources.setCityId((account.getCity() != null) ? account.getCity().getId() : null);
        resources.setCreateDate(account.getCreateDate());
        resources.setDistrictId((account.getDistrict() != null) ? account.getDistrict().getId() : null);
        resources.setExpireDate(account.getExpireDate());
        resources.setGender(account.getGender());
        resources.setId(account.getId());
        resources.setImage(account.getImage());
        resources.setCountryId((account.getCountry() != null) ? account.getCountry().getId() : null);
        resources.setRangeId((account.getRange() != null) ? account.getRange().getId() : null);
        resources.setLanguageCode(account.getOperationLanguage().getCode());
        resources.setLanguageId(account.getOperationLanguage().getId());
        resources.setLatitude(account.getLatitude());
        resources.setAge(account.getAge());
        resources.setBarCode(account.getBarCode());

        return resources;
    }

    public List<CustomerAccountResources> toResources(List<CustomerAccount> accountList) {
        List<CustomerAccountResources> resourcesList = new ArrayList<>();

        accountList.forEach(account -> {

            CustomerAccountResources resources = toResources(account);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public CustomerAccount toCustomerAccount() {
        CustomerAccount account = new CustomerAccount();
        account.setEmail(this.email);
        account.setPassword(this.password);
        account.setLastName(this.lastName);
        account.setFirstName(this.firstName);
        account.setMobile(this.mobile);
        account.setApartmentNumber(this.apartmentNumber);
        account.setBirthDate(this.birthDate);
        account.setBuildingNumber(this.buildingNumber);
        account.setCreateDate(this.createDate);
        account.setExpireDate(this.expireDate);
        account.setGender(this.gender);
        account.setId(this.id);
        account.setImage(this.image);
        account.setDeviceToken(this.deviceToken);
        account.setDevicePlatform(this.devicePlatform);
        account.setLatitude(this.latitude);
        account.setLongitude(this.longitude);
        account.setAge(this.age);
        account.setBarCode(this.barCode);

        if (languageId!=null){

            OperationLanguage language=new OperationLanguage();
            language.setId(this.languageId);
            language.setCode(this.languageCode);
            account.setOperationLanguage(language);

        }
            if (districtId != null) {
                District district = new District();
                district.setId(this.districtId);
                account.setDistrict(district);
            }

        if (cityId != null) {
            City city = new City();
            city.setId(this.cityId);
            account.setCity(city);
        }

        if (rangeId != null) {
            AgeRange ageRange = new AgeRange();
            ageRange.setId(this.rangeId);
            account.setRange(ageRange);

        }

        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            account.setStatus(status);
        }

        if (countryId != null) {
            Country country = new Country();
            country.setId(this.countryId);
            account.setCountry(country);
        }

        return account;


    }

    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        Country country = applicationContext.getBean(CountryService.class).findById(this.countryId);

        CountryCaption countryCaption = applicationContext.getBean(CountryCaptionService.class).findByCountryAndLanguage(country, operationLanguage);


        City city = applicationContext.getBean(CityService.class).findById(this.cityId);

        CityCaption cityCaption = applicationContext.getBean(CityCaptionService.class).findByCityAndLanguage(city, operationLanguage);

        District district = applicationContext.getBean(DistrictService.class).findById(this.cityId);

        DistrictCaption districtCaption = applicationContext.getBean(DistrictCaptionService.class).findByDistrictAndLanguage(district, operationLanguage);


        if (countryCaption != null) {
            this.setNationalityName(countryCaption.getNationalityName());
        }

        if (cityCaption != null) {
            this.setCityName(cityCaption.getName());
        }

        if (districtCaption != null) {
            this.setDistrictName(districtCaption.getName());
        }
    }


}
