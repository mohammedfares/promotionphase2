package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.MerchantBranchCaptionService;
import com.dc.Promotion.service.MerchantBranchService;
import com.dc.Promotion.service.StatusCaptionService;
import com.dc.Promotion.service.StatusService;
import com.dc.Promotion.utils.Utils;
import org.springframework.context.ApplicationContext;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static jdk.nashorn.internal.objects.NativeMath.round;


public class MerchantBranchResources {
    private Long id;
    private String contactNumber;
    private LocalDateTime createDate;
    private LocalDateTime expiryDate;
    private String address;
    private Double longitude;
    private Double latitude;
    private Long merchantId;
    private Long statusId;
    private String statusCaption;
    private String name;
    private List<MerchantBranchCaptionResources> captionList;
    private Long merchantBranch;
    private Double distance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public List<MerchantBranchCaptionResources> getCaptionList() {
        return captionList;
    }

    public void setCaptionList(List<MerchantBranchCaptionResources> captionList) {
        this.captionList = captionList;
    }

    public String getStatusCaption() {
        return statusCaption;
    }

    public void setStatusCaption(String statusCaption) {
        this.statusCaption = statusCaption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getMerchantBranch() {
        return merchantBranch;
    }

    public void setMerchantBranch(Long merchantBranch) {
        this.merchantBranch = merchantBranch;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public static MerchantBranchResources toResource(MerchantBranch merchantBranch) {

        MerchantBranchResources resources = new MerchantBranchResources();
        resources.setMerchantBranch(merchantBranch.getId());
        resources.setId(merchantBranch.getId());
        resources.setAddress(merchantBranch.getAddress());
        resources.setContactNumber(merchantBranch.getContactNumber());
        resources.setCreateDate(merchantBranch.getCreateDate());
        resources.setExpiryDate(merchantBranch.getExpiryDate());
        resources.setLatitude(merchantBranch.getLatitude());
        resources.setLongitude(merchantBranch.getLongitude());
        resources.setMerchantId(merchantBranch.getMerchant().getId());
        resources.setStatusId(merchantBranch.getStatus().getId());

        if (merchantBranch.getMerchantBranchCaptionList() != null) {
            resources.setCaptionList(MerchantBranchCaptionResources.toResource(merchantBranch.getMerchantBranchCaptionList()));
        }

        return resources;
    }

    public static List<MerchantBranchResources> toResource(List<MerchantBranch> merchantBranch) {
        List<MerchantBranchResources> resources = new ArrayList<>();
        merchantBranch.forEach(merchantBranch1 -> {
            MerchantBranchResources branchResources = toResource(merchantBranch1);
            resources.add(branchResources);
        });
        return resources;

    }

    public MerchantBranch toMerchantBranch() {
        MerchantBranch merchantBranch = new MerchantBranch();
        merchantBranch.setId(this.id);
        merchantBranch.setAddress(this.address);
        merchantBranch.setContactNumber(this.contactNumber);
        merchantBranch.setCreateDate(this.createDate);
        merchantBranch.setExpiryDate(this.expiryDate);
        merchantBranch.setLatitude(this.latitude);
        merchantBranch.setLongitude(this.longitude);
        if (statusId != null) {

            Status status = new Status();
            status.setId(statusId);
            merchantBranch.setStatus(status);
        }
        if (merchantId != null) {
            Merchant merchant = new Merchant();
            merchant.setId(merchantId);
            merchantBranch.setMerchant(merchant);
        }
        return merchantBranch;
    }


    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage, Double longitudeBranch, Double latitudeBranch) {

        Status status = applicationContext.getBean(StatusService.class).findById(this.statusId);

        StatusCaption statusCaption = applicationContext.getBean(StatusCaptionService.class).findByStatusAndLanguage(status, operationLanguage);

        this.setStatusCaption(statusCaption.getName());

        MerchantBranch merchantBranch = applicationContext.getBean(MerchantBranchService.class).findById(this.id);

        MerchantBranchCaption merchantBranchCaption = applicationContext.getBean(MerchantBranchCaptionService.class).find(merchantBranch, operationLanguage);

        if (merchantBranchCaption == null) {
            List<MerchantBranchCaption> merchantBranchCaptionList = applicationContext.getBean(MerchantBranchCaptionService.class).find(merchantBranch);
            if (merchantBranchCaptionList != null && !merchantBranchCaptionList.isEmpty()) {
                merchantBranchCaption = (merchantBranchCaptionList.get(0));
            }
        }

        if (merchantBranchCaption != null) {
            this.setName(merchantBranchCaption.getName());
            this.setAddress(merchantBranchCaption.getAddress());
        }

        if(latitudeBranch!=null && longitudeBranch!=null){
            double dis=Utils.distance(this.latitude,this.longitude,latitudeBranch,longitudeBranch,"K");
//            dis=Math.floor(dis* 100) / 100;
            this.setDistance(Utils.roundDouble(dis));
        }
        //this.setDistance((Utils.distance(this.latitude,this.longitude,latitudeBranch,longitudeBranch,"K")));
    }


}
