package com.dc.Promotion.resources;

import com.dc.Promotion.entities.City;
import com.dc.Promotion.entities.CityCaption;
import com.dc.Promotion.entities.OperationLanguage;
import org.apache.catalina.LifecycleState;

import java.util.ArrayList;
import java.util.List;

public class CityCaptionResources {

    private Long id;

    private String description;

    private String name;

    private Long cityId;

    private Long languageId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public static CityCaptionResources toResources(CityCaption caption)
    {
        CityCaptionResources resources=new CityCaptionResources();
        resources.setCityId(caption.getCity().getId());
        resources.setDescription(caption.getDescription());
        resources.setId(caption.getId());
        resources.setLanguageId(caption.getLanguage().getId());
        resources.setName(caption.getName());
        return resources;

    }

    public static List<CityCaptionResources> toResources(List<CityCaption> captionList)
    {
       List <CityCaptionResources> resourcesList=new ArrayList<>();
        captionList.forEach( cityCaption -> {
            CityCaptionResources resources=toResources(cityCaption);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public CityCaption toCityCaption()
    {
        CityCaption caption=new CityCaption();
        caption.setName(this.name);
        caption.setDescription(this.description);
        caption.setId(this.id);
        if(languageId!=null)
        {
            OperationLanguage language=new OperationLanguage();
            language.setId(this.languageId);
            caption.setLanguage(language);
        }
        if(cityId!=null)
        {
            City city=new City();
            city.setId(this.cityId);
            caption.setCity(city);
        }
        return caption;

    }
}
