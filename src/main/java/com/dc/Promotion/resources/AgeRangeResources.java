package com.dc.Promotion.resources;

import com.dc.Promotion.entities.AgeRange;
import org.hibernate.validator.constraints.EAN;

import java.util.ArrayList;
import java.util.List;

public class AgeRangeResources {


    private Long id;
    private String code;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static AgeRangeResources toResources(AgeRange ageRange)
    {
        AgeRangeResources resources=new AgeRangeResources();
        resources.setCode(ageRange.getCode());
        resources.setId(ageRange.getId());
        return resources;
    }

    public static List<AgeRangeResources> toResources(List<AgeRange> ageRangeList)
    {
        List<AgeRangeResources> resourcesList=new ArrayList<>();
        ageRangeList.forEach(ageRange -> {
            AgeRangeResources resources=toResources(ageRange);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public AgeRange toAgeRange()
    {
        AgeRange ageRange=new AgeRange();
        ageRange.setId(this.id);
        ageRange.setCode(this.code);
        return ageRange;
    }
}
