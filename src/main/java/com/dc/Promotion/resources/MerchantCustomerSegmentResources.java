package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MerchantCustomerSegmentResources {

    private Long id;

    private String alias;

    private String gender;

    private Double longitude;

    private Double latitude;

    private Integer ageFrom;

    private Integer ageTo;

    private Long notifiedCustomer;

    private Double radius;

    private Long merchantId;

    private Long merchantUserId;

    private Long cityId;

    private Long districtId;

    private Long statusId;

    private LocalDateTime createDate;

    private Long totalNotification;

    private Long totalViews;

    private Long totalCustomer;

    private List<CountryResources>countryList ;

    private List<String> nationalityList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Integer getAgeFrom() {
        return ageFrom;
    }

    public void setAgeFrom(Integer ageFrom) {
        this.ageFrom = ageFrom;
    }

    public Integer getAgeTo() {
        return ageTo;
    }

    public void setAgeTo(Integer ageTo) {
        this.ageTo = ageTo;
    }

    public Long getNotifiedCustomer() {
        return notifiedCustomer;
    }

    public void setNotifiedCustomer(Long notifiedCustomer) {
        this.notifiedCustomer = notifiedCustomer;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getMerchantUserId() {
        return merchantUserId;
    }

    public void setMerchantUserId(Long merchantUserId) {
        this.merchantUserId = merchantUserId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getTotalNotification() {
        return totalNotification;
    }

    public void setTotalNotification(Long totalNotification) {
        this.totalNotification = totalNotification;
    }

    public Long getTotalViews() {
        return totalViews;
    }

    public void setTotalViews(Long totalViews) {
        this.totalViews = totalViews;
    }

    public List<CountryResources> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<CountryResources> countryList) {
        this.countryList = countryList;
    }

    public Long getTotalCustomer() {
        return totalCustomer;
    }

    public void setTotalCustomer(Long totalCustomer) {
        this.totalCustomer = totalCustomer;
    }


    public static MerchantCustomerSegmentResources toResources(MerchantCustomerSegment segment)
    {
        MerchantCustomerSegmentResources resources=new MerchantCustomerSegmentResources();
        resources.setAgeFrom(segment.getAgeFrom());
        resources.setAgeTo(segment.getAgeTo());
        resources.setAlias(segment.getAlias());
        if(segment.getCity()!=null)
        resources.setCityId(segment.getCity().getId());
        if(segment.getDistrict()!=null)
        resources.setDistrictId(segment.getDistrict().getId());
        resources.setGender(segment.getGender());
        resources.setId(segment.getId());
        resources.setLatitude(segment.getLatitude());
        resources.setLongitude(resources.getLongitude());
        resources.setMerchantId(segment.getMerchant().getId());
        resources.setNotifiedCustomer(segment.getNotifiedCustomer());
        resources.setRadius(segment.getRadius());
        resources.setMerchantUserId(segment.getMerchantUser().getId());
        resources.setStatusId(segment.getStatus().getId());
        resources.setCreateDate(segment.getCreateDate());
        resources.setTotalCustomer(segment.getTotalCustomer());
        resources.setTotalNotification(segment.getTotalNotification());
        resources.setTotalViews(segment.getTotalViews());
        return resources;
    }

    public static List<MerchantCustomerSegmentResources> toResources(List<MerchantCustomerSegment> segmentList)
    {
        List<MerchantCustomerSegmentResources> resourcesList=new ArrayList<>();
        segmentList.forEach(merchantCustomerSegment -> {
            MerchantCustomerSegmentResources resources=MerchantCustomerSegmentResources.toResources(merchantCustomerSegment);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public MerchantCustomerSegment toMerchantCustomerSegment()
    {
        MerchantCustomerSegment segment=new MerchantCustomerSegment();
        segment.setAgeFrom(this.ageFrom);
        segment.setAgeTo(this.ageTo);
        segment.setAlias(this.alias);
        segment.setNotifiedCustomer(this.notifiedCustomer);
        segment.setRadius(this.radius);
        segment.setGender(this.gender);
        segment.setId(this.id);
        segment.setLatitude(this.latitude);
        segment.setLongitude(this.longitude);
        segment.setCreateDate(this.createDate);
        if(cityId!=null)
        {
            City city=new City();
            city.setId(this.cityId);
            segment.setCity(city);
        }
       if(districtId!=null)
       {
           District district=new District();
           district.setId(this.districtId);
           segment.setDistrict(district);
       }
        if(merchantId!=null)
        {
            Merchant merchant=new Merchant();
            merchant.setId(this.merchantId);
            segment.setMerchant(merchant);
        }
       if(merchantUserId!=null)
       {
           MerchantUser merchantUser=new MerchantUser();
           merchantUser.setId(this.merchantUserId);
           segment.setMerchantUser(merchantUser);
       }

       if(statusId!=null)
       {
           Status status=new Status();
           status.setId(this.statusId);
           segment.setStatus(status);
       }

        return segment;

    }
}
