package com.dc.Promotion.resources;

import com.dc.Promotion.entities.CustomerAccount;
import com.dc.Promotion.entities.CustomerItemView;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.Status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CustomerItemViewResources {

    private Long id;

    private LocalDateTime createDate;

    private Long customerAccountId;

    private Long promoItemId;

    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getCustomerAccountId() {
        return customerAccountId;
    }

    public void setCustomerAccountId(Long customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    public Long getPromoItemId() {
        return promoItemId;
    }

    public void setPromoItemId(Long promoItemId) {
        this.promoItemId = promoItemId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static CustomerItemViewResources toResources(CustomerItemView itemView)
    {
        CustomerItemViewResources resources=new CustomerItemViewResources();
        resources.setCreateDate(itemView.getCreateDate());
        resources.setCustomerAccountId(itemView.getCustomerAccount().getId());
        resources.setId(itemView.getId());
        resources.setPromoItemId(itemView.getPromoItem().getId());
        resources.setStatusId(itemView.getStatus().getId());
        return resources;
    }

    public static List<CustomerItemViewResources> toResources(List<CustomerItemView>itemViewList)
    {
        List<CustomerItemViewResources> resourcesList=new ArrayList<>();
        itemViewList.forEach(customerItemView -> {

            CustomerItemViewResources resources=toResources(customerItemView);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public CustomerItemView toCustomerItemView()
    {
        CustomerItemView itemView=new CustomerItemView();

        itemView.setCreateDate(this.createDate);
        itemView.setId(this.id);
        if(customerAccountId !=null)
        {
            CustomerAccount account=new CustomerAccount();
            account.setId(this.customerAccountId);
            itemView.setCustomerAccount(account);
        }

        if(statusId!=null)
        {
            Status status=new Status();
            status.setId(this.statusId);
            itemView.setStatus(status);
        }

        if(promoItemId!=null)
        {
            PromoItem promoItem=new PromoItem();
            promoItem.setId(this.promoItemId);
            itemView.setPromoItem(promoItem);
        }
        return itemView;
    }
}
