package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MediaPromoViewResources {
    private Long id;
    private LocalDateTime createDate;
    private Long mediaAccountId;
    private Long promoId;
    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getMediaAccountId() {
        return mediaAccountId;
    }

    public void setMediaAccountId(Long mediaAccountId) {
        this.mediaAccountId = mediaAccountId;
    }

    public Long getPromoId() {
        return promoId;
    }

    public void setPromoId(Long promoId) {
        this.promoId = promoId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }
    public static MediaPromoViewResources mediaPromoViewResources(MediaPromoView mediaPromoView){

        MediaPromoViewResources resources=new MediaPromoViewResources();
        resources.setId( mediaPromoView.getId());
        resources.setCreateDate(mediaPromoView.getCreateDate());
        resources.setStatusId(mediaPromoView.getStatus().getId());
        resources.setMediaAccountId(mediaPromoView.getMediaAccount().getId());
        resources.setPromoId(mediaPromoView.getPromo().getId());
        return  resources;}



    public static List<MediaPromoViewResources> toMediaItemViewResources(List<MediaPromoView> mediaPromoViews){
        List<MediaPromoViewResources> resources=new ArrayList<>();
        mediaPromoViews.forEach(mediaPromoView  -> {
            MediaPromoViewResources viewResources=mediaPromoViewResources(mediaPromoView);
            resources.add(viewResources);

        });


        return  resources;


    }
    public MediaPromoView toMediaItemView(){

        MediaPromoView mediaPromoView=new MediaPromoView();
        mediaPromoView.setId(this.id);
        mediaPromoView.setCreateDate(this.createDate);
        if(statusId==null) {
            Status status = new Status();
            status.setId(statusId);
            mediaPromoView.setStatus(status);
        }
        if(mediaAccountId==null){

            MediaAccount mediaAccount=new MediaAccount();
            mediaAccount.setId(mediaAccountId);
            mediaPromoView.setMediaAccount(mediaAccount);
        }
        if(promoId==null){

            Promo promo=new Promo();
            promo.setId(promoId);
            mediaPromoView.setPromo(promo);
        }
        return  mediaPromoView  ;


    }
}
