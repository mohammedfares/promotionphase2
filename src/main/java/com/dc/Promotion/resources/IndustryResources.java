package com.dc.Promotion.resources;

import com.dc.Promotion.entities.Industry;
import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class IndustryResources {

    private Long id;

    private String image;

    private Long statusId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static IndustryResources toResources(Industry industry)
    {
        IndustryResources resources=new IndustryResources();
        resources.setId(industry.getId());
        resources.setImage(industry.getImage());
        resources.setStatusId(industry.getStatusId().getId());
        return resources;
    }

    public static List<IndustryResources> toResources(List<Industry> industryList)
    {
        List<IndustryResources> resourcesList=new ArrayList<>();
        industryList.forEach(industry -> {
            IndustryResources resources=toResources(industry);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public  Industry toIndustry()
    {
        Industry industry=new Industry();
        industry.setId(this.id);
        industry.setImage(this.image);
        if(statusId!=null){
            Status status=new Status();
            status.setId(this.statusId);
            industry.setStatusId(status);
        }
        return industry;

    }
}
