package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.*;
import com.dc.Promotion.entities.Merchant;
import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Promo;
import com.dc.Promotion.entities.Status;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PromoResources {
    private Long id;
    private String coverPhoto;
    private LocalDateTime expiryDate;
    private LocalDateTime createDate;
    private LocalDateTime startDate;
    private String code;
    private String barcode;
    private Long viewNo;
    private Long merchantId;
    private Long statusId;
    private String statusCaption;
    private String name;
    private String description;
    private Long languageId;
    private Long point;
    private String merchantName;
    private Long promoId;
    private List<PromoCaptionResources> captionList;

    private List<PromoItemResources> promoItemList;

    private List<MerchantBranchResources> branchList;

    private OperationLanguage operationLanguage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public LocalDateTime getExpiryDate() {
        return expiryDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setExpiryDate(LocalDateTime expiryDate) {
        this.expiryDate = expiryDate;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Long getViewNo() {
        return viewNo;
    }

    public void setViewNo(Long viewNo) {
        this.viewNo = viewNo;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public List<PromoCaptionResources> getCaptionList() {
        return captionList;
    }

    public void setCaptionList(List<PromoCaptionResources> captionList) {
        this.captionList = captionList;
    }

    public List<PromoItemResources> getPromoItemList() {
        return promoItemList;
    }

    public void setPromoItemList(List<PromoItemResources> promoItemList) {
        this.promoItemList = promoItemList;
    }

    public List<MerchantBranchResources> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<MerchantBranchResources> branchList) {
        this.branchList = branchList;
    }

    public String getStatusCaption() {
        return statusCaption;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatusCaption(String statusCaption) {
        this.statusCaption = statusCaption;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public OperationLanguage getOperationLanguage() {
        return operationLanguage;
    }

    public void setOperationLanguage(OperationLanguage operationLanguage) {
        this.operationLanguage = operationLanguage;
    }

    public Long getPoint() {
        return point;
    }

    public void setPoint(Long point) {
        this.point = point;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }


    public Long getPromoId() {
        return promoId;
    }

    public void setPromoId(Long promoId) {
        this.promoId = promoId;
    }

    public static PromoResources toResources(Promo promo) {
        PromoResources resources = new PromoResources();
        resources.setId(promo.getId());
        resources.setCode(promo.getCode());
        resources.setBarcode(promo.getBarcode());
        resources.setCoverPhoto(promo.getCoverPhoto());
        resources.setCreateDate(promo.getCreateDate());
        resources.setExpiryDate(promo.getExpiryDate());
        resources.setStartDate(promo.getStartDate());
        resources.setMerchantId(promo.getMerchant().getId());
        resources.setStatusId(promo.getStatus().getId());
        resources.setViewNo(promo.getViewNo());
        resources.setPromoId(promo.getId());
        resources.setPoint(promo.getPoint());
        resources.setCaptionList(PromoCaptionResources.toResources(promo.getPromoCaptionList()));


        return resources;
    }

    public static List<PromoResources> toResources(List<Promo> promo) {

        List<PromoResources> resources = new ArrayList<>();
        promo.forEach(promo1 -> {
            PromoResources promoResources = toResources(promo1);
            resources.add(promoResources);
        });
        return resources;
    }

    public Promo toPromo() {
        Promo promo = new Promo();
        promo.setId(this.id);
        promo.setCode(this.code);
        promo.setBarcode(this.barcode);
        promo.setCoverPhoto(this.coverPhoto);
        promo.setCreateDate(this.createDate);
        promo.setExpiryDate(this.expiryDate);
        promo.setViewNo(this.viewNo);
        promo.setStartDate(this.startDate);
        if (merchantId != null) {
            Merchant merchant = new Merchant();
            merchant.setId(merchantId);
            promo.setMerchant(merchant);
        }
        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            promo.setStatus(status);
        }
        return promo;
    }


    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        Promo promo = applicationContext.getBean(PromoService.class).findById(this.id);

        PromoCaption promoCaption = applicationContext.getBean(PromoCaptionService.class).find(promo, operationLanguage);

        Status status = applicationContext.getBean(StatusService.class).findById(this.statusId);

        StatusCaption statusCaption = applicationContext.getBean(StatusCaptionService.class).findByStatusAndLanguage(status, operationLanguage);


        if (promoCaption == null) {
            promoCaption = applicationContext.getBean(PromoCaptionService.class).findAllByPromo(promo).get(0);
        }

        MerchantCaption merchantCaption = applicationContext.getBean(MerchantCaptionService.class).findByIdAndLanguage(this.merchantId, operationLanguage);
        if (merchantCaption == null)
            merchantCaption = applicationContext.getBean(MerchantCaptionService.class).findByMerchantId(this.merchantId).get(0);
        List<MerchantBranchResources> merchantBranchResources = MerchantBranchResources.toResource(
                applicationContext.getBean(MerchantBranchService.class).findAllByPromo(promo));
        if (statusCaption != null)
            this.setStatusCaption(statusCaption.getName());
        if (merchantCaption != null)
            this.setMerchantName(merchantCaption.getName());
        if (promoCaption != null) {
            this.setName(promoCaption.getName());
            this.setDescription(promoCaption.getDescription());
            if (promoCaption.getOperationLanguage() != null)
                this.setLanguageId(promoCaption.getOperationLanguage().getId());
        }

        for(MerchantBranchResources merchantBranchResourceObject: merchantBranchResources){
            merchantBranchResourceObject.initResoucreCaption(applicationContext,operationLanguage,null,null);
        }
        if (merchantBranchResources != null) {
            this.setBranchList(merchantBranchResources);
        }
    }
}
