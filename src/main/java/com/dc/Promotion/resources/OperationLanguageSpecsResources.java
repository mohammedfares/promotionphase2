package com.dc.Promotion.resources;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.Status;

import java.util.ArrayList;
import java.util.List;

public class OperationLanguageSpecsResources {
    private Long id;
    private String code;
    private String name;
    private String direction;
    private Long status;

    private List<ItemSpecificationCaptionResources> specificationList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public List<ItemSpecificationCaptionResources> getSpecificationList() {
        return specificationList;
    }

    public void setSpecificationList(List<ItemSpecificationCaptionResources> specificationList) {
        this.specificationList = specificationList;
    }

    public static OperationLanguageSpecsResources toResources(OperationLanguage operationLanguage) {
        OperationLanguageSpecsResources resources = new OperationLanguageSpecsResources();
        resources.setId(operationLanguage.getId());
        resources.setCode(operationLanguage.getCode());
        resources.setDirection(operationLanguage.getDirection());
        resources.setName(operationLanguage.getName());
        resources.setStatus(operationLanguage.getStatus().getId());
        resources.setSpecificationList (ItemSpecificationCaptionResources.toResources(operationLanguage.getSpecificationList()));
        return resources;
    }

    public static List<OperationLanguageSpecsResources> toResources(List<OperationLanguage> operationLanguage) {
        List<OperationLanguageSpecsResources> resources = new ArrayList<>();
        operationLanguage.forEach(operationLanguage1 -> {
            OperationLanguageSpecsResources languageResources = toResources(operationLanguage1);
            resources.add(languageResources);

        });
        return resources;
    }

    public OperationLanguage toOperationLanguage(){
        OperationLanguage  language=new OperationLanguage();
        language.setId(this.id);
        language.setCode(this.code);
        language.setName(this.name);
        language.setDirection(this.direction);
        if(status==null){

            Status status=new Status();
            status.setId(this.status);
            language.setStatus(status);
        }
        return  language;
    }
}
