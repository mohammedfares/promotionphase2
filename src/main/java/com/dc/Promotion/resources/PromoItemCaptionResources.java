package com.dc.Promotion.resources;

import com.dc.Promotion.entities.OperationLanguage;
import com.dc.Promotion.entities.PromoItem;
import com.dc.Promotion.entities.PromoItemCaption;

import java.util.ArrayList;
import java.util.List;

public class PromoItemCaptionResources {
    private Long id;
    private String name;
    private String description;
    private Long languageId;
    private Long promoItem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public Long getPromoItem() {
        return promoItem;
    }

    public void setPromoItem(Long promoItem) {
        this.promoItem = promoItem;
    }

    public static PromoItemCaptionResources toResources(PromoItemCaption promoItemCaption){
        PromoItemCaptionResources resources=new PromoItemCaptionResources();
        resources.setId(promoItemCaption.getId());
        resources.setName(promoItemCaption.getName());
        resources.setDescription(promoItemCaption.getDescription());
        resources.setLanguageId(promoItemCaption.getLanguage().getId());
        resources.setPromoItem(promoItemCaption.getPromoItem().getId());
        return resources;

    }
    public static List<PromoItemCaptionResources> toResources(List<PromoItemCaption> captionList){
        List<PromoItemCaptionResources> resourcesList=new ArrayList<>();
        captionList.forEach(caption -> {
            PromoItemCaptionResources resources=toResources(caption);
            resourcesList.add(resources);
        });
        return  resourcesList;
    }

    public PromoItemCaption toPromoItemCaption(){
        PromoItemCaption promoItemCaption=new PromoItemCaption();
        promoItemCaption.setId(this.id);
        promoItemCaption.setName(this.name);
        promoItemCaption.setDescription(this.description);
        if(promoItem!=null){

            PromoItem promoItem=new PromoItem();
            promoItem.setId(this.promoItem);
            promoItemCaption.setPromoItem(promoItem);
        }
        if(languageId!=null){
            OperationLanguage language=new OperationLanguage();
            language.setId(languageId);
            promoItemCaption.setLanguage(language);

        }

        return  promoItemCaption;
    }
}
