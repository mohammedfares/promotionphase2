package com.dc.Promotion.resources;



public class PageResource {

    private Long count;
    private Integer totalPages;
    private Object pageList;

    public PageResource() {
    }

    public PageResource(Long count, Integer totalPages, Object pageList) {
        this.count = count;
        this.pageList = pageList;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Object getPageList() {
        return pageList;
    }

    public void setPageList(Object pageList) {
        this.pageList = pageList;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
