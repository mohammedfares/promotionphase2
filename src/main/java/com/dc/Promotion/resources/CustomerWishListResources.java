package com.dc.Promotion.resources;

import com.dc.Promotion.entities.*;
import com.dc.Promotion.service.*;
import org.springframework.context.ApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CustomerWishListResources {

    private Long id;

    private String aliasName;

    private String tags;

    private LocalDateTime createDate;

    private Long merchantId;

    private Long categoryId;

    private Long customerAccountId;

    private Long statusId;

    private String merchantName;

    private String categoryName;

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Long getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getCustomerAccountId() {
        return customerAccountId;
    }

    public void setCustomerAccountId(Long customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public static CustomerWishListResources toResources(CustomerWishList wishList) {
        CustomerWishListResources resources = new CustomerWishListResources();
        resources.setCreateDate(wishList.getCreateDate());
        resources.setCustomerAccountId(wishList.getCustomerAccount().getId());
        resources.setId(wishList.getId());
        resources.setStatusId(wishList.getStatus().getId());
        resources.setAliasName(wishList.getAliasName());
        resources.setTags(wishList.getTags());
        if (wishList.getCategory() != null)
            resources.setCategoryId(wishList.getCategory().getId());
        if (wishList.getMerchant() != null)
            resources.setMerchantId(wishList.getMerchant().getId());
        return resources;
    }

    public static List<CustomerWishListResources> toResources(List<CustomerWishList> wishListList) {
        List<CustomerWishListResources> resourcesList = new ArrayList<>();
        wishListList.forEach(wishList -> {
            CustomerWishListResources resources = toResources(wishList);
            resourcesList.add(resources);
        });
        return resourcesList;
    }

    public CustomerWishList toCustomerWishList() {
        CustomerWishList customerWishList = new CustomerWishList();
        customerWishList.setCreateDate(this.createDate);
        customerWishList.setId(this.id);
        if (customerAccountId != null) {
            CustomerAccount account = new CustomerAccount();
            account.setId(customerAccountId);
            customerWishList.setCustomerAccount(account);
        }

        if (statusId != null) {
            Status status = new Status();
            status.setId(this.statusId);
            customerWishList.setStatus(status);
        }
        if (merchantId != null) {
            Merchant merchant = new Merchant();
            merchant.setId(this.merchantId);
            customerWishList.setMerchant(merchant);
        }
        if (categoryId != null) {
            ItemCategory itemCategory = new ItemCategory();
            itemCategory.setId(this.categoryId);
            customerWishList.setCategory(itemCategory);
        }
        return customerWishList;


    }

    public void initResoucreCaption(ApplicationContext applicationContext, OperationLanguage operationLanguage) {

        if(this.merchantId!=null && merchantId!=0)
        {
            Merchant merchant= applicationContext.getBean(MerchantService.class).findById(this.merchantId);
            MerchantCaption merchantCaption= applicationContext.getBean(MerchantCaptionService.class).findByMerchant(merchant,operationLanguage);
            if(merchantCaption==null){
              OperationLanguage  languageEn=applicationContext.getBean(OperationLanguageService.class).findByCode("en");
                merchantCaption= applicationContext.getBean(MerchantCaptionService.class).findByMerchant(merchant,languageEn);
            }
            this.merchantName=merchantCaption.getName();
        }
        if(this.categoryId!=null && this.categoryId!=0)
        {
            ItemCategory category=applicationContext.getBean(ItemCategoryService.class).findById(this.categoryId);
            ItemCategoryCaption categoryCaption=applicationContext.getBean(ItemCategoryCaptionService.class).findByCategoryAndLanguage(category,operationLanguage);

            if(categoryCaption==null){
                OperationLanguage  languageEn=applicationContext.getBean(OperationLanguageService.class).findByCode("en");
                categoryCaption=applicationContext.getBean(ItemCategoryCaptionService.class).findByCategoryAndLanguage(category,languageEn);
            }

            this.categoryName=categoryCaption.getName();

        }

    }



}
