//package com.dc.Promotion.resources;
//
//import com.dc.Promotion.entities.Nationality;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class NationalityResources {
//    private Long id;
//    private String code;
//    private String key;
//
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getCode() {
//        return code;
//    }
//
//
//    public void setCode(String code) {
//        this.code = code;
//    }
//
//    public String getKey() {
//        return key;
//    }
//
//    public void setKey(String key) {
//        this.key = key;
//    }
//
//    public  NationalityResources toResources(Nationality nationality){
//
//        NationalityResources nationalityResources=new NationalityResources();
//        nationalityResources.setId(nationality.getId());
//        nationalityResources.setCode(nationality.getCode());
//        nationalityResources.setKey(nationality.getKey());
//        return  nationalityResources;
//
//    }
//    public  List<NationalityResources> toResources(List<Nationality> nationality){
//        List<NationalityResources> resources=new ArrayList<>();
//        nationality.forEach(nationality1 -> {
//            NationalityResources nationalityResources=toResources(nationality1);
//            resources.add(nationalityResources);
//
//        });
//        return  resources;
//    }
//
//    public Nationality toNationality(){
//
//        Nationality nationality=new Nationality();
//        nationality.setCode(this.code);
//        nationality.setId(this.id);
//        nationality.setKey(this.key);
//        return  nationality;
//    }
//
//}
